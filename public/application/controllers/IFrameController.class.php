<?php

require_once('CustomSmartyController.class.php');
require_once('Constants.class.php');

/**
 * Short Description of IFrameController.
 * 
 * Long description of IFrameController.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.8
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 28/01/2013  1.0     Brian Etherington     Initial Version
 * 05/04/2013  1.1     Brian Etherington     Requested by Joe berry.  Remove preselection of CCTV INSTALLATION
 * 05/04/2013  1.2     Brian Etherington     Added Skin handling
 * 09/04/2013  1.3     Brian Etherington     Tracker Base 245
 * 09/05/2013  1.4     Brian Etherington     Added Noise requirements to IFrames with conditional code for SWANN
 *                                           specific requirements.
 * 14/05/2013  1.5     Brian Etherington     Add Job Type drop down list for Noise.
 *                                           Hide Service Type and Manufacturer for SWANN
 * 28/05/2013  1.6     Brian Etherington     Force login check with every IFrame page request
 *                                           to allow for Noise and swann to be used in the same browser session
 *                                           Move language parameter check to the login method.
 * 30/05/2013  1.7     Brian Etherington     Resolve problem with user job filter not applied to job id search
 *                                           Change Session userid to IFrameUserID
 *                                           Change Session IframeSkin to IFrameSkin
 *                                           change skin parameter from positional to name/value ie skin=noise
 *                                           Remove login authentication check
 *                                           Move ServiceBase notes annotation from start of notes field to the end
 * 10/06/2013  1.8     Brian Etherington     Store Job Booking Form Notes field changed to ReportFault field.
 ******************************************************************************/

class IFrameController extends CustomSmartyController {
    
    public $user;
    public $lang = 'en'; 
    
    public function __construct() { 
        
        parent::__construct(); 
                      
       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
        $this->session = $this->loadModel('Session'); 
        
        if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
               
        $this->smarty->assign('_skin',$this->session->IFrameSkin);
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages'); 
        
        /* =========== smarty assign =============== */
        //$this->smarty->assign('ref', '');
        //$this->smarty->assign('Search', '');
        //$this->smarty->assign('ErrorMsg', '');
        
    }
       
    public function JobTrackingFormAction( $args ) {
               
        $user_model = $this->loadModel('Users');
        
        // move language check to login method
        //if (isset($args['lang'])) { 
        //    $this->lang = $args['lang'];
        //    $this->session->lang =$this->lang;  
        //}
        
        $localised_messages = $this->messages->getPage('Job',$this->lang);
        $this->smarty->assign('page', $localised_messages);
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                       
            $this->user = $user_model->getUser($this->session->IFrameUserID);                          
            $SkylineBusinessModel = $this->loadModel('SkylineBusinessModel');
            
            if (isset($_POST['joblist'])) {
                // job list is to support some home grown paging wizard
                // designed by Joe.
                
                $jobs = explode(',', $_POST['joblist']);;
                $job_index = $_POST['job_index'];                                
            
            } else if (isset($_POST['JobID']) ) {
                
                $jobs = array( $_POST['JobID'] );
                $job_index = 0;
                                
            } else {
                
                //$this->log('Track a Job - findJobs');
                //$this->log($this->user);
                
                // Perform a job search....               
                $jobs = $SkylineBusinessModel->findJobs( $_POST, $this->user );
                $job_index = 0;
                
                //$this->log($jobs);
                
            }
                
            if (count($jobs) > 0) {
                
                // Fetch details for a single job....               
                $result = $SkylineBusinessModel->getJobDetails( $jobs[$job_index], $this->user );
                
                // check if job was a valid job for this logged in user....
                if (!empty($result['error'])) {
                    $result = null;
                }
       
            } else {
                
                $result = null;
            }
                          
            if ($result !== null) {
                
                $this->smarty->assign('Status','OK');
                $this->smarty->assign('JobID',$result['JobID']);
                $this->smarty->assign('ASC', $result['service_provider']); 
                $this->smarty->assign('Customer', $result['customer']);
                $num_appts = count($result['appointments']);
                if ($num_appts > 0) {
                    $appt = $result['appointments'][$num_appts-1]['AppointmentDate'];
                    if (strtotime($appt) >= strtotime(date('Y-m-d'))) {
                        $this->smarty->assign('Appointment', $appt);
                    }
                }
                $this->smarty->assign('Job', $result['job']);
                $this->smarty->assign('joblist', count($jobs) > 1 ? $jobs : null);
                $this->smarty->assign('job_index', $job_index);
                
            } else {
                    
                $this->smarty->assign('Status','Error');
                $this->smarty->assign('JobID','');
                $this->smarty->assign('ASC', '');
                $this->smarty->assign('Customer', '');
                $this->smarty->assign('Job', '');
                $this->smarty->assign('joblist', null);
                $this->smarty->assign('job_index', 0);
                
            }
                
            $this->smarty->display('iframes/job_tracking_response.tpl');


        } else {
                       
            if ($this->Login( $args, $user_model, 'JobTrackingFormAction' )) {
                $this->smarty->assign('ClientName', $this->user->ClientName);
                $this->smarty->display('iframes/job_tracking.tpl'); 
            } 
        }
    
    }
    
    public function JobBookingFormAction( $args ) {
        
        $user_model = $this->loadModel('Users');
        
        // move language check to login method
        //if (isset($args['lang'])) { 
        //    $this->lang = $args['lang'];
        //    $this->session->lang =$this->lang;  
        //}
        
        $localised_messages = $this->messages->getPage('Job',$this->lang);
        $this->smarty->assign('page', $localised_messages);
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                                           
            $this->user = $user_model->getUser($this->session->IFrameUserID);   
                       
            $SkylineBusinessModel = $this->loadModel('SkylineBusinessModel');
            $SkylineQueriesModel = $this->loadModel('SkylineQueriesModel');
            
            /* Tracker Base 245
             * For all jobs we need to send the Network and Client down to the 
             * Service Centre, for now we will send this down in the Notes field. 
             * When booking a job through the new iframe, write the following 
             * line as the first line in the Notes field. 
             * Network: <network name> Client: <client name> 
             * The 2 fields contents should be converted to upper case
             */
            
            $network = strtoupper($_POST['NetworkName']);
            $client = strtoupper($_POST['ClientName']);
            //$notes = "\nNetwork: $network Client: $client";
            
            //$_POST['Notes'] = $_POST['Notes'].$notes;
            $_POST['Notes'] = "Network: $network Client: $client";
            
            if (empty($_POST['JobTypeID'])) {
                $_POST['JobTypeID'] = $SkylineQueriesModel->getJobTypeFromServiceType($_POST['ServiceTypeID']);
            }
            
            $result = $SkylineBusinessModel->CreateJob( $_POST, $localised_messages );
            
            if ( $result['status'] == 'SUCCESS' ) {
                
                $job = $SkylineBusinessModel->getJobDetails($result['jobId']); 
                              
                $this->smarty->assign('Status','OK');
                $this->smarty->assign('JobID',$job['JobID']);
                $this->smarty->assign('ASC', $job['service_provider']); 
                $this->smarty->assign('Customer', $job['customer']);
                $num_appts = count($job['appointments']);
                if ($num_appts > 0) {
                    $appt = $job['appointments'][$num_appts-1]['AppointmentDate'];
                    if (strtotime($appt) >= strtotime(date('Y-m-d'))) {
                        $this->smarty->assign('Appointment', $appt);
                    }
                }
                $this->smarty->assign('Job', $job['job']);
                
            } else {
                
                $this->smarty->assign('Status','Error');
                $this->smarty->assign('JobID','');
                $this->smarty->assign('ASC', '');
                $this->smarty->assign('Customer', '');
                $this->smarty->assign('Job', '');
                    
            }
          
            $this->smarty->display('iframes/job_booking_response.tpl');
            
        } else { 
                                           
            if ($this->Login( $args, $user_model, 'JobBookingFormAction' ) === false) {
                return; 
            } 
                                  
            $SkylineBusinessModel = $this->loadModel('SkylineBusinessModel');
            $SkylineQueriesModel = $this->loadModel('SkylineQueriesModel');
            
            $titles = $SkylineBusinessModel->getTitles($this->user->DefaultBrandID);
                                 
            $skyline_model = $this->loadModel('Skyline');
            $job_type_list = $skyline_model->getJobTypes();
            $manufacturer_list   = $skyline_model->getManufacturer('', $this->user->NetworkID);            
            $product_type_list   = $skyline_model->getUnitTypes();
            
            if (!isset($args['skin'])) {
                
                $jobtype_id = 3; // Hardcode (!) job type as 3 = Installation
                $job_types = array(array( 'JobTypeID' => 3, 'Name' => 'Installation' )); // Hardcode (!) job type as 3 = Installation
                
                // pre-set manufacturer to Swann only
                $manufacturers = array();
                foreach($manufacturer_list as $row) {
                     if (strtoupper($row['ManufacturerName']) == 'SWANN') {
                        $manufacturers[] = $row;
                        break;
                    }               
                }
                
                // just to be on the safe side...
                if (count($manufacturers) == 0) {
                    $manufacturers = $manufacturer_list;
                }

                // pre-set product type to CCTV only
                $product_types = array();
                foreach($product_type_list as $row) {
                     if (strtoupper($row['UnitTypeName']) == 'CCTV') {
                        $product_types[] = $row;
                        break;
                    }               
                }
                
                // just to be on the safe side...
                if (count($product_types) == 0) {
                    $product_types = $product_type_list;
                }
                
            } else {
                
                $jobtype_id = count($job_type_list) > 0 ? $job_type_list[0]['JobTypeID'] : null;
                $job_types = $job_type_list;
                $manufacturers = $manufacturer_list;
                $product_types = $product_type_list;
                
            }
            
            //$service_types = $SkylineQueriesModel->getClientServiceTypes($this->user->ClientID, $jobtype_id);
            //$service_types = $skyline_model->getServiceTypes($this->user->DefaultBrandID, $jobtype_id, $this->user->NetworkID, $this->user->ClientID );
            $service_types = $skyline_model->getServiceTypes(null, $jobtype_id, $this->user->NetworkID, $this->user->ClientID );

            if (count($manufacturers) > 0 && count($product_types) > 0) {
                $model_model = $this->loadModel('Models');
                $sql = "select ModelID from model where Status='ACTIVE' and ModelNumber='UNKNOWN' and ManufacturerID=:ManufacturerID and UnitTypeID=:UnitTypeID";
                $params = array( 'ManufacturerID' => $manufacturers[0]['ManufacturerID'],
                                 'UnitTypeID' => $product_types[0]['UnitTypeID'] );
                $unknown_models = $model_model->Select($sql, $params);
                if (count($unknown_models) > 0) {
                    $model_id = $unknown_models[0]['ModelID'];
                } 
                
                $sql2 = "select ModelID, ModelNumber from model where Status='ACTIVE' and ManufacturerID=:ManufacturerID and UnitTypeID=:UnitTypeID";
                $models = $model_model->Select($sql2, $params);
            }
                      
            $country_model = $this->loadModel('Country');
            $country_id = $country_model->getCountryId('UK');
            $counties = $skyline_model->getCounties($country_id, Constants::SKYLINE_BRAND_ID);
            
            if (empty($this->user->BranchID)) {
                $client_model = $this->loadModel('Clients');
                $sql = "select DefaultBranchID from client where ClientID=:ClientID and Status='Active'";
                $params = array( 'ClientID' => $this->user->ClientID );
                $clients = $client_model->Select($sql, $params);
                if (count($clients) > 0) {
                    $branch_id = $clients[0]['DefaultBranchID'];
                } else {
                    $branch_id = null;
                }
            } else {
                $branch_id = $this->user->BranchID;
            }
            
            $this->smarty->assign('titles', $titles);
            $this->smarty->assign('job_types', $job_types);
            $this->smarty->assign('service_types', $service_types);
            $this->smarty->assign('manufacturers', $manufacturers);
            $this->smarty->assign('product_types', $product_types);
            $this->smarty->assign('counties', $counties);
            $this->smarty->assign('NetworkID', $this->user->NetworkID);
            $this->smarty->assign('NetworkName', $this->user->NetworkName);
            $this->smarty->assign('ClientID', $this->user->ClientID);
            $this->smarty->assign('ClientName', $this->user->ClientName);
            $this->smarty->assign('BranchID', $branch_id);
            $this->smarty->assign('BrandID', $this->user->DefaultBrandID);
            $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
            $this->smarty->assign('Username', $this->user->Username);
            $this->smarty->assign('CountryID', $country_id);
            //$this->smarty->assign('JobTypeID', $jobtype_id);
            $this->smarty->assign('ProductLocation', 'Customer');
            $this->smarty->assign('ModelID', isset($model_id) ? $model_id : null );
            $this->smarty->assign('models', isset($models) ? $models : array() );
            
            $this->smarty->display('iframes/job_booking.tpl');

        }
    
    }
    
    public function PostcodeLookupAction( /* $args */ ) { 
                
        $postcode_model = $this->loadModel('Postcode');
        $addresses = $postcode_model->getAddress($_POST['postcode']);
                
        if(!is_array($addresses) || $addresses[0][0]=='Wrong Post Code') {
            $this->smarty->assign('addresses', false);
        } else {
            $this->smarty->assign('addresses', $addresses);
        }
               
        echo $this->smarty->fetch('iframes/ajax/postcode.tpl');
  
    }
    
    public function ModelLookupAction( /* $args */ ) { 
        
        $model = $this->loadModel('Models');
        $params = array( 'ManufacturerID' => $_POST['ManufacturerID'],
                         'UnitTypeID' => $_POST['UnitTypeID'] ); 
        $sql = "select ModelID, ModelNumber from model where Status='ACTIVE' and ManufacturerID=:ManufacturerID and UnitTypeID=:UnitTypeID";
        $models = $model->Select($sql, $params);
        
        $options = array();
        foreach($models as $model) {
            $options[] = array( 'text' => $model['ModelNumber'], 'value' => $model['ModelID']);
        }
        
        echo json_encode(array('options' => $options));          
  
    }
    
    public function ServiceTypeLookupAction( /* $args */ ) {
        
        $user_model = $this->loadModel('Users');
        $this->user = $user_model->getUser($this->session->IFrameUserID);

        $skyline_model = $this->loadModel('Skyline');
        $service_types = $skyline_model->getServiceTypes(null, $_POST['JobTypeID'], $this->user->NetworkID, $this->user->ClientID );
        
        $options = array();
        foreach($service_types as $service_type) {
            $options[] = array( 'text' => $service_type['Name'], 'value' => $service_type['ServiceTypeID']);
        }
        
        echo json_encode(array('options' => $options));          
  
    }
    
    private function Login( $args, $user_model, $message ) {
        
        // Force login with every Iframe page load
        // to allow for noise and Swann to be tested within the same browser session.
        //
        
        /*if (isset($this->session->IFrameUserID)) {
            
            $this->user = $user_model->getUser($this->session->IFrameUserID);
            
        } else { */
           
        // 30/05/2013
        // As part of enhanced security no longer necessary to do authentication.  
        // Password parameter is no longer being passed with the url
            $username = $args[0];
            //$password = $args[1];

            /*$this->user = $user_model->checkUser($username, $password);
            
            if (!$this->user->AuthOK) {                
                $this->AuthenticationFailure( $message, $username, $password);
                return false;
            }*/
            
            $this->user = $user_model->GetUser($username);
            // check if user was a valid user
            if ($this->user->Password == '') {                
                $this->AuthenticationFailure( $message, $username, '');
                return false;
            }

            $this->session->IFrameUserID = $this->user->Username;
            
            if (isset($args['skin'])) {
                $this->session->IFrameSkin = '/skins/'.$args['skin'];
                $this->smarty->assign('_skin', $this->session->IFrameSkin);    
            } else {
                unset($this->session->IFrameSkin);
                $this->smarty->assign('_skin', '');
            }
            
            if (isset($args['lang'])) { 
                $this->lang = $args['lang'];
                $this->session->lang =$this->lang;  
            }
            
        //}
        
        return true;
    }
    
    private function AuthenticationFailure( $method, $username, $password ) {
        $this->log("Authentication Failure: Method = $method Username = $username Password = $password","Authentication_Failure_");
        $this->smarty->assign('username', $username);
        $this->smarty->assign('password', $password);
        $this->smarty->display('iframes/authentication_failure.tpl');
    }
}

?>
