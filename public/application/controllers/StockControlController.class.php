<?php

/**
 * Description
 *
 * This class handles all actions of StockControll module
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.00
 *   
 * Changes
 * Date        Version Author                Reason
 * 30/04/2013  1.00    Andris Polnikovs      Initial Version
 * 
 * **************************************************************************** */
require_once('CustomSmartyController.class.php');
include_once ('SkylineRESTClient.class.php');

class StockControlController extends CustomSmartyController {

    public $config;
    public $session;
    public $skyline;
    public $user;
    public $messages;
    private $lang = 'en';
    public $debug = false;

    public function __construct() {
        parent::__construct();



        /* ==========================================
         * Read Application Config file.
         * ==========================================
         */
        $this->config = $this->readConfig('application.ini');

        /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
        $this->session = $this->loadModel('Session');

        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages');

        /* =========== smarty assign =============== */
        $this->smarty->assign('ref', '');
        $this->smarty->assign('Search', '');
        $this->smarty->assign('ErrorMsg', '');




        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */

        $user_model = $this->loadModel('Users');
        $this->user = $user_model->GetUser($this->session->UserID, true);
        if (isset($_GET['name']) && $_GET['password'] && isset($_GET['type'])) {
            if ($this->debug)
                $this->log($_GET, 'SB_access_log_');
            foreach ($_GET as $k => $r) {
                if ($k != "name" && $k != "password") {


                    $_GET[$k] = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $r);
                }
            }
            if ($this->debug)
                $this->log('--Update remove', 'SB_access_log_');
            if ($this->debug)
                $this->log($_GET, 'SB_access_log_');
        }
        if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser($this->session->UserID);

            $this->smarty->assign('loggedin_user', $this->user);

            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);

            if ($this->user->BranchName) {
                $this->smarty->assign('logged_in_branch_name', " - " . $this->user->BranchName . " " . $this->config['General']['Branch'] . " ");
            } else {
                $this->smarty->assign('logged_in_branch_name', "");
            }

            if ($this->session->LastLoggedIn) {
                $this->smarty->assign('last_logged_in', " - " . $this->config['General']['LastLoggedin'] . " " . date("jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign('last_logged_in', '');
            }

            $topLogoBrandID = $this->user->DefaultBrandID;

            //$this->log(var_export($this->user, true));
        } else {

            $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;

            $this->smarty->assign('session_user_id', '');
            $this->smarty->assign('_theme', 'skyline');
            // $this->smarty->assign('name','');
            // $this->smarty->assign('last_logged_in','');
            // $this->smarty->assign('logged_in_branch_name', '');
            $this->redirect('index', null, null);
        }

        /* checking for sa user */
        $this->checkUserSPID();

        $this->page = $this->messages->getPage("stockControl", $this->lang);
        $this->smarty->assign('page', $this->page);
    }

    public function indexAction($args) {
        $this->redirect('StockControlController', 'defaultAction');
    }

    public function stockAction($args) {
        $this->redirect('StockControlController', 'defaultAction');
    }

    public function defaultAction($args) {


        ///session
        $this->session->mainTable = "sp_part_stock_template";
        $this->session->mainPage = "stock";
        $this->session->controller = "StockControl";
        $this->page = $this->messages->getPage("stock", $this->lang);
        //session


        $spid = $this->user->ServiceProviderID;
        //models
        $StockModel = $this->loadModel('StockControl');
        $datatable = $this->loadModel('DataTable');
        //models





        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);
        if (isset($keys_data[1])) {
            $keys = $keys_data[1];
        } else {
            $keys = array();
        }


        if ($this->user->SuperAdmin == 1) {

            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        //suppliers list
        $supplierModel = $this->loadModel('Suppliers');
        $supplierList = $supplierModel->getSPSuppliers($spid);
        $this->smarty->assign('supplierList', $supplierList);

        //manufacturer list
        $manufacturerModel = $this->loadModel('Manufacturers');
        $manufacturerList = $manufacturerModel->getAllSpManufacturers($spid);
        $this->smarty->assign('manufacturerList', $manufacturerList);

        //Model list
        $ModelsModel = $this->loadModel('Models');
        $ModelList = $ModelsModel->getAllSPModels($spid);
        $this->smarty->assign('ModelList', $ModelList);

        ///smarty assign
        $this->smarty->assign('data_keys', $keys);
        $this->smarty->assign('SuperAdmin', $this->user->SuperAdmin);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        $this->smarty->assign('page', $this->page);

        ///smarty assign


        $this->smarty->display('stockControl/stockMain.tpl');
    }

    public function tableDisplayPreferenceSetupAction($args) {
        $this->page = $this->messages->getPage($args['page'], $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

        $datatable = $this->loadModel('DataTable');

        $columnStrings = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID'], 'user');
        $columnStringsSA = $datatable->getColumnStrings(1, $this->page['config']['pageID'], 'sa');



        $keys = $datatable->getAllFieldNames($args['table']);


        $this->smarty->assign('data_keys', $keys[1]);




        if ($columnStrings) {
            $columnDisplayString = explode(",", $columnStrings['ColumnDisplayString']);
            $columnOrderString = explode(',', $columnStrings['ColumnOrderString']);
            $columnNameString = explode(",", $columnStrings['ColumnNameString']);
        } else {
            $columnDisplayString = array();
            $columnOrderString = "";
            $columnNameString = array();
        }

        if ($columnStringsSA) {
            $columnDisplayStringSA = explode(",", $columnStringsSA['ColumnDisplayString']);
            $columnOrderStringSA = explode(',', $columnStringsSA['ColumnOrderString']);
            $columnNameStringSA = explode(",", $columnStringsSA['ColumnNameString']);
            $columnStatusStringSA = explode(",", $columnStringsSA['ColumnStatusString']);
            $this->smarty->assign('columnStatusStringSA', $columnStatusStringSA);
        } else {
            $columnDisplayStringSA = array();
            $columnOrderStringSA = "";
            $columnNameStringSA = array();
        }

        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnDisplayStringSA', $columnDisplayStringSA);
        $this->smarty->assign('columnOrderStringSA', $columnOrderStringSA);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
        $this->smarty->assign('controller', $this->session->controller);

        $this->smarty->assign('typeAction', $this->session->mainPage);

        $this->smarty->display('systemAdmin/tableDisplayPreferences.tpl');
    }

    public function saveDisplayPreferencesAction($args) {
        $typeAction = $args['typeAction'];
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $model = $this->loadModel('DataTable');

        $model->saveDisplayPreferences($this->user->UserID, $this->page['config']['pageID'], $_POST);
        $this->redirect($this->session->controller . 'Controller', $typeAction . "Action");
    }

    public function processStockAction($args) {

        $models_model = $this->loadModel('Models');
        $colour_model = $this->loadModel('Colours');
        $suppliers_model = $this->loadModel('Suppliers');
        $suppliers = $suppliers_model->getSPSuppliers($this->user->ServiceProviderID);
        $this->smarty->assign('suppliers', $suppliers);
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        if ($this->user->ServiceProviderID == "") {
            die($this->page['Errors']['no_service_provider_id_error']);
        }
        $this->smarty->assign('page', $this->page);


        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);


        $models = $models_model->getAllSPModels($this->user->ServiceProviderID);
        $colours = $colour_model->getAllSPColours($this->user->ServiceProviderID);
        $this->smarty->assign('models', $models);
        $this->smarty->assign('colours', $colours);
        //edit,copy
        if (isset($args['id'])) {
            $model = $this->loadModel('StockControl');

            $this->smarty->assign('mode', "update");

            $datarow = $model->getData($args['id'], $this->session->mainTable);

            $this->smarty->assign('datarow', $datarow);
            $this->smarty->display('stockControl/stockPartForm.tpl');
        } else {

            $datarow = [
                "Status" => "Active",
                "PartNumber" => "",
                "PurchaseCost" => "",
                "Description" => "",
                "MinStock" => "",
                "MakeUpTo" => "",
                "ServiceProviderColourID" => "",
                "PrimaryServiceProviderModelID" => "",
                "DefaultServiceProviderSupplierID" => "",
            ];


            $this->smarty->assign('mode', "New");
            $this->smarty->assign('datarow', $datarow);


            $this->smarty->display('stockControl/stockPartForm.tpl');
        }
    }

    public function savePartStockTemplateAction($args) {


        $model = $this->loadModel('StockControl');
        if ($_POST['SpPartStockTemplateID'] != '') {
            $model->updatePartStockTemplate($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertPartStockTemplate($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect($this->session->controller);
    }

    public function deleteStockAction($args) {
        $model = $this->loadModel('StockControl');
        $model->deleteStock($args['id']);
        $this->redirect($this->session->controller);
    }

    public function resetDisplayPreferencesAction($args) {
        $typeAction = $args['typeAction'];

        $this->page = $this->messages->getPage($typeAction, $this->lang);
        $model = $this->loadModel('DataTable');

        $model->resetDisplayPreferences($this->user->UserID, $this->page['config']['pageID']);
        $this->redirect($this->session->controller . 'Controller', $typeAction . "Action");
    }

    public function loadStockTableAction($args) {

        $dd = $_GET;
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable = $this->loadModel('DataTable');
        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }
        $table = $this->session->mainTable;
        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            if ($this->user->SuperAdmin == 1 && isset($args['spid'])) {
                $sp = $args['spid'];
            } else {
                $sp = false;
            }
        }



        $keys_data = $datatable->getAllFieldNames($table, $this->user->UserID, $this->page['config']['pageID']);
        if (isset($keys_data[0])) {
            $columns = $keys_data[0];
        } else {
            $columns = $keys_data;
        }


        // $columns=$datatable->getAllFieldNames('currency');
        //$columns="";


        $joins = "join service_provider sp On sp.ServiceProviderID= $table.ServiceProviderID
              
              left join user u on u.UserID=$table.CreatedUserID
              left join user u2 on u2.UserID=$table.ModifiedUserID
               left join service_provider_colour c on c.ServiceProviderColourID=$table.ServiceProviderColourID
              left join service_provider_supplier sup on sup.ServiceProviderSupplierID=$table.DefaultServiceProviderSupplierID
               
                ";
        //$joins="";

        $where = '';
        if (isset($args['supplierFilter'])) {
            $id = $args['supplierFilter'];
            $where.=" and DefaultServiceProviderSupplierID=$id";
        }
        if (isset($args['ModelsFilter'])) {
            $id = $args['ModelsFilter'];
            //$where.=" and ServiceProviderModelID=$id"; TODO need finish this filter when multiple models can be assigned
        }

        if ($this->debug)
            $this->log("start", "datatableData");
        $data = $datatable->datatableSS($table, $columns, $dd, $joins, $extraColumns = array('<input type="checkbox">'), 0, false, $inactive, $sp, false, '', $where);
        if ($this->debug)
            $this->log($data, "datatableData");
        echo json_encode($data);
    }

    public function stockPartHistoryAction($args) {
        $id = $args['id'];
        $model = $this->loadModel('StockControl');
        $templateData = $model->getData($id);



        $this->smarty->assign("Data", $templateData);
        $this->smarty->assign("id", $id);
        $this->smarty->display('stockControl/stockPartHistory.tpl');
    }

    //data table serverside function

    public function loadStockHistoryListAction($args) {
        $dd = $_GET;
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable = $this->loadModel('DataTable');


        $id = $args['id'];

        $columns = [
          " spsrh.SpStockReceivingHistoryID",
"if(PartStatusID in (1,4),'Incoming',if(PartStatusID in (11),'Outgoing','-'))",
            "spsh.DateTime",
"spsi.SPPartOrderID",
"spsi.JobID",
"concat_ws(' ',u1.ContactFirstName,u1.ContactLastName)",
"sps.CompanyName",
"count(1)",
"DespatchNo",
"InvoiceNo",
"spsrh.UnitPrice*count(1)",
            "SpGRNID",
"ps.PartStatusName",

            ];

        $t = "sp_part_stock_template";

        $joins = "
               join sp_part_stock_item spsi  on spsi.SPPartStockTemplateID=$t.SpPartStockTemplateID
join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
join service_provider_supplier sps on sps.ServiceProviderSupplierID=spo.ServiceProviderSupplierID

join sp_part_stock_history spsh on spsh.SPPartStockItemID=spsi.SPPartStockItem
join part_status ps on ps.PartStatusID=spsh.SPPartStatusID
join user u1 on u1.UserID=spsh.UserID
left join sp_stock_receiving_history spsrh on spsrh.SpStockReceivingHistoryID=spsi.SpStockReceivingHistoryID
                ";
        $groupBy = "group by spsh.SPPartStatusID, date_format(spsh.DateTime,'%d/%m/%d %H:%i')";

        $where = "and $t.SpPartStockTemplateID=$id ";
        $extraColumns=[
            "<a   class='viewA' onclick='showNotes(this)'>View</a>"
        ];
        $data = $datatable->datatableSS($t, $columns, $dd, $joins, $extraColumns, 0, false, 'no', false, "", $groupBy, $where, true);

        echo json_encode($data);
    }

    public function stockReiceivingWindowAction($args) {
        $this->page = $this->messages->getPage("stockReceiving", $this->lang);
        if ($this->user->ServiceProviderID == "") {
            die($this->page['Errors']['no_service_provider_id_error']);
        }
        $suppliers_model = $this->loadModel('Suppliers');
        $suppliers = $suppliers_model->getSPSuppliers($this->user->ServiceProviderID);
        $models_model = $this->loadModel('Models');
        $colour_model = $this->loadModel('Colours');
        $stock_model = $this->loadModel('StockControl');



        $stockTemplates = $stock_model->getSpPartTemplates($this->user->ServiceProviderID);
        $models = $models_model->getAllSPModels($this->user->ServiceProviderID);
        $colours = $colour_model->getAllSPColours($this->user->ServiceProviderID);




        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('stockTemplates', $stockTemplates);
        $this->smarty->assign('models', $models);
        $this->smarty->assign("suppliers", $suppliers);
        $this->smarty->assign("colours", $colours);
        $this->smarty->display('stockControl/stockReceivingWindow.tpl');
    }

    //data table function
    public function loadStockReceivingHistoryListAction($args) {

        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = "0";
        }



        $t = "sp_part_order";


        $columns = array("SPPartOrderID", "date_format(ReceviedDate,'%d/%m/%Y %H:%i')", "OrderNo", "CompanyName", "concat_ws(' ',u.ContactFirstName,u.ContactLastName)");


        $joins = "join user u on u.UserID=$t.ReceivedByUserID
               left join service_provider_supplier sps on sps.ServiceProviderSupplierID=$t.ServiceProviderSupplierID";
        //$joins="";

        if ($this->debug)
            $this->log("start", "datatableData");
        $data = $datatable->datatableSS($t, $columns, $dd, $joins, $extraColumns = array('<input type="checkbox">'), 0, false, "", $sp);
        if ($this->debug)
            $this->log($data, "datatableData");
        echo json_encode($data);
    }

    public function checkOrderNoAction($args) {
        $model = $this->loadModel('StockControl');
        $res = $model->checkOrderNo($_POST['orderNo'], $this->user->ServiceProviderID);
        if ($res) {
            echo json_encode($res);
        } else {
            echo "";
        }
    }

    public function getSpPartTemplateDataAction($args) {
        $model = $this->loadModel('StockControl');
        $res = $model->getSpPartTemplateData($this->user->ServiceProviderID, $_POST['PartNumber']);
        echo json_encode($res);
    }

    public function insertStockItemAction() {



        $model = $this->loadModel('StockControl');
        //getting order data
        $on = $model->checkOrderNo($_POST['OrderNo'], $this->user->ServiceProviderID);
        if ($on['OrderedDate'] == "") {
            $model->insertStockItem($this->user->ServiceProviderID, $_POST);
        } else {
            $model->receiveGSPNStockItem($_POST, $on);
        }
    }

    //job update page ataching parts to job
    public function loadSPPartsListAction($args) {

        $datatable = $this->loadModel('DataTable');
        $table = "sp_part_stock_template";

        $columns = ["$table.SpPartStockTemplateID",
            "PartNumber",
            "Description",
            "0.00",
            "0.00",
            "(select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
               where 
                sppsi.SPPartStockTemplateID=$table.SPPartStockTemplateID
                and sps.Available='Y' and sps.InStock='Y'    
                )"];



        $sp = $this->user->ServiceProviderID;
        //$joins="";

        $where = " and $table.SuspendPart='No' and $table.Accessory='No'";
        $joins = "";
        if (isset($args['modelID'])) {
            $md = $args['modelID'];
            $where.=" and stm.ServiceProviderModelID=$md";
            $joins = "  join service_provider_model_to_sp_part_stock_template stm on stm.SpPartStockTemplateID=$table.SPPartStockTemplateID";
        }
        //if charge type == warranty filtering out chargeable only parts
        if ($args['chargeType'] == "w") {
            $where.=" and $table.ChargeablePartOnly='No'";
        }

        if ($this->debug)
            $this->log("start", "datatableData");
        $data = $datatable->datatableSS($table, $columns, $_GET, $joins, $extraColumns = array('<input onclick="useThisItem(this)" type="checkbox">'), 0, false, false, $sp, false, false, $where);
        if ($this->debug)
            $this->log($data, "datatableData");
        echo json_encode($data);
    }

    public function getStockItemDataAction($args) {
        $model = $this->loadModel('StockControl');
        $datarow = $model->getData($args['id'], "sp_part_stock_template");
        echo json_encode($datarow);
    }

    public function getOrderHistoryDataAction($args) {

        $model = $this->loadModel('StockControl');
        $datarow = $model->getOrderHistoryData($_POST['id'], $this->user->ServiceProviderID);
        echo json_encode($datarow);
    }

    public function loadOrderReceivingHistoryListAction($args) {

        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = "0";
        }



        $t = "sp_part_order";
        $orderID = $args['id'];

        $columns = ["distinct(spst.PartNumber)", "spst.Description", "(select count(*) from sp_part_stock_item spsi2 where spsi2.SPPartOrderID=$orderID and spsi2.SPPartStockTemplateID=spst.SPPartStockTemplateID)"];


        $joins = " join sp_part_stock_item spsi on spsi.SPPartOrderID=$t.SPPartOrderID and $t.SPPartOrderID=$orderID
                join sp_part_stock_template spst on spst.SpPartStockTemplateID=spsi.SPPartStockTemplateID";
        //$joins="";

        if ($this->debug)
            $this->log("start", "datatableData");
        $data = $datatable->datatableSS($t, $columns, $dd, $joins, $extraColumns = array(), 0, false, "", $sp);
        if ($this->debug)
            $this->log($data, "datatableData");
        echo json_encode($data);
    }

    public function minimumStockRoutineAction() {

        if ($this->user->ServiceProviderID == "") {
            die($this->page['Errors']['no_service_provider_id_error']);
        }
        $this->smarty->display('stockControl/stockMinimumStockRoutine.tpl');
    }

    public function minimumStockRoutineExportAction($args) {

        $model = $this->loadModel('StockControl');
        $type = $args['type'];
        $qr = $model->getExportData($type, $this->user->ServiceProviderID);
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

//this line is important its makes the file name
        header("Content-Disposition: attachment;filename=skyline-stock-export-" . date("d_m_Y") . ".csv ");

        header("Content-Transfer-Encoding: binary ");


// these will be used for keeping things in order.
        $col = 0;
        $row = 0;

// This tells us that we are on the first row
        $first = true;
        $separator = "	";
        $separator = ",";
        $expa = "";

        if ($type == 1) {
            $expa.="Minimum Stock Routine";
            $expa.= "\r\n";
            $expa.="Type All Stock";
            $expa.= "\r\n";
            $expa.="Created Date  " . date("d/m/Y");
            ;
            $expa.= "\r\n";
            $expa.="Created By  " . $this->user->ContactFirstName . " " . $this->user->ContactLastName;
            $expa.= "\r\n";
            $expa.= "\r\n";
        }

        if ($type == 2) {
            $expa.="Minimum Stock Routine";
            $expa.= "\r\n";
            $expa.="Type Stock At Minimum Level Only";
            $expa.= "\r\n";
            $expa.="Created Date  " . date("d/m/Y");
            ;
            $expa.= "\r\n";
            $expa.="Created By  " . $this->user->ContactFirstName . " " . $this->user->ContactLastName;
            $expa.= "\r\n";
            $expa.= "\r\n";
        }

        while ($qrow = mysql_fetch_assoc($qr)) {

            // Ok we are on the first row
            // lets make some headers of sorts
            if ($first) {

                foreach ($qrow as $k => $v) {
                    $field = $k;
                    // take the key and make label
                    // make it uppper case and replace _ with ' '
                    if (preg_match('/\\r|\\n|,|"/', $k)) {
                        $field = '"' . str_replace('"', '""', $k) . '"';
                        $field = '"' . str_replace('\r', '', $k) . '"';
                        $field = '"' . str_replace('\n', '', $k) . '"';
                        $field = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
                    }
                    $expa.= $field . $separator;
                    if ($this->debug)
                        $this->log($field . $separator, 'export_CSV_log_');
                    $col++;
                }

                // prepare for the first real data row
                $col = 0;
                $row++;
                $first = false;
                $expa.= "\r\n";
            }
            // go through the data
            foreach ($qrow as $k => $v) {
                $field = $v;
                // take the key and make label
                // make it uppper case and replace _ with ' '
                if (preg_match('/\\r|\\n|,|"/', $v)) {
                    $field = '"' . str_replace('"', '""', $k) . '"';
                    $field = '"' . str_replace('\r', '', $k) . '"';
                    $field = '"' . str_replace('\n', '', $k) . '"';
                    $field = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
                }
                $expa.= $field . $separator;
            }
            $expa.= "\r\n";
        }
        if ($this->debug)
            $this->log($expa, 'export_CSV_log_');
        echo $expa;
//$this->xlsEOF();
        exit();
    }

    //cheks if user got service provider id
    private function checkUserSPID() {
        //$this->print_d($this->user);
        if ($this->user->ServiceProviderID == "") {
            if ($this->user->SuperAdmin == 1) {
                $this->smarty->assign("SuperAdmin", true);
                $this->user->ServiceProviderID = 8;
            } else {

                $this->smarty->assign('NotPermited', true);
                $this->smarty->assign('type', "error");
                $this->smarty->assign('msgTitle', $this->page['Errors']['error']);
                $this->smarty->assign('msgText', $this->page['Errors']['no_service_provider_id_error']);
                $this->smarty->display("popup/UserMessage.tpl");
                return true;
            }
        } else {
            return false;
        }
    }

    public function getStockItemQtyAction($args) {
        $model = $this->loadModel('StockControl');
        $datarow = $model->getStockItemQty($args['id'], $this->user->ServiceProviderID);
        echo json_encode($datarow);
    }

    public function stockUsageReportAction($args) {

        $this->page = $this->messages->getPage("stock", $this->lang);
        if ($this->user->ServiceProviderID == "") {
            die($this->page['Errors']['no_service_provider_id_error']);
        }
        $this->smarty->display('stockControl/stockUsageReport.tpl');
    }

    public function StockUsageReportGenAction($args) {
        $model = $this->loadModel('StockControl');

        $qr = $model->getExportData(3, $this->user->ServiceProviderID, $_GET['dateFrom'], $_GET['dateTo']);
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

//this line is important its makes the file name
        header("Content-Disposition: attachment;filename=skyline-stock-export-" . date("d_m_Y") . ".csv ");

        header("Content-Transfer-Encoding: binary ");


// these will be used for keeping things in order.
        $col = 0;
        $row = 0;

// This tells us that we are on the first row
        $first = true;
        $separator = "	";
        $separator = ",";
        $expa = "";


        $expa.="Stock Usage Report";
        $expa.= "\r\n";
        $expa.="Date From: " . $_GET['dateFrom'];
        $expa.= "\r\n";
        $expa.="Date To: " . $_GET['dateTo'];
        $expa.= "\r\n";
        $expa.="Created Date  " . date("d/m/Y");
        ;
        $expa.= "\r\n";
        $expa.="Created By  " . $this->user->ContactFirstName . " " . $this->user->ContactLastName;
        $expa.= "\r\n";
        $expa.= "\r\n";


        while ($qrow = mysql_fetch_assoc($qr)) {

            // Ok we are on the first row
            // lets make some headers of sorts
            if ($first) {

                foreach ($qrow as $k => $v) {
                    $field = $k;
                    // take the key and make label
                    // make it uppper case and replace _ with ' '
                    if (preg_match('/\\r|\\n|,|"/', $k)) {
                        $field = '"' . str_replace('"', '""', $k) . '"';
                        $field = '"' . str_replace('\r', '', $k) . '"';
                        $field = '"' . str_replace('\n', '', $k) . '"';
                        $field = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
                    }
                    $expa.= $field . $separator;
                    if ($this->debug)
                        $this->log($field . $separator, 'export_CSV_log_');
                    $col++;
                }

                // prepare for the first real data row
                $col = 0;
                $row++;
                $first = false;
                $expa.= "\r\n";
            }
            // go through the data
            foreach ($qrow as $k => $v) {
                $field = $v;
                // take the key and make label
                // make it uppper case and replace _ with ' '
                if (preg_match('/\\r|\\n|,|"/', $v)) {
                    $field = '"' . str_replace('"', '""', $k) . '"';
                    $field = '"' . str_replace('\r', '', $k) . '"';
                    $field = '"' . str_replace('\n', '', $k) . '"';
                    $field = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
                }
                $expa.= $field . $separator;
            }
            $expa.= "\r\n";
        }
        if ($this->debug)
            $this->log($expa, 'export_CSV_log_');
        echo $expa;
//$this->xlsEOF();
        exit();
    }

    public function stockOrderingAction($args) {
        $datatable = $this->loadModel('DataTable');
        $this->session->mainTable = "part";
        $this->session->mainPage = "stockOrdering";
        $this->session->controller = "StockControl";


        $this->page = $this->messages->getPage("stockOrdering", $this->lang);

        $suppliers_model = $this->loadModel('Suppliers');
        $suppliers = $suppliers_model->getSPSuppliers($this->user->ServiceProviderID);




        $keys = [
            "Part ID",
            "Supplier",
            "Stock Code",
            "Description",
            "Purchase Cost",
            "Status",
            "Requisition No",
            "Quantity Required",
            "Type",
            "JobID",
        ];

        ///smarty assign
        $this->smarty->assign("suppliers", $suppliers);
        $this->smarty->assign('data_keys', $keys);
        $this->smarty->assign('SuperAdmin', $this->user->SuperAdmin);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        $this->smarty->assign('page', $this->page);
        $stockModel = $model = $this->loadModel('StockControl');
        $statuses = $stockModel->getOrderingStatuses();
        $this->smarty->assign('statuses', $statuses);
        $AllStatuses = $stockModel->getAllOrderingStatuses();
        $this->smarty->assign('AllStatuses', $AllStatuses);
        ///smarty assign


        $this->smarty->display('stockControl/stockOrdering.tpl');
    }

    //data table serverside function

    public function loadStockOrderingTableAction($args) {
        $dd = $_GET;
        $this->page = $this->messages->getPage("stockOrdering", $this->lang);
        $datatable = $this->loadModel('DataTable');

        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = "0";
        }



        $t = "part";

        $where = "and $t.PartStatusID in (2,6,18,19) and spsta.PartStatusID in (2,6,18,19) and spst.ServiceProviderID=$sp";

        if (isset($args['suplierID']) && $args['suplierID'] != 0) {
            $where.=" and sps.ServiceProviderSupplierID=" . $args['suplierID'];
        }

        if (isset($args['statusesSelect']) && $args['statusesSelect'] != 0) {
            $where.=" and $t.PartStatusID=" . $args['statusesSelect'];
        }
        !isset($args['viewType']) ? $viewType = 1 : $viewType = $args['viewType'];

        if ($viewType == 2) {

            $columns = ["$t.PartID",
                "sps.CompanyName",
                "spst.PartNumber",
                "spst.Description",
                "spst.PurchaseCost",
                "spsta.PartStatusName",
                "$t.SPPartRequisitionID",
                "$t.Quantity",
                "if($t.JobID is null ,'Stock','Job')",
                "$t.JobID",
            ];
            $sp = false;

            $joins = " join sp_part_stock_template spst on spst.SpPartStockTemplateID=$t.SPPartStockTemplateID
               join service_provider_supplier sps on sps.ServiceProviderSupplierID = $t.SupplierID
              join part_status spsta on spsta.PartStatusID=$t.PartStatusID
              left join sp_part_requisition spr on spr.SPPartRequisitionID=$t.SPPartRequisitionID
              
                ";






            $groupBy = "group by $t.PartID";
        }
        if ($viewType == 1 || $viewType == 'undefined') {

            $columns = ["GROUP_CONCAT(DISTINCT $t.PartID SEPARATOR ',')",
                "sps.CompanyName",
                "spst.PartNumber",
                "spst.Description",
                "spst.PurchaseCost",
                "spsta.PartStatusName",
                "$t.SPPartRequisitionID",
                "sum($t.Quantity)",
                "if($t.JobID is null ,'Stock','Job')",
                "$t.JobID",
            ];
            $sp = false;

            $joins = " join sp_part_stock_template spst on spst.SpPartStockTemplateID=$t.SPPartStockTemplateID
               join service_provider_supplier sps on sps.ServiceProviderSupplierID = $t.SupplierID
              join part_status spsta on spsta.PartStatusID=$t.PartStatusID
              left join sp_part_requisition spr on spr.SPPartRequisitionID=$t.SPPartRequisitionID
              
                ";






            $groupBy = "group by $t.SupplierID,$t.SpPartStockTemplateID,$t.SPPartRequisitionID,$t.PartStatusID";
        }


        $data = $datatable->datatableSS($t, $columns, $dd, $joins, array('<input type="checkbox">'), 0, false, 'no', $sp, "", $groupBy, $where, true);

        echo json_encode($data);
    }

    public function changePartsStatusAction($args) {
        $d = $_POST;
        if (!is_array($d['parts'])) {
            $d['parts'] = array($d['parts']);
        }
        $model = $this->loadModel('StockControl');
        $model->changePartsStatus($d['parts'], $d['status']);
        echo"ok";
    }

    public function testAction() {
        $model = $this->loadModel('StockControl');
        $model->insertStockItemHistory('2089', 6, 157751);
    }

    public function getSPManufacturerModelsAction($args) {

        $id = $_POST['id'];
        if ($id == "") {
            $ModelsModel = $this->loadModel('Models');
            $modelList = $ModelsModel->getAllSPModels($spid);
        } else {
            $manufacturerModel = $this->loadModel('Manufacturers');
            $modelList = $manufacturerModel->getSPManufacturerModels($spid, $id);
        }
        echo json_encode($modelList);
    }

    public function getStockLevelsAction($args) {
        $this->smarty->display('stockControl/stockLevels.tpl');
    }

    public function orderStockFormAction($args) {

        $stockControlModel = $this->loadModel('StockControl');
        $suppliersModel = $this->loadModel('Suppliers');
        $serviceProviderModel = $this->loadModel('ServiceProviders');
        $currencyModel = $this->loadModel('Currency');
        $id = $args['id'];
        $data = $stockControlModel->getAllTemplateData($id, $this->user->ServiceProviderID);
        $this->smarty->assign('data', $data);

        $assocSuppliers = $stockControlModel->getPartTemplateAssocSuppliers($id, $this->user->ServiceProviderID);
        $this->smarty->assign('assocSuppliers', $assocSuppliers);
        $Suppliers = $suppliersModel->getSPSuppliers($this->user->ServiceProviderID);
        $this->smarty->assign('Suppliers', $Suppliers);
        $spData = $serviceProviderModel->getServiceProvider($data['ServiceProviderID']);
        $this->smarty->assign('spData', $spData);
        $spCurrCode = $currencyModel->getCurrencyCode($spData['CurrencyID']);
        $supCurrCode = $currencyModel->getSuppliersCurrencyCode($data['DefaultServiceProviderSupplierID']);
//        echo"<pre>";
//       print_r($data);
//       echo"</pre>";
        $currencyRate = $currencyModel->getSPtoSupplierExchangeRate($data['ServiceProviderID'], $data['DefaultServiceProviderSupplierID']);
        $this->smarty->assign('spCurrCode', $spCurrCode);
        $this->smarty->assign('supCurrCode', $supCurrCode);
        $this->smarty->assign('currencyRate', $currencyRate);

        //if ordering from job page]
        if (isset($args['mode'])) {
            if ($args['mode'] == 2) {
                $this->smarty->assign('mode', 2);
            }
        } else {
            $this->smarty->assign('mode', 1);
        }

        isset($args['qty']) ? $qty = $args['qty'] : $qty = "";
        $this->smarty->assign("qty", $qty);
        $this->smarty->display('stockControl/orderStockForm.tpl');
    }

    public function getSupCurrencyDataAction($args) {
        $spid = $_POST['spid'];
        $sup = $_POST['sup'];
        $currencyModel = $this->loadModel('Currency');
        $currencyRate = $currencyModel->getSPtoSupplierExchangeRate($spid, $sup);
        $supCurrCode = $currencyModel->getSuppliersCurrencyCode($sup);
        $response = [
            'rate' => $currencyRate,
            'code' => $supCurrCode
        ];
        echo json_encode($response);
    }

    public function createPendingItemsAction($args) {
        $stockControlModel = $this->loadModel('StockControl');
        $p = $_POST;
        $stockControlModel->updateTemplateCost($p['SpPartStockTemplateID'], $p['PurchaseCost']);

        $stockControlModel->createPendingItems($this->user->ServiceProviderID, $p['SpPartStockTemplateID'], $p['qtyReq'], $p['ServiceProviderSupplier']);
        $_POST = array();
        $this->redirect('StockControlController', 'defaultAction');
    }

    public function setRequisitionAction($args) {
        if (isset($_POST['parts'])) {
            $parts = $this->nomalizeIDs($_POST['parts']);
            $status = $_POST['status'];
            $stockControlModel = $this->loadModel('StockControl');
            $reqIDs = $stockControlModel->setRequisition($parts, $status);
            echo json_encode($reqIDs);
        }
    }

    //generates pdf for Requisition
    //args  reqIds array of req ids
    public function printRequisitionAction($args) {
        $reqIDs = $args['reqIds'];
        $stockControlModel = $this->loadModel('StockControl');
        $data = $stockControlModel->getRequisitionData($reqIDs, $this->user->ServiceProviderID);
        $this->smarty->assign("data", $data);
        $html = $this->smarty->fetch('stockControl/pdf/requisition.tpl');

        include(APPLICATION_PATH . '/../libraries/MPDF56/mpdf.php');
        $mpdf = new mPDF();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }

    public function printOrdersAction($args) {
        $ordersIDs = $args['ordersIds'];
        $stockControlModel = $this->loadModel('StockControl');
        $data = $stockControlModel->getOrdersData($ordersIDs, $this->user->ServiceProviderID);
        $this->smarty->assign("data", $data);
        $html = $this->smarty->fetch('stockControl/pdf/orders.tpl');

        include(APPLICATION_PATH . '/../libraries/MPDF56/mpdf.php');
        $mpdf = new mPDF();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }

    public function MakeTaggedOrdersAction($args) {
        $model = $this->loadModel('StockControl');
        if (isset($_POST['parts'])) {
            $parts = $this->nomalizeIDs($_POST['parts']);
            $ordids = $model->MakeTaggedOrders($parts);
            echo json_encode($ordids);
        }
    }

    public function checkRequisitionAction($args) {



        $parts = $this->nomalizeIDs($_POST['parts']);


        $stockControlModel = $this->loadModel('StockControl');
        $ret = $stockControlModel->checkRequisition($parts);
        echo json_encode($ret);
    }

    public function validateReqAction($args) {
        $req = $_POST['req'];

        $existOnly = isset($_POST['existOnly']) ? true : false;
        $stockControlModel = $this->loadModel('StockControl');
        $ret = $stockControlModel->validateReq($req, $existOnly);
        echo$ret;
    }

    public function approveReqAction($args) {
        $req = $_POST['req'];
        $stockControlModel = $this->loadModel('StockControl');
        $ret = $stockControlModel->approveReq($req);
    }

    //checks if parts status can be changed eg its now 18 or 02
    public function checkIfCanApplyNewStatusAction($args) {
        $parts = $this->nomalizeIDs($_POST['parts']);
        $stockControlModel = $this->loadModel('StockControl');
        $ret = $stockControlModel->checkIfCanApplyNewStatus($parts);
        echo json_encode($ret);
    }

    public function validateMakeTaggedOrdersAction($args) {
        $parts = $this->nomalizeIDs($_POST['parts']);
        $stockControlModel = $this->loadModel('StockControl');
        $ret = $stockControlModel->validateMakeTaggedOrders($parts);
        echo json_encode($ret);
    }

    //this function onverts array or string with coma separated ids to simple array
    public function nomalizeIDs($IDs) {
        if (is_array($IDs)) {
            foreach ($IDs as $i) {
                $tmp = explode(",", $i);
                foreach ($tmp as $u) {
                    $ret[] = $u;
                }
            }
        } else {
            $tmp = explode(",", $IDs);
            foreach ($tmp as $u) {
                $ret[] = $u;
            }
        }
        return $ret;
    }

    public function getColourKeyAction($args) {
        $stockControlModel = $this->loadModel('StockControl');
        $colourKey = $stockControlModel->getColourKey($this->user->ServiceProviderID);
        $this->smarty->assign("da", $colourKey);
        $this->smarty->display('popup/partColourKey.tpl');
    }

    public function stockReceivingOrdersAction($args) {








        $keys = [
            "Part ID",
            "Order Number",
            "Supplier",
            "Date Ordered",
            "Stock Code",
            "Description",
            "Order Qty",
            "Received Qty",
            "Currency",
            "Unit Purchase Price ()",
            "Receive Date",
        ];

        ///smarty assign

        $this->smarty->assign('data_keys', $keys);
        $this->smarty->assign('SuperAdmin', $this->user->SuperAdmin);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);






        ///smarty assign
        ////////////////-----------new------------------------/////////////////////
        $this->session->mainTable = "part";
        $this->session->mainPage = "stockReceiving";
        $this->session->controller = "StockControl";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $stockModel = $model = $this->loadModel('StockControl');
        $suppliers_model = $this->loadModel('Suppliers');
        $suppliers = $suppliers_model->getSPSuppliers($this->user->ServiceProviderID);
        $this->smarty->assign("suppliers", $suppliers);




        $this->smarty->display('stockControl/stockReceiving.tpl');
    }

    public function getALLSPOrdersAction($args) {
        $this->log($_GET);
        $stockModel = $model = $this->loadModel('StockControl');
        $ol = $stockModel->getALLSPOrders($this->user->ServiceProviderID, $_GET['query']);
        foreach ($ol as $r) {
            $re[] = $r['SPPartOrderID'];
        }
        $resp = [
            "query" => $_GET['query'],
            "suggestions" => $re
        ];
        echo json_encode($resp);
    }

    public function loadStockReceivingTableAction($args) {
        $dd = $_GET;
        $this->page = $this->messages->getPage("stockOrdering", $this->lang);
        $datatable = $this->loadModel('DataTable');

        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = "0";
        }



        $t = "sp_part_stock_item";



        if (isset($args['suplierID']) && $args['suplierID'] != 0) {
            $where.=" and sps.ServiceProviderSupplierID=" . $args['suplierID'];
        }






        $this->log($args, "cofee");
        $args[0] = isset($args[0]) ? $args[0] : "";
        switch ($args[0]) {
            case "Outstanding":
            case "": {
                    $where = " and $t.SPPartStatusID in (3)";

                    $columns = [
                        "GROUP_CONCAT(DISTINCT $t.SPPartStockItem SEPARATOR ',')",
                        "$t.SPPartOrderID",
                        "sps.CompanyName",
                        "date_format(spo.OrderedDate,'%d/%m/%Y')",
                        "spst.PartNumber",
                        "spst.Description",
                        "count(*)",
                        "null",
                        "spc.CurrencyCode",
                        "$t.PurchaseCost",
                        "date_format(spo.ReceviedDate,'%d/%m/%Y %H:%i')",
                    ];
                    $sp = false;

                    $joins = " join sp_part_order spo on spo.SPPartOrderID=$t.SPPartOrderID
join service_provider_supplier sps on sps.ServiceProviderSupplierID=spo.ServiceProviderSupplierID
join sp_part_stock_template spst on spst.SpPartStockTemplateID=$t.SPPartStockTemplateID
left join service_provider_currency spc on spc.CurrencyID=sps.DefaultCurrencyID
              
                ";






                    $groupBy = " group by $t.SPPartOrderID,$t.SPPartStockTemplateID";





                    break;
                }

            case "Received": {

                    $where = " and $t.SPPartStatusID not in (3)";

                    $columns = [
                        "GROUP_CONCAT(DISTINCT $t.SPPartStockItem SEPARATOR ',')",
                        "$t.SPPartOrderID",
                        "sps.CompanyName",
                        "date_format(spo.OrderedDate,'%d/%m/%Y')",
                        "spst.PartNumber",
                        "spst.Description",
                        "ssrh.QtyOrdered",
                        "ssrh.QtyReceived",
                        "spc.CurrencyCode",
                        "$t.PurchaseCost",
                        "date_format(ssrh.DateReceived,'%d/%m/%Y %H:%i')",
                    ];
                    $sp = false;

                    $joins = " join sp_part_order spo on spo.SPPartOrderID=$t.SPPartOrderID
join service_provider_supplier sps on sps.ServiceProviderSupplierID=spo.ServiceProviderSupplierID
join sp_part_stock_template spst on spst.SpPartStockTemplateID=$t.SPPartStockTemplateID
    Join sp_stock_receiving_history ssrh on ssrh.SpStockReceivingHistoryID=$t.SpStockReceivingHistoryID
left join service_provider_currency spc on spc.CurrencyID=sps.DefaultCurrencyID
              
                ";






                    $groupBy = " group by ssrh.SpStockReceivingHistoryID, $t.SPPartOrderID,$t.SPPartStockTemplateID";


                    break;
                }
            case "All": {

                    $where = " ";

                    $columns = [
                        "GROUP_CONCAT(DISTINCT $t.SPPartStockItem SEPARATOR ',')",
                        "$t.SPPartOrderID",
                        "sps.CompanyName",
                        "date_format(spo.OrderedDate,'%d/%m/%Y')",
                        "spst.PartNumber",
                        "spst.Description",
                        "ssrh.QtyOrdered",
                        "ssrh.QtyReceived",
                        "spc.CurrencyCode",
                        "$t.PurchaseCost",
                        "date_format(ssrh.DateReceived,'%d/%m/%Y %H:%i')",
                    ];
                    $sp = false;

                    $joins = " join sp_part_order spo on spo.SPPartOrderID=$t.SPPartOrderID
join service_provider_supplier sps on sps.ServiceProviderSupplierID=spo.ServiceProviderSupplierID
join sp_part_stock_template spst on spst.SpPartStockTemplateID=$t.SPPartStockTemplateID
    left Join sp_stock_receiving_history ssrh on ssrh.SpStockReceivingHistoryID=$t.SpStockReceivingHistoryID
left join service_provider_currency spc on spc.CurrencyID=sps.DefaultCurrencyID
              
                ";






                    $groupBy = " group by ssrh.SpStockReceivingHistoryID, $t.SPPartOrderID,$t.SPPartStockTemplateID";


                    break;
                }
        }





        $data = $datatable->datatableSS($t, $columns, $dd, $joins, array('<input type="checkbox">'), 0, false, 'no', $sp, "", $groupBy, $where, true);

        echo json_encode($data);
    }

    public function ReceivePartsFormAction($args) {

        $stockModel = $model = $this->loadModel('StockControl');
        $currencyModel = $this->loadModel('Currency');
        $items = $args['id'];
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $data = $stockModel->getPartReceiveData($items);
        $currencyRate = $currencyModel->getSPtoSupplierExchangeRate($data['ServiceProviderID'], $data['DefaultServiceProviderSupplierID']);

        $this->smarty->assign('data', $data);
        $this->smarty->assign('currencyRate', $currencyRate);
        $this->smarty->display('stockControl/receivePartForm.tpl');
    }

    public function ReceivePartsAction($args) {



        $stockModel = $model = $this->loadModel('StockControl');
        $items = $this->nomalizeIDs($_POST['items']);
        $stockModel->ReceiveParts($_POST, $items, $_POST['items']);
    }

    public function ReceiveMultiPartAction($args) {
        $items = $this->nomalizeIDs($_POST['items']);
        $stockModel = $model = $this->loadModel('StockControl');
        $currencyModel = $this->loadModel('Currency');
        $supplierData = $stockModel->getStockItemSupplierData($items[0]); //getting supplier id form item 0, as all items must be from same supplier
        $supplier = $supplierData['ServiceProviderSupplierID'];
        $supplierCurrencyID = $supplierData['DefaultCurrencyID'];
        $supCurrCode = $currencyModel->getCurrencyCode($supplierCurrencyID);
        $spCurrCode = $currencyModel->getCurrencyCode($supplierData['CurrencyID']); //service Provider Currency ID
        $currencyRate = $currencyModel->getSPtoSupplierExchangeRate($this->user->ServiceProviderID, $supplier);
        $_POST['currencyRate'] = $currencyRate;
        $_POST['supcurrencyCode'] = $supCurrCode;
        $_POST['spCurrency'] = $spCurrCode;
        $stockModel->ReceivePartsMulti($_POST, $items);
    }
    
    public function GRNFormAction($args){
         $supplierModel = $this->loadModel('Suppliers');
        $supplierList = $supplierModel->getSPSuppliers($this->user->ServiceProviderID);
        $this->smarty->assign('supplierList', $supplierList);
         $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
          $this->smarty->display('stockControl/GRNForm.tpl');
    }
    
    public function createGRNAction($args){
        
       
        $stockControlModel = $this->loadModel('StockControl');
       $GRNID=$stockControlModel->createGRN($_POST);
        echo"$GRNID";
    }
    
    public function printGRNAction($args){
        
        $stockControlModel = $this->loadModel('StockControl');
        $id=$args[0];
         $data = $stockControlModel->getGRNData($id,$this->user->ServiceProviderID);
        if($data){
        $this->smarty->assign("data", $data);
        $html = $this->smarty->fetch('stockControl/pdf/GRN.tpl');
        }else{
            $html = "<p>No data available for selected criteria.</p>";
        }
        
        include(APPLICATION_PATH . '/../libraries/MPDF56/mpdf.php');
        $mpdf = new mPDF();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }
    public function showHistoryNoteAction($args){
        $stockControlModel = $this->loadModel('StockControl');
        $data= $stockControlModel->showHistoryNote($args[0]);
        echo $data;
    }

}

?>
