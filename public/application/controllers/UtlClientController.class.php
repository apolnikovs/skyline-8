<?php

/**
 * UtlClientController.class.php
 * 
 * Implementation of Client for Samsung API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com> 
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 08/05/2012  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

require_once('CustomSmartyController.class.php');

class UtlClientController extends CustomSmartyController  {
    public $debug = false;                                                      /* Turn on or off debugging */
    public $config;  
    public $session;
    public $messages;
     
    public function __construct() {
        parent::__construct(); 

        $this->config = $this->readConfig('application.ini');                   /* Read Application Config file. */
        $this->session = $this->loadModel('Session');                           /* Initialise Session Model */
        $this->messages = $this->loadModel('Messages');                         /* Initialise Messages Model */
    }
         
    
    /**
     * jobsToWebMethods
     * 
     * Send all jobs in the UTL network marked not donloaded to SC to the UTL api
     * 
     * On successful transmission they will be marked as downloaded
     * 
     * @param $args - Arguments passed
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function jobsToWebMethods($args) {
        $job_model = $this->loadModel('Job');
        
        $jIds = $job_model->getWhereNotDowloadedToSc($this->config['UTL']['NetworkName']);    /* Get All the jobs to send to UTL */
        
        if (is_array($jIds)) {                                                  /* If we have matches */
            foreach($jIds as $jId) {
                $this->jobsToWebMethods(array('JobID' => $jId));                /* Send each job to UTL Web methods */
            } /* next jId */
        } /* fi is_array $job_model */
    }
    
    
    /**
     * sendToWebMethods
     * 
     * Get the details of a newly booked (ie service centre walk in) job and pass
     * them via the API to UTL
     * 
     * As the Samsung API works by use of XML data in the body of the POST rather
     * than passing pareters (as skyline / servicebase / rma it will use the 
     * sendToUtl function in this class rather than the API library routine.
     * 
     * @param $args - Arguments passed
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function sendToWebMethods($args) {
        $branches_model = $this->loadModel('Branches');
        $clients_model = $this->loadModel('Client');
        $country_model = $this->loadModel('Country');
        $county_model = $this->loadModel('County');
        $customer_model = $this->loadModel('Customer');
        $customer_titles_model = $this->loadModel('CustomerTitles');
        $job_model = $this->loadModel('Job');
        $manufacturers_model = $this->loadModel('Manufacturers');
        $models_model = $this->loadModel('Models');
        $network_model = $this->loadModel('ServiceNetworks');
        $nsp_model = $this->loadModel('NetworkServiceProvider');
        $service_types_model = $this->loadModel('ServiceTypes');
        
        $headers = array();
        
        $jId = $args['JobID'];
                
        $url = $this->config['UTL']['UtlApiUrl'];
        
        $headers[] = 'Content-Type: text/xml';
        $headers[] = 'UTL-Transaction-ID: '.$jId;
        $headers[] = 'UTL-Target-Environment: '.$this->config['UTL']['TargetEnvironment'];
        
        if ($this->config['UTL']['UtlApiUrl'] == 'Live')
        
        $recJob = $job_model->fetchAllDetails($jId);                            /* Get Job Data */
        $recCustomer  = $customer_model->fetchRow($recJob['CustomerID']);       /* Get Customer Data */
        if (! is_int($recCustomer['CustomerTitleID'])) {                        /* Get Customer Title */
            $recTitles = array('Title' => "");
        } else {
            $recTitles = $customer_titles_model->fetchRow(array('CustomerTitleID' => $recCustomer['CustomerTitleID']));
        }
        if (! is_int($recCustomer['CountyID'])) {                               /* Get County */
            $recCounty = array('Name' => "");
        } else {
            $recCounty = $county_model->fetchRow($recCustomer['CountyID']);
        }
        if (! is_int($recCustomer['CountryID'])) {                              /* Get Country */
            $recCountry = array('Name' => "");
        } else {
            $recCountry = $country_model->fetchRow($recCustomer['CountryID']);
        }
        $recServiceType = $service_types_model->fetchRow(array('ServiceTypeID' => $recJob['ServiceTypeID']));
        
        if (is_null($recJob['ServiceProviderID'])) {                            /* Check if  no service provider  */
            $this->log("putServiceRequest: Job {$args[0]} has no assigned ServiceProviderID",'utl_api_');
        }
        
        $recNsp = $nsp_model->fetchWhere("ServiceProviderID = {$recJob['ServiceProviderID']}
                                          AND NetworkID = {$network_model->getNetworkId($this->config['UTL']['NetworkName'])}");
                                          
        $recBranch = $branches_model->fetchRow(array('BranchID' => $recJob['BranchID']));   /* Get the Branch Details */
        
        $recClient = $clients_model->fetchRow(array('ClientID' => $recJob['ClientID']));
        
        $headers[] = 'UTL-Client-Name: '.$recClient[0]['ClientName'];
        /* TODO: Client prefix */
       
        $this->smarty->assign('job',$recJob);
        $this->smarty->assign('country', $recCountry);
        $this->smarty->assign('county', $recCounty);
        $this->smarty->assign('customer', $recCustomer);
        $this->smarty->assign('customer_title', $recTitles);
        $this->smarty->assign('service_type', $recServiceType[0]);
        $this->smarty->assign('nsp',$recNsp[0]); 
        $this->smarty->assign('branch', $recBranch[0]);
        $this->smarty->assign('client', $recClient[0]);
        
        if (is_null($recJob['ModelID'])) {
            $this->smarty->assign('model_code',$recJob['ServiceBaseModel']);    /* No model ID so return service base model */
        } else {
            $recModel = $models_model->fetchRow(array('ModelID' => $recJob['ModelID']));    /* Model ID so get model detials */
            $this->smarty->assign('model_code',$recModel['ModelNumber']);       /* Output Model number based on ModelID */
        }
        
        if (is_null($recJob['ManufacturerID'])) {
            $this->smarty->assign('manufacturer',$recJob['ServiceBaseManufacturer']);    /* No Manufacturer ID so return service base mmanufacturer */
        } else {
            $recModel = $manufacturers_model->getManufacturer($recModel['ModelNumber']);    /* Manufacturer  ID so get manufacturer detials */
            $this->smarty->assign('manufacturer',$recModel['ManufacturerName']);       /* Output Manufacturer number based on ManufacturerID */
        }
        
        $consent = array (                                                      /* Data conesnt - need to convert db enum to Samsung integer */
                          'DataProtectionMobile' => $this->putConsentMeaning($recCustomer['DataProtectionMobile']),
                          'DataProtectionEmail' => $this->putConsentMeaning($recCustomer['DataProtectionEmail']),
                          'DataProtectionHome' => $this->putConsentMeaning($recCustomer['DataProtectionHome']),
                          'DataProtectionOffice' => $this->putConsentMeaning($recCustomer['DataProtectionOffice']),
                          'DataProtectionSMS' => $this->putConsentMeaning($recCustomer['DataProtectionSMS']),
                          'DataProtectionLetter' => $this->putConsentMeaning($recCustomer['DataProtectionLetter'])
                         );
        $this->smarty->assign('consent', $consent);
        
        $xml_code = $this->smarty->fetch('api/utl/towebmethods.tpl'); 
        
        $response = $this->utlCall(
                                       $headers,
                                       $xml_code, 
                                       $url,
                                       $recNsp[0]['TrackingUsername'], 
                                       $recNsp[0]['TrackingPassword']
                                      );
        $this->log("toWebMethods :$jId : xml Response:\n{$response['raw']}",'utl_api_');
        
        $response = json_decode(json_encode($response['response']),TRUE);
        
        if ($response['raw']['http_code'] == 200 ) {                            /* Check if API call successful */
            $this->log("toWebMethods :$jId : SUCCESS",'utl_api_');              /* Yes */
            
            $job_model->update(                                                 /* Update Job with downloaded */
                                array(
                                      'JobID' => $jId,
                                      'DownloadedToSC' => 1
                                     )
                               );
     
        } else {
           $this->log("toWebMethods :$jId : FAIL",'utl_api_');
           
                       $job_model->update(                                      /* Update Job with not downloaded */
                                array(
                                      'JobID' => $jId,
                                      'DownloadedToSC' => 0
                                     )
                               );
        }
    }
    

    /**
     * getConsentMeaning
     * 
     * Get the meaning of data consent values - UTL use same as Samsung
     * 
     * @param integer $type     The Samsung Data Consent Value (1,2,3)
     * 
     * @return string   The description of the consent value for the database
     *                  enum field
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function getConsentMeaning($type) {
        $values = array (
                         1 => 'Marketing',
                         2 => 'SVCCampaign',
                         3 => 'Both'
                        );
        
        return($values[$type]);
    }
    
    /**
     * putConsentMeaning - UTL use same as Samsung
     * 
     * Put the meaning of Data consent values from the dtabase enum
     * 
     * @param string $type     The data conest value stored in the database
     * 
     * @return integer   The Samsung Data Consent Value (1,2,3) 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function putConsentMeaning($type) {
        switch($type) {
            case 'Marketing':
                $retval = 1;
                break;
            
            case 'SVCCampaign':
                $retval = 2;
                break;
            
            case 'Both':
                $retval = 3;
                break;
            
            default:
                $retval = null;
                break;
        }
        
        return($retval);
    }
    
    /**
     * clearArray
     * 
     * Clear out emply arrays for blank values that appear in the format return 
     * by Samsung
     * 
     * @param array $arr    The array to be cleared
     * 
     * @return array    Array with blank values removed
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    private function clearArray($arr) {
        $retVal = array();
               
        foreach ($arr as $k=>$v) {
            
            if (is_array($arr[$k])) {
                if (count($arr[$k]) == 0) {
                    $retVal[$k] = null;
                }
                else
                {
                    $retVal[$k] = $this->clearArray($arr[$k]);
                } /* fi count */
            } else {
                $retVal[$k] = $arr[$k];
            } /* fi is_array */
        } /* next array */
        
        return($retVal);
    }
    
    /**
     * utlCall
     * 
     * Exceute call to the UTL API
     * 
     * @param array $args - Array of the arguments to be turned in XML
     *                      (or string of generated XML)
     * @param string $url - URL to be called
     * @param string $user - Username for API
     * @param string $pass - Password for API
     * 
     * @return array   Curl response information and information retuned by API
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    protected function utlCall($headers, $args, $url, $user, $pass) {
        
        $ch = curl_init();
        
        if (is_array($args)) {
            $xml_build['ASC_JOB_REQUEST']['JOB_REQUEST'] = $args;

            $xml = $this->SerializeAsXML($xml_build);                           /* Get  XML from array */
        } else {
            $xml = $args;                                                       /* XML sent as parmeter */
        }
        
        $this->log("XML sent out to $url : \n $xml","utl_api_");
        
        $headers[] = 'Content-Type: text/xml';
        $headers[] = 'Content-Length: '.strlen ($xml);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $pass);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        /*curl_setopt($ch, CURLOPT_HTTPHEADER, array ('Accept: ' . $this->acceptType));*/



        $response = curl_exec($ch);         
        $info = curl_getinfo($ch);


        curl_close($ch);

        //$this->log(var_export($response,true));

        $raw = $response;                                                   /* Store response, (used in some specific API logs) */
        /*$this->log(var_export($info,true));*/
        if ($this->debug) $this->log(var_export($raw,true));
        if (is_array($info)) {
            if (stripos($info['content_type'],'json') !== false) {
                $response = json_decode($response,true);
            } else if (stripos($info['content_type'],'xml') !== false) {
                $response = preg_replace( "/[^\x0A\x0D\x20-\x7E]/", "", $response );
                $response = (array) simplexml_load_string($response);
            }
        }

        return array ( 'response' => $response, 
                        'info' => $info,
                        'raw' => $raw);

        
    }   
    
    
    /**
     * Description
     * 
     * Create CML from an object or Array
     * 
     * @param array / object $obj  Array or object to turn to xeml
     *        integer $depth       Optional varible to specify how deep to
     *                             traverse
     * 
     * @return array recordSetSerialise   Associative array containing respose
     * 
     * @author Brian Etherington <b.etherington@pccsuk.com>  
     **************************************************************************/
    protected function SerializeAsXML($obj, $depth = 0) {
        
        $d = '';
        
        if (is_array($obj) || is_object($obj)) {
            
            $tabs = '';
            
            for ($i = 0; $i <= $depth; $i++) {
                $tabs .= "\t";
            }
            
            foreach ($obj as $o_i => $o_v) {
                
                $useTab = false;
                /* If element is numeric then ignore as xml tags can not be numeric. Data being passed should have been through recordSetSerialise when building response */
                if (!is_int($o_i)) {
                    /*Not numeric - so use tag */               
                    $d .= $tabs . "<{$o_i}>";
                }
                if (is_array($o_v) || is_object($o_v)) {
                    $d .= "\n" . $this->SerializeAsXML($o_v, ($depth + 1));
                    $useTab = true;
                } else {
                    $d .= htmlentities($o_v, ENT_COMPAT);
                }
                if ($useTab) {
                    $d .= $tabs;
                }
                if (!is_int($o_i)) {
                    /*Not numeric - so use tag */    
                    $d .= "</{$o_i}>\n";
                }
            }
            
        }
        
        return $d;
    }
    
    
    /**
     * recordSetSerialise
     * 
     * Serialise a recordset so ecah record is under number=>tag for easy
     * xml construction
     * 
     * @param array $recordset     Recordset from DbQuery, 
     *        string $tag          Tag to surround each record        
     * 
     * @return array    Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function recordSetSerialise($recordset, $tag) {
        $response = array();
        
        foreach ($recordset as  $n => $field) {
            $response[$n][$tag] = $field;
        }
        return($response);
    }
    
}

?>
