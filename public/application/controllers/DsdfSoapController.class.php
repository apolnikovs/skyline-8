<?php
/**
 * DsdfSoapController.class.php
 * 
 * Soap Server for DSDF Interface
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link 
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 22/01/2013  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

require_once('SkylineSOAPController.class.php');
require_once('xml2array.php');

class DsdfSoapController extends SkylineSOAPController {
    public $debug = false;                                                       /* Turn on or off debugging */
    protected $wsdl = 'dsdf.wsdl';
       
    public function test($n) {
        return("Hello $n");
    }
    
    /**
     * dsdf
     * 
     * Deal with dsdf for contact
     * 
     * @param string $xml   Parameters in xml
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function dsdf($xml) {
        $data = xml2array($xml);                                                /* Turn passed xml parameters into array */
        $response = '<?xml version="1.0" encoding="utf-8">';
        $response .= "\n".'<DSDF xmlns="http://www.digitaleurope.org/ns/dsdf/">';
        
        if ( ! isset($data['DSDF']['mainData']['mainDataBatchDocumentType']) ) $data['DSDF']['mainData']['mainDataBatchDocumentType'] = '';
        if ( ! isset($data['DSDF']['mainData']['mainDataSystemUserLogin']) ) $data['DSDF']['mainData']['mainDataSystemUserLogin'] = '';
        if ( ! isset($data['DSDF']['mainData']['mainDataSystemUserPassword']) ) $data['DSDF']['mainData']['mainDataSystemUserPassword'] = '';
        
        $user = $data['DSDF']['mainData']['mainDataSystemUserLogin'];           /* Get username and password */
        $pass= $data['DSDF']['mainData']['mainDataSystemUserPassword'];
        
        $this->getUserDetails($user);                                           /* Get the user details for the specified user */
        if ( $this->authorise($pass) == false ) {                               /* Is the user authorised ? */
            $response .= "\n  <mainData>";                                      /* No */
            $response .= "\n    <mainDataBatchDocumentType>RET</BatchDocumentType>";
            $response .= "\n    <mainDataResultCode>1</mainDataResultCode>";
            $response .= "\n    <mainDataResultInfo>Unauthorised</mainDataResultInfo>";
            $response .= "\n  </mainData>";   
        } else {                                                                /* Yes is authorised */
            switch($data['DSDF']['mainData']['mainDataBatchDocumentType']) {    /* Get the transaction type */
                case 'ORD':                                                     /* XML contains a new service order (ticket) */
                    $response .= $this->ord($data['DSDF']);
                    break;
                
                case 'ORU':                                                     /* XML contains an update to an existing ticket */               
                case 'CLA':                                                     /* XML contains a claim submission based on an existing ticket reference or new claim data */                
                case 'PAR':                                                     /* XML contains a spare part order */        
                case 'MLR':                                                     /* XML contains a request for a list of models or parts or the status of a single part */
                case 'TLR':                                                     /* XML contains a request for a list of new tickets */
                case 'TRQ':                                                     /* XML contains a request for ticket details */
                    $response .= "\n  <mainData>";                              
                    $response .= "\n    <mainDataBatchDocumentType>RET</BatchDocumentType>";
                    $response .= "\n    <mainDataResultCode>1</mainDataResultCode>";
                    $response .= "\n    <mainDataResultInfo>Not yet implemented in skyline<mainDataResultInfo>";
                    $response .= "\n  </mainData>";
                    break;
                
                default:
                    $response .= "\n  <mainData>";                              
                    $response .= "\n    <mainDataBatchDocumentType>RET</BatchDocumentType>";
                    $response .= "\n    <mainDataResultCode>1</mainDataResultCode>";
                    $response .= "\n    <mainDataResultInfo>No such transaction type {$data['DSDF']['mainData']['mainDataBatchDocumentType']}</mainDataResultInfo>";
                    $response .= "\n  </mainData>";
            }
        }
        
        $response .= "\n</DSDF>";
        
        return($response);
    }
    
    /**
     * ord
     * 
     * Create a new job
     * 
     * @param array $data   Passed parameters 
     * 
     * @return string containing gerenerated source
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    private function ord($data) {
        $manufacturers_model = $this->loadModel('Manufacturers');
                
        $response = '';
        
        /*
         * Manufacturer
         */
        if (isset($data['mainData']['mainDataManufacturerName'])) {             /* Deal with manufacturer field if set */  
            $job_fields['ManufacturerID'] = $manufacturers_model->getManufacturerId($data['Manufacturer']); /* Get the ID of the manufacturer */
            if (is_null($job_fields['ManufacturerID'])) {                       /* Manufacturer not found */
                unset ($job_fields['ManufacturerID']);                          /* No Match, so don't want ManufacturerID */
                $job_fields['ServiceBaseManufacturer'] = $data['mainData']['mainDataManufacturerName'];   /* So set the free text ServiceBaseManufacturer */
            } /* is_null $fields['ManufacturerID'] */
        }
        
        if (isset($data['mainData']['mainDataIncidentNumberCall'])) $job_fields['AgentRefNo'] = $data['mainData']['mainDataIncidentNumberCall'];
        if (isset($data['mainData']['mainDataIncidentNumberManufacturer'])) $job_fields['NetworkRefNo'] = $data['mainData']['mainDataIncidentNumberManufacturer'];
        if (isset($data['mainData']['mainDataIncidentNumberCall'])) $job_fields['AgentRefNo'] = $data['mainData']['mainDataIncidentNumberCall'];

        
        return($response);
    }
    
}

?>
