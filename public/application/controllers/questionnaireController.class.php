<?php

require_once('CustomSmartyController.class.php');
require_once('Constants.class.php');

/**
 * Short Description of QuestionnaireController.
 * 
 * Long description of QuestionnaireController.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 10/04/2013  1.0     Brian Etherington     Initial Version
 ******************************************************************************/

class questionnaireController extends CustomSmartyController {
    
    public $user;
    public $lang = 'en'; 
    
    public function __construct() { 
        
        parent::__construct(); 
                      
       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
        $this->session = $this->loadModel('Session'); 
        
        if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }

        $this->smarty->assign('_skin',$this->session->QuestionnaireSkin);
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages'); 
        
        
    }
    
    public function __call($name, $arguments) {
        /*
         * Allow this controller to be called without a method name.
         * The method name is efectively the Job No parameter
         * e.g. http://...../questionnaire/job_no
         */
        
        $job_id = strlen($name) > 6 ? substr($name, 0, -6) : null;
        $this->indexAction( array( $job_id, count($arguments[0]) > 0 ? $arguments[0][0] : null ) );
    }
    
    
    public function indexAction( $args ) {
        
        //$user_model = $this->loadModel('Users');
        
        if (isset($args['lang'])) { 
            $this->lang = $args['lang'];
            $this->session->lang =$this->lang;  
        }
        
        if( isset($args['skin']) ) {
            $this->session->QuestionnaireSkin = '/skins/'.$args['skin'];
            $this->smarty->assign('_skin',$this->session->QuestionnaireSkin);
        }
        
        $localised_messages = $this->messages->getPage('questionnaire',$this->lang);
        $this->smarty->assign('page', $localised_messages); 
               
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {            
            $postData=$_POST;           
            $postData['Who_suggested_our_services_to_you']=isset($postData['Who_suggested_our_services_to_you']) ? $postData['Who_suggested_our_services_to_you'] : "";
            $postData['permission_to_use_comments']=isset($postData['permission_to_use_comments']) ? $postData['permission_to_use_comments'] : "No";
         
            //get logo name created by srinvias
            $questionnaire_model = $this->loadModel('Questionnaire');
            $log = $questionnaire_model->GetLog($_POST['_log']);                      
            
            $brandID = $log['BrandID'];
            $brand_model = $this->loadModel('Brands');
            $brand=$brand_model->getBrandByID($brandID);
     
            $this->smarty->assign('logo',$brand_model->getBrandLogo($brand['BrandName']));
            //end of getting logo            
            $qustionnaire_business_model = $this->loadModel('QuestionnaireBusinessModel');
            $qustionnaire_business_model->CreateQuestionnaire( $postData ); 

            $this->smarty->display('questionnaires/'.$_POST['_questionnaire_type'].'_response.tpl');
            
        } else {
            
            if (empty($args[0])) {
                    echo "<br /><br /><br /><br />
                    <center><h3>Missing Questionnaire reference No.</h3></center>" ;
                exit;
            }
                
            
            $questionnaire_model = $this->loadModel('Questionnaire');
            $log = $questionnaire_model->GetLog($args[0]);
            
            if ($log == null) 
            {   
                // commited srinvas because it is showing 404 pages when quesionarire is not found
                //throw new Exception("Questionnaire reference {$args[0]} Not Found");
                echo "<br /><br /><br /><br />
                    <center><h3>Questionnaire reference {$args[0]} Not Found.</h3></center>" ;
                exit;
            }
            
            /*/* srinvias : Avoid multiple submission same quesionaire by customer    */    
            if ($questionnaire_model->ValidateCustomerQuestionaire($args[0])) 
            {   
                // throw new Exception("Questionnaire reference {$args[0]} Not Found");
                 echo "<br /><br /><br /><br />
                     <center><h3>You are already Submitted this Questionnaire. </h3></center>" ;
                 exit;
            } 
           
             
            $jobid = $log['JobID'];      
            $type = $log['Type'];
            
            $skyline_business_model = $this->loadModel('SkylineBusinessModel');
            $job_details = $skyline_business_model->getJobDetails( $jobid );          
            
            if ($job_details['error'] != '')
                throw new Exception($job_details['error']);
            
            $brandID = $log['BrandID'];
            $brand_model = $this->loadModel('Brands');
            $brand=$brand_model->getBrandByID($brandID);
            $brandLogo=$brand_model->getBrandLogo($brand['BrandName']);
            $this->smarty->assign('logo',$brandLogo);
            
            //getting log width
            if(defined('SUB_DOMAIN')) {
                    $sub_domain = SUB_DOMAIN;
                } else {
                    $sub_domain = '';
                }

                if(preg_match('/www/', $_SERVER['HTTP_HOST']))
                {
                    $wwwString = '';
                }
                else
                {
                    $wwwString = 'www.';
                }

                if(isset($_SERVER['HTTPS'])) { 
                    //$domain_name = 'https://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
                     $domain_name = 'https://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";

                } else { 
                    //$domain_name = 'http://' . $_SERVER["HTTP_HOST"] . $sub_domain."/";
                    $domain_name = 'http://'.$wwwString. $_SERVER["HTTP_HOST"] . $sub_domain."/";
                } 
            
                $brandLogo = $domain_name . $this->config['Path']['brandLogosPath'].$brandLogo;
                // $brandLogo = 'http://local.skyline/images/brandLogos/1000_logo.png';  
            
            list($logowidth, $logoheight, $logotype, $logoattr) =  getimagesize($brandLogo);
            //end of logo width
            $logowidth=isset($logowidth) ? $logowidth : 0 ;
            $this->smarty->assign('logowidth',$logowidth);
            
            $this->smarty->assign('log',$log['QuestionnaireLog']);
            $this->smarty->assign('brand',$brand['BrandName']);
            $this->smarty->assign('brandID',$brandID);
            $this->smarty->assign('manufacturer',$job_details['job']['manufacturer']);
            $this->smarty->assign('service_agent',$job_details['service_provider']['name']);
            
            //get browser name created by srinvias
            $browser = $this->browserDetect();            
            $this->smarty->assign('browser',$browser);
            //end 
            
            //get required questionnaire id for respective brand             
            $Questionnaire         = $this->loadModel('Questionnaire');
            $qID                   = $Questionnaire->getRequiredQuestionnair($brandID);           
            $datarow               = $Questionnaire->getQuestionnaireAttribute($qID); 
            
            
            //written by srinivas for getting new Questionary for Brand
       
            $this->smarty->assign('datarow',$datarow);
            $this->smarty->assign('job_id',$job_details['JobID']);
            $this->smarty->assign('customer_id',$job_details['customer']['id']);

            $this->smarty->display('questionnaires/'.$type.'.tpl');
            
        }
        
    }
    
    public function browserDetect(){
        $browser="";
        if(strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),strtolower("MSIE"))){$browser="ie";}
        else if(strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),strtolower("Presto"))){$browser="opera";}
        else if(strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),strtolower("CHROME"))){$browser="chrome";}
        else if(strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),strtolower("SAFARI"))){$browser="safari";}
        else if(strpos(strtolower($_SERVER["HTTP_USER_AGENT"]),strtolower("FIREFOX"))){$browser="firefox";}
        else {$browser="other";}
        return $browser;
    }

    
}


?>
