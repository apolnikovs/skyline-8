<script>
$(popupPrompt1).keypress(function(e){
    if (e.which == 13){
        $("#approveButtonS").click();
    }
    
});

$(document).ready(function(){

});
</script>
<div id="popupPrompt1" style="width: 600px;">
    <fieldset>
        <legend>{$popupData.legend|default:'Confirm'}</legend>
        <p style="text-align:center">{$popupData.text|default:'Are you sure?'}</p>
        {if $popupData.inputLabel|default:''!=""}
            <p id="validationMsgP" style="text-align:center;color:red;"><br></p>
            <p style="text-align:center">
                <span>{$popupData.inputLabel}<span style="color:red">*</span>:</span><input onchange="$('#validationMsgP').html('Working...')" type="text" id="{$popupData.inputID}">   
            </p>
        {/if}
        {if !$popupData.oneBut|default:""}
        <p>
            <button id="approveButtonS" class="gplus-blue" style="margin-left:260px" onclick="{$popupData.confButAction|default:''};">{$popupData.confButText|default:'Continue'}</button>
            <button class="gplus-blue" style="float:right" onclick="{$popupData.cancelButAction|default:'$.colorbox.close()'}">{$popupData.cancelButText|default:'Cancel'}</button>
        </p>
        {else}
            <p>
            <button id="approveButtonS" class="gplus-blue" style="margin-left:260px" onclick="{$popupData.confButAction|default:'$.colorbox.close()'}">{$popupData.confButText|default:'Ok'}</button>
            </p>
        {/if}
    </fieldset>

</div>
      