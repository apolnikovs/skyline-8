{extends "DemoLayout.tpl"}

{block name=config}

{$PageId = $SkillsSetSPPage}

{$def=0}
{if $SuperAdmin}{$def=1}{/if}
 {if !isset($allocOnly)||$allocOnly!='AllocationOnly'}
     {$def=1}
     {/if}
    {if $allocOnly=='NoViamente'}
     {$def=2}
     {/if}
{$PageId = $AppointmentDiaryPage}

{/block}

{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" /> 
   <style type="text/css" >
      
    .ui-combobox-input {
        width:270px;
     }   
         
</style>
{/block}

{block name=scripts}
{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
        



<script type="text/javascript">
    
     function adjustPanelHeights ()
     {
        $('#secondDiv').css("height", '');
        $('#firstDiv').css("height", ''); 
        $('#innerFirstDiv').css("height", ''); 
         
        $height1 = $('#firstDiv').height();
        $height2 = $('#secondDiv').height();

        $maxHeight = 400;
        
        if($height1>$maxHeight)
        {
           $maxHeight = $height1;
        } 
        
        if($height2>$maxHeight)
        {
           $maxHeight = $height2;
        } 

        $maxHeight = $maxHeight+50;
        
        $('#firstDiv').css("height", $maxHeight+"px");
        $('#secondDiv').css("height", $maxHeight+"px");
        $('#innerFirstDiv').css("height", ($maxHeight-30)+"px");
     }
    function getSkillsSet(mode)
        {
           
                 var $RepairSkillID     = $("#RepairSkillID").val();
                 var $ServiceProviderID = $("#ServiceProviderID").val();


                 $skillsSetList = '<tr><td class="firstTD" >{$page['Labels']['appointment_type']|escape:'html'}</td><td class="secondTD" >{$page['Labels']['service_time']|escape:'html'}</td><td class="thirdTD" >{$page['Labels']['engineers_required']|escape:'html'}</td></tr>';
                  
                                         
                 if($RepairSkillID && $RepairSkillID!='' && $ServiceProviderID && $ServiceProviderID!='')
                 {


                     //Getting skills for selected repair skill.   
                     $.post("{$_subdomain}/Data/getRepairSkillSkillsSet/"+urlencode($RepairSkillID)+'/'+urlencode($ServiceProviderID),        

                     '',      
                     function(data){
                             var $skillsSet = eval("(" + data + ")");

                             if($skillsSet)
                             {

                                 for(var $i=0;$i<$skillsSet.length;$i++)
                                 {
                                         if($skillsSet[$i]['SPSkillsetID'])
                                         {
                                            $checkBoxChecked   = " checked='checked' ";
                                            $serviceTime       = $skillsSet[$i]['SPServiceDuration'];
                                            $engineersRequired = $skillsSet[$i]['SPEngineersRequired'];
                                            if(mode=='unchecked'){ dsp='none';}else{ dsp='block'}
                                         }
                                         else
                                         {
                                            $checkBoxChecked   = " ";
                                            $serviceTime       = $skillsSet[$i]['ServiceDuration'];
                                            $engineersRequired = $skillsSet[$i]['EngineersRequired'];
                                            if(mode=='checked'){ dsp='none';}else{ dsp='block'}
                                         }
                                         if(dsp=='none'){
                                        $skillsSetList +=  '<tr style="display:none"> <td class="firstTD" ><input class="APCheckBox" type="checkbox" '+$checkBoxChecked+'  id="APType_'+$skillsSet[$i]['SkillsetID']+'"  name="SkillsetID[]" value="'+$skillsSet[$i]['SkillsetID']+'" >&nbsp;&nbsp;'+$skillsSet[$i]['Type']+'</td> <td class="secondTD" > <input type="text" class="text" style="width:30px;" id="SkillsetIDST_'+$skillsSet[$i]['SkillsetID']+'"  name="SkillsetIDST_'+$skillsSet[$i]['SkillsetID']+'" value="'+$serviceTime+'" ></td><td class="thirdTD" ><input type="text" class="text" style="width:30px;" id="SkillsetIDER_'+$skillsSet[$i]['SkillsetID']+'"  name="SkillsetIDER_'+$skillsSet[$i]['SkillsetID']+'" value="'+$engineersRequired+'" ></td></tr>';
                                        }
                                         if(dsp=='block'){
                                        $skillsSetList +=  '<tr > <td class="firstTD" ><input class="APCheckBox" type="checkbox" '+$checkBoxChecked+'  id="APType_'+$skillsSet[$i]['SkillsetID']+'"  name="SkillsetID[]" value="'+$skillsSet[$i]['SkillsetID']+'" >&nbsp;&nbsp;'+$skillsSet[$i]['Type']+'</td> <td class="secondTD" > <input type="text" class="text" style="width:30px;" id="SkillsetIDST_'+$skillsSet[$i]['SkillsetID']+'"  name="SkillsetIDST_'+$skillsSet[$i]['SkillsetID']+'" value="'+$serviceTime+'" ></td><td class="thirdTD" ><input type="text" class="text" style="width:30px;" id="SkillsetIDER_'+$skillsSet[$i]['SkillsetID']+'"  name="SkillsetIDER_'+$skillsSet[$i]['SkillsetID']+'" value="'+$engineersRequired+'" ></td></tr>';
                                        }
                             }
                             }

                              $("#AppointmentTypeTable").html('');
                              $("#AppointmentTypeTable").html($skillsSetList);

                           //  $('#secondDiv').css("height", '');
                          //   $('#firstDiv').css("height", '');
                             
                             adjustPanelHeights();
                     });
                 }
                 else
                 {
                     $("#AppointmentTypeTable").html('');
                     $("#AppointmentTypeTable").html($skillsSetList);
                     
                    // $('#secondDiv').css("height", '');
                  //   $('#firstDiv').css("height", '');
                     adjustPanelHeights();
                     
                     
                 }

        }
    function inactiveRow(nRow, aData)
    {
       
        $('td:eq(0)', nRow).html( '<input type="radio" name="SP_RepairSkillID" value="'+aData[0]+'" >'+aData[1]);
        
       
        if (aData[3]=="In-active")
        {  
             $('td:eq(2)', nRow).html( '<img src="{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png" align="center"  width="20" height="20" >' );
        }
        else
        {   
              $('td:eq(2)', nRow).html( '<img src="{$_subdomain}/css/Skins/{$_theme}/images/green_tick.png" align="center" width="20" height="20" >' );
        }
    } 
    
    
    
    
    
    
           
       
    
    
    $(document).ready(function() {
    
	        //click handler for help icons starts here.
       $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
        //click handler for help icons ends here.              
                     
        
		
    //Click handler for cancelChanges.
        $(document).on('click', '[name="SP_RepairSkillID"]', 
        function() {

                
                showDataForm();
                clearDataForm();
                $("#RepairSkillID").val($(this).val());
                setValue("#RepairSkillID");
                
                

        });  
    
    
    
     
     
    
     
     function showHomePage()
     {
        clearDataForm(); 
        $("#addRecord").show();
        $("#navigationButtons").show();
        
        
        $("#saveRecord").hide();
        $("#cancelChanges").hide();
        $("#formDiv").hide();
       // $('#innerFirstDiv').css("height", '');
      //  $('#firstDiv').css("height", '');
      //  $('#secondDiv').css("height", '');
        $("#defaultDiv").show();
        adjustPanelHeights();
     }
    
    
       adjustPanelHeights();
     
     
       
       
       //Click handler for add engineer.
        $(document).on('click', '#addRecord', 
                      function() {

                        showDataForm();
                        clearDataForm();
                        return false;
                          
                      });
                      
        //Click handler for cancelChanges.
        $(document).on('click', '#cancelChanges', 
                      function() {
                      
                        document.location.href = "{$_subdomain}/AppointmentDiary/skillsSetSP/spID="+$("#ServiceProviderID").val();
                        return false;
                          
                      }); 
                      
       
      {if $showUpdatePage} 
         
         showDataForm();
                      
      {/if}  
        
        
      {if $tInfo} 
          
           $("#centerInfoText").html("{$page['Text']['data_saveed_msg']|escape:'html'}").fadeIn('slow').delay("5000").fadeOut('slow');  
      
      {/if}
          
           
          
          
     function clearDataForm()
     {
        $("#RepairSkillID").val('');
        
        
        
     }   
                      
     function showDataForm()
     {
        $("#defaultDiv").hide();
      //  $('#secondDiv').css("height", '');
        $("#formDiv").show();
        adjustPanelHeights();

        $("#saveRecord").show();
        $("#cancelChanges").show();


        $("#addRecord").hide();
        $("#navigationButtons").hide();
        
     }
     
    
    
    
    
     {if $SuperAdmin}
     
        $( "#ServiceProviderID" ).combobox( { change: function() { document.location.href="{$_subdomain}/AppointmentDiary/skillsSetSP/spID="+$("#ServiceProviderID").val(); } } );
    
     {/if}
    
    
    $( "#RepairSkillID" ).combobox( { change: function() { 

        
        getSkillsSet('checked');
        


    } } );
    
    
    
    $('button[aria-haspopup=true]').css("width", "310px");
    $('button[aria-haspopup=true]').css("border-radius", "7px");
    
    
     $(document).on('click', '#saveRecord', 
                            function() {
                            
                                 $("#HidddenRepairSkillID").val($("#RepairSkillID").val());
                                 
                                 
                                 
                                 $("#saveRecord").hide();
                                 $("#cancelChanges").hide();
                                 $("#processDisplayText").show();
                            
                                 $('#skillsSetSPForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                                       HidddenRepairSkillID: {
                                                            required: true
                                                       },
                                                        
                                                        
                                                       HiddenField:
                                                       {
                                                           required: function (element) {
                                                               
                                                                $returnValue = false;
                                                               
                                                                $('.APCheckBox').each(function() {
                                                                
                                                                    
                                                                    if ($(this).is(':checked')) 
                                                                    {
                                                                       
                                                                        $sksetID = $(this).val();
                                                                        
                                                                        if($("#SkillsetIDST_"+$sksetID).val()==''  || $("#SkillsetIDST_"+$sksetID).val()=='0' || $("#SkillsetIDER_"+$sksetID).val()==''  || $("#SkillsetIDER_"+$sksetID).val()=='0')
                                                                        {
                                                                            $returnValue = true;    
                                                                        }
                                                                        
                                                                    } 
                                                                    
                                                                    
                                                                });
                                                                
                                                                return $returnValue;
                                                                
                                                                
                                                            }
                                                       }
                                                    
                                                   },
                                            messages: {
                                            
                                                        HidddenRepairSkillID: {
                                                            required: "{$page['Errors']['skill_type']|escape:'html'}"
                                                        },
                                                       
                                                        HiddenField:
                                                        {
                                                            required: "{$page['Errors']['service_time']|escape:'html'}"
                                                        }
                                                      
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                                
                                                error.insertAfter( element );
                                                
                                                $("#processDisplayText").hide();
                                                $("#saveRecord").show();
                                                $("#cancelChanges").show();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                                
                                                
                                                $.post("{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/",        

                                                        $("#skillsSetSPForm").serialize(),      
                                                        function(data){
                                                            // DATA NEXT SENT TO COLORBOX
                                                            var p = eval("(" + data + ")");

                                                                if(p['status']=="ERROR")
                                                                    {
                                                                        
                                                                        $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  
                                                                        
                                                                        $("#processDisplayText").hide();
                                                                        $("#saveRecord").show();
                                                                        $("#cancelChanges").show();

                                                                    }
                                                                    else if(p['status']=="OK")
                                                                    {

                                                                     
                                                                       document.location.href = "{$_subdomain}/AppointmentDiary/skillsSetSP/spID="+$("#ServiceProviderID").val()+"/tInfo=1";
                                                                       
                                                                       
                                                                    }
                                                        }); //Post ends here...
                                                
                                                
                                                
                                            }
                                                    
                            
                            
                            });
    
    });
    
    
    
    
    
                       
        //Service Proivder Skill Set data table starts here...   
        $('#SPSSResults').PCCSDataTable( {


                "aoColumns": [ 
                                            /* SP_RepairSkillID */  {  "bVisible":    false },    
                                            /* Skill Name. */   {  "bSortable":    false }
                                    ],






                displayButtons:  '',    
                htmlTablePageId: 'innerFirstDiv',
                htmlTableId:     'SPSSResults',
                fetchDataUrl:    '{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/fetch/{$ServiceProviderID}',
                fnRowCallback:   'inactiveRow',
                searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                iDisplayLength:  12,
                sDom: 't<"#dataTables_command">rp',
                sPaginationType: 'two_button',
                hidePaginationNorows:  true,
                formInsertButton:'insert_save_btn',
                
                            frmErrorRules:   {
                                            
                                                         
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                  
                                                       
                                              },                     
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"



            });
         //Service Proivder Skill Set data table ends here...       
    
    
    
    
    
        
    
    
    
    
         {if $DiaryWizardSetup}
         
            $('#menu').after('<div class="customOverlay" style="display: block; opacity: 0.25; top:0; cursor: default; width:955px;height:200px;"></div>');
        
         {/if}
             
             
             
         
        $(document).on('click', '#nextButton', 
                                function() {

                    if($('[name="SP_RepairSkillID"]').length) 
                    {    
                     document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val();    
                    }
                    else
                    {
                        alert("{$page['Errors']['skill_set']|escape:'html'}");
                    }
                    return false;

            });

        $(document).on('click', '#previousButton', 
                                function() {

                    document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=6";    

                    
                    return false;
                });    
    
    
    });
    
    

</script>


    
{/block}

{block name=body}

    
<div class="breadcrumb" {if $def==0}style="display:none"{/if}>
    
    
    
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        <a href="{$_subdomain}/index/siteMap" >{$page['Text']['site_map']|escape:'html'}</a>  / 
        <a href="{$_subdomain}/AppointmentDiary/" >{$page['Text']['page_title']|escape:'html'}</a> / 
        {$page['Text']['defaults']|escape:'html'}
    </div>
</div>
 
{include file='include/site_map_menu.tpl'}

        
<div class="main" id="appointmentDiary" >   
   
   <div class="siteMapPanel" >
         <form id="skillsSetSPForm" name="skillsSetSPForm" method="post"  action="#" class="inline">
       
             
            <fieldset>
            
             
                        <div id="firstDiv" class="firstDiv borderDiv">
                           
                           <div id="innerFirstDiv" >
                            
                            {if $SuperAdmin}
                            
                             <select  name="ServiceProviderID" id="ServiceProviderID"  class="text auto-hint" >
                                {foreach from=$serviceProvidersList item=sp}
                                 <option value="{$sp.ServiceProviderID}" {if $ServiceProviderID eq $sp.ServiceProviderID}selected="selected"{/if} >{$sp.CompanyName|escape:'html'}</option>
                                 {/foreach}

                             </select>  
                                 
                            {else}
                            
                                <input type="hidden" name="ServiceProviderID" id="ServiceProviderID" value="{$ServiceProviderID|escape:'html'}" >
                                
                             {/if}
                                 
                            <table id="SPSSResults" border="0" cellpadding="0" cellspacing="0" class="browse" style="padding-top:4px;" >
                                <thead>
                                        <tr>
                                                <th></th> 
                                                <th  title="{$page['Text']['skills_set']|escape:'html'}"  >{$page['Text']['skills_set']|escape:'html'}</th>
                                        </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table> 
                                                
                                                
                           </div>
                           
                          {* <p style="padding-top:5px;" >
                           <input type="checkbox" name="adViamente" id="adViamente"  >{$page['Text']['viamente_enabled']|escape:'html'}
                            
                           </p> *}
                           
                        </div>
                        
                        <div id="secondDiv" class="secondDiv borderDiv" > 
                            <div style="width:100%;" id="defaultDiv" > 
                                    
                                     {if $def==1}
                                    <p style="text-align:center;" >
                                        
                                         <img src="{$_subdomain}/css/Skins/{$_theme}/images/viamente-skyline.png" width="560" height="278" style="margin:0px; padding:0px;" >
                                    </p>
                                    
                                    <p style="margin-left:0px;text-align:center;padding:0px 100px 0px 100px;font-size:14px; line-height:21px; font-weight:normal; font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif; color:#737373;" >
                                        <span style="color:#336699;padding-left:20px;" >{$page['Text']['default_text1']|escape:'html'} </span>{$page['Text']['default_text2']|escape:'html'}

                                    </p>
        {/if}
         
        {if $def==2}
                                    <p style="text-align:center;" >
                                       
                                         <img src="{$_subdomain}/images/diary/skyline_diary.png" width="570"  style="margin:0px; padding:0px;" >
                                    </p>
                                    
                                    <p >
                                        <img src="{$_subdomain}/images/diary/skyline_diary_circles.png" width="570"   style="margin:0px; padding:0px;" >
                                         
                                    </p>
        {/if}
        
        {if $def==0}
                                    <p style="text-align:center;" >
                                       
                                         <img src="{$_subdomain}/images/diary/samsung_logo.png"   style="margin:0px; padding:0px;float:left" >
                                    </p>
                                    
                                    <p >
                                        <img src="{$_subdomain}/images/diary/one_touch_booking.png" width="565"   style="margin-top:90px; padding:0px; margin-bottom:90px;" >
                                         
                                    </p>
                                    <p >
                                        <img src="{$_subdomain}/images/diary/samsung_logo_standard.png"   style="margin:0px; padding:0px;float:right" >
                                         
                                    </p>
        {/if}
        


                                    <div id="centerInfoText" style="diplay:none;padding-top:20px;" class="centerInfoText" ></div>
                            
                            </div>
                                
                                
                                 
                                
                                
                            <div style="width:100%;display:none;" id="formDiv" > 
                                 
                                {if $datarow.RepairSkillID !=''}
                                    
                                    <h3 id="SiteMapPageHeader"  >{$page['Text']['skillset_update_page']|escape:'html'}</h3> 
                                   
                                 
                                {else}
                                         
                                    <h3 id="SiteMapPageHeader"  >{$page['Text']['skillset_insert_page']|escape:'html'}</h3>
                                    
                                {/if}   
                                   
                                 <div id="errorCenterInfoText" style="diplay:none;padding-top:20px;" class="centerInfoText" ></div>
                            
                                 
                                
                                <p id="RepairSkillIDElement" >
                                  <label class="fieldLabel" for="RepairSkillID" >{$page['Labels']['skill_type']|escape:'html'}:</label>
                                   
                                  &nbsp;&nbsp;
                                  <select name="RepairSkillID" id="RepairSkillID" >
                                      
                                      <option value="">{$page['Text']['select_default']|escape:'html'}</option>
                                      
                                      {foreach from=$skillSet item=ss}
                                        <option value="{$ss.RepairSkillID}" {if $datarow.RepairSkillID eq $ss.RepairSkillID}selected="selected"{/if} >{$ss.RepairSkillName|escape:'html'}</option>
                                      {/foreach}
                                  
                                  </select>
                                      
                                 <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="RepairSkillIDHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >     
                                      
                                 <span style="margin-left:180px;" >     
                                  <input type="hidden" name="HidddenRepairSkillID" id="HidddenRepairSkillID" value="" >    
                                 <span>         
                                  
                                </p>
                                <p>
                                    <span id="skcheckall1" style="display:none"><input  type="checkbox" onclick="getSkillsSet('checked');$('#skcheckall1').hide();$('#skcheckall2').show();$(this).attr('checked',false);"> Show Selected Only</span>
                                    <span id="skcheckall2"  ><input checked=checked  type="checkbox"  onclick="getSkillsSet('all');$('#skcheckall2').hide();$('#skcheckall1').show();$(this).attr('checked','checked');"> Show All</span>
                                </p>
                                <p style="text-align:center;" >
                                    <input type="hidden" name="HiddenField" id="HiddenField" value="" >
                                    <table id="AppointmentTypeTable">
                                       
                                        
                                    </table>
                                </p>
                                   
                            </div>    
                        </div>
                       
                       <div id="thirdDiv" class="firstDiv assistDiv" >
                        <input type="submit" name="addRecord" id="addRecord"  class="form-button"   value="{$page['Buttons']['add_record']|escape:'html'}" >
                       </div>
                       
                       
                       <div id="fourthDiv" class="secondDiv assistDiv" >  
                       
                           <input type="submit" name="saveRecord" id="saveRecord" style="display:none;"   class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 
                           &nbsp;&nbsp;
                           <input type="submit" name="cancelChanges" id="cancelChanges" style="display:none;"  class="form-button"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                           
                           <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                       
                       
                           {if $DiaryWizardSetup}
                                      <p style="text-align:center;position:relative;" id="navigationButtons" >
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                            {/if}
                           
                       </div>
                           
                           
                   </fieldset> 

        
        </form>
    </div>  
                                    
</div>
{/block}
