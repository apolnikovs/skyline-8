{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Skyline  Diary"}
{$PageId = 289}

    


{/block}
{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
{/block}

{block name=scripts}

     
        <link rel="stylesheet" href="{$_subdomain}/css/contextMenu/jquery.contextMenu.css" type="text/css" charset="utf-8" /> 
       
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery.ui.timepicker.css?v=0.3.1" type="text/css" />
        <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
        <script type="text/javascript" src="{$_subdomain}/js/jquery.contextMenu.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jspostcode.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.Editable.js"></script>
       
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.timepicker.js?v=0.3.1"></script>

<script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>


<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
<link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.full.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.extensions.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/date.js"></script>

   
<style type="text/css" >
      
    .ui-combobox-input {
         width:270px;
        
     }  
     
     #LunchBreakDurationElement input {
         width:235px;
        
     }
         
</style>


<script>
    var oTable;
    var selectedRow;
    $(document).ready(function() {
    /* Init the table */
	oTable = $('#holiday_table').dataTable( {
"sDom": '<"left"lr><"top"f>t<"bottom"><"centered"pi><"clear">',
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 10, 15, 25, 50, 100 , -1], [10, 15, 25, 50, 100, "All"]],
"iDisplayLength" : 10,
"bPaginate": true,
"aoColumns": [ 
			
			
			           
			  { "bVisible":    false },
			 
			    null,
                            null,
                            null,
                        
                            null,
                            null,
                         
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                            
                            
                            
                            
		] ,
                "aaSorting": [ [3,'asc'], [4,'asc'] ]
,
"sEmptyTable": "There are no records"
}

);
    
    /* Add a dblclick handler to the rows - this could be used as a callback */
	$("#holiday_table2 tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
      $.post("{$_subdomain}/Diary/showHolidayCard",{ update:aData[0] },
function(data) {

$.colorbox({ html:data, title:"Edit Engineers Holidays & Special Appointments",escKey: false,
    overlayClose: false
,onComplete:function(data){
		
		$('#hStartTime').timepicker();
		$('#hStartDate').datepicker( { dateFormat: "dd/mm/yy"});
		$('#hEndTime').timepicker();
		$('#hEndDate').datepicker( { dateFormat: "dd/mm/yy"});
	
    
    }

});


})
    }
    
  
        });
        /* Add a click handler for the edit row */
	$('#hEdittBtn').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
      $.post("{$_subdomain}/Diary/showHolidayCard",{ update:aData[0] },
function(data) {

$.colorbox({ html:data, title:"Edit Engineers Holidays & Special Appointments",escKey: false,
    overlayClose: false
,onComplete:function(data){
		
		$('#hStartTime').timepicker();
		$('#hStartDate').datepicker( { dateFormat: "dd/mm/yy"});
		$('#hEndTime').timepicker();
		$('#hEndDate').datepicker( { dateFormat: "dd/mm/yy"});
	
    
    }

});


})
    }
    
  
        });
        });
        
        
$(document).ready(function() {  
          /* Add a insert button click handler  */
	$("#hInsertBtn2").click(function() {
		
	
    
      $.post("{$_subdomain}/Diary/showHolidayCard",{ o:false },
function(data) {

$.colorbox({ html:data, title:"Edit Engineers Holidays & Special Appointments",escKey: false,
    overlayClose: false
,onComplete:function(data){
		
		$('#hStartTime').timepicker();
		$('#hStartDate').datepicker( { dateFormat: "dd/mm/yy"});
		$('#hEndTime').timepicker();
		$('#hEndDate').datepicker( { dateFormat: "dd/mm/yy"});
	
    
    }

});

}
)
    
                
	});
        /* Add a click handler to the rows - this could be used as a callback */
	$("#holiday_table tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
     $('#fDateFilter2').datepicker( 
     {
              dateFormat: "dd/mm/yy",
                       showOn: 'both',
			buttonImage: "{$_subdomain}/css/Base/images/calendar.png",
			buttonImageOnly: true,
                        buttonText:"Filter by date",
                        onClose: function(dateText, inst) { 
                        
                       
                        
                        },
                        onSelect: function(dateText, inst) { 
                        filterTable(dateText);

          }
          }
     
     );
        
        });
        /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}
function deleteConfirmation(){
 var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                $('#selectedID').val(aData[0]);
                
if (anSelected!="")  // null if we clicked on title row
    {
$.colorbox({ html:$('#hDeleteMsgDiv').html(), title:"Confirmation",escKey: false,
    overlayClose: false
,onComplete:function(data){
	
    
    }

});
}else{
alert("Please select a appointment to delete.");
}

}

$(document).ready(function() 
{ 
 $("img[class='ui-datepicker-trigger']").each(function()
 { 
  $(this).attr('style', 'position:relative;float:right; top:3px; left:3px;');
 });
}); 

function filterTable(name){
$('input[type="text"]','#holiday_table_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#holiday_table_filter').trigger(e);


}
function showProcImg(){
$.colorbox({ html:$('#waitDiv2').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();
}
var aidArray="";

function addSelectedApp(aid){

n=aidArray.indexOf(aid+",");

if(n>-1){
 aidArray=aidArray.replace(aid+",",'');
}else{
aidArray=aidArray+aid+",";
}
//console.log(aidArray);
}
function markAsConfirmed(){
$.post("{$_subdomain}/Diary/markAsConfirmed",{ arr:aidArray },
function(data) {


location.reload(true);
});
}

</script>
{/block}




{block name=body}

   <div class="main" id="home">
       <div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/AppointmentDiary">Defaults</a> / Appointment Table
    </div>
    
</div>
         <fieldset style="padding:0xp;">
            <legend >Samsung One Touch appointments</legend>
            <input type="hidden" name="selectedID" value="" id="selectedID">
            <input type="hidden" name="fDateFilter" value="" id="fDateFilter" style="float:right;position: relative" onchange="">
    <table id="holiday_table" border="0" cellpadding="0" cellspacing="0" class="browse" >
                 
                 <thead>
                     <tr>
                <th class="diaryDataTable" style="background-position:right bottom;">ID</th>
                <th class="diaryDataTable" style="background-position:right bottom;">SO</th>
                <th class="diaryDataTable" style="background-position:right bottom;">TIME SLOT</th>
                <th class="diaryDataTable" style="background-position:right bottom;">DATE</th>
                <th class="diaryDataTable" style="background-position:right bottom;">POSTCODE</th>
                <th class="diaryDataTable" style="background-position:right bottom;">CUSTOMER NAME</th>
                <th  class="diaryDataTable" style="background-position:right bottom;">MANUFACTURER</th>
                <th  class="diaryDataTable" style="background-position:right bottom;">PRODUCT TYPE</th>
                <th  class="diaryDataTable" style="background-position:right bottom;">APPOINTMENT TYPE</th>
                <th  class="diaryDataTable" style="background-position:right bottom;">S/O NO.</th>
                <th  class="diaryDataTable" style="background-position:right bottom;">STATUS</th>
                <th  class="diaryDataTable" style="background-position:right bottom;">TAG</th>
                
               
                </tr>
                </thead>
                <tbody>
                   {for $e=0 to $data|@count-1}
                   <tr>
                       <td style="font-size:10px;">{$data[$e]['AppointmentID']}</td>
                       <td style="font-size:10px;">{$e+1}</td>
                       <td style="font-size:10px;">{$data[$e]['AppointmentTime']}</td>
                       <td style="font-size:10px;width:70px">{$data[$e]['AppointmentDate']}</td>
                       <td style="font-size:10px;" title=" {$data[$e]['fulladress']}">{$data[$e]['PostalCode']}</td>
                       <td style="font-size:10px;">{$data[$e]['custName']}</td>
                       <td style="font-size:10px;">Samsung</td>
                       <td style="font-size:10px;">{$data[$e]['ServiceBaseUnitType']}</td>
                       <td style="font-size:10px;">{$data[$e]['SkillsetName']}</td>
                       <td style="font-size:10px;">{$data[$e]['NetworkRefNo']}</td>
                       <td style="font-size:10px;{if $data[$e]['ConfirmedByUser']=="Confirmed"}background-color:green;color:white;{/if}" >{$data[$e]['ConfirmedByUser']}</td>
                       <td style="font-size:10px;"><input type="checkbox" onclick="addSelectedApp({$data[$e]['AppointmentID']});"></td>
                   </tr>
                   
                   {/for}
                        
                       
                 
                </tbody>
             </table>
            <div id="holidayButtons">
                <button type="button" id="hInsertBtn" onclick="showProcImg();markAsConfirmed();" class="btnStandard" style="float:none;color:white;"><span style="color:white">Confirm Appointment</span></button>
                
            </div>
         </fieldset>
    </div>
                  
    
    <div id="waitDiv2" style="display:none">
     <div style="min-height:100px;min-width: 100px;">
        <img src="{$_subdomain}/images/processing.gif">
        <div style="text-align: center">Please wait!</div></div> </div>   
        
{/block}
