{extends "DemoLayout.tpl"}

{block name=config}
{$Title = $page['Text']['page_title']|escape:'html'}
{$PageId = $BookingPerformancePage}
{$fullscreen=true}
{/block}


{block name=afterJqueryUI}
   
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
                width:199px;
            }
    </style>    
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>

   
   
{/block}



{block name=scripts}

       
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <script type="text/javascript">
      
      
      
      $(document).ready(function() {
      //check display tickbox written by srinivas 
      $dAll='';
      $dispAll =  $('input[name=displayAll]:checkbox:checked').val();
      if($dispAll != undefined) {                                
                             $dAll='dispAll='+$dispAll+'/';
                              }
      //created by Soubhik for making work of display Cancelled Jobs
      $dCancel='';
      $dispCancel =  $('input[name=displayCancelled]:checkbox:checked').val();
      if($dispCancel != undefined) {                                
                             $dCancel='displayCancelled='+$dispCancel+'/';
                              }
      //created by Soubhik for making work of Refresh Status
      $(document).on('click', "#RefreshStatusPreferences", function(event) { 
            event.preventDefault();
            window.location.href = '{$_subdomain}/Job/bookingPerformance/sType=0';
	});
      //created by Soubhik for opening popup for System Status Preferences
      $(document).on('click', "#StatusPreferences", function(event) { 
            event.preventDefault();
	    //It opens color box popup page.              
	    $.colorbox({   
		inline:		true,
		href:		"#DivStatusPreferences",
		title:		'',
		opacity:	0.75,
		height:		940,
		width:		720,
		overlayClose:	false,
		escKey:		false,
		onLoad: function() {
		    //$('#cboxClose').remove();
		},
		onClosed: function() {
		    //location.href = "#EQ7";
		},
		onComplete: function() {
		    $.colorbox.resize();
		}
	    }); 
	});
        //created by Soubhik for making work of saving Preferences
        $(document).on('click', "#insert_save_btn", function() { 
	    $("#insert_save_btn").hide();
	    $("#cancel_btn").hide();
	    $("#processDisplayText").show();
	    
	    $.post("{$_subdomain}/Data/updateUserStatusPreferences/Page=bookingPerformance/Type=js",        
		    $("#StatusPreferencesForm").serialize(), 
		    function(data) {
			//var p = eval("(" + data + ")");
			document.location.href = "{$_subdomain}/Job/bookingPerformance";
		    }
	    ); //Post ends here...

	    return false;

       });
       //created by Soubhik for making work of display all Tick Box
       $('#StatusTagAll').click(function(){
            tagcheckbox($(this));
        });
        $('#StatusDisplaySelected').click(function(){
            selectedcheckbox($(this));
        });
        $('#StatusClearAll').click(function(){
            clearcheckbox($(this));
        });
        
        clearcheckbox = function(idobj){
            var elmnt = idobj.attr('id').replace("ClearAll","");
            $(("#"+elmnt+"DisplaySelected")).removeAttr("checked");
            $('.'+elmnt+'CheckBox').each(function () {
		$(this).removeAttr("checked"); 
                $(("#"+elmnt+"Priority"+$(this).val())).hide();
                $(this).parent().show();
	    });
	    return false;
        };
        
        tagcheckbox = function(idobj){
            var elmnt = idobj.attr('id').replace("TagAll","");
            $('.'+elmnt+'CheckBox').each(function () {
		$(this).attr("checked", true); 
		$(("#"+elmnt+"Priority"+$(this).val())).show();
		$(this).parent().show();
	    });
	    return false;
        };
        
        selectedcheckbox = function(idobj){
            var elmnt = idobj.attr('id').replace("DisplaySelected","");
            if(idobj.is(':checked')) {
		$('.'+elmnt+'CheckBox').each(function() {
		    if(!this.checked) {
			$(this).parent().hide(); 
		    }
		 });
	    } else {
		$('.'+elmnt+'CheckBox').each(function() {
		    $(this).parent().show(); 
		});
	    }
        };
      //End of code
                $(document).on('click', '.cvt', 
                        function() {
                        
                            if($(this).val()=="dr")
                            {
                                $("#dateRangeBoxes").show();
                            } 
                            else
                            {
                                
                                $("#dateRangeBoxes").hide();
                                
                                $cdt = 'd';
                                if($(this).val()=="ytd")
                                {
                                    $cdt = 'm';    
                                }
                                else if($(this).val()=="lm")
                                {
                                    $cdt = 'w';    
                                }
                            //check display tickbox written by srinivas
                            $dispAll =  $('input[name=displayAll]:checkbox:checked').val();
                             if($dispAll === undefined) {
                                document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/cvt='+$(this).val()+'/cdt='+$cdt+'/';
                              }else{
                                document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/cvt='+$(this).val()+'/cdt='+$cdt+'/'+'dispAll='+$dispAll+'/';
                              }
                                
                            }
                        
                            
                        }
                );
                
                
                
                $(document).on('click', '.cdt', 
                        function() {
                        
                                $cvt           =  $('input[name=cvt]:radio:checked').val();
                                
                                $date_from     =  $("#date_from").val();
                                
                                if($date_from)
                                {
                                     $dateArray    = $date_from.split("/");
                                     
                                     $date_from = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                                
                                $date_to = $("#date_to").val();
                                
                                if($date_to)
                                {
                                     $dateArray    = $date_to.split("/");
                                     
                                     $date_to = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                                
                                 //check display tickbox written by srinivas
                            $dispAll =  $('input[name=displayAll]:checkbox:checked').val();
                             if($dispAll === undefined) {
                                 document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/cvt='+$cvt+'/date_from='+urlencode($date_from)+'/date_to='+urlencode($date_to)+'/cdt='+$(this).val()+'/';
                              }else{
                                 document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/cvt='+$cvt+'/date_from='+urlencode($date_from)+'/date_to='+urlencode($date_to)+'/cdt='+$(this).val()+'/'+'dispAll='+$dispAll+'/';
                              }
                               
                            
                        }
                );
                
                
                $(document).on('click', '#dr_go', 
                        function() {
                        
                                $cvt =  $('input[name=cvt]:radio:checked').val();
                                $cdt =  $('input[name=cdt]:radio:checked').val();
                                
                                
                                $date_from     =  $("#date_from").val();
                                
                                if($date_from)
                                {
                                     $dateArray    = $date_from.split("/");
                                     
                                     $date_from = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                                
                                $date_to = $("#date_to").val();
                                
                                if($date_to)
                                {
                                     $dateArray    = $date_to.split("/");
                                     
                                     $date_to = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                                
                                 //check display tickbox written by srinivas
                            $dispAll =  $('input[name=displayAll]:checkbox:checked').val();
                             if($dispAll === undefined) {
                                  document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/cvt='+$cvt+'/date_from='+urlencode($date_from)+'/date_to='+urlencode($date_to)+'/cdt='+$cdt+'/';
                              }else{
                                  document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/cvt='+$cvt+'/date_from='+urlencode($date_from)+'/date_to='+urlencode($date_to)+'/cdt='+$cdt+'/'+'dispAll='+$dispAll+'/';
                              } 
                               
                            
                                return false;
                        }
                );
      
      //created by Soubhik for making work of display all Tick Box
       $(document).on('click', '#displayAll', 
                        function() {
                         $cvt           =  $('input[name=cvt]:radio:checked').val();
                         $cdt =  $('input[name=cdt]:radio:checked').val();       
                         if($cvt=='dr'){
                                $date_from     =  $("#date_from").val();
                                
                                if($date_from)
                                {
                                     $dateArray    = $date_from.split("/");
                                     
                                     $date_from = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                                
                                $date_to = $("#date_to").val();
                                
                                if($date_to)
                                {
                                     $dateArray    = $date_to.split("/");
                                     
                                     $date_to = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                               $appendDate='/cvt='+$cvt+'/date_from='+urlencode($date_from)+'/date_to='+urlencode($date_to)+'/cdt='+$cdt+'/'; 
                            }
                            else
                            {
                                $appendDate='/cvt='+$cvt+'/cdt='+$cdt+'/'; 
                            }
                            //check display tickbox written by srinivas
                            $dispAll =  $('input[name=displayAll]:checkbox:checked').val();
                             if($dispAll === undefined) {
                                 document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+$appendDate+'/'+$dCancel;
                              }else{
                                 document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+$appendDate+'dispAll='+$dispAll+'/'+$dCancel+'/';
                              }
                         }     
                );
      //end of Display all click
      
      //created by Soubhik for making work include cancelled jobs
      $(document).on('click', '#displayCancelled', 
                        function() {
                         $cvt           =  $('input[name=cvt]:radio:checked').val();
                         $cdt =  $('input[name=cdt]:radio:checked').val();       
                         if($cvt=='dr'){
                                $date_from     =  $("#date_from").val();
                                
                                if($date_from)
                                {
                                     $dateArray    = $date_from.split("/");
                                     
                                     $date_from = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                                
                                $date_to = $("#date_to").val();
                                
                                if($date_to)
                                {
                                     $dateArray    = $date_to.split("/");
                                     
                                     $date_to = $dateArray[2]+'-'+$dateArray[1]+'-'+$dateArray[0];
                                }
                               $appendDate='/cvt='+$cvt+'/date_from='+urlencode($date_from)+'/date_to='+urlencode($date_to)+'/cdt='+$cdt+'/'; 
                            }
                            else
                            {
                                $appendDate='/cvt='+$cvt+'/cdt='+$cdt+'/'; 
                            }
                            //check display tickbox written by srinivas
                            $dispAll =  $('input[name=displayAll]:checkbox:checked').val();
                            $displayCancelled =  $('input[name=displayCancelled]:checkbox:checked').val();
                             if($displayCancelled === undefined) {
                                 document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+$appendDate+'/'+$dAll;
                              }else{
                                 document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+$appendDate+'dispAll='+$dispAll+'/displayCancelled='+$displayCancelled+'/';
                              }
                         }     
                );
        //end of Include Cancelled Jobs
      
                $( "#date_from" ).datepicker({
			dateFormat: "dd/mm/yy",
                        showOn: "button",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        maxDate: '0',
                        changeMonth: true,
                        changeYear: true,
                        onClose: function(dateText, inst) { 
                        
                        if($("#date_from").val()!="dd/mm/yyyy")
                        {    
                            $("#date_from").removeClass("auto-hint"); 
                        }
                        
                        },
                        onSelect: function(dateText, inst) { 
                        
                             
                        }
                    });  
                    
                    
                     $( "#date_to" ).datepicker({
			dateFormat: "dd/mm/yy",
                        showOn: "button",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        maxDate: '0',
                        changeMonth: true,
                        changeYear: true,
                        onClose: function(dateText, inst) { 
                        
                        if($("#date_from").val()!="dd/mm/yyyy")
                        {    
                            $("#date_from").removeClass("auto-hint"); 
                        }
                        
                        },
                        onSelect: function(dateText, inst) { 
                        
                             
                        }
                    });
      
   
                {if $SuperAdmin}
            
                $("#jncNetworkID").combobox({ change: function() { 
                
                    document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/';
            
                } });
                
                {/if}
                
                {if $SuperAdmin or $NetworkUser}
                    
                    $("#jncClientID").combobox({ change: function() { 

                        document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/';

                    } });
                    
                {/if}
                
                {if $SuperAdmin or $NetworkUser or $ClientUser}
                $("#jncBranchID").combobox({ change: function() { 
                
                   document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/';
            
                } });
                {/if}
                
                
                $("#jncBrandID").combobox({ change: function() { 
                
                  document.location.href = '{$_subdomain}/Job/bookingPerformance/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/';
            
                } });
                
                
               
               //created srinvas for custom sorting order for makeing total row at end of the page
               jQuery.fn.dataTableExt.oSort['custom-asc']  = function(x,y) {      
                if (x.indexOf("<b>") != -1) return 0; // keep this row at top
                    if (y.indexOf("<b>") != -1) return 0; // keep this row at top
                    x=parseInt(x);y=parseInt(y);
                    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
                };

                jQuery.fn.dataTableExt.oSort['custom-desc'] = function(x,y) {      
                if (x.indexOf("<b>") != -1) return 0; // keep this row at top
                    if (y.indexOf("<b>") != -1) return 0; // keep this row at top
                    x=parseInt(x);y=parseInt(y);    
                    return ((x < y) ?  1 : ((x > y) ? -1 : 0));
                };
               //custom sorting 2
                              jQuery.fn.dataTableExt.oSort['custom2-asc']  = function(x,y) {      
                if (x.indexOf("<b>") != -1) return 0; // keep this row at top
                    if (y.indexOf("<b>") != -1) return 0; // keep this row at top                   
                    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
                };

                jQuery.fn.dataTableExt.oSort['custom2-desc'] = function(x,y) {      
                if (x.indexOf("<b>") != -1) return 0; // keep this row at top
                    if (y.indexOf("<b>") != -1) return 0; // keep this row at top                    
                    return ((x < y) ?  1 : ((x > y) ? -1 : 0));
                };
               // end 
               //Booking Performance data table starts here...   
                $('#bookingPerformanceResults').PCCSDataTable( {

                        displayButtons: "",
                        bServerSide: false,
                        htmlTablePageId:    'bookingPerformanceResultsPanel',
                        htmlTableId:        'bookingPerformanceResults',
                        fetchDataUrl:       '{$_subdomain}/Job/bookingPerformanceResult/NetworkID='+$('#jncNetworkID').val()+'/ClientID='+$('#jncClientID').val()+'/BranchID='+$('#jncBranchID').val()+'/BrandID='+$('#jncBrandID').val()+'/cvt='+$('input[name=cvt]:radio:checked').val()+'/date_from='+$('#date_from').val().replace('/','-')+'/date_to='+$('#date_to').val().replace('/','-')+'/cdt='+$('input[name=cdt]:radio:checked').val()+'/'+$dAll+'/'+$dCancel,
                        searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                        colorboxForceClose:false,
                        hidePaginationNorows:  true,
                        sDom: 'Rt<"#dataTables_command">rpli',                                         
                        iDisplayLength : 50,
                        aaSorting: [[ 1, "desc" ]],
                        aoColumns: [
			{ bVisible: false, bSortable:false },
                        { "sType": "custom2"}, //{ bSortable:false },
                        { "sType": "custom" },            
                        { "sType": "custom" },  
                       { "sType": "custom" },  
                       { "sType": "custom" },  
                       { "sType": "custom" },              
                       { "sType": "custom" }  
                       
                        ]

                    });
               //Booking Performance data table ends here...  
               
               
               setTimeout(function () { location.reload(1); }, 300000);
       
      });
      
      
      
      //Booking Performance chart starts here..
      google.load("visualization", "1", { packages:["corechart"] });
      google.setOnLoadCallback(drawChart);
      function drawChart() 
      {
               var data = new google.visualization.DataTable();
               var gridLines = 0;
               
               var $statsResult = [
                    {foreach from=$statsResult item=sname}
                       ["{$sname.0}", {$sname.1}, "{$sname.2}"]{if not $sname@last},{/if} 
                    {/foreach}
                    ];



                maxrows = 0;        
                data.addColumn('string', "");
                $total_rows = 0;
                $data_rows  = new Array();
                $data_rows[0] = new Array();
                $data_rows[0][0] = '';
                
                
                $cCnt = 1;
                
                for(var $i = 0; $i < $statsResult.length; $i++) {
		
		    data.addColumn('number', $statsResult[$i][0], $statsResult[$i][2]);

		    data.addColumn({ type:'string',role:'tooltip' }); // tooltip col.

		    $total_rows += $statsResult[$i][1];

		    //$data_rows[0][$i+1] = $statsResult[$i][1];
		    $data_rows[0][$cCnt++] = $statsResult[$i][1];
		    $data_rows[0][$cCnt++] = $statsResult[$i][0];

		    if (gridLines <  $statsResult[$i][1]) {
			gridLines =  $statsResult[$i][1]+1;
			maxrows = $statsResult[$i][1];
		    }

                }
                
                                
                
                data.addRows($data_rows);

                var options = {
                
               // title: 'Total Overdue Jobs: '+$total_rows,
               'chartArea': { 
                              'width': $(window).width()-350, 
                              'height': '80%',
                              'left' : '70' 
                             },
                'legend': { 
                            'position' : 'right',
                            'textStyle' : { 'fontSize' : '10' } 
                          },
                'vAxis': { 
                    
                            title: "{$page['Text']['quantity']|escape:'html'}", 
                            titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
                            slantedText: false,
                            minValue: 0,
                            maxValue: maxrows,
                            textColor: '#ffffff',
                            gridlines: { count: gridLines } 
                        },
                
                'hAxis': { 
                
                            'title': "{$graph_title}",
                             titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
                             slantedText: false
                
                        }       
             

                };

                var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                chart.draw(data, options);
                
                 // a click handler which grabs some values then redirects the page
                google.visualization.events.addListener(chart, 'select', function() {
                // grab a few details before redirecting
                
                var selection = chart.getSelection();
                
                //var col = selection[0].column;
                
                 var col = data.getColumnId(selection[0].column);
                 
                {*
                document.location.href = '{$_subdomain}/Job/overdueJobs'+'/client='+urlencode({$client})+'/manufacturer='+urlencode({$manufacturer})+'/brand='+urlencode({$brand}) + '/ojBy=' + urlencode('{$ojBy}') + '/tatType=' + urlencode('{$tatType}') + '/btnName=' + urlencode('{$btnName}') + '/btnValue=' + urlencode('{$btnValue}') + '/btnValue2=' + urlencode('{$btnValue2}') + '/serviceProvider=' + urlencode({$serviceProvider}) +'/sType=' + col;
                *}

                });
      }
      
      //Booking Performance chart ends here..
      
     
      
</script>

{/block}

{block name=body}

<div class="breadcrumb" {if $fullscreen==true}style="width:100%"{/if} >
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        <a href="{$_subdomain}/index/siteMap" >{$page['Text']['site_map_title']|escape:'html'}</a> /
        
        {$page['Text']['page_title']|escape:'html'}
    </div>
</div>

    
    
<div class="main" id="home" {if $fullscreen==true}style="width:100%;min-width: 950px"{/if} >
                               
    
   
      <div class="bookingPerformancePanel" {if $fullscreen==true}style="min-width: 950px;margin: auto"{/if} >
         <form id="bookingPerformanceForm" name="bookingPerformanceForm" method="post"  action="#" class="inline">
       
             
            <fieldset>
            <legend title="" >{$page['Text']['page_title']|escape:'html'}</legend>
             
            <p>
            
                {$page['Text']['page_text']|escape:'html'}
                
            </p> 
             </fieldset> 

        
        </form>
                
                
    </div>   
    
                <br>
                <!-- Added two line by Soubhik -->
                <p>
		    <div id="chart_div"></div>
                    <a href="#" id="StatusPreferences" style="float:right;font-size:12px;margin-left: 25px;">{$page['Text']['status_preferences']|escape:'html'}</a>&nbsp;&nbsp;
                    <a href="#" id="RefreshStatusPreferences" style="float:right;font-size:12px; " >{$page['Text']['refresh_status']|escape:'html'}</a>&nbsp;&nbsp;
		</p>
                <!-- End of code -->
                                    
     <br><br>        
     
     <form name="bpForm" action="{$_subdomain}/Job/bookingPerformance" method="post" >
     <table cellpadding="0" cellpsacing="0">
         <tr>
             <td width="350px" >
               
                {if $SuperAdmin}

                  <p style="margin-top:20px;margin-bottom:2px;" >
                         <label for="jncNetworkID" style="width:70px;" >{$page['Text']['network']|escape:'html'}: </label>
                         &nbsp;&nbsp;
                         <select name="NetworkID" id="jncNetworkID" class="text" >
                             <option value="-1" selected="selected">{$page['Text']['all_text']|escape:'html'}</option>
                             {foreach $NetworkList as $network}
                                 <option value="{$network.NetworkID}"  {if $network.NetworkID eq $NetworkID} selected="selected" {/if} >{$network.CompanyName|escape:'html'}</option>
                             {/foreach}
                         </select>
                  </p>

                {else}

                  <input type="hidden" name="NetworkID" id="jncNetworkID"  value="{$NetworkID|escape:'html'}" >

                {/if}

                {if $SuperAdmin or $NetworkUser}
                    <p id="jncClientIDElement" style="margin-bottom:2px;" >
                         <label for="jncClientID" style="width:70px;" >{$page['Text']['client']|escape:'html'}: </label>
                         &nbsp;&nbsp;  <select name="ClientID" id="jncClientID" class="text" >

                                         <option value="-1" selected="selected">{$page['Text']['all_text']|escape:'html'}</option>

                                        {foreach $ClientList as $client}

                                             <option value="{$client.ClientID}" {if $client.ClientID eq $ClientID} selected="selected" {/if} >{$client.ClientName|escape:'html'}</option>

                                         {/foreach}
                                        </select>
                     </p>



                {else}

                      <input type="hidden" name="ClientID" id="jncClientID"  value="{$ClientID|escape:'html'}" >

                {/if}


                {if $SuperAdmin or $NetworkUser or $ClientUser}
                    <p id="jncBranchIDElement" style="margin-bottom:2px;" >
                         <label for="jncBranchID" style="width:70px;" >{$page['Text']['branch']|escape:'html'}: </label>
                         &nbsp;&nbsp;  <select name="BranchID" id="jncBranchID" class="text" >

                                         <option value="-1" selected="selected">{$page['Text']['all_text']|escape:'html'}</option>

                                        {foreach $BranchList as $branch}

                                             <option value="{$branch.BranchID}" {if $branch.BranchID eq $BranchID} selected="selected" {/if} >{$branch.BranchName|escape:'html'}</option>

                                         {/foreach}
                                        </select>
                     </p>



                {else}

                      <input type="hidden" name="BranchID" id="jncBranchID"  value="{$BranchID|escape:'html'}" >

                {/if}


                 <p id="jncBrandIDElement"  style="margin-bottom:2px;" >
                      <label for="jncBrandID" style="width:70px;" >{$page['Text']['brand']|escape:'html'}: </label>
                      &nbsp;&nbsp;  <select name="BrandID" id="jncBrandID" class="text" >

                                      <option value="-1" selected="selected">{$page['Text']['all_text']|escape:'html'}</option>

                                     {foreach $BrandList as $brand}

                                          <option value="{$brand.BrandID}" {if $brand.BrandID eq $BrandID} selected="selected" {/if}  >{$brand.BrandName|escape:'html'}</option>

                                      {/foreach}
                                     </select>
                 </p>
          
          
          
          </td> 
          <td style="width:520px;padding-right:0xp;" >
                {$page['Text']['chart_view_type']|escape:'html'}: 
                
                <input type="radio" class="cvt" name="cvt"  {if $cvt eq 'mtd'} checked="checked" {/if} value="mtd" > {$page['Text']['mtd_full']|escape:'html'}
                <input type="radio" class="cvt" name="cvt" {if $cvt eq 'lm'} checked="checked" {/if}  value="lm" > {$page['Text']['last_month']|escape:'html'}
                <input type="radio" class="cvt" name="cvt" {if $cvt eq 'ytd'} checked="checked" {/if} value="ytd" > {$page['Text']['ytd_full']|escape:'html'}
                <input type="radio" class="cvt" name="cvt" {if $cvt eq 'dr'} checked="checked" {/if}  value="dr" > {$page['Text']['date_range']|escape:'html'}
          
                 <br><br><br>
                 {$page['Text']['chart_display_type']|escape:'html'}: 
                 
                <input type="radio" class="cdt" name="cdt" {if $cdt eq 'd'} checked="checked" {/if} value="d" > {$page['Text']['daily']|escape:'html'}
                <input type="radio" class="cdt" name="cdt" {if $cdt eq 'w'} checked="checked" {/if} value="w" > {$page['Text']['weekly']|escape:'html'}
                <input type="radio" class="cdt" name="cdt" {if $cdt eq 'm'} checked="checked" {/if} value="m" > {$page['Text']['monthly']|escape:'html'}
                
          </td>
          <td>
              <span id="dateRangeBoxes" style="padding:0px;margin:0px;{if $cvt neq 'dr'}display:none;{/if}" >
                  
                <label style="width:70px;" >{$page['Text']['date_from']|escape:'html'}:</label> <input type="text" name="date_from" id="date_from" value="{$date_from|escape:'html'}" ><br>
                <label style="width:70px;" >{$page['Text']['date_to']|escape:'html'}:</label> <input type="text" name="date_to" id="date_to" value="{$date_to|escape:'html'}" >

                <input type="submit" class="btnStandard" name="dr_go" id="dr_go"  value="{$page['Buttons']['go_btn']|escape:'html'}" >
              
              </span>
          </td>
         </tr>
         
     
                               
    </table>  
                
    <div style="float:right;margin-right: 1em;font-size:1em;">
        <input type="checkbox" {if isset($dispAll) && $dispAll eq 'all'} checked="checked" {/if}value="all" name="displayAll" id="displayAll" />Display Zero Jobs<br />
        <input type="checkbox" {if isset($displayCancelled) && $displayCancelled eq 'cancelled'} checked="checked" {/if}value="cancelled" name="displayCancelled" id="displayCancelled" />Include Cancelled Jobs
    </div>      
    </form>                           
                               
    <div class="bookingPerformanceResultsPanel" >    
    <!-- label style="font-size:1em;float:right;clear: both" >{$page['Text']['live_jobs_only']|escape:'html'} ( {$page['Text']['excludes_cancelled']|escape:'html'} )</label--> 
      
    <table id="bookingPerformanceResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
        <thead>
                <tr>
                        <th></th>
                        
                        {if $BranchID and $BranchID neq '-1'}
                            
                            <th title="{$page['Text']['brand']|escape:'html'}" >{$page['Text']['brand']|escape:'html'}</th>
                            
                        {else if $ClientID and $ClientID neq '-1'}
                            
                            <th title="{$page['Text']['branch']|escape:'html'}" >{$page['Text']['branch']|escape:'html'}</th>
                            
                        {else if $NetworkID and $NetworkID neq '-1'}
                        
                            <th title="{$page['Text']['client']|escape:'html'}" >{$page['Text']['client']|escape:'html'}</th>
                            
                        {else}
                            
                            <th title="{$page['Text']['network']|escape:'html'}" >{$page['Text']['network']|escape:'html'}</th>    
                        
                        {/if}
                        
                        <th align="center" title="{$page['Text']['tdy_full']|escape:'html'}" >{$page['Text']['tdy']|escape:'html'}</th>
                        <th title="{$page['Text']['last_month']|escape:'html'}" >{$page['Text']['last_month']|escape:'html'}</th>
                        <th title="{$page['Text']['mtd_full']|escape:'html'}"  >{$page['Text']['mtd']|escape:'html'}</th>
                        <th title="{$page['Text']['ytd_full']|escape:'html'}" >{$page['Text']['ytd']|escape:'html'}</th>
                        <th title="{$page['Text']['last_4months']|escape:'html'}" >{$page['Text']['last_4months']|escape:'html'}</th>
                        <th title="{$page['Text']['total']|escape:'html'}" >{$page['Text']['total']|escape:'html'}</th>
                        
                        
                </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table>  
       
    
                     
   </div>
        
  <div style="display:none;">

         <div id="DivStatusPreferences" class="SystemAdminFormPanel">
    
	    <form id="StatusPreferencesForm" name="StatusPreferencesForm" method="post" action="#" class="inline">

                <fieldset>
		    
                    <legend title="">{$page['Text']['status_preferences']|escape:'html'}</legend>
		    
                    <p><label id="suggestText"></label></p>
		    
		    <p style="text-align:right;">
			<a id="StatusTagAll" href="#" style="text-decoration:underline">
			    {$page['Text']['tag_all']|escape:'html'}
			</a>
			&nbsp;&nbsp;
			<a id="StatusClearAll" href="#" style="text-decoration:underline">
			    {$page['Text']['clear_all']|escape:'html'}
			</a>
			&nbsp;&nbsp;
			<input type="checkbox" id="StatusDisplaySelected" name="StatusDisplaySelected" value="1" />
			&nbsp;&nbsp;
			{$page['Text']['display_selected']|escape:'html'}
			&nbsp;&nbsp;
			<br/><br/>
		    </p>

		    <div style="height:500px;overflow-y:scroll; padding:0px; margin:0px;">
			<p>     
			    {foreach $jobStatusList as $js}
				<span>
				    <input type="checkbox" style="width:30px;" class="text StatusCheckBox" name="StatusID[]" value="{$js.StatusID|escape:'html'}" {if $js.Exists eq true}checked="checked"{/if}>
				    &nbsp;&nbsp;
				    {$js.StatusName|escape:'html'}
				<br>
				</span>
			    {/foreach} 
			</p>
		    </div>    

		    <p>
			<br/><br/>
			<span class="bottomButtons">
			    <input type="submit" name="insert_save_btn" class="btnStandard" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			    &nbsp;
			    <input type="submit" name="cancel_btn" class="btnCancel" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['cancel']|escape:'html'}" />
			    <span id="processDisplayText" style="color:red;display:none;">
				<img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" />
				&nbsp;&nbsp;
				{$page['Buttons']['process_record']|escape:'html'}
			    </span>    
			</span>
		    </p>

		</fieldset>    

	    </form>        

	</div>
  </div>
    
                                    
    
                                    
</div>
{/block}
