{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $StatusPermissions}
    {/block}
    
    
    
    {block name=afterJqueryUI}
   <script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
   
    <style type="text/css" >

        .ui-combobox-input {
             width:270px;
         }  
          /* added by srinivas for enabling full page */
        .container, #header, .breadcrumb, #container-main .main  {
        width: 98%;
        min-width: 950px;                       
        }
        /* end */
    </style>

    <script type="text/javascript">
        $.fn.jPicker.defaults.images.clientPath = "{$_subdomain}/css/themes/pccs/images/colorPicker/";
    </script>
    {/block}
    
    

    {block name=scripts}



    <script type="text/javascript">
        
    var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
            
        
    function gotoEditPage($sRow)
    {
        
       if($sRow!='No matching records found') {
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       }
       
    }

    function inactiveRow(nRow, aData)
    {
        
        if (aData[3]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(3)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
                  $('td:eq(3)', nRow).html( $statuses[0][0] );
        }
    }
    
  

    $(document).ready(function() {
        
           $("#PermissionType").combobox({
            change: function() {
                if($("#PermissionType").val() != "") {
                    $(location).attr('href', '{$_subdomain}/LookupTables/systemStatusPermissions/'+urlencode($("#PermissionType").val()));
                }
            }
        }); 

                

                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');

                                });







                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                    
                    $('#APResults').PCCSDataTable( {
                        "sDom" : 'Rft<"#dataTables_command">rpli', 
                        "bServerSide": false,        
			"aoColumns": [ 
			     { 'mDataProp':0, 'bVisible': false },  
                             { 'mDataProp':1},
                             { 'mDataProp':2, "sWidth" :  "200px"},			    
                             { "mData" : null, 
                            "mRender": function ( data, type, full ) {
                            return '{$permissionType|capitalize}';
                             }
                          },
                         { 'mDataProp':3}
                           
			],
                            
                            "aaSorting": [[ 1, "asc" ]],
                            
                            
                            
                               
                            displayButtons:  "U",
                            
                            frmErrorRules:   {
                                            
                                                    Name:
                                                        {
                                                            required: true
                                                        }, 
                                                    Description:
                                                        {
                                                            required: true
                                                        }
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                   
                                                    Name:
                                                        {
                                                            required: "{$page['Errors']['name_error']|escape:'html'}"
                                                        }, 
                                                    Description:
                                                        {
                                                            required: "{$page['Errors']['description_error']|escape:'html'}"
                                                        }, 
                                                    Status:
                                                        {
                                                            required: "{$page['Errors']['status_error']|escape:'html'}"
                                                        }    
                                              },                     
                            
                            popUpFormWidth:  715,
                            popUpFormHeight: 330,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/LookupTables/systemStatusPermissionForm/{$permissionType}/update/',
                            updateDataUrl:   '{$_subdomain}/LookupTables/ProcessData/StatusPermission/',
                            formUpdateButton:'update_save_btn',
                            
                            colorboxFormId:  "SSPForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'APResultsPanel',
                            htmlTableId:     'APResults',
                            fetchDataUrl:    '{$_subdomain}/LookupTables/ProcessData/StatusPermission/fetch/{$permissionType}/',
                            formCancelButton:'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            //fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  50,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
                   
    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="LTTopPanel" >
                    <form id="accessPermissionsForm" name="accessPermissionsForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['page_title']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="LTResultsPanel" id="APResultsPanel" >
                    
                    Select Permission Type :
                    <select id="PermissionType" name="PermissionType">
                        {foreach $permissionTypeList key=pk item=pi}
                            <option value="{$pk}"{if $permissionType == $pk} selected='selected'{/if}>{$pi}</option>
                        {/foreach}
                    </select>

                    <table id="APResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th></th>
                                        <th width="30%" title="entity" >{$page['Text'][$permissionType]|escape:'html'}</th>
                                        <th title="desc" >{$page['Text'][$permissionTypeDesc]|escape:'html'}</th>
                                        <th title="type" >Permission Type</th>
                                        <th title="type" >Assigned Statuses</th>
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                                
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


    </div>
                        
                        



{/block}



