{extends "WallboardLayout.tpl"}

{block name=config}
{$Title = "$Title - Wall Board"}
{$PageId = $wallboardPage}
{/block}

{block name=scripts }
  <script type="text/javascript" charset="utf-8">
      $( document ).ready( function() {
        var $body = $('body'); //Cache this for performance
        
        var setBodyScale = function() {
          var scaleFactor = 0.15,
            scaleSource = $body.width(),
            maxScale = 600,
            minScale = 30;

          var fontSize = scaleSource * scaleFactor; //Multiply the width of the body by the scaling factor:

          if (fontSize > maxScale) fontSize = maxScale;
          if (fontSize < minScale) fontSize = minScale; //Enforce the minimum and maximums

          $('body').css('font-size', fontSize + '%');
        }
      
          $(window).resize(function(){
          setBodyScale();
        });
        
        //Fire it when the page first loads:
        setBodyScale();
      });
      $(document).ready(function() {

            setInterval("location.reload(true)", 60000);

        });   
    </script>
{/block}

{block name=body}


<table width=100% id="wall_board" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
<tbody>

<tr style="background-color:#064DA2; color:#FFF; text-align:center;">
{foreach from=$data key=k item=v  name=foo}
    {if $smarty.foreach.foo.index == 6}
    {break}
  {/if}
<th height="1%"><h5 class="fittext1">{$k|date_format:"%a %e %b "}</h5></th>
 
{/foreach}
</tr>
<tr style="background-color:#50423A; color:#FFF; text-align:center;font-size:170%;">
  {foreach from=$data key=k item=v  name=foo}
    {if $smarty.foreach.foo.index == 6}
    {break}
  {/if}
  {$total[$k]=0}
  {$totalE[$k]=0}
  
<td><h1 class="fittext1" class="black_shadow">{if isset($data[$k]['BROWN GOODS'].apCountType)}{$data[$k]['BROWN GOODS'].apCountType}{$brown[$k]=$data[$k]['BROWN GOODS'].apCountType}{$total[$k]=$data[$k]['BROWN GOODS'].apCountTotal}{$totalE[$k]=$data[$k]['BROWN GOODS'].totalEngineers}{else}{$brown[$k]=0}0{/if}</h1></td>
{/foreach}
</tr>
<tr style="background-color:#e3e3e3; color:#666; text-align:center;font-size:170%;">
    {foreach from=$data key=k item=v  name=foo}
    {if $smarty.foreach.foo.index == 6}
    {break}
  {/if}
<td><h1 class="fittext1">{if isset($data[$k]['WHITE GOODS'].apCountType)}{$data[$k]['WHITE GOODS'].apCountType}{$white[$k]=$data[$k]['WHITE GOODS'].apCountType}{$total[$k]=$data[$k]['WHITE GOODS'].apCountTotal}{$totalE[$k]=$data[$k]['WHITE GOODS'].totalEngineers}{else}{$white[$k]=0}0{/if}</h1></td>
{/foreach}

</tr>
<tr style="background-color:#a2a2a2; color:#666; text-align:center;font-size:170%;">
{foreach from=$data key=k item=v  name=foo}
    {if $smarty.foreach.foo.index == 6}
    {break}
  {/if}
<td><h1 class="fittext1">{if isset($total)}{$total[$k]-$brown[$k]-$white[$k]}{else}0{/if}</h1></td>
{/foreach}
</tr>
<tr style="height:20px; background-color:#FFF; border-top:1px solid black; border-bottom:1px solid black;"></tr>
<tr style="background-color:#064DA2; color:#FFF; text-align:center; height:20%;">
    

    
   {foreach from=$data  key=k item=v  name=foo}
    {if $smarty.foreach.foo.index == 6}
    {break}
  {/if}
    
  <td>

  
  

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" frame="VOID" rules="ALL" style="font-family: Arial,Helmet,Freesans,sans-serif;border-collapse: collapse;
    border-spacing:0; margin:0px; padding:0px;">
  <tbody>
  <tr>
    <td width="80%" colspan="2"><h1 class="fittext1" style="margin: 0px; padding: 0px; text-align: center;font-size:350% ">{if isset($total[$k])}{$total[$k]}{else}0{/if}</h1>
    </td>
  </tr>
   <tr>
     <td align="center" valign="bottom" style="text-align: center; border-bottom:none; border-right:1px solid white; padding-top:5%;">
          <h2  style="margin: 0px; padding: 0px; text-align: center; ">{if isset($totalE[$k])}{$totalE[$k]}{else}0{/if}</h2>  
   </td>
   <td align="center" valign="bottom" style="text-align: center;  border-right:1px solid white; border-bottom:none; padding-top:5%;">
   <h3  style="margin: 0px; padding: 0px; text-align: center; font-size:150%; ">{if isset($totalE[$k])&&$totalE[$k]>0}{($total[$k] / $totalE[$k])|string_format:"%.1f"}{else}0{/if}</h3>
  </td>
   </tr>
   <tr>
     <td  valign="bottom" style="border-right:1px solid white; border-bottom:none;">
       <h6 style="margin:0px; padding:0px;">ENG</h6>   
     </td>
     <td  valign="bottom" style="border-right:1px solid white; border-bottom:none;">
      <h6 style="margin:0px; padding:0px;">AVG</h6>
     </td>
   </tr>   
</tbody></table>

  
  
    
</td>

  {/foreach}
</tr>
</tbody>
</table>


{/block}