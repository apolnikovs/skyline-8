{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $JobFaultCodesPage}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
    <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
    <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />

    <style type="text/css" >
        .ui-combobox-input {
            width:280px;
        }
    </style>
{/block}
{block name=scripts}
<script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
<script type="text/javascript">
var $statuses = [
    {foreach from=$statuses item=st}
       ["{$st.Name}", "{$st.Code}"],
    {/foreach}
];
function gotoEditPage($sRow)
{
    $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
    $('#updateButtonId').trigger('click');  
}
function showTablePreferences(){
    $.colorbox({
        href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=jobFaultCodes/table=job_fault_code",
        title: "Table Display Preferences",
        opacity: 0.75,
        overlayClose: false,
        escKey: false
    });
}
$(document).ready(function() {
    $("#serviceProviderFilter").combobox({
        change: function() {
            $(location).attr('href', '{$_subdomain}/LookupTables/jobFaultCodes/'+urlencode($("#serviceProviderFilter").val()));
        }
    });
    var displayButtons = "UAD";
    $('#JobFaultCodesResults').PCCSDataTable( {
        aoColumns: [ 
            /* JobFaultCodeID */{ bVisible:   false },
            /* JobFaultCode */null,
            /* FieldNo */null,
            /* FieldType */null,
            /* NonFaultCode */null,
            /* MainInFault */null,
            /* MainOutFault */null,
            /* Status */null,
              { "mData" : null, "bSortable": false, "sWidth" : "20px",
                            "mRender": function ( data, type, full ) {
                            return '<input class="taggedRec" type="checkbox" onclick="countTagged()" value="'+full.CountyID+'" name="check'+full.CountyID+'">';
                             }
                             }
        ],
        displayButtons:	displayButtons,
        bServerSide: false,
        addButtonId:	'addButtonId',
        addButtonText:	'{$page['Buttons']['insert']|escape:'html'}',
        createFormTitle:	'{$page['Text']['insert_page_legend']|escape:'html'}',
        createAppUrl:	'{$_subdomain}/LookupTables/jobFaultCodes/insert/',
        createDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodes/',
        formInsertButton:	'insert_save_btn',
        deleteButtonId:	"deleteButtonID",
        aaSorting: [[ 1, "asc" ]],
        deleteAppUrl:	'{$_subdomain}/LookupTables/jobFaultCodes/deleteForm/',
        deleteDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodes/delete/',
        formDeleteButton:	'delete_save_btn',
        frmErrorRules: {
            JobFaultCode: { required: true },
            {if $SupderAdmin eq true}
                ServiceProviderID: { required: true },
            {/if}
            FieldNo:{ 
                    required: true, 
                    digits: true 
            },
            MinLength:{ 
                    required: function(element){
                        if($("#RestrictLength").is(':checked'))
                            return true;
                        else
                            return false;
                    },
                    digits: true
            },
            MaxLength:{ 
                    required: function(element){
                        if($("#RestrictLength").is(':checked'))
                            return true;
                        else
                            return false;
                    },
                    digits: true
            },
            RepairTypeID: { 
                required: function(element){
                    if($("#ReqOnlyForRepairType").is(':checked'))
                        return true;
                    else
                        return false;
                }
            }
        },
        frmErrorMessages: {
            JobFaultCode: { required: "{$page['Errors']['jobfault_code_error']|escape:'html'}" },
            {if $SupderAdmin eq true}
                ServiceProviderID: { required: "{$page['Errors']['serviceProvider_error']|escape:'html'}" },
            {/if}
            FieldNo:{        
                required: "{$page['Errors']['fieldno_error']|escape:'html'}",
                digits: "{$page['Errors']['fieldno_invalid_error']|escape:'html'}"
            },
            MinLength:{  
                required: "{$page['Errors']['minlength_error']|escape:'html'}",
                digits: "{$page['Errors']['minlength_invalid_error']|escape:'html'}"
            },
            MaxLength:{
                required: "{$page['Errors']['maxlength_error']|escape:'html'}",
                digits: "{$page['Errors']['maxlength_invalid_error']|escape:'html'}"
            },
            RepairTypeID: {
                required: "{$page['Errors']['repairtype_error']|escape:'html'}"
            }
        },
        popUpFormWidth:	715,
        popUpFormHeight:	430,
        updateButtonId:	'updateButtonId',
        updateButtonText:	'{$page['Buttons']['edit']|escape:'html'}',
        updateFormTitle:	'{$page['Text']['update_page_legend']|escape:'html'}',
        updateAppUrl:	'{$_subdomain}/LookupTables/jobFaultCodes/update/',
        updateDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodes/',
        formUpdateButton:	'update_save_btn',
        colorboxFormId:	"CForm",
        frmErrorMsgClass:	"fieldError",
        frmErrorElement:	"label",
        htmlTablePageId:	'JobFaultCodesResultsPanel',
        htmlTableId:	'JobFaultCodesResults',
        fetchDataUrl:	'{$_subdomain}/LookupTables/ProcessData/JobFaultCodes/fetch/'+urlencode("{$sId}")+'/'+"{$dataStatus}",
        formCancelButton:	'cancel_btn',
        dblclickCallbackMethod: 'gotoEditPage',
        //fnRowCallback:      'inactiveRow',
        searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
        tooltipTitle:	"{$page['Text']['tooltip_title']|escape:'html'}",
        iDisplayLength:	25,
        formDataErrorMsgId: "suggestText",
        frmErrorSugMsgClass:"formCommonError"
    });
    
    {*var oTable = $('#JobFaultCodesResults').dataTable( {
    "sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
    //"sDom": 'Rlfrtip',
    "bServerSide": true,
                    fnRowCallback:  function(nRow, aData){
                    },
        "sAjaxSource": "{$_subdomain}/LookupTables/loadJobFaultCodesTable",

                    "fnServerData": function ( sSource, aoData, fnCallback ) {
                            /* Add some extra data to the sender */
                            aoData.push( { "name": "more_data", "value": "my_value" } );
                            $.getJSON( sSource, aoData, function (json) { 
                                    /* Do whatever additional processing you want on the callback, then tell DataTables */
                                    fnCallback(json)
                                    $('#tt-Loader').hide();
                                    $('#JobFaultCodesResults').show();

                            } );
                            },
    "bPaginate": true,
    "sPaginationType": "full_numbers",
    "aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
    "iDisplayLength" : 25,
     "aoColumns": [
                            {for $er=0 to $data_keys|@count-1}
                                    {$vis=1}                
                                {if $er==0}{$vis=0}{/if}
                                { "bVisible":{$vis} },
                               {/for} 
                                   { "bVisible":1,"bSortable":false }
                    ]
    });//datatable end
       /* Add a click handler to the rows - this could be used as a callback */
            $("#JobFaultCodesResults tbody").click(function(event) {
                    $(oTable.fnSettings().aoData).each(function (){
                            $(this.nTr).removeClass('row_selected');
                    });
                    $(event.target.parentNode).addClass('row_selected');
                    var anSelected = fnGetSelected( oTable );
                    var aData = oTable.fnGetData(anSelected[0]); // get datarow
        if (anSelected!="")  // null if we clicked on title row
        {
                    $('#exportType').val(aData[0]);
                    }
            });
     /* Get the rows which are currently selected */
    function fnGetSelected( oTableLocal )
    {
        var aReturn = new Array();
        var aTrs = oTableLocal.fnGetNodes();
        for ( var i=0 ; i<aTrs.length ; i++ )
        {
            if ( $(aTrs[i]).hasClass('row_selected') )
                aReturn.push( aTrs[i] );
        }
        return aReturn;
    }
        /* Add a click handler for the edit row */
    $('button[id^=edit]').click( function() {
        var anSelected = fnGetSelected( oTable );
        var aData = oTable.fnGetData(anSelected[0]); // get datarow
        if (anSelected!="")  // null if we clicked on title row
        {
            console.log(anSelected);
            //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
            $.colorbox({
                href:"{$_subdomain}/LookupTables/processJobFaultCodes/id="+anSelected[0].id,
                title: "Edit JobFaultCodes",
                opacity: 0.75,
                width:800,
                overlayClose: false,
                escKey: false
            });
        }else{
            alert("Please select row first");
        }	
    } );    
     /* Add a click handler for the delete row */
    $('button[id^=delete]').click( function() {
        var anSelected = fnGetSelected( oTable );
        var aData = oTable.fnGetData(anSelected[0]); // get datarow
        if (anSelected!="")  // null if we clicked on title row
        {
            //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
            if (confirm('Are you sure you want to delete this entry from database?')) {
                window.location="{$_subdomain}/LookupTables/deleteJobFaultCodes/id="+anSelected[0].id
            } else {
            // Do nothing!
            }
        }else{
            alert("Please select row first");
        }
    } );
    /* Add a dblclick handler to the rows - this could be used as a callback */
    $("#JobFaultCodesResults  tbody").dblclick(function(event) {
        $(oTable.fnSettings().aoData).each(function (){
            $(this.nTr).removeClass('row_selected');
        });
        $(event.target.parentNode).addClass('row_selected');
        var anSelected = fnGetSelected( oTable );
        var aData = oTable.fnGetData(anSelected[0]); // get datarow
        if (anSelected!="")  // null if we clicked on title row
        {
            $.colorbox({
                href:"{$_subdomain}/LookupTables/processJobFaultCodes/id="+anSelected[0].id,
                title: "Edit Job Fault Codes",
                opacity: 0.75,
                width:800,
                overlayClose: false,
                escKey: false
            });
        }else{
            alert("Please select row first");
        }
    } );
    $('#serviceProviderSelect').change( function() {
        if($(this).val()!="0"){
            $('#tt-Loader').show();
            $('#JobFaultCodesResults').hide();
            oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesTable/supplierid="+$(this).val()+"/");
        }else{
            $('#tt-Loader').show();
            $('#JobFaultCodesResults').hide();
            oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesTable/");
        }
    } );  
    $('input[id^=inactivetick]').click( function() {
        $('#unaprovedtick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
            $('#tt-Loader').show();
            $('#SuppliersResults').hide();
            oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesTable/inactive=1/");
        }else{
            $('#tt-Loader').show();
            $('#SuppliersResults').hide();
            oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadJobFaultCodesTable/");
        }
    });*}

{* Updated By Praveen Kumar N : Jquery Inactive Function to send the inactive status [START] *}
$('input[id^=inactivetick]').click( function() {
                     
		    if ($('#inactivetick').is(':checked')) {
			
			    window.location="{$_subdomain}/LookupTables/jobFaultCodes/"+urlencode("{$sId}")+'/'+"In-active";
		     } else {
			    window.location="{$_subdomain}/LookupTables/jobFaultCodes/";
		     }		
                 
	} ); 
	
	$('input[id^=unaprovedtick]').click( function() {
      
     
		     if ($('#unaprovedtick').is(':checked')) {
			window.location="{$_subdomain}/LookupTables/jobFaultCodes/"+urlencode("{$sId}")+'/'+"Pending";
		     } else {
			window.location="{$_subdomain}/LookupTables/jobFaultCodes/";
		     }                    
		
                 
	} );  
	
{* [END] *}

});//doc ready end
function JobFaultCodesInsert()
{
    $.colorbox({
        href:"{$_subdomain}/LookupTables/processJobFaultCodes/",
        title: "Insert Job Fault Codes",
        opacity: 0.75,
        width:800,
        overlayClose: false,
        escKey: false
    });
}
function JobFaultCodesEdit()
{
    var anSelected = fnGetSelected( oTable );
    var aData = oTable.fnGetData(anSelected[0]);               
    $.colorbox({
        href:"{$_subdomain}/LookupTables/processJobFaultCodes/id="+aData,
        title: "Edit Job Fault Codes",
        opacity: 0.75,
        width:800,
        overlayClose: false,
        escKey: false
    });
}
function filterTable(name){
    $('input[type="text"]','#JobFaultCodesResults_filter').val(name);
    e = jQuery.Event("keyup");
    e.which = 13;
    $('input[type="text"]','#JobFaultCodesResults_filter').trigger(e);
}
</script>
{/block}
{block name=body}
<div style="float:right">
    <a href="#" onclick="showTablePreferences();">Display Preferences</a>
</div>
<div class="breadcrumb" style="width:100%">
    <div>
        <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['breadCrumb_lookup_text']|escape:'html'}</a> / {$page['Text']['breadCrumb_jobFaultCode_text']|escape:'html'}
    </div>
</div>
<div class="main" id="home" style="width:100%">
    <div class="ServiceAdminTopPanel" >
        <fieldset>
            <legend title="" >{$page['Text']['page_text']|escape:'html'}</legend>
            <p>
                <label>{$page['Text']['page_short_description']|escape:'html'}</label>
            </p>
        </fieldset>
    </div>
	    
    <div class="ServiceAdminResultsPanel" id="JobFaultCodesResultsPanel">
        <form id="nIdForm" class="nidCorrections">
        {if isset($splist)}
            {$page['Labels']['serviceProvider_label']|escape:'html'}
            <select id="serviceProviderFilter" name="serviceProviderFilter">
                <option value="">select Service Provider</option>
                {foreach $splist as $s}
                    <option value="{$s.ServiceProviderID}" {if $sId eq $s.ServiceProviderID}selected="selected"{/if}>{$s.Acronym}</option>
                {/foreach}
            </select>
        {/if}
        </form> 
	{* Updated By Praveen *}
        <br/> {*added by thirumal *}
	<div style="padding-left:400px;"><button onclick="window.location='{$_subdomain}/LookupTables/jobFaultCodesLookups/'" style="position:absolute;margin-left: 0px" class="gplus-blue" type="button" id="FaultCodeLookupsButton">Fault Code Lookups</button></div>
        <table id="JobFaultCodesResults" border="0" cellpadding="0" cellspacing="0" class="browse">
            <thead>
                <tr>
                    <th title="{$page['Text']['job_fault_codeid']|escape:'html'}" >{$page['Text']['job_fault_codeid']|escape:'html'}</th>
                    <th title="{$page['Text']['job_fault_code']|escape:'html'}" >{$page['Text']['job_fault_code']|escape:'html'}</th>
                    <th title="{$page['Text']['field_no']|escape:'html'}"  >{$page['Text']['field_no']|escape:'html'}</th>
                    <th title="{$page['Text']['field_type']|escape:'html'}" >{$page['Text']['field_type']|escape:'html'}</th>
                    <th title="{$page['Text']['non_fault_code']|escape:'html'}" >{$page['Text']['non_fault_code']|escape:'html'}</th>
                    <th title="{$page['Text']['main_in_fault']|escape:'html'}" >{$page['Text']['main_in_fault']|escape:'html'}</th>
                    <th title="{$page['Text']['main_out_fault']|escape:'html'}" >{$page['Text']['main_out_fault']|escape:'html'}</th>
                    <th title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                    <th align="center"> &nbsp; <input title="" type='checkbox' value=0 id='check_all' /> </th>
                </tr>
            </thead>
            <!--<thead>
                <tr>
                <th>
                </th>
                <th style="width:10px;text-align: center"><input type="checkbox"></th>
                </tr>
            </thead>-->
            <tbody>
            </tbody>
        </table>
    </div>
    {*<div class="bottomButtonsPanelHolder" style="position: relative;">
        <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
            <button type="button" id="edit"  class="gplus-blue">Edit</button>
            <button type="button" onclick="JobFaultCodesInsert()" class="gplus-blue">Insert</button>
            <button type="button" id="delete" class="gplus-red">Delete</button>
        </div>
    </div>
    <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div> *}  
   {* Updated By Praveen Kumar N : Show Inactive Tick Box to display Inactive Records [START] *}
     <div style="width:100%;text-align: right"><input id="inactivetick"  type="checkbox"  value="In-active" {if $dataStatus eq 'In-active'} checked=checked {/if} > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
     {if $SupderAdmin=="1"} <input id="unaprovedtick"  type="checkbox"  {if $dataStatus eq 'Pending'}checked=checked{/if} > Show Unapproved {/if}</div>
     {* [END]*}
    <hr>
    <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/lookupTables'">Finish</button>
</div>
{/block}