{if $addresses eq false or $addresses[0][0] eq "{$page['Errors']['wrong_post_code']|escape:'html'}"}
EM1011
{else}
    
    <script type=text/javascript>
       console.log($('#postSelect_{$multiAddressLookupFlag}').attr('line-height'));
       $('#selectOutput').height((({$dropdown_size}+1)*16)+35);
    </script>

    {if $multiAddressLookupFlag neq ''}
        
        <label for="postSelect_{$multiAddressLookupFlag}" class="fieldLabel" >{$page['Labels']['choose_address']|escape:'html'}: <sup>*</sup></label>&nbsp;&nbsp;
        {* <select title="Please double click to select address." size="{$dropdown_size}" style="height:{$dropdown_height}px;" name="postSelect_{$multiAddressLookupFlag}" id="postSelect_{$multiAddressLookupFlag}" ondblclick="addAddressFields(this.value, '{$multiAddressLookupFlag}')"> *}
        <select title="Please double click to select address." size="{$dropdown_size}" name="postSelect_{$multiAddressLookupFlag}" id="postSelect_{$multiAddressLookupFlag}" ondblclick="addAddressFields(this.value, '{$multiAddressLookupFlag}')">
            <option title="Please double click to select address." selected="selected" value="" style="color:red;" >Please double click to select address.</option>
            {foreach $addresses as $address}
            <option title="Please double click to select address." value="{$address[6]|lower|capitalize|escape:'html'} {$address[7]|lower|capitalize|escape:'html'}, {$address[5]|lower|capitalize|escape:'html'} {$address[0]|lower|capitalize|escape:'html'},{$address[2]|lower|capitalize|escape:'html'},{$address[3]|lower|capitalize|escape:'html'},{$address[12]|lower|capitalize|escape:'html'},{$address[13]|lower|capitalize|escape:'html'},{$address[4]|lower|capitalize|escape:'html'}" >{$address[11]|lower|capitalize|escape:'html'}</option>
            {/foreach}
        </select>
                   
    {else}
        <label for="postSelect" class="fieldLabel" >{$page['Labels']['choose_address']|escape:'html'}: <sup>*</sup></label>&nbsp;&nbsp;
        {* <select title="Please double click to select address." size="{$dropdown_size}" style="height:{$dropdown_height}px;" name="postSelect" id="postSelect" ondblclick="addFields(this.value)"> *}
        <select title="Please double click to select address." size="{$dropdown_size}" name="postSelect" id="postSelect" ondblclick="addFields(this.value)">
            <option title="Please double click to select address." value="" selected="selected" style="color:red;" >Please double click to select address.</option>
            {foreach $addresses as $address}
            <option title="Please double click to select address." value="{$address[6]|lower|capitalize|escape:'html'} {$address[7]|lower|capitalize|escape:'html'}, {$address[5]|lower|capitalize|escape:'html'} {$address[0]|lower|capitalize|escape:'html'},{$address[2]|lower|capitalize|escape:'html'},{$address[3]|lower|capitalize|escape:'html'},{$address[12]|lower|capitalize|escape:'html'},{$address[13]|lower|capitalize|escape:'html'},{$address[4]|lower|capitalize|escape:'html'}" >{$address[11]|lower|capitalize|escape:'html'}</option>
            {/foreach}
        </select>
        
    {/if}
    
{/if}
