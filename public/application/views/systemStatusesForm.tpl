
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
 <script type="text/javascript">   
     
 $(document).ready(function() {
 
     $('#Colour').jPicker( {
          window:
          {
            expandable: true,
			position:
			{
			  x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
			  y: '150px' // acceptable values "top", "bottom", "center", or relative px value
			}
          }
        }
     
    );
    
    $(".Color").css({ width: "25px", height: "24px", padding: "0px" });
  
    
     $( "#TimelineStatus" ).combobox(); 
     $( "#AutoSendSMS" ).combobox(); 
     $( "#AutoSendEmail" ).combobox(); 
    
   }); 
   
  </script>  
    
    <div id="SSFormPanel" class="SystemAdminFormPanel" >
    
                <form id="SSForm" name="SSForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
        
                       
                         
                         
                         <p>
                            <label class="fieldLabel" for="StatusName" >{$page['Labels']['status_name']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="StatusName" value="{$datarow.StatusName|escape:'html'}" id="StatusName" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="Colour" >{$page['Labels']['colour']|escape:'html'}:<sup>*</sup></label>

                            &nbsp;&nbsp; <input  type="text" class="text"  name="Colour" value="{$datarow.Colour|escape:'html'}" id="Colour" style="width:270px;" >

                          </p>
                          
                          
                          <p>
                            <label class="fieldLabel" for="TimelineStatus" >{$page['Labels']['timeline_status']|escape:'html'}:</label>

                            &nbsp;&nbsp; <select name="TimelineStatus" id="TimelineStatus" class="text" >
                                    <option value=""  {if $datarow.TimelineStatus eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $TimelineStatusList as $TimelineStatus}

                                   
                                        <option value="{$TimelineStatus.Code}"   {if $datarow.TimelineStatus eq $TimelineStatus.Code}selected="selected"{/if}>{$TimelineStatus.Name|escape:'html'}</option>
                                   
                                    {/foreach}

                                </select>

                          </p>
                          
                          
                          
                          <p>
                            <label class="fieldLabel" for="StatusTAT" >{$page['Labels']['statusTAT']|escape:'html'}:</label>

                            &nbsp;&nbsp; <input  type="text" class="text"  name="StatusTAT" value="{$datarow.StatusTAT|escape:'html'}" id="StatusTAT" style="width:30px;" >
                            
                            {* <input  type="radio" name="StatusTATType"  value="Minutes" {if $datarow.StatusTATType eq 'Minutes'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['minutes']|escape:'html'}</span> *}
                            <input  type="radio" name="StatusTATType"  value="Hours" {if $datarow.StatusTATType eq 'Hours'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['hours']|escape:'html'}</span> 
                            <input  type="radio" name="StatusTATType"  value="Days" {if $datarow.StatusTATType eq 'Days'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['days']|escape:'html'}</span> 

                          </p>
                          
                          
                           <p>
                            <label class="fieldLabel" for="AutoSendSMS" >{$page['Labels']['auto_send_sms']|escape:'html'}:</label>

                            &nbsp;&nbsp; <select name="AutoSendSMS" id="AutoSendSMS" class="text" >
                                    <option value="-1"  >{$page['Text']['new_feature_coming_soon']|escape:'html'}</option>

                                </select>

                          </p>
                          
                          
                           <p>
                            <label class="fieldLabel" for="AutoSendEmail" >{$page['Labels']['auto_send_email']|escape:'html'}:</label>

                            &nbsp;&nbsp; <select name="AutoSendEmail" id="AutoSendEmail" class="text" >
                                    <option value="-1"  >{$page['Text']['new_feature_coming_soon']|escape:'html'}</option>

                                </select>

                          </p>
                          
                          


                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    



                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="StatusID"  value="{$datarow.StatusID|escape:'html'}" >
                                   
                                    {if $datarow.StatusID neq '' && $datarow.StatusID neq '0'}
                                        
                                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
