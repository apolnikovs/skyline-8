<head>
    <script>
    function validateHolidayForm(f){
        var bool = true;
        var errorStr = "";
        if($('#bholidayDate').val() == "")
        {
            errorStr+='Please select Bank Holiday Date.\n';
            bool = false;
        }
        if($("#holidayName").val() == "")
        {
            errorStr+='Please enter Bank Holiday Name.\n';
            bool = false;
        }
        if($("#bankHolidayStatus").val() == "")
        {
            errorStr+='Please select Bank Holiday Status.\n';
            bool = false;
        }
        if(!bool)
        {
            alert(errorStr);
            return false;
        }
        else
        {
            $.post("{$_subdomain}/AppointmentDiary/insertBankHoliday/", $("#bankHolidayCardForm").serialize(),function(data){
            if (data.status == "OK") {
                $("#secondaryDivText").html('<b>'+data.message+'</b>');
                $("#mainDiv").hide();
                $("#secondaryDiv").show();
                $(this).colorbox.resize();
            }
        },"json").error(function() { alert('Internal Server Error') ;});
        }
    }
    </script>
    </head>
    <body>
    
        <fieldset>            
            <legend>Insert a New Bank Holiday</legend>
            <div id="mainDiv">
            <form name="bankHolidayCardForm" id ="bankHolidayCardForm" method="post" action="">
            <table>
                <tr>
                    <td>Date</td>
                    <td>Bank Holiday Name</td>
                    <td>Status</td>
                </tr>
                <tr>
                    <td><input type="text" name="bholidayDate" id="bholidayDate" value=""></td>
                    <td><input type="text" name="holidayName" id="holidayName" value=""></td>
                    <td>
                        <select id="bankHolidayStatus" style="width:150px" name="bankHolidayStatus">
                            <option value="">Select</option>
                            <option value="Closed">Closed</option>
                            <option value="Open">Open</option>
                        </select>
                    </td>                    
                </tr>
            </table>
            <div style="text-align: center;">
                <button type="button" onclick="validateHolidayForm(this.form)" class="btnStandard">Save Changes</button>
                <button type="button" onclick="$.colorbox.close()" class="btnStandard">Cancel Changes</button>
            </div>
            <input type="hidden" name="cId" id="cId" value="{$cId}" />
                </form></div>
                <div id="secondaryDiv" style="display: none; text-align: center;">
                    <span id="secondaryDivText"></span><br><br>
                    <button type="button" onclick="location.reload();" class="btnStandard">Ok</button>
                </div>
        </fieldset> 
    </body>