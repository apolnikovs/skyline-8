{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $SystemUsers}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
        
    function gotoEditPage($sRow) {
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');

    }
    
    {* User type lowest level *}
    function inactiveRow(nRow, aData) {
        
        if (aData[6]==$statuses[1][1]) {  
            $(nRow).addClass("inactive");

            $('td:eq(5)', nRow).html( $statuses[1][0] );
        } else {
            $(nRow).addClass("");
                  $('td:eq(5)', nRow).html( $statuses[0][0] );
        }
    }
    
   
 

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/OrganisationSetup/systemUsers/'+urlencode($("#nId").val()));
            }
        });
        $("#cId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/OrganisationSetup/systemUsers/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val()));
            }
        });
        $("#bId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/OrganisationSetup/systemUsers/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val())+'/'+urlencode($("#bId").val()));
            }
        });
                //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/organisationSetup');

                                });


                  
                  $(document).on('click', '#quick_find_btn', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#PostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectOutput").slideDown("slow");
                                           

                                            $("#BuildingNameNumber").val('');	
                                            $("#Street").val('');
                                            $("#LocalArea").val('');
                                            $("#TownCity").val('');
                                            
                                            $('#PostalCode').focus();

                                    }
                                    else
                                    {

                                        $('#selectOutput').html(data); 
                                        $("#selectOutput").slideDown("slow");
                                        
                                        $("#postSelect").focus();
                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')");
                            },
                            url:'{$_subdomain}/index/addresslookup' 
                        } ); 
                    return false;
                    
                    
                });                 

                $(document).on('change', 'select[name="company_dropdown"]', function(){
                    console.info( $(this).attr('id') );
                    if($(this).val() != ''){
                        $('select[id!="'+$(this).attr('id')+'"]').attr({ 'disabled' : 'disabled' });
                    }else{
                        $('select[name="company_dropdown"]').removeAttr('disabled')
                    }
                });

                 /* Add a change handler to the county dropdown - strats here*/
                $(document).on('change', '#CountyID', 
                    function() {
                         
                         
                         $("#CountyID option:selected").each(function () {
                            $selected_cc_id =  $(this).attr('id');
                            $country_id_array = $selected_cc_id.split('_');
                            if($country_id_array[1]!='0')
                            {
                                $("#CountryID").val($country_id_array[1]);
                                $("#CountryID").blur();
                                $("#CountryID").attr("disabled","disabled").addClass('disabledField');
                            }
                            else
                            {
                                 $("#CountryID").val('');
                                 $("#CountryID").removeAttr("disabled").removeClass('disabledField');
                            }
                            
                            
                        });

                        
                        
                    }      
                );
              /* Add a change handler to the county dropdown - ends here*/
              
              /* Add a change handler to the network dropdown - strats here*/
                /*$(document).on('change', '#nId', 
                    function() {
                         
                        $(location).attr('href', '{$_subdomain}/OrganisationSetup/systemUsers/'+urlencode($("#nId").val())); 
                    }      
                );*/
              /* Add a change handler to the network dropdown - ends here*/
              
              /* Add a change handler to the client dropdown - strats here*/
                /*$(document).on('change', '#cId', 
                    function() {

                        $(location).attr('href', '{$_subdomain}/OrganisationSetup/systemUsers/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val())); 
                    }      
                );*/
                
                /* Add a change handler to the branch dropdown - strats here*/
                /*$(document).on('change', '#bId', 
                    function() {

                        $(location).attr('href', '{$_subdomain}/OrganisationSetup/systemUsers/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val())+'/'+urlencode($("#bId").val())); 
                    }      
                );*/

                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UA";
                     
                     {else}

                         var displayButtons =  "U";

                     {/if} 
                     

// 'UserID', 'ContactFirstName', 'ContactLastName', 't2.CompanyName', 't3.CompanyName',  't4.ClientName AS CompanyName', 't5.BranchName AS CompanyName', 't1.Status');
                    
//    $.validator.addMethod("loginRegex", function(value, element) {
//        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
//    }, "Username must contain only letters, numbers, or dashes.");
                    
                    
                    $('#SNResults').PCCSDataTable( {
                              "aoColumns": [ 
                                                    /* UserID */  {  "bVisible":    false },   
                                                    /* Full Name */   null,
                                                    /* Username */   null,
                                                    /* Customer */   null,
                                                    /* Search Scope */  null,
                                                    /* Access Right */  {  "bSortable":    false, "bSearchable":    false },
                                                    /* Status */  null
                                                ],
                            "aaSorting": [[ 2, "asc" ]],
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/OrganisationSetup/SystemUsers/insert/',
                            createDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/SystemUsers/',
                            createFormFocusElementId:'Username',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            frmErrorRules:   {
                                        Username: { required: true },
                                        Password: { required: true },
                                       
                                        ContactFirstName: { required: true },
                                       
                                       
                                        ContactEmail: { email:true },
                                        SecurityQuestionID: { required: true },
                                        Answer: { required: true },
                                        UserType: { 
                                        
                                                            
                                        
                                        required:  function (element) {
                                                                if($("#SuperAdminFlag").val()=='1')
                                                                {

                                                                    return false;
                                                                }
                                                                else
                                                                {
                                                                    return true;
                                                                }
                                                            } 
                                        
                                        },
                                        NetworkID: {
                                            required: function (element) {
                                                if($("#UserType").val()=='Client' || $("#UserType").val()=='Branch')
                                                    return true;
                                                else
                                                    return false;
                                            }
                                        },
                                        ClientID: {
                                            required: function (element) {
                                                if($("#UserType").val()=='Branch')
                                                    return true;
                                                else
                                                    return false;
                                            }
                                        },
                                        CompanyID: { required:  function (element) {
                                                                if($("#SuperAdminFlag").val()=='1')
                                                                {

                                                                    return false;
                                                                }
                                                                else
                                                                {
                                                                    return true;
                                                                }
                                                            } 
                                                            },
                                        DefaultBrandID:  { required:  function (element) {
                                                                if($("#SuperAdminFlag").val()=='1')
                                                                {

                                                                    return false;
                                                                }
                                                                else
                                                                {
                                                                    return true;
                                                                }
                                                            } 
                                                            
                                                          },
                                        Status: { required: true }
                                             },
                           frmErrorMessages: {
                                        Username: { 
                                            required: "{$page['Errors']['username']|escape:'html'}"
//                                            ,
//                                            loginRegex: "Login format not valid"
                                            },
                                        Password: {
                                            required: "{$page['Errors']['password']|escape:'html'}"
                                            
                                            },
                                        ContactFirstName: {
                                            required: "{$page['Errors']['forename']|escape:'html'}"
                                            },
                                        ContactEmail: {
                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                            },  
                                        SecurityQuestionID: {
                                            required: "{$page['Errors']['question']|escape:'html'}"
                                            },   
                                        Answer: {
                                            required: "{$page['Errors']['answer']|escape:'html'}"
                                            },   
                                        UserType: {
                                            required: "{$page['Errors']['usertype']|escape:'html'}"
                                            },
                                        NetworkID: {
                                            required: "{$page['Errors']['branchNetwork']|escape:'html'}"
                                            },
                                        ClientID: {
                                            required: "{$page['Errors']['branchNetworkClient']|escape:'html'}"
                                            },
                                        CompanyID: {
                                            required: "{$page['Errors']['comapny']|escape:'html'}"
                                            },                                           
                                        DefaultBrandID: {
                                            required: "{$page['Errors']['brand']|escape:'html'}"
                                            },    
                                        Status: {
                                            required: "{$page['Errors']['status']|escape:'html'}"
                                            }                                           
                                        },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/OrganisationSetup/SystemUsers/update/',
                            updateDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/SystemUsers/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'CompanyName',
                            
                            colorboxFormId:  "SNForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'SNResultsPanel',
                            htmlTableId:     'SNResults',
                            fetchDataUrl:    '{$_subdomain}/OrganisationSetup/ProcessData/SystemUsers/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$cId}"),
                            formCancelButton:'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/organisationSetup" >{$page['Text']['organisation_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="serviceNetworksForm" name="serviceNetworksForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="SNResultsPanel" >
                    {if $SupderAdmin eq true} 
                        <form id="nIdForm">
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="company_dropdown" id="nId" >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                       <br /><span style="padding-right: 59px;">{$page['Labels']['client_label']|escape:'html'}</span>
                       <select name="cId" id="cId" >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $nClients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                       {$page['Labels']['branch_label']|escape:'html'}
                       <select name="bId" id="bId" >
                            <option value="" {if $bId eq ''}selected="selected"{/if}>{$page['Text']['select_branch']|escape:'html'}</option>

                            {foreach $cBranches as $branch}

                                <option value="{$branch.BranchID}" {if $bId eq $branch.BranchID}selected="selected"{/if}>{$branch.BranchName|escape:'html'}</option>

                            {/foreach}
                        </select>                        
                        </form>
                    {/if} 
                    <table id="SNResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th></th> 
                                        <th width="20%" title="{$page['Text']['fullname']|escape:'html'}" >{$page['Text']['fullname']|escape:'html'}</th>
                                        <th width="10%" title="{$page['Text']['username']|escape:'html'}" >{$page['Text']['username']|escape:'html'}</th>
                                        <th width="20%" title="{$page['Text']['company']|escape:'html'}" >{$page['Text']['company']|escape:'html'}</th>
                                        <th width="10%" title="{$page['Text']['search_scope']|escape:'html'}" >{$page['Text']['search_scope']|escape:'html'}</th>
                                        <th width="30%" title="{$page['Text']['access_right']|escape:'html'}" >{$page['Text']['access_right']|escape:'html'}</th>
                                        <th width="10%" title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                                        
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



