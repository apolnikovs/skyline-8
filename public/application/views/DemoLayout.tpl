<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    
    {* ################################################################# 
     # 
     #  Define Global Varables
     #
     ################################################################### *}
     
        {$HomePage = 0}
        {$OutstandingJobsPage = 1}
        {$OverdueJobsPage = 2}
        {$ClosedJobsPage = 92}
        {$PerformancePage = 3}
        {$LinksPage = 4}
        {$HelpPage = 5}
        {$LoginPage = 6}
        {$ProductDetailsPage = 7}
        {$JobConfirmationPage = 8}
        {$ProductMatchesPage = 9}
        {$JobMatchesPage = 10}
        {$JobBookingPage= 11}
        {$OpenJobsPage = 12}
        {$RegistrationPage = 13}
        {$JobBookingProcessPage = 14}
        {$ErrorPage = 15}
        {$JobDetailsPage = 16}
        {$PerformancePage = 17}
        {$SiteMapPage = 18}
        {$HelpPage = 19}
        {$AppointmentDiaryPage = 20}
        {$SkillsAreaMapPage = 21}
        {$SkillsSetSPPage = 22}
        {$RAJobsPage = 23}
        {$DiaryDefaultsPage = 24}
        {$JobBookingDiaryPage = 25}
        {$GraphicalAnalysisPage = 26}
        

        {$AdminHomePage = 30}
        {$AuditTrailActions = 31}
        {$ContactHistoryActions = 32}
        {$CustomerTitles = 33}
        {$JobTypes = 34}
        {$PaymentTypes = 35}
        {$SecurityQuestions = 36}
        {$VATRates = 37}
        {$Country = 38}
        {$County = 39}
        {$ServiceTypes = 40}
        {$ServiceNetworks = 41}
        {$Clients = 42}
        {$Brands = 43}
        {$Branches = 44}
        {$Manufacturers = 45}
        {$UnitTypes = 46}
        {$Models = 47}
        {$PricingStructure = 48}
        {$ProductNumbers = 49}
        {$ClientServiceTypes = 50}
        {$ServiceProviders = 51}
        {$SystemUsers = 52}
        {$UserRoles = 53}
        {$CentralServiceAllocations = 54}
        {$TownAllocations = 55}
        {$PostcodeAllocations = 56}
        {$UnallocatedJobs = 57}
        {$RAStatusTypes = 58}
        {$SystemStatuses = 59}
        {$ExtendedWarrantor = 60}
        {$Couriers = 61}
        {$CallManagerPage = 62}
        {$BookingPerformancePage = 63}
        {$DiaryPerformancePage = 64}
        {$BoughtOutGuarantee = 65}
        {$UnitTypeManufacturer = 66}
        {$BulkSbImport = 67}
	
        
        {$StockOrderingPage = 86}
        {$StockPage = 87}
        {$DiaryPage = 88}
        {$HolidayDiaryPage = 89}
	{$DiaryInterface = 90}
        {$AccessPermissions = 91}
        {$AccessoriesPage = 92}
        {$Suppliers = 93}
        {$StatusPermissions = 94}
        {$Accessory = 100}
        {$Colour = 101}
        {$ProductCode = 102}
        {$Currency = 103}
        {$MobilePhoneNetworkPage = 104}

        {$SMSPage = 105}
        {$PartOrderStatusPage = 106}
        {$JobBookingSettingsPage = 107}
        {$JobFaultCodesPage = 108}
        {$JobFaultCodesLookupsPage = 109}
        {$PartFaultCodesPage = 110}
        {$PartFaultCodesLookupsPage = 111}
        {$PartLocationPage = 112}
        {$ShelfLocationPage = 113}
        {$RepairTypePage = 114}
        {$PartCategoryPage = 115}
        {$PartSubCategoryPage = 116}
        {$PartStatusColourPage = 117}
        {$GroupHeadings = 118}
        {$SatisfactionQuestionnaire = 119}

        {$Title = ""}
        {$PageTitle = ""}
        {$fullscreen = false}
	{$newjquery = false}
        {block name=config}
        {/block} 

     {* ###################################################################
     #  SMARTY Functions:
     # 
     #   name:  menu
     #   args:  $menu_items
     #
     ##################################################################### *}
    
    {function menu}
    {strip}
         {foreach $menu_items as $item}
             {if $item[2] eq ''}
             <button type="button" class="disabled {if $item[3] ne ''}{$item[3]}{/if}" >
                 <span class="label">{$item[1]}</span>
             </button>
             {elseif $item[2] eq 'ONCLICK'}
             <button type="button" {if $item[3] ne ''}class="{$item[3]}"{/if} onclick="{$item[4]};">
                 <span class="label">{$item[1]}</span>
             </button>
             {else}
             <button type="button" {if $item[3] ne ''}class="{$item[3]}"{/if} onclick="document.location.href='{$_subdomain}{$item[2]}';" >
                 <span class="label">{$item[1]}</span>
             </button>                 
             {/if}
         {/foreach}   
    {/strip}
    {/function}
       
    {* #################################################################### *}
    
     
    <head>
       
        <title>SkyLine - {$Title}</title>
        
        {* #################################################################
         #
         # Add your meta tags here  
         #
         ################################################################### *}
        <meta name="title" content="" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="language" content="en" />
        <meta name="author" content="BookArt" />  
        
        <script type="text/javascript">
           // set a global javascript variable for _SubDomain that can be 
           // used in included javascript files (eg jquery.dataTablesPCCS.js
           var _SUBDOMAIN = '{$_subdomain}';
       </script>
          
        {* JQuery *}
	
	{* Because of certain bugs in jQuery 1.8.2 some pages need to use newer version *}
	
	{if $newjquery}
	    <script type="text/javascript" src="{$_subdomain}/js/jquery-1.10.1.min.js"></script>
	    <script>$.migrateMute = true;</script> {* Disable dumping warnings to console *}
	    <script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery-migrate-1.2.1.min.js"></script>
	{else}
	    <script type="text/javascript" src="{$_subdomain}/js/jquery-1.8.2.min.js"></script>
	{/if}
       
	{* Base CSS *}
	<link rel="stylesheet" href="{$_subdomain}/css/base_css.php?size=950" type="text/css" media="screen" charset="utf-8" />
        
	{* Skin CSS *}
	<link rel="stylesheet" href="{$_subdomain}/css/skin_css.php?skin={$_theme}&size=950" type="text/css" media="screen" charset="utf-8" />
	
        {* Site Stylesheet *}
	{*<link rel="stylesheet" href="{$_subdomain}/css/styles.php?skin={$_theme}&size=950" type="text/css" media="screen" charset="utf-8" />*}
        
        {* printing styles *}
        {*<link rel="stylesheet" href="{$_subdomain}/css/Skins/{$_theme}/lessblue/print.css" type="text/css" media="print" charset="utf-8" />*}  
          
        
        {* IE only styles *}
	<!--[if lt IE 8]><link rel="stylesheet" href="{$_subdomain}/css/Base/lessblue/ie.css" type="text/css" media="screen" /><![endif]-->
        <!--[if IE 8]>
	    <link rel="stylesheet" type="text/css" href="{$_subdomain}/css/Base/lessblue/ie8.css">
        <![endif]-->
        <!--[if IE 7]>
	    <link rel="stylesheet" type="text/css" href="{$_subdomain}/css/Base/lessblue/ie7.css">
        <![endif]-->
        
        {* Jquery debug scripts *}
        <script type="text/javascript" src="{$_subdomain}/blackbird/blackbird.js"></script>
        <link type="text/css" rel="Stylesheet" href="{$_subdomain}/blackbird/blackbird.css" />
        <link type="text/css" rel="Stylesheet" href="{$_subdomain}/css/qtip/jquery.qtip.css" />
        
	
	{* JavaScript/CSS commonly included files in templates are ported here *}
	<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />
	<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>
	<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>
	<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>
	<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>
	<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.1.9.4.min.js"></script>
	<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/pccs.string.functions.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/pccs.string.functions.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.qtip.min.js"></script>
	
	{* IE6 JSON fix *}
	<!--[if (IE 6)|(IE 7)]>
	    <script type="text/javascript" src="{$_subdomain}/js/json2.js"></script>
	<![endif]-->

	<!--[if IE 7]>
	    <script type="text/javascript" src="{$_subdomain}/js/json2.js"></script>
	<![endif]-->
       
        {block name=scripts}{/block} 
        
        {* JQuery UI *}

	<script type="text/javascript" src="{$_subdomain}/js/jquery-ui-1.9.1.custom.min.js"></script>
	<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery-ui-1.9.1.custom.min.css" type="text/css" />
	
	{*
        {if $newjquery}
	    <script type="text/javascript" src="{$_subdomain}/js/jquery-ui-1.10.3.custom.min.js"></script>
	    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery-ui-1.10.3.custom.min.css" type="text/css" />
	{else}
	    <script type="text/javascript" src="{$_subdomain}/js/jquery-ui-1.9.1.custom.min.js"></script>
	    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery-ui-1.9.1.custom.min.css" type="text/css" />
        {/if}
	*}
	
        {* change default page lenths in datatables written by srinivas *}        
        <script>
        $.fn.dataTable.defaults.aLengthMenu = [[25, 50, 100, 500, -1], [25, 50, 100, 500, "All"]];
        </script>        
        {* end written by srinvas *}
        
        {block name=afterJqueryUI}{/block}            
        <script>
           // session timer will auto reload page when session time expires
           // Andris <a.polnikovs@pccsuk.com>
          //   $(document).ready(function() {
//             $.post( '{$_subdomain}/Default/getExpirityMilisecsFromNow/',        
//                        $('#ServiceProvidersForm').serialize(),      
//                        function(data) {
//                         setInterval("location.reload(true)", data*1000);
//                        });
        
            
           // });
        </script>
    </head>
        
    <body>
    
	
        <div class="container" id="container-main" {if $fullscreen}style="width:98%;min-width: 950px"{/if}>
            
           {if $showTopLogoBlankBox}
             <div class="blankBox" >&nbsp;</div> 
            {/if}                       
            <div id="header" {if $fullscreen}style="width:98%;min-width: 950px"{/if}>
                    {if $PageId == $HomePage}
			<span id="flags" style="right: 10px; bottom: -30px;">
			    {if isset($englishFlag) && $englishFlag}
				<a href="{$_subdomain}/index/index/lang=en"><img src="{$_subdomain}/images/country/gb.png" alt="English" title="English"></a>&nbsp;
			    {/if}
			    {if isset($germanFlag) && $germanFlag}
				<a href="{$_subdomain}/index/index/lang=de"><img src="{$_subdomain}/images/country/de.png" alt="Deutsche" title="Deutsche"></a>
			    {/if} 
			</span>
                    {/if}
                    
                    {* if $_brandLogo ne ''}
                         <a href="{$_subdomain}/index/index"><img src="{$_subdomain}/images/brandLogos/{$_brandLogo}" alt="" id="logo" border="0"/></a>
                    {else}
                        <a href="{$_subdomain}/index/index"><img src="{$_subdomain}/images/{$_theme}/header_logo.png" alt="" id="logo" border="0"/></a>
                    {/if *}
                    
                    <a href="{$_subdomain}/index/index"><img src="{$_subdomain}/images/brandLogos/{$_brandLogo|default: 'default_logo.png'}" alt="" id="logo" border="0"/></a>
                    
		    {if isset($loggedin_user) && $loggedin_user->DefaultBrandID == 2030}
			<a style="display:block; position:relative; float:right; margin-top:27px;" href="{$_subdomain}/index/index"><img src="{$_subdomain}/images/samsung_secondary.png" alt="" id="secondaryLogo" border="0"/></a>
		    {elseif isset($secondaryLogo)}
			<a style="display:block; position:relative; float:right; margin-top:27px;" href="{$_subdomain}/index/index"><img src="{$_subdomain}/images/samsung_secondary.png" alt="" id="secondaryLogo" border="0"/></a>
		    {/if}
                    
                    
                    {if $session_user_id != ''}
			<span id="welcome">Welcome {$name} {$logged_in_branch_name} {$last_logged_in}{if !isset($ServiceBase)}<a href="{$_subdomain}/Login/logout">Logout</a>{/if}</span>
                    {/if}
                    
                    
            </div> 
                    
            {block name=body}{/block} 
           
        </div>
            
        {* <div style="height: 20px"/> *}

<!--[if IE 6]>
<script type="text/javascript" src="{$_subdomain}/js/DD_belatedPNG_0.0.8a.js"></script>
<script>
$(document).ready(function() {
    DD_belatedPNG.fix('img, .node');
});//end document ready function
</script>
<![endif]--> 

{* ***********************************************************************
    
    IMPORTANT NOTE: The closing body and html tags are now appended to the output stream
                    in index.php.
         
    </body>
                
</html>

**************************************************************************** *}
