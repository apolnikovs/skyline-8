<?php

/**
 * Short Description of CustomModel.
 * 
 * Long description of CustomModel.
 *
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.10
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version
 * ??/??/2012  1.1     Brian Etherington
 * ??/??/2012  1.2     Brian Etherington  
 * 28/08/2012  1.3     Brian Etherington     Added InsertRow, UpdateRow, DeleteRow methods 
 * 07/11/2012  1.4     Brian Etherington     Added log and loadModel methods   
 * 15/01/2013  1.5     Brian Etherington     Changed $columns argument to ServeDataTables
 *                                           allow for each element to optionally be either
 *                                           a simple string column_name or an
 *                                           array( column_name, formatted_column_name) 
 *                                           eg array('MyDate', 'DATE_FORMAT(MyDate,"%d/%m/%y")') 
 * 23/01/2013  1.6     Brian Etherington     send database errors to ERRORS_yyyy-mm-dd log file.
 * 15/02/2013  1.7     Brian Etherington     amended ServeDataTables to improve performance.
 * 26/03/2013  1.8     Brian Etherington     amended ServeDataTables to exclude application filtering
 *                                           via $args['where'] clause from the unfiltered total.
 * 04/03/2013  1.9     Brian Etherington     added groupby parameter to servedatatables argument array
 *                                           because adding a group by clause to the end of the where 
 *                                           paramter broke the record count statment.
 * 09/05/2013  1.10    Brian Etherington     PUT BACK code for FindRows and NULL in filter for ServeDataTables.
 *                                           This code was removed/deleted according to Netbeans/GIT history on 7/05/2013.
 *                                           Looks like it was a GIT merge resolution problem.
 ******************************************************************************/
 
abstract class CustomModel {
   
    // beginning of docblock template area
    /**#@+
     * @access private
     */
    private static $db = array();
    private static $generator = null;
    
    private $PreparedStatement = null;
    /**#@-*/
    
    // beginning of docblock template area
    /**#@+
     * @access protected
     */
    protected $controller;  
    protected $last_error = null;
    public $debug = false;
    /**#@-*/
       
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param object $controller 
     */
    public function __construct(CustomController $controller) {
        $this->controller = $controller;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    protected function log( $message, $tag = "" ) {
        $this->controller->log( $message, $tag );
    } 
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    protected function loadModel( $model_path ) {
        return $this->controller->loadModel( $model_path );
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    protected function getLastError() {
        return $this->last_error;
    }
       
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $connection_string
     * @param string $user
     * @param string $password
     * @return PDO $db_connection object
     */
    protected function Connect( $connection_string, $user, $password,
                                $err_mode=PDO::ERRMODE_EXCEPTION ) {
               
        foreach(self::$db as $connection) {

            if ($connection[0] == $connection_string) {
                return $connection[1];
            }
            
        }
        
        $db_connection = new PDO($connection_string, $user, $password);
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, $err_mode);
        $db_connection->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, 1);
        
        self::$db[] = array($connection_string, $db_connection);
  
        return $db_connection;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param PDO $conn object
     * @param string $sql
     * @param array $params_array
     * @return associative array
     */
    protected function Query( PDO $conn, $sql, $params_array = null, $mode=PDO::FETCH_ASSOC ) {
               
        if ($this->debug) {
            $this->controller->log('Query: '.$sql);
            $this->controller->log('Params: '.var_export($params_array,true));
        }
        
        $prep = $conn->prepare($sql); 
        if (is_array($params_array) ) {
            $this->bindParams( $prep, $params_array );
        }
	
	//$this->log($sql);

        if (!$prep->execute()) {
            $this->controller->log($this->lastPDOError(), 'Errors_');
            return false;
        }
        return $prep->fetchAll( $mode );
        
    }
    
    protected function FindRows( PDO $conn, TableSQL $table, array $params ) {
        $cmd = $table->findCommand( $params );
        return $this->Query($conn, $cmd, $params);      
    }

    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param PDO $conn object
     * @param TableSQL $table object
     * @param array $params_array
     * @return boolean false if error, $pk if no error
     */
    protected function InsertRow( PDO $conn, TableSQL $table, array $params ) {
               
        $cmd = $table->insertCommand( $params );
        if ($this->Execute($conn, $cmd, $params) === false) 
                return false;
               
        if ($table->getIsAutoinc()) {
            return $conn->lastInsertId();
        }
                
        $pk_array = $table->getPrimaryKey();
        if (count($pk_array) == 1) {
            $pk = $params[$pk_array[0]];
        } else {
            $pk = array();
            foreach($pk_array as $key) {
                $pk[$key] = $params[$key];
            }
        }
        return $pk;
        
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param PDO $conn object
     * @param TableSQL $table object
     * @param array $params_array
     * @return boolean $result
     */
    protected function UpdateRow( PDO $conn, TableSQL $table, array $params ) {
        
        $cmd = $table->updateCommand( $params );
        return $this->Execute($conn, $cmd, $params);
        
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param PDO $conn object
     * @param TableSQL $table object
     * @param array $params_array or string $where_cluase
     * @return boolean $result
     */
    protected function DeleteRow( PDO $conn, TableSQL $table, $params ) {
        
        // generate a delete command and delete row from table...
        if (is_array($params)) {
            // $where is an array of primary key values....
            $cmd = $table->deleteCommand();
            $this->Execute( $conn, $cmd, $params );
        } else {
            // $where is a where condition string...
            // Note: No parameters in this case.
            $cmd = $table->deleteCommand( $params );
            $this->Execute( $conn, $cmd );
        }
        
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param PDO $conn object
     * @param string $sql
     * @param array $params_array
     * @return boolean 
     */
    protected function Execute( PDO $conn, $sql, $params_array = null ) {
        
        if ($this->debug) {
            $this->controller->log('Execute: '.$sql);
            $this->controller->log('Params: '.var_export($params_array,true));
        }
        
        if ( is_null($this->PreparedStatement) || $this->PreparedStatement[0] != $sql ) {
            $prep = $conn->prepare($sql); 
            $this->PreparedStatement = array( $sql, $prep );
        }
        
        if (is_array($params_array) ) {
            $this->bindParams( $this->PreparedStatement[1], $params_array );
        }
        
        if (!$this->PreparedStatement[1]->execute()) {
            $this->controller->log($this->lastPDOError(), 'Errors_');
            return false;
        }
        return true;
            
        
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @return string 
     */
    protected function lastPDOError() {
        
        $error = PDOStatement::errorInfo();
        return $error[0] . ' ' . $error[2];
        
    }
             
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param PDOStatement &$statement object
     * @param array &$params_array
     */
    private function bindParams( PDOStatement &$statement, &$params_array ) {
               
        foreach( $params_array as $key => $value ) {

            if ( is_int($value) ) 
                $param = PDO::PARAM_INT;
            elseif ( is_bool($value) ) 
                $param = PDO::PARAM_BOOL;
            elseif ( is_null($value) ) 
                $param = PDO::PARAM_NULL;
            elseif ( is_string($value) ) 
                $param = PDO::PARAM_STR;
            else 
                $param = PDO::PARAM_STR;

            $statement->bindValue(":$key", $value, $param);
            
        }
        
    }
      
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param int $length 
     * @return string
     */
    public function GenPassword($length = 8) {
        // $length max = 32
        return substr(md5(rand().rand()), 0, $length);
    }
     
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param int $syllables
     * @param boolean $use_prefix
     * @return string
     */
    public function GenReadablePassword($syllables = 3, $use_prefix = false) {
        
        // See http://www.anyexample.com/programming/php/php__password_generation.xml

        // 20 prefixes
        $prefix = array('aero', 'anti', 'auto', 'bi', 'bio',
                        'cine', 'deca', 'demo', 'dyna', 'eco',
                        'ergo', 'geo', 'gyno', 'hypo', 'kilo',
                        'mega', 'tera', 'mini', 'nano', 'duo');

        // 10 random suffixes
        $suffix = array('dom', 'ity', 'ment', 'sion', 'ness',
                        'ence', 'er', 'ist', 'tion', 'or'); 

        // 8 vowel sounds 
        $vowels = array('a', 'o', 'e', 'i', 'y', 'u', 'ou', 'oo'); 

        // 20 random consonants 
        $consonants = array('w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 
                            'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'qu');

        $password = $use_prefix ? $this->RandomArrayElement($prefix) : '';
        $password_suffix = $this->RandomArrayElement($suffix);

        for($i=0; $i<$syllables; $i++) {
            // selecting random consonant
            $doubles = array('n', 'm', 't', 's');
            $c = $this->RandomArrayElement($consonants);
            if (in_array($c, $doubles)&&($i!=0)) { // maybe double it
                if (rand(0, 2) == 1) // 33% probability
                    $c .= $c;
            }
            $password .= $c;
            //

            // selecting random vowel
            $password .= $this->RandomArrayElement($vowels);

            if ($i == $syllables - 1) // if suffix begin with vovel
                if (in_array($password_suffix[0], $vowels)) // add one more consonant 
                    $password .= $this->RandomArrayElement($consonants);

        }

        // selecting random suffix
        $password .= $password_suffix;

        return $password;
    }

    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array &arr 
     * @return array
     */
    private function RandomArrayElement(&$arr) {
        return $arr[rand(0, sizeof($arr)-1)];
    }
    
    /**
     * Return LoremIpsum Generator Class object.
     * 
     * Usage:
     * The only public method in the class is getContent.
     *
     * Description:
     * string getContent( int $wordCount [, string $format = html] [, boolean $loremipsum = true] )
     *
     * Returns the desired amount of content as a string.
     *
     * Parameters:
     *
     * $wordCount
     * The number of words to be returned.
     *
     * $format
     * The output mode, one of ‘html’, ‘txt’, or ‘plain’. HTML by default.
     *
     * HTML: The content is divided into paragraphs, using the paragraph ( <p></p> ) tag.
     * Text: The content is divided into paragraphs with the leading line of each paragraph tabbed
     * Plain: The content is returned unformatted
     *
     * $loremipsum
     * Whether or not the content should begin with ‘Lorem ipsum’. True by default.
     *
     * Example:
     *
     * <code> 
     * // 100 words in html format
     * $text = $this->LoremIpsum()->getContent(100);
     * 
     * // 100 words without any formatting
     * $text = $this->LoremIpsum()->getContent(100, 'plain');
     * 
     * // 100 words with 'text' formatting
     * $text = $this->LoremIpsum()->getContent(100, 'txt');
     * 
     * // 100 words with html format, not beginning with lorem ipsum
     * $text = $this->LoremIpsum()->getContent(100, NULL, false);
     * // or
     * $text = $this->LoremIpsum()->getContent(100, 'html', false);
     * </code>
     *  
     * Additional Notes:
     * 
     * Both the HTML and Text output modes use paragraph formatting. 
     * The mean word count of each paragraph is predetermined and can be set 
     * in the constructor, currently the default is 100. Note that this is the 
     * mean word count, the actual word count for each paragraph will vary in 
     * the same way the length of each sentence will vary.
     * 
     * @return object LoremIpsumGenerator 
     */
    public function LoremIpsum() {
        
        if (self::$generator == null) {
            include_once('LoremIpsum.class.php');
            self::$generator = new LoremIpsumGenerator;
        }
        return self::$generator;
    }

    /**
     * 
     * A sample function docblock
     * @global string document the use $_myvar
     * @param string $conn name to declare
     * @param string $table database table name
     * @param string $columns 
     * @param array $args 
     * @return array
     * @tutorial
     * <ul>
     *  <li>test</li>
     * </ul>
     * @example
     *   <code>
     *       $output = array(
     *           'sEcho' => (int) $args['sEcho'],
     *           'iTotalRecords' => $unfilteredTotal,
     *           'iTotalDisplayRecords' => $filteredTotal,
     *           'aaData' => $result->fetchAll()
     *      );
     *  </code>
     * 
     *********************************************/
    public function ServeDataTables($conn, $table, $columns, $args,$columns_heading = array()) {
        // $this->controller->log($columns,"dt_query");
        if ($this->debug) $this->controller->log(array('ServeDataTables: ',
                                                        $table,
                                                        $columns,
                                                        $args));
          if ($this->debug) $this->controller->log($args,"ServeDataTablesArgs");
        /* 
	 * Paging
	 */
        
	$limit = '';
	if ( isset( $args['iDisplayStart'] ) && $args['iDisplayLength'] != '-1' ) {
            $limit = 'LIMIT '. (int) $args['iDisplayStart']. ', '. (int) $args['iDisplayLength'];
	}
 
        
        
        /*
	 * Ordering
	 */
        $order = '';
	if ( isset( $args['iSortCol_0'] ) ) { 
            
            $order = 'ORDER BY  ';
            //check mDataProp for sorting column if set, otherwise use  iSortingCols
            
            if(isset($args['mDataProp_'.$args['iSortCol_0']])){ 
                
                $columns = array_values($columns);
                $columns_heading = array_values($columns_heading);
                
                while ($iheading = current($columns_heading)) {
                    if (strtolower($iheading) == strtolower($args['mDataProp_'.$args['iSortCol_0']])) {
                        $keyindex = key($columns_heading);
                        $order .= $columns[($keyindex)]." ".$args['sSortDir_0'].", ";
                    }
                    next($columns_heading);
                }
                
            }
            else{
                for ( $i=0 ; $i<(int) $args['iSortingCols']; $i++ ) {
                    if ( $args[ 'bSortable_'.(int)$args['iSortCol_'.$i] ] == 'true' ) {
                        if (isset($columns[ (int) $args['iSortCol_'.$i] ])&&is_array($columns[ (int) $args['iSortCol_'.$i] ])) {
                            $order .= $columns[ (int) $args['iSortCol_'.$i] ][0]." ".$args['sSortDir_'.$i].", ";
                        } else {
                            $order .= $columns[ (int) $args['iSortCol_'.$i] ]." ".$args['sSortDir_'.$i].", ";
                        }
                    }
                }
            }
		
            $order = substr_replace( $order, "", -2 );
            if ( $order == 'ORDER BY' ) {
		$order = '';
            }
	}
        
        /* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
        
	$where = '';
	if ( isset($args['sSearch']) && $args['sSearch'] != '' ) {
            $where = 'WHERE (';
            for ( $i=0 ; $i<count($columns) ; $i++ ) {
                if (is_array($columns[$i])) {
                    $where .= $columns[$i][0].' LIKE '.$conn->quote( '%'.$args['sSearch'].'%' ).' OR ';
                } else {
                    $where .= $columns[$i].' LIKE '.$conn->quote( '%'.$args['sSearch'].'%' ).' OR ';
                }
            }
            $where = substr_replace( $where, "", -3 );
            $where .= ')';
	}        
        
        /* Individual column filtering */
	for ( $i=0 ; $i<count($columns) ; $i++ ) {
            if ( isset($args['bSearchable_'.$i]) && $args['bSearchable_'.$i] == 'true' && $args['sSearch_'.$i] != '' ) {
		if ( $where == '' ) {
                    $where = 'WHERE ';
		} else {
                    $where .= ' AND ';
		}
                if (is_array($columns[$i])) {
                    $col = $columns[$i][0];
                } else {
                    $col = $columns[$i];
                }
                $search = $args['sSearch_'.$i];
                if (strtoupper($search) == 'NULL') {
                    $where .= $col.' IS NULL ';
                } else {
                    $where .= $col.' LIKE '.$conn->quote('%'.$search.'%').' ';
                }
            }
	}

        if (!empty($args['where'])) {
            $where .= ($where == '' ? ' WHERE ' : ' AND ') . $args['where'];
        }
        
        if(isset($args['dbColumns']) && is_array($args['dbColumns']))
        {
             foreach($args['dbColumns'] as $col){
                # add to WHERE statement if set and not empty
                if(isset($args[$col]) && $args[$col] != ''){
                    $where .= ($where != '' ? ' AND ' : ' WHERE ') . " $col Like '%". $args[$col] ."%'" ;
                }
            }
            
        }
        
        if (empty($args['groupby'])) {
            $groupby = '';
        } else {
            $groupby = ' GROUP BY '.$args['groupby'];
        }
       
        /*
	 * SQL queries
	 * Get data to display
         * 
         * NOTE: There is a performance issue with SQL_CALC_FOUND_ROWS on large datasets
         *       see http://www.mysqlperformanceblog.com/2007/08/28/to-sql_calc_found_rows-or-not-to-sql_calc_found_rows/
         *       experience here confirms SQL_CALC_FOUND_ROWS on a very large table is dire!!!!
	 */
        
	/*$query = 'SELECT SQL_CALC_FOUND_ROWS '
               . str_replace(' , ', ' ', implode(', ', $columns))
	       . " FROM $table $where $order $limit";
        $this->controller->log($query);
        $result = $this->conn->query($query);*/
	
	/* Data set length after filtering */
	/*$query = 'SELECT FOUND_ROWS()';
        $this->controller->log($query);
        $filteredTotal = $this->conn->query($query)->fetchColumn();*/
        
        
        //This is variable value is used for to decide whether we need to fetch rows count or not on each page loaded.
        $cacheRowsCount = 1000;      
               
	/* Result set after filtering */
        /* 22/11/2012 this DISTINCT KLUGE added because job related files are getting duplicate records
         * added via RMA Tracker API update.  There is no plan/intention for this to be resolved.  
         * Instead it has to be resolved by ignoring these records in the select.  
         * The instruction is to perform this kluge on all files even if they do not suffer fronm this API bug
         * in case they should ever have invalid duplicate rows for whatever reason. What a shambles.
         */
        if (isset($args['DISTINCT'])) {
             $query = 'SELECT DISTINCT ';
               //. str_replace(' , ', ' ', implode(', ', $columns))
               $sep = '';
               foreach($columns as $col) {
                   $query .=  $sep;
                   if (is_array($col)) {
                       $query .=  $col[1];
                   } else {
                       $query .=  $col;
                   }
                   $sep = ', ';
               }
              
	       $query .= " FROM $table $where $groupby $order $limit";           
        } else {
            $query = 'SELECT ';
               //. str_replace(' , ', ' ', implode(', ', $columns))
               $sep = '';
               foreach($columns as $col) {
                   $query .=  $sep;
                   if (is_array($col)) {
                       $query .=  $col[1];
                   } else {
                       $query .=  $col;
                   }
                   $sep = ', ';
               }                    
	       $query .=  " FROM $table $where $groupby $order $limit";
        }
        
        
        
//        echo $query."<BR/>";
        
        
        //$this->controller->log($query,"dt_query");
        if ($this->debug) $this->controller->log('ServeDataTables: '.$query);

        // 26/06/2012 
        // return only indexed result set to client
        $result = $conn->query($query, PDO::FETCH_NUM);
        
        $table_key = md5(trim($table));
	$filter_key = md5(trim($table).trim($where));
        
        // make sure session has been started....
        if (session_id()==='') session_start();
        
        /* Total data set length */
        if (isset($_SESSION[$table_key])) {
            $unfilteredTotal = $_SESSION[$table_key];
        } else {
            $query = "SELECT COUNT(*) FROM $table";
            if (!empty($where)) {
                // exclude application filtering from unfiltered total
                $query .= ' ' . $where;
            }
            
            if($groupby)
            {
                $query = "SELECT COUNT(*) FROM (".$query." ".$groupby.") as temp_table";
            }
            

            if ($this->debug) $this->controller->log('ServeDataTables: '.$query);
            $unfilteredTotal = $conn->query($query)->fetchColumn();
            // only cache unfilteredTotal for large files
            if ($unfilteredTotal > $cacheRowsCount) $_SESSION[$table_key] = $unfilteredTotal;
        }
        
        /*if(!isset($args['iTotalRecords'])  || (isset($args['iTotalRecords']) && isset($args['iTotalRecords'])<$cacheRowsCount))
        {  
            $query = "SELECT COUNT(*) FROM $table $where";
            if ($this->debug) $this->controller->log('ServeDataTables: '.$query);
            $args['iTotalRecords'] = $conn->query($query)->fetchColumn();
        }*/
        
        /* Data set length after filtering */
        if (empty($where)) {
            $filteredTotal = $unfilteredTotal;
        } else {
            if (isset($_SESSION[$filter_key])) {
                $filteredTotal = $_SESSION[$filter_key];
            } else {
                $query = "SELECT COUNT(*) FROM $table $where";
                
                if($groupby)
                {
                    $query = "SELECT COUNT(*) FROM (".$query." ".$groupby.") as temp_table";
                }
                
                
                if ($this->debug) $this->controller->log('ServeDataTables: '.$query);
                $filteredTotal = $conn->query($query)->fetchColumn();
                // only cache filteredTotal for large files
                if ($unfilteredTotal > $cacheRowsCount) $_SESSION[$filter_key] = $filteredTotal;
            }
        }
        
        /*if(!isset($args['iTotalDisplayRecords']) || (isset($args['iTotalDisplayRecords']) && isset($args['iTotalDisplayRecords'])<$cacheRowsCount))
        {    
            $query = "SELECT COUNT(*) FROM $table $where $order";
            if ($this->debug) $this->controller->log('ServeDataTables: '.$query);

            $args['iTotalDisplayRecords'] = $conn->query($query)->fetchColumn();
        }*/
        
        /*
	 * Output
	 */
        if(!isset($args['sEcho'])) {
            $args['sEcho'] = '';
        }
        
	/*$output = array(
		'sEcho' => (int) $args['sEcho'],
		'iTotalRecords' => ($args['iTotalRecords'])?$args['iTotalRecords']:0,
		'iTotalDisplayRecords' => ($args['iTotalDisplayRecords'])?$args['iTotalDisplayRecords']:0,
                'aaData' => $result->fetchAll()
	);*/
        
        //edit by nivas to show associate array for datatables column reorder and its sorting
        if(isset($args['assoc'])){
            $resultData=$result->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
            $resultData=$result->fetchAll();
        }
        $output = array(
		'sEcho' => (int) $args['sEcho'],
		'iTotalRecords' => $unfilteredTotal,
		'iTotalDisplayRecords' => $filteredTotal,
                'aaData' =>  $resultData
	);
        
	
        
        return $output;
    }
       
}

?>
