<?php

/**
 * Short Description of CustomRESTClient.
 * 
 * Long description of CustomRESTClient.
 *
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.0
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version 
 ****************************************************************************/

abstract class CustomRESTClient {
    
    // beginning of docblock template area
    /**#@+
     * @access protected
     */
    protected $controller;
    /**#@-*/
    
    // beginning of docblock template area
    /**#@+
     * @access private
     */
    private $username = null;
    private $password = null;
    private $acceptType = 'json';
    /**#@-*/
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    public function __construct(CustomController $controller) {
        $this->controller = $controller;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @return string $username
     */
    public function getUsername() {
        return $this->username;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @return string $password
     */
    public function getPassword() {
        return $this->password;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }  
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @return string $password
     */
    public function getResponseFormat() {
        return $this->acceptType;
    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $password
     */
    public function setResponseFormat($type) {
        $type = strtolower($type);
        if ($type == 'json' || $type == 'xml') {
            $this->acceptType = $type;
        } else {
            throw new Exception('Unrecognised response format ' . $type);
        }
    }
   
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param string $url
     * @param array $args
     * @param string $verb
     * @return array $response
     */
    protected function execute($url = null, $args = null, $verb = 'GET') {
        
        $ch = curl_init();
	
	try {
            
            if ($this->username !== null && $this->password !== null) {
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
            }
            
            //$data = (isnull($args)) ? json_encode(array()) : json_encode($args);
            
            switch (strtoupper($verb)) {
                
		case 'GET':
                    //if (!is_null($args)) $url .=  '?' . http_build_query($args,'','&',PHP_QUERY_RFC3986);
                    if (!is_null($args)) $url .=  '?' . http_build_query($args);
                    break;
                
		case 'POST':
                    if (!is_null($args)) {
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
                        $this->controller->log('REST Client POST url='.$url.' args='.http_build_query($args),'rest_client_');
                    }
                    curl_setopt($ch, CURLOPT_POST, true);
                    break;
                
                case 'PUT':
                    if (!is_null($args)) {
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
                        $this->controller->log('REST Client PUT url='.$url.' args='.http_build_query($args),'rest_client_');
                    }
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    //curl_setopt($ch, CURLOPT_PUT, true);
                    break;
                
		case 'DELETE':
                    if (!is_null($args)) curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    break;
                
		default:
                    throw new InvalidArgumentException($verb . ' is an invalid REST verb.');
                    
            }
            
            curl_setopt($ch, CURLOPT_TIMEOUT, 120);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array ('Accept: ' . $this->acceptType));
            
            $response = curl_exec($ch);         
            $info = curl_getinfo($ch);
            
            curl_close($ch);
            
            //$this->controller->log(var_export($response,true));
            
            $raw = $response;                                                   /* Store response, (used in some specif API logs */
            
            if (is_array($info)) {
                if (stripos($info['content_type'],'json') !== false) {
                    $response = json_decode($response,true);
                } else if (stripos($info['content_type'],'xml') !== false) {
                    $response = (array) simplexml_load_string($response);
                }
            }
            
            return array ( 'response' => $response, 
                           'info' => $info,
                           'raw' => $raw);
            
	} catch (Exception $e) {
            
            $this->controller->log($e->getMessage());
            
            curl_close($ch);
            throw $e;
	}
        
    }      

    
}

?>
