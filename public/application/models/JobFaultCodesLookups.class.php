<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of JobFaultCodesLookups Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     04/06/2013
 */
class JobFaultCodesLookups extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            "LookupName",
            "Description",
            "ManufacturerID",
            "Status",
	    "ApproveStatus",
            "ExcludeFromBouncerTable",
            "ForcePartFaultCode",
            "PartFaultCodeNo",
            "JobTypeAvailability",
            "PromptForExchange",
            "RelatedPartFaultCode",
            "RepairTypeIDForChargeable",
            "RepairTypeIDForWarranty",
            "RestrictLookup",
            "RestrictLookupTo",
            "ServiceProviderID"
            
        ];
    }

    public function insertJobFaultCodesLookups($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"]=isset($P["Status"])?$P["Status"]:"Active";
	$P["ApproveStatus"]=isset($P["ApproveStatus"])?$P["ApproveStatus"]:"Approved";
        $id = $this->SQLGen->dbInsert('job_fault_code_lookup', $this->fields, $P, true, true);
        return $id;
    }

    public function updateJobFaultCodesLookups($P,$spid) {
       $P["ServiceProviderID"] = $spid;
        $P["Status"]=isset($P["Status"])?$P["Status"]:"Active";
	$P["ApproveStatus"]=isset($P["ApproveStatus"])?$P["ApproveStatus"]:"Approved";
        $id = $this->SQLGen->dbUpdate('job_fault_code_lookup', $this->fields, $P,"JobFaultCodeLookupID=".$P['JobFaultCodeLookupID'], true);
    }

    public function getJobFaultCodesLookupsData($id) {
        $sql = "select * from job_fault_code_lookup where JobFaultCodeLookupID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

    public function deleteJobFaultCodesLookups($id) {
        $sql = "update job_fault_code_lookup set Status='In-Active' where JobFaultCodeLookupID=$id";
        $this->execute($this->conn, $sql);
    }

    ////job_fault_code_lookup functions 

    public function checkIfOrderSection($id) {
        $sql = "select DisplayOrderSection from job_fault_code_lookup where PartStatusID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0]['DisplayOrderSection'];
    }
    
    public function addLinkedItem($p,$c){
        $u=$this->controller->user->UserID;
        $sql="insert into job_fault_code_lookup_restricted_models  (JobFaultCodeLookupID,ServiceProviderModelID,ModifiedUserID,ModifiedDate) values ($p,$c,$u,now())";
        $this->execute($this->conn, $sql);
    }
    public function delLinkedItem($p,$c){
        $sql="delete from job_fault_code_lookup_restricted_models where JobFaultCodeLookupID=$p and ServiceProviderModelID =$c";
        $this->execute($this->conn, $sql);
    }
    public function loadLinkedItem($p){
         $u=$this->controller->user->UserID;
         if($this->controller->user->SuperAdmin==1){
             $t="model";
             $m="ModelID";
         }else{
             $t="service_provider_model";
             $m="ServiceProviderModelID";
         }
        $sql="select am.ServiceProviderModelID as ServiceProviderModelID,concat_ws(' ',ModelName,ModelNumber) as `ItemName`  from job_fault_code_lookup_restricted_models am
                join $t m on m.$m=am.ServiceProviderModelID

            where JobFaultCodeLookupID=$p";
        return $this->Query($this->conn, $sql);
    }
    public function getAllSPLookups($spid,$jobFaultCodeID){
        $sql="select * from job_fault_code_lookup dd where Status='Active' and ServiceProviderID=$spid 
                and JobFaultCodeLookupID not in (select JobFaultCodeLookupID  from job_fault_code_lookup_to_job_fault_code aa where aa.JobFaultCodeID=$jobFaultCodeID)";
        $res = $this->query($this->conn, $sql);
        return $res;
    }
    
    public function fetch($args) {	
        $spId = isset($args['firstArg'])?$args['firstArg']:'';
	$dataStatus = isset($args['secondArg'])?$args['secondArg']:'';	
        $dbTables = "job_fault_code_lookup AS T1 LEFT JOIN manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID";
        $dbTablesColumns = [
            "T1.JobFaultCodeLookupID", 
            "T1.LookupName",
            "T1.Description",
            "T2.ManufacturerName",
            "T1.ExcludeFromBouncerTable", 
            "T1.ForcePartFaultCode",
            "T1.JobTypeAvailability", 
            "T1.PromptForExchange", 
            "T1.Status",
	    "T1.ApproveStatus"
        ];
        $args['where']    = " T1.isDeleted='No' ";
        if($spId != "")
            $args['where'].= " AND T1.ServiceProviderID='".$spId."' ";
	/* Updated By Praveen Kumar N : Show Inactive Records [START] */
	if($dataStatus != "" && $dataStatus == 'Inactive') {
            $args['where'].= " AND T1.Status='".$dataStatus."' ";
	}
	if($dataStatus != "" && $dataStatus == 'Pending') {
            $args['where'].= " AND T1.ApproveStatus='".$dataStatus."' ";
	}
	/* [END] */
        $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
 	return $output;
    }
    
    public function processData($args) {
	if(!isset($args['JobFaultCodeLookupID']) || !$args['JobFaultCodeLookupID']) {
	    return $this->create($args);
        } else {
            return $this->update($args);
        }
    }
    
    public function create($args) {
        $q = [
            "LookupName",
            "Description",
            "ServiceProviderID",
            "ManufacturerID",
            "ExcludeFromBouncerTable",
            "ForcePartFaultCode",
            "PartFaultCodeNo",
            "JobTypeAvailability",
            "PromptForExchange",
            "RelatedPartFaultCode",
            "RepairTypeIDForChargeable",
            "RepairTypeIDForWarranty",
            "RestrictLookup",
            "RestrictLookupTo",
            "Status",
	    "ApproveStatus"
        ];
	/*$q="INSERT INTO job_fault_code_lookup (LookupName, Description, ServiceProviderID, ManufacturerID, ExcludeFromBouncerTable, ForcePartFaultCode, PartFaultCodeNo, 
         * JobTypeAvailability, PromptForExchange, RelatedPartFaultCode, RepairTypeIDForChargeable, RepairTypeIDForWarranty, RestrictLookup, RestrictLookupTo, Status, CreatedDate, CreatedUserID)
	VALUES (:LookupName, :Description, :ServiceProviderID, :ManufacturerID, :ExcludeFromBouncerTable, :ForcePartFaultCode, :PartFaultCodeNo, :JobTypeAvailability, :PromptForExchange, :RelatedPartFaultCode, :RepairTypeIDForChargeable, :RepairTypeIDForWarranty, :RestrictLookup, :RestrictLookupTo, :Status, :CreatedDate, :CreatedUserID)";*/
        
        $values = array("LookupName"=>$args['LookupName'], "Description"=>$args['Description'], "ServiceProviderID"=>$args['ServiceProviderID'], "ManufacturerID"=>$args['ManufacturerID'], "ExcludeFromBouncerTable"=>(!isset($args['ExcludeFromBouncerTable'])?'No':'Yes'), 
        "ForcePartFaultCode"=>(!isset($args['ForcePartFaultCode'])?'No':'Yes'), "PartFaultCodeNo"=>$args['PartFaultCodeNo'], "JobTypeAvailability"=>$args['JobTypeAvailability'], "PromptForExchange"=>(!isset($args['PromptForExchange'])?'No':'Yes'), "RelatedPartFaultCode"=>(!isset($args['RelatedPartFaultCode'])?'No':'Yes'), 
        "RepairTypeIDForChargeable"=>$args['RepairTypeIDForChargeable'], "RepairTypeIDForWarranty"=>$args['RepairTypeIDForWarranty'], "RestrictLookup"=>(!isset($args['RestrictLookup'])?'No':'Yes'), "RestrictLookupTo"=>(!isset($args['RestrictLookupTo'])?'':$args['RestrictLookupTo']), "Status"=>(!isset($args['Status'])?'Active':'Inactive'), "ApproveStatus"=>(!isset($args['ApproveStatus'])?'Approved':'Pending'));
        $id = $this->SQLGen->dbInsert('job_fault_code_lookup', $q, $values, true, true);
        /*if($id > 0)
        {
            $sql="delete from job_fault_code_lookup_restricted_models where JobFaultCodeLookupID='".$id."'";
            $this->execute($this->conn, $sql);
            foreach($args['ModelId'] as $vals)
            {
                $insertSql="insert into job_fault_code_lookup_restricted_models (JobFaultCodeLookupID, ServiceProviderModelID, ModifiedUserID, ModifiedDate) values ('".$id."', '".$vals."', '".$this->controller->user->UserID."', now())";
                $this->execute($this->conn, $insertSql);
            }
        }*/
	if($id) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been inserted successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function update($args) {
	$q="UPDATE job_fault_code_lookup SET LookupName=:LookupName, Description=:Description, ServiceProviderID=:ServiceProviderID, ManufacturerID=:ManufacturerID, 
        ExcludeFromBouncerTable=:ExcludeFromBouncerTable, ForcePartFaultCode=:ForcePartFaultCode, PartFaultCodeNo=:PartFaultCodeNo, JobTypeAvailability=:JobTypeAvailability, 
        PromptForExchange=:PromptForExchange, RelatedPartFaultCode=:RelatedPartFaultCode, RepairTypeIDForChargeable=:RepairTypeIDForChargeable, 
        RepairTypeIDForWarranty=:RepairTypeIDForWarranty, RestrictLookup=:RestrictLookup, RestrictLookupTo=:RestrictLookupTo, Status=:Status, ApproveStatus=:ApproveStatus, ModifiedUserID=:ModifiedUserID, 
        ModifiedDate=:ModifiedDate WHERE JobFaultCodeLookupID=:JobFaultCodeLookupID";
        
	$values = array("LookupName"=>$args['LookupName'], "Description"=>$args['Description'], "ServiceProviderID"=>$args['ServiceProviderID'], "ManufacturerID"=>$args['ManufacturerID'], 
        "ExcludeFromBouncerTable"=>(!isset($args['ExcludeFromBouncerTable'])?'No':'Yes'), "ForcePartFaultCode"=>(!isset($args['ForcePartFaultCode'])?'No':'Yes'), 
        "PartFaultCodeNo"=>$args['PartFaultCodeNo'], "JobTypeAvailability"=>$args['JobTypeAvailability'], "PromptForExchange"=>(!isset($args['PromptForExchange'])?'No':'Yes'), 
        "RelatedPartFaultCode"=>(!isset($args['RelatedPartFaultCode'])?'No':'Yes'), "RepairTypeIDForChargeable"=>$args['RepairTypeIDForChargeable'], 
        "RepairTypeIDForWarranty"=>$args['RepairTypeIDForWarranty'], "RestrictLookup"=>(!isset($args['RestrictLookup'])?'No':'Yes'), 
        "RestrictLookupTo"=>(!isset($args['RestrictLookupTo'])?'':$args['RestrictLookupTo']), "Status"=>(!isset($args['Status'])?'Active':'Inactive'), "ApproveStatus"=>(!isset($args['ApproveStatus'])?'Approved':'Pending'),
        "ModifiedDate"=>date("Y-m-d H:i:s"),"ModifiedUserID"=>$this->controller->user->UserID, "JobFaultCodeLookupID"=>$args['JobFaultCodeLookupID']);
	$query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been updated successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function fetchRow($args) {
        $sql = 'SELECT * FROM job_fault_code_lookup WHERE JobFaultCodeLookupID=:JobFaultCodeLookupID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':JobFaultCodeLookupID' => $args['JobFaultCodeLookupID']));
        $result = $fetchQuery->fetch();
        return $result;
    }
    
    public function deleteJobFaultCodeLookup($id) {
        $q = "update job_fault_code_lookup set isDeleted='Yes' where JobFaultCodeLookupID=:JobFaultCodeLookupID";
	$values = array("JobFaultCodeLookupID" => $id);
        $query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been deleted successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
}
?>
