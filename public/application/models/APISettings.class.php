<?php

/**
 * APISettings.class.php
 * 
 * Database access routines for the Settings API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.10
 * 
 * 01/06/2012  1.00    Andrew J. Williams    Initial Version
 * 23/08/2012  1.01    Andrew J. Williams    TrackerBase VMS Log 29 - Alterations to Skyline API to work with ServiceBase
 * 17/10/2012  1.02    Andrew J. Williams    GetSettings should search for client number on NetworkServiceProvider and default client account numbers and PutSettings should pass all clients for a specif service provider
 * 24/10/2012  1.03    Andrew J. Williams    Changes to PutSettings to allow selectiuon by ServiceCentre id for attached and unasigned clients
 * 26/10/2012  1.04    Andrew J. Williams    Issue 111 - PutSettings Failing on GetUnitTypes
 * 02/11/2012  1.05    Andrew J. Williams    Issue 117 - Altered query for selecting unit types
 * 08/11/2012  1.06    Andrew J. Williams    Issue 124 - APISettings can't find REST Client
 * 28/11/2012  1.07    Andrew J. Williams    In query to get service types extra coning requires in network_service_provider join
 * 19/12/2012  1.08    Andrew J. Williams    Issue 162 - In Settings API only send active records
 * 20/02/2013  1.09    Brian Etherington     Added ServiceTypeName to getSettingsServiceTypes in addition to ServiceType currently
 *                                           expected by Service Base client. Reason for this change is because there are duplicate XML tags 
 *                                           generated which is both confusing and invalid.
 *                                           Note: ServiceType should be removed as soon as Service Base client is updated to use ServiceTypeName.
 * 22/02/2013  1.10    Brian Etherington     Remove ServiceType from getSettingsServiceTypes because it is not used by Service Base after all.
 *****************************************************************************/

require_once('CustomModel.class.php');
include_once ('SkylineRESTClient.class.php');


class APISettings extends CustomModel {
    
    #public $debug = false;
            
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    
    /**
     * Skyline API  ServiceBase.PutSettings
     * API Spec Section 8.1.4.1
     *  
     * This routine will select all the client account numbers belonging to a
     * service provider .
     * 
     * @param $spId     Service Provider ID     
     * 
     * @return Array of responses from ServiceBase
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     **************************************************************************/
    
    public function putSettings($spId) {
        $sql = "
                SELECT DISTINCT
			IF (ISNULL(clt.`AccountNumber`),
				nsp.`AccountNo`
			,
				clt.`AccountNumber`
			) AS `AccountNumber`
		FROM
			`client` clt LEFT JOIN `network_client` nc ON clt.`ClientID` = nc.`ClientID`
			             LEFT JOIN `network_service_provider` nsp ON nc.`NetworkID` = nsp.`NetworkID`
		WHERE
			(
				nsp.`ServiceProviderID` = $spId
				AND ISNULL(clt.`ServiceProviderID`)
			)
			OR clt.`ServiceProviderID` = $spId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        $output = array();
        
        if ($this->debug) $this->controller->log("APISettings::putSettings  Query Response = ".var_export($output,true),"settings_api_");
        
        /* call the put setting API for each client */
        for ($n = 0; $n < count($result); $n++ ) {
            if(isset($result[$n]['AccountNumber'])) {
                $output[$n] = $this->putClientSettings($result[$n]['AccountNumber'],$spId);    
            }
        } /* next $n */
        return($output);
    }
    
    /**
     *  Description
     * 
     * Skyline API  ServiceBase.PutClientSettings
     * API Spec Section 8.1.4.1
     *  
     * The Service Centre will be sent the following settings to update an 
     * existing trade account or insert a new one if it does not exist.
     * 
     * @param $ClientAccountNo  The client account number we wish to put the settongs for.
     *        $spId             The service provider ID we are interested in        
     * 
     * @return getSettings   Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     **************************************************************************/
    
    public function putClientSettings($ClientAccountNo,$spId) {
        $sql = "
                SELECT
			clt.`ClientID` AS `ClientNumber`,
			clt.`AccountNumber` AS `ClientAccountNo`,
			clt.`ClientName` AS `ClientName`,
                        clt.`ClientShortName` AS `ClientShortName`,
			clt.`BuildingNameNumber` AS `ClientBuildingName`,
			clt.`Street` AS `ClientStreet`,
			clt.`LocalArea`  AS `ClientArea`,
			clt.`TownCity` AS `ClientTown`,
			clt.`PostalCode` AS `ClientPostCode`,
			IF (ISNULL(clt.`ContactPhoneExt`) OR clt.`ContactPhoneExt` = '',            -- Is extension blank or null
				clt.`ContactPhone`                                                  -- Yes, so just display phone number
			,
				CONCAT(clt.`ContactPhone`,' ext ', clt.`ContactPhoneExt`)           -- No, so display number and extension  
			) AS `ClientTelephoneNo`,
			clt.`ContactEmail` AS `ClientEmail`,
			vat.`Code` AS `VATCode`,
			clt.`InvoiceNetwork` AS `OnlineInvoicing`,
			clt.`EnableCompletionStatus` As `EnableCompletionStatus`,
			clt.`ForcePolicyNo` AS `ForcePolicyNo`,
			clt.`PromptETD` AS `PromptETD`,
			clt.`TradeSpecificUnitTypes` AS `AccountSpecificUnitTypes`,
			clt.`TradeSpecificManufacturers` AS `AccountSpecificManufacturers`,
                        clt.`TradeSpecificCallTypes` AS `TradeSpecificCallTypes`,
                        clt.`TradeSpecificJobTypes` AS `TradeSpecificJobTypes`,
			CONCAT(sp.`IPAddress`,':',sp.`Port`) AS `Site`,				    -- Need to know as client what site to call
			u.`Username` AS `username`,
			u.`Password` AS `password`
                FROM
			`client` clt LEFT JOIN `vat_rate` vat ON clt.`VATRateID` = vat.`VatRateID`,
			`network_client` nc,
			`network_service_provider` nsp,
			`service_provider` sp,
			`user` u
		WHERE
			clt.`ClientID` = nc.`ClientID`
			  AND nc.`NetworkID` = nsp.`NetworkID`
			    AND nsp.`ServiceProviderID` = sp.`ServiceProviderID`
			      AND sp.`ServiceProviderID` = u.`ServiceProviderID`
			    AND nsp.`ServiceProviderID` = $spId  
			AND 
			(
				clt.`AccountNumber` = '$ClientAccountNo'
			OR
                                clt.`AccountNumber` IS NULL 
				AND nsp.`AccountNo` = '$ClientAccountNo'
			)
                        AND clt.`Status` = 'Active'
               ";
        
        if ($this->debug) $this->controller->log("APISettings::putClientSettings  Query = $sql","settings_api_");
        
        $result = $this->Query($this->conn, $sql);
        
        if ($this->debug) $this->controller->log("APISettings::putClientSettings  Query Response = ".var_export($result,true),"settings_api_");
        
        $output = array();
        
        /* We may have several results if he client is linked to several several service providers */
        for ($n = 0; $n < count($result); $n++ ) {
            
            $rec_unit_type = $this->getSettingsClientUnitTypes($ClientAccountNo,'FieldCallRate','field call');
            //$result[$n]['Account']['UnitTypes'] = array( $this->recordSetSerialise($rec_unit_type, 'UnitType') );

            $rec_unit_type_workshop = $this->getSettingsClientUnitTypes($ClientAccountNo,'WorkshopRate', 'workshop');
                
            for ($w = 0; $w < count($rec_unit_type_workshop); $w++) {

                if (is_array($rec_unit_type)) {

                    for ($f = 0; $f < count($rec_unit_type); $f++) {                            
                        $match = 0;
                        if (
                            ($rec_unit_type[$f]['UnitTypeName'] == $rec_unit_type_workshop[$w]['UnitTypeName']) 
                            && ($rec_unit_type[$f]['RepairSkill'] == $rec_unit_type_workshop[$w]['RepairSkill'])
                            && ($rec_unit_type[$f]['CompletionStatus'] == $rec_unit_type_workshop[$w]['CompletionStatus'])
                        ) {
                            $rec_unit_type['WorkshopRate'] = $rec_unit_type_workshop[$w]['WorkshopRate'];
                            $match = 1;
                            break;
                        } 
                    }
                    if ($match == 0) { 
                        $rec_unit_type[$f+1]['UnitType']['UnitTypeName'] = $rec_unit_type_workshop[$w]['UnitTypeName'];
                        $rec_unit_type[$f+1]['UnitType']['RepairSkill'] = $rec_unit_type_workshop[$w]['RepairSkill'];
                        $rec_unit_type[$f+1]['UnitType']['CompletionStatus'] = $rec_unit_type_workshop[$w]['CompletionStatus'];
                        $rec_unit_type[$f+1]['UnitType']['WorkshopRate'] = $rec_unit_type_workshop[$w]['WorkshopRate'];
                    }
                }
            }
            $result[$n]['UnitTypes'] = json_encode($rec_unit_type);
            $result[$n]['Manufacturers'] = json_encode($this->getSettingsClientManufacturers($ClientAccountNo));
            $result[$n]['ServiceTypes'] = json_encode($this->getSettingsServiceTypes($ClientAccountNo));
            
            $restClient = new SkylineRESTClient($this->controller);
            
            $output[$n] = $restClient->sbPutSettings($result[$n]);
            
            if ( ! $output[$n]['response']['ResponseCode'] == "SC001" ) {
                /* Failue */

                $message = array (
                                'Details' => 'User: '.$result[0]['username'].' SLNumber: '.$output[$n]['response']['SLNumber'].' SCJobNo: '.$output[$n]['response']['SCJobNo'],
                                'API Error code' => $output[$n]['response']['ResponseCode'],
                                'info' => $output[$n]['info']
                                );

                $this->log($message,"settings_api_");

                /* TODO: mail */
            }
        } /* next $n */
        return($output);
    }
    
    /**
      * Description
      * 
      * Skyline API  Skyline.GetSettings
      * API Spec Section 1.1.4.2
      *  
      * Download trade account settings
      * 
      * @param $UserId     UserID (should be got from authorisation)
      *        $where      String containing where cluase for ClientAccountNo if required           
      * 
      * @return getSettings   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      */
    
    public function getSettingsClientAccountNo($UserID, $where) {
        $sql = "
                SELECT DISTINCT
			clt.`ClientID` AS `ClientNumber`,
			IF (ISNULL(clt.`AccountNumber`),
				nsp.`AccountNo` 
			,
				clt.`AccountNumber`) AS `ClientAccountNo`,
			clt.`ClientName` AS `ClientName`,
			clt.`ClientShortName`,
			clt.`BuildingNameNumber` AS `ClientBuildingName`,
			clt.`Street` AS `ClientStreet`,
			clt.`LocalArea`  AS `ClientArea`,
			clt.`TownCity` AS `ClientTown`,
			clt.`PostalCode` AS `ClientPostCode`,
			IF (ISNULL(clt.`ContactPhoneExt`) OR clt.`ContactPhoneExt` = '',            -- Is extension blank or null
				clt.`ContactPhone`                                                  -- Yes, so just display phone number
			,
				CONCAT(clt.`ContactPhone`,' ext ', clt.`ContactPhoneExt`)           -- No, so display number and extension  
			) AS `ClientTelephoneNo`,
			clt.`ContactEmail` AS `ClientEmail`,
			vat.`Code` AS `VATCode`,
			clt.`InvoiceNetwork` AS `OnlineInvoicing`,
			clt.`EnableCompletionStatus` AS `EnableCompletionStatus`,
			clt.`ForcePolicyNo` AS `ForcePolicyNo`,
			clt.`PromptETD` AS `PromptETD`,
			clt.`TradeSpecificUnitTypes` AS `TradeSpecificUnitTypes`,
			clt.`TradeSpecificManufacturers` AS `TradeSpecificManufacturers`,
			clt.`TradeSpecificCallTypes`,
			clt.`TradeSpecificJobTypes`
                FROM
			`client` clt LEFT JOIN `vat_rate` vat ON clt.`VATRateID` = vat.`VatRateID`,
			`service_provider` sp,
			`user` u,
			`network` n LEFT JOIN `client` dc ON n.`DefaultClientID` = dc.`ClientID`,
			`network_client` nc,
			`network_service_provider` nsp 
		WHERE
			(			
				u.`ServiceProviderID` = nsp.`ServiceProviderID`			-- Service centre
				AND nsp.`NetworkID` = nc.`NetworkID`				-- belongs to network
				AND nsp.`NetworkID` = n.`NetworkID`
				AND nc.`ClientID` = clt.`ClientID`				-- has clients
				AND 
				(
					ISNULL(clt.`ServiceProviderID`)				-- which are not assigned to a service centre
					OR u.`ServiceProviderID` = clt.`ServiceProviderID`	-- or are assigned to the current user's service centre
				)
			)
                        AND u.`UserID` = $UserID
                        AND clt.`Status` = 'Active'    
			$where
               ";
               
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }
    
     /**
      * Description
      * 
      * Skyline API ServiceBase.PutSettings 
      *             Skyline.GetSettings
      * API Spec Section 8.1.4.1
      *          Section 8.1.4.2
      *  
      * Get the UnitTypes for a specific client
      * 
      * @param $ClientAccountNo   The account number of the client we are interested in
      *                           (Got from previous query)
      *        $type       Field or Workshop (xml has different fields depending type - see spec)
      *                    enum (field call, workshop)       
      * 
      * @return getSettingsClientUnitTypes   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      *************************************************************************/
    
    public function getSettingsClientUnitTypes($ClientAccountNo, $fName, $jSite) {
        $sql = "
                SELECT DISTINCT
                       ut.`UnitTypeName` AS `UnitTypeName`,
                        rs.`RepairSkillName` AS `RepairSkill`,
                        IF (ISNULL(cs.`CompletionStatus`),
                                'COMPLETE JOB'
                        ,
                                cs.`CompletionStatus`
                        ) AS `CompletionStatus`,
                        IF (ISNULL(ups.`ServiceRate`),
                                0.00
                        ,
                                ups.`ServiceRate`
                        ) AS `$fName`
                FROM
                        `unit_type` ut LEFT JOIN `unit_client_type` uct ON ut.UnitTypeID = uct.UnitTypeID AND uct.`Status` = 'Active'
                                        LEFT JOIN `repair_skill` rs ON ut.`RepairSkillID` = rs.`RepairSkillID`
                                        LEFT JOIN `client` clt ON uct.`ClientID` = clt.`ClientID`
                                        LEFT JOIN `unit_pricing_structure` ups ON uct.ClientID = ups.ClientID AND ut.UnitTypeID = ups.UnitTypeID
                                        LEFT JOIN `completion_status` cs ON ups.`CompletionStatusID` = cs.`CompletionStatusID`
                                        LEFT JOIN `network_client` nc ON clt.`ClientID` = nc.`ClientID`
                                        LEFT JOIN `network_service_provider` nsp ON nc.`NetworkID` = nsp.`NetworkID`
                                        LEFT JOIN `network` n ON nsp.NetworkID = n.NetworkID
                WHERE
                        (
                                ups.`JobSite` = '$jSite'
                                OR ISNULL(ups.`JobSite`)
                        )
                        AND
                        (
                                clt.`AccountNumber` = '$ClientAccountNo'
                                OR
                                (
                                        nsp.`AccountNo`  = '$ClientAccountNo'
                                        AND n.DefaultClientID = clt.ClientID
                                )
                        )
              ";
                            
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }    
    
    
    /**
     * Skyline API  Skyline.GetSettings
     *              Skyline.PutSettings
     * API Spec Section 8.1.4.1
     *          Section 8.1.4.2 
     * 
     * Get the Service Types for a specific client
     * 
     * @param $ClientAccountNo   The account number of the client we are interested in
     *                           (Got from previous query)       
     * 
     * @return Associative array containing recordset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function getSettingsServiceTypes($ClientAccountNo) {
        $sql = "
                SELECT
                        st.`ServiceTypeName` AS `ServiceTypeName`,
			jt.`Type` AS `JobType`,
			st.`Code` AS `ServiceTypeCode`,
			st.`Chargeable`,
			st.`Warranty`,
			st.`DateOfPurchaseForced`,
			st.`OverrideTradeSettingsPolicy`,
			st.`ForcePolicyNoAtBooking`,
			st.`OverrideTradeSettingsModel`,
			st.`ForceModelNumber`,
			clt.`ClientID`
                FROM
			(`service_type` st LEFT JOIN `service_type_alias` sta ON st.`ServiceTypeID` = sta.`ServiceTypeID` AND sta.`Status` = 'Active')
			                   LEFT JOIN `client` clt ON sta.`ClientID` = clt.`ClientID`
                                           LEFT JOIN `network_client` nc ON clt.`ClientID` = nc.`ClientID`
                                           LEFT JOIN `network_service_provider` nsp ON nc.`NetworkID` = nsp.`NetworkID` and nsp.`AccountNo` = '$ClientAccountNo'
                                           LEFT JOIN `network` n ON nsp.NetworkID = n.NetworkID,
			`job_type` jt           
                WHERE
			st.`JobTypeID` = jt.`JobTypeID`
			AND
                        (
                                clt.`AccountNumber` = '$ClientAccountNo'
                                OR
                                (
                                        nsp.`AccountNo`  = '$ClientAccountNo'
                                        AND n.DefaultClientID = clt.ClientID
                                )
                        )
              ";
                            
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }

    
     /**
      * Description
      * 
      * Skyline API  Skyline.GetSettings
      * API Spec Section 1.1.4.2
      *  
      * Get the Manufacturers for a specific client
      * 
      * @param $ClientAccountNo   The account number of the client we are interested in
      *                           (Got from previous query)
      *
      * @return getSettingsClientManufacturers   Associative array containing recordset
      * 
      * @author Andrew Williams <a.williams@pccsuk.com>  
      */
    
    public function getSettingsClientManufacturers($ClientAccountNo) {
        $sql = "
                SELECT DISTINCT
			mft.`ManufacturerName` AS `ManufacturerName`
		FROM
			`client` clt,
			`network_client` net_clt,
			`network_manufacturer` net_mft,
			`manufacturer` mft,
                        `network` n,
                        `network_service_provider` nsp
		WHERE
			clt.`ClientID` = net_clt.`ClientID`			-- Client has networks
			  AND net_clt.`NetworkID` = net_mft.`NetworkID` AND net_mft.`Status` = 'Active'
			    AND net_mft.`ManufacturerID` = mft.`ManufacturerID`	-- Network has manucturers
                          AND net_clt.`NetworkID` = nsp.`NetworkID`
                          AND nsp.`NetworkID` = n.`NetworkID`
			AND 
                        (
                                clt.`AccountNumber` = '$ClientAccountNo'
                                OR
                                (
                                        nsp.`AccountNo`  = '$ClientAccountNo'
                                        AND n.DefaultClientID = clt.ClientID
                                )
                        )
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result);
    }

}

?>
