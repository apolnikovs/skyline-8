<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');

class SimpleAPI extends CustomModel {

    private $conn;
    public $config;
    
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( 
	    $this->controller->config['DataBase']['Conn'],
	    $this->controller->config['DataBase']['Username'],
	    $this->controller->config['DataBase']['Password'] 
	);  
        
	$this->config = $this->controller->readConfig('application.ini');
  
    }
    
    
    
    public function validateUser($data) {
	
	$hash = md5($data["Password"] . "|sKyLiNe|" . strrev($data["Password"]));
	
	$q = "SELECT * FROM user WHERE Username = :userName AND Password = :password";
	$values = ["userName" => $data["Username"], "password" => $hash];
	$result = $this->query($this->conn, $q, $values);
	if(count($result) == 1) {
	    session_start();
	    $_SESSION["UserID"] = $result[0]["Username"];
	    $_SESSION["Pwd"] = $data["Password"];
	    return true;
	} else {
	    return false;
	}
	
    }
    
    
    
    public function updateSBRA($job) {
	
	if(!$job["ServiceProviderID"]) {
	    $this->controller->log("SimpleAPI->updateSBRA Service Provider ID not found");
	    return false;
	}
	
	$syncEnabled = self::getSBRASyncEnabled($job["ServiceProviderID"]);
	if(!$syncEnabled) {
	    $this->controller->log("SimpleAPI->updateSBRA Sync is not enabled");
	    return false;
	}
	
	$url = self::getSBURL($job["ServiceProviderID"]);
	
	if(!$url) {
	    $this->controller->log("SimpleAPI->updateSBRA URL is not found");
	    return false;
	}
	
	$url .= "/RAUpdate";
	
	$record = self::getLastRARecord($job["JobID"]);
	$name = isset($_SESSION["EngineerName"]) ? $_SESSION["EngineerName"] : "";
	$login = self::getSPLogin($job["ServiceProviderID"]);
	
	if(!$login) {
	    $this->controller->log("SimpleAPI->updateSBRA Login lookup has failed");
	    return false;
	}
	
	$data = [
	    "SBJobNo" => $job["ServiceCentreJobNo"],
	    "SLJobNo" => $job["JobID"],
	    "RAAction" => "RA Update",
	    "RANotes" => $record["Notes"],
	    "SLUsername" => $name,
	    "SBStatus" => $record["SBAuthorisationStatus"]
	];
	
	$config = [
	    CURLOPT_POST => 1,
	    CURLOPT_USERPWD => $login["Username"] . ":" . $login["Password"],
	    CURLOPT_HEADER => 0,
	    CURLOPT_URL => $url,
	    CURLOPT_FRESH_CONNECT => 1,
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_FORBID_REUSE => 1,
	    CURLOPT_TIMEOUT => 120,
	    CURLOPT_POSTFIELDS => http_build_query($data)
	];
	
	$curl = curl_init();
	curl_setopt_array($curl, $config);
	if(!$result = curl_exec($curl)) {
	   throw new Exception("SimpleAPI RA POST to Service Base failed.");
	}
	curl_close($curl);
	
	return $result;
	
    }
    
    
    
    public function getSBURL($serviceProviderID) {
	
	$q = "SELECT CONCAT(sp.IPAddress, ':', sp.Port) AS url FROM service_provider AS sp WHERE sp.ServiceProviderID = :spID";
	$values = ["spID" => $serviceProviderID];
	$result = $this->query($this->conn, $q, $values);
	return (isset($result[0]["url"]) ? $result[0]["url"] : null);
	
    }
    
    
    
    public function getLastRARecord($jobID) {
	
	$q = "	SELECT	    *

		FROM	    ra_history AS rh

		LEFT JOIN   ra_status AS rs ON rh.RAStatusID = rs.RAStatusID
		LEFT JOIN   authorisation_types AS at ON rs.AuthorisationTypeID = at.AuthorisationTypeID
		LEFT JOIN   sb_authorisation_statuses AS sbas ON rs.SBAuthorisationStatusID = sbas.SBAuthorisationStatusID

		WHERE	    rh.JobID = :jobID

		ORDER BY    rh.ModifiedDate DESC 

		LIMIT	    1
	     ";
	
	$values = ["jobID" => $jobID];
	
	$result = $this->query($this->conn, $q, $values);
	
	return (isset($result[0]) ? $result[0] : null);
	
    }
    
    
    
    public function getSPLogin($serviceProviderID) {
	
	$q = "	SELECT	Username,
			Password
		FROM	user
		WHERE	ServiceProviderID = :spID
	     ";
	$values = ["spID" => $serviceProviderID];
	$this->controller->log("SimpleAPI->getSPLogin Service Provider ID: " . $serviceProviderID);
	$result = $this->query($this->conn, $q, $values);
	$this->controller->log("SimpleAPI->getSPLogin Result: " . var_export($result, true));
	return (isset($result[0]) ? $result[0] : null);
	
    }
    
    
    
    public function getSBRASyncEnabled($serviceProviderID) {
	
	$q = "SELECT SBRASync FROM service_provider WHERE ServiceProviderID = :spID";
	$values = ["spID" => $serviceProviderID];
	$result = $this->query($this->conn, $q, $values);
	if(count($result) == 1 && isset($result[0]["SBRASync"]) && $result[0]["SBRASync"] == "Yes") {
	    return true;
	} else {
	    return false;
	}
	
    }
    
    
    
    public function getJobID($data) {
	
	$q = "SELECT COUNT(*) FROM job WHERE JobID = :jobID";
	$values = ["jobID" => $data["SLJobNo"]];
	$result = $this->query($this->conn, $q, $values);
	if (count($result) == 1) {
	    return $data["SLJobNo"];
	} else {
	    $q = "SELECT JobID FROM job WHERE ServiceCentreJobNo = :sbID";
	    $values = ["sbID" => $data["SBJobNo"]];
	    $result = $this->query($this->conn, $q, $values);
	    return isset($result[0]["JobID"]) ? $result[0]["JobID"] : null;
	}
	
    }    
    
    
    
}
?>