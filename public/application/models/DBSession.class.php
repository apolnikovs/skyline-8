<?php
require_once('Session.class.php');

class DBSession extends Session {  
    
    private $conn;
    private $select = null;
    private $insert = null;
    private $delete = null;
    private $clean = null;
    
    /* =======================================================================
     * Session Table DDL
     * 
     * CREATE TABLE sessions (    
     *   id varchar(32) NOT NULL,    
     *   access int(10) unsigned,    
     *   data text,   
     *   PRIMARY KEY (id)
     * );
     * 
     */
    
    public function  __construct($Controller) {
    
        $result = session_set_save_handler( 
                                  array($this, '_open'),                         
                                  array($this, '_close'),   
                                  array($this, '_read'),
                                  array($this, '_write'),                         
                                  array($this, '_destroy'),                         
                                  array($this, '_clean') );
               
        parent::__construct($Controller);            
        
    }
    
    public function __destruct() {
        //$this->controller->log('call _destruct...');
        session_write_close();
    }
    
    public function _open($save_path, $session_name) { 
        
        //$this->controller->log('call _open '.$save_path.' / '.$session_name);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        return true;
        
    } 
    
    public function _close() {
        
        //$this->controller->log('call _close...');
        
        return true;
    }
    
    public function _read($id) { 
        
        //$this->controller->log('call _read...');
        
        if ($this->select == null) {
            $this->select = $this->conn->prepare('SELECT data FROM sessions WHERE id = :id');
        }
        
        $this->select->bindValue (':id', $id, PDO::PARAM_INT); 
        $this->select->execute();
        
        $rows = $this->select->fetchAll();
     
        return empty($rows) ? false : $rows[0]['data'] ;
        
    }
    
    public function _write($id, $data) {
        
        //$this->controller->log('call _write...');
  
        $access = time(); 
           
        if ($this->insert == null) {
            $this->insert = $this->conn->prepare("insert into sessions (id, access, data) values(:id, :access, :data) on duplicate key update access=:access, data=:data;"); 
        };
        
        $this->insert->bindValue (':id', $id, PDO::PARAM_INT);
        $this->insert->bindValue (':access', $access, PDO::PARAM_INT);
        $this->insert->bindValue (':data', $data);
        
        return $this->insert->execute();
              
    }
    
    public function _destroy($id) {
        
        //$this->controller->log('call _destroy...');
        
        if ($this->delete == null) {
            $this->delete = $this->conn->prepare('DELETE FROM sessions WHERE id = :id');
        }
        
        $this->delete->bindValue (':id', $id, PDO::PARAM_INT);        
        return $this->delete->execute();         
        
   }
   
   public function _clean($max) {   
       
       $this->controller->log('call _clean...');
    
        $old = time() - $max;      
       
        if ($this->clean == null) {
            $this->clean = $this->conn->prepare('DELETE FROM sessions WHERE access < :old');
        }
        
        $this->clean->bindValue (':old', $old, PDO::PARAM_INT);        
        return $this->clean->execute(); 
   }
    
}
?>
