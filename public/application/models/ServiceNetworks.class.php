<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Service Networks Page in Organisation Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class ServiceNetworks extends CustomModel {
    
    private $conn;
    private $dbColumns = ['NetworkID', 'CompanyName', "SendASCReport", "SendNetworkReport", 'CreditLevel',  'ReorderLevel', 'Status'];
    private $table = "network";
    
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
    /*
    * Description
    * 
    * This method is for fetching data from database
    * 
    * @param array $args Its an associative array contains where clause, limit and order etc.
    * @global $this->conn
    * @global $this->tables
    * @global $this->dbColumns
    * @return array 
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */  
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
        $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        
       
        return  $output;
        
    }
    
    
    
    /*
    * Description
    * 
    * This method calls update method if the $args contains primary key.
    * 
    * @param array $args Its an associative array contains all elements of submitted form.
    * @return array It contains status and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
    */
    
    public function processData($args) {

	if(!isset($args['NetworkID']) || !$args['NetworkID']) {
	    return $this->create($args);
	} else {
	    return $this->update($args);
	}

    }
    
    
     
     /*
     * Description
     * 
     * This method is used for to validate name.
     *
     * @param integer $CompanyName  
     * @param integer $NetworkID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($CompanyName, $NetworkID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT NetworkID FROM '.$this->table.' WHERE CompanyName=:CompanyName  AND NetworkID!=:NetworkID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':CompanyName' => $CompanyName, ':NetworkID' => $NetworkID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['NetworkID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to deduct CreditLevel by 1 and returns CreditLevel and ReorderLevel as array after deduction .
     *
     
     * @param integer $NetworkID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function CreditDeduction($NetworkID) {
        
         
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT NetworkID, CreditLevel, ReorderLevel, ContactEmail, CreditLevelMailSentOn FROM '.$this->table.' WHERE NetworkID=:NetworkID AND Status=:Status';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':Status' => 'Active', ':NetworkID' => $NetworkID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result))
        {   
            $result['CreditLevel'] = $result['CreditLevel']-1;
            
            $result['CreditLevelSendMail'] = 0;
            if($result['CreditLevel']<$result['ReorderLevel'] && $result['CreditLevelMailSentOn']<date("Y-m-d"))
            {
                $result['CreditLevelSendMail'] = 1;
                $result['CreditLevelMailSentOn'] = date("Y-m-d");
            }
            
            
            //Deducting CreditLevel by 1.
            $sql2 = 'UPDATE '.$this->table.' SET CreditLevel=:CreditLevel, CreditLevelMailSentOn=:CreditLevelMailSentOn WHERE NetworkID=:NetworkID';
            $updateQuery = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $updateQuery->execute(array(':CreditLevel' => $result['CreditLevel'], ':CreditLevelMailSentOn' => $result['CreditLevelMailSentOn'], ':NetworkID' => $NetworkID));
            
            
        }
        
        return $result;
    
    }
    
    
    
    /*
    * Description
    * 
    * This method is used for to insert data into database.
    *
    * @param array $args  
    * @global $this->table 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function create($args) {
        
        $sql = 'INSERT INTO ' . $this->table . ' 
			    (
			    CompanyName, 
			    BuildingNameNumber, 
			    PostalCode, 
			    Street, 
			    LocalArea, 
			    TownCity, 
			    CountyID, 
			    CountryID, 
			    ContactPhone, 
			    ContactPhoneExt, 
			    ContactEmail, 
			    CreditLevel, 
			    ReorderLevel, 
			    CreditPrice, 
			    VATRateID, 
			    VATNumber, 
			    InvoiceCompanyName, 
			    SageReferralNominalCode, 
			    SageLabourNominalCode, 
			    SagePartsNominalCode, 
			    SageCarriageNominalCode, 
			    Status,
			    ServiceManagerForename,
			    ServiceManagerSurname,
			    AdminSupervisorForename,
			    AdminSupervisorSurname,
			    ServiceManagerEmail,
			    AdminSupervisorEmail,
			    NetworkManagerForename,
			    NetworkManagerSurname,
			    NetworkManagerEmail,
			    SendASCReport,
			    SendNetworkReport

			    ) VALUES (
			    
			    :CompanyName, 
			    :BuildingNameNumber, 
			    :PostalCode, 
			    :Street, 
			    :LocalArea, 
			    :TownCity, 
			    :CountyID, 
			    :CountryID, 
			    :ContactPhone, 
			    :ContactPhoneExt, 
			    :ContactEmail, 
			    :CreditLevel, 
			    :ReorderLevel, 
			    :CreditPrice, 
			    :VATRateID, 
			    :VATNumber, 
			    :InvoiceCompanyName, 
			    :SageReferralNominalCode, 
			    :SageLabourNominalCode, 
			    :SagePartsNominalCode, 
			    :SageCarriageNominalCode, 
			    :Status
			    :ServiceManagerForename,
			    :ServiceManagerSurname,
			    :AdminSupervisorForename,
			    :AdminSupervisorSurname,
			    :ServiceManagerEmail,
			    :AdminSupervisorEmail,
			    :NetworkManagerForename,
			    :NetworkManagerSurname,
			    :NetworkManagerEmail,
			    :SendASCReport,
			    :SendNetworkReport
			    )';
       
        if($this->isValid($args['CompanyName'], 0)) {
            
            $insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            
            $insertQuery->execute([
                ':CompanyName' => $args['CompanyName'], 
                ':BuildingNameNumber' => $args['BuildingNameNumber'], 
                ':PostalCode' => $args['PostalCode'],
                ':Street' => $args['Street'],
                ':LocalArea' => $args['LocalArea'],
                ':TownCity' => $args['TownCity'],
                ':CountyID' => ($args['CountyID'] == '') ? NULL : $args['CountyID'],
                ':CountryID' => ($args['CountryID'] == '') ? NULL : $args['CountryID'],
                ':ContactPhone' => $args['ContactPhone'],
                ':ContactPhoneExt' => $args['ContactPhoneExt'],
                ':ContactEmail' => $args['ContactEmail'],
                ':CreditLevel' => $args['CreditLevel'],
                ':ReorderLevel' => $args['ReorderLevel'],
                ':CreditPrice' => $args['CreditPrice'],
                ':VATRateID' => ($args['VATRateID'] == '') ? NULL : $args['VATRateID'],
                ':VATNumber' => $args['VATNumber'],
                ':InvoiceCompanyName' => $args['InvoiceCompanyName'],
                ':SageReferralNominalCode' => $args['SageReferralNominalCode'],
                ':SageLabourNominalCode' => $args['SageLabourNominalCode'],
                ':SagePartsNominalCode' => $args['SagePartsNominalCode'],
                ':SageCarriageNominalCode' => $args['SageCarriageNominalCode'],
                ':Status' => $args['Status'],
		":ServiceManagerForename" => $args["ServiceManagerForename"],
		":ServiceManagerSurname" => $args["ServiceManagerSurname"],
		":AdminSupervisorForename" => $args["AdminSupervisorForename"],
		":AdminSupervisorSurname" => $args["AdminSupervisorSurname"],
		":ServiceManagerEmail" => $args["ServiceManagerEmail"],
		":AdminSupervisorEmail" => $args["AdminSupervisorEmail"],
		":NetworkManagerForename" => $args["NetworkManagerForename"],
		":NetworkManagerSurname" => $args["NetworkManagerSurname"],
		":NetworkManagerEmail" => $args["NetworkManagerEmail"],
		":SendASCReport" => $args["SendASCReport"],
		":SendNetworkReport" => $args["SendNetworkReport"]
	    ]);
        
	    return ['status' => 'OK',
		    'message' => $this->controller->page['Text']['data_inserted_msg']
		   ];
        
	} else {
            
            return ['status' => 'ERROR',
                    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)];
	    
        }
	
    }
    
    
    
    /*
    * Description
    * 
    * This method is used for to fetch a row from database.
    *
    * @param array $args
    * @global $this->table  
    * @return array It contains row of the given primary key.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function fetchRow($args) {
        
        $sql =	'SELECT	NetworkID, 
			CompanyName, 
			BuildingNameNumber, 
			PostalCode, 
			Street, 
			LocalArea, 
			TownCity, 
			CountyID, 
			CountryID, 
			ContactPhone, 
			ContactPhoneExt,
			ContactEmail, 
			CreditLevel, 
			ReorderLevel, 
			CreditPrice, 
			VATRateID, 
			VATNumber, 
			InvoiceCompanyName, 
			SageReferralNominalCode, 
			SageLabourNominalCode, 
			SagePartsNominalCode,
			SageCarriageNominalCode, 
			Status,
			ServiceManagerForename,
			ServiceManagerSurname,
			ServiceManagerEmail,
			AdminSupervisorForename,
			AdminSupervisorSurname,
			AdminSupervisorEmail,
			NetworkManagerForename,
			NetworkManagerSurname,
			NetworkManagerEmail,
			SendASCReport,
			SendNetworkReport
			    
		FROM	' . $this->table . ' 
		    
		WHERE	NetworkID = :NetworkID';
	
        $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        
        $fetchQuery->execute([':NetworkID' => $args['NetworkID']]);
        $result = $fetchQuery->fetch();
        
        return $result;
	
    }
    
    
    
    /*
    * Description
    * 
    * This method is used for to udpate a row into database.
    *
    * @param array $args
    * @global $this->table   
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function update($args) {
        
        if($this->isValid($args['CompanyName'], $args['NetworkID'])) {        
            
	    $sql = 'UPDATE  ' . $this->table . ' 
		
		    SET	    CompanyName = :CompanyName, 
			    BuildingNameNumber = :BuildingNameNumber, 
			    PostalCode = :PostalCode, 
			    Street = :Street, 
			    LocalArea = :LocalArea, 
			    TownCity = :TownCity, 
			    CountyID = :CountyID, 
			    CountryID = :CountryID, 
			    ContactPhone = :ContactPhone, 
			    ContactPhoneExt = :ContactPhoneExt, 
			    ContactEmail = :ContactEmail, 
			    CreditLevel = :CreditLevel, 
			    ReorderLevel = :ReorderLevel, 
			    CreditPrice = :CreditPrice, 
			    VATRateID = :VATRateID, 
			    VATNumber = :VATNumber, 
			    InvoiceCompanyName = :InvoiceCompanyName, 
			    SageReferralNominalCode = :SageReferralNominalCode, 
			    SageLabourNominalCode = :SageLabourNominalCode, 
			    SagePartsNominalCode = :SagePartsNominalCode,
			    SageCarriageNominalCode = :SageCarriageNominalCode, 
			    Status = :Status,
			    ServiceManagerForename = :ServiceManagerForename,
			    ServiceManagerSurname = :ServiceManagerSurname,
			    AdminSupervisorForename = :AdminSupervisorForename,
			    AdminSupervisorSurname = :AdminSupervisorSurname,
			    ServiceManagerEmail = :ServiceManagerEmail,
			    AdminSupervisorEmail = :AdminSupervisorEmail,
			    NetworkManagerForename = :NetworkManagerForename,
			    NetworkManagerSurname = :NetworkManagerSurname,
			    NetworkManagerEmail = :NetworkManagerEmail,
			    SendASCReport = :SendASCReport,
			    SendNetworkReport = :SendNetworkReport
			    
		    WHERE   NetworkID = :NetworkID';
        
	    $updateQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	    
	    $updateQuery->execute([
		':CompanyName' => $args['CompanyName'], 
		':BuildingNameNumber' => $args['BuildingNameNumber'], 
		':PostalCode' => $args['PostalCode'],
		':Street' => $args['Street'],
		':LocalArea' => $args['LocalArea'],
		':TownCity' => $args['TownCity'],
		':CountyID' => ($args['CountyID'] == '') ? NULL : $args['CountyID'],
		':CountryID' => ($args['CountryID'] == '') ? NULL : $args['CountryID'],
		':ContactPhone' => $args['ContactPhone'],
		':ContactPhoneExt' => $args['ContactPhoneExt'],
		':ContactEmail' => $args['ContactEmail'],
		':CreditLevel' => $args['CreditLevel'],
		':ReorderLevel' => $args['ReorderLevel'],
		':CreditPrice' => $args['CreditPrice'],
		':VATRateID' => ($args['VATRateID'] == '') ? NULL : $args['VATRateID'],
		':VATNumber' => $args['VATNumber'],
		':InvoiceCompanyName' => $args['InvoiceCompanyName'],
		':SageReferralNominalCode' => $args['SageReferralNominalCode'],
		':SageLabourNominalCode' => $args['SageLabourNominalCode'],
		':SagePartsNominalCode' => $args['SagePartsNominalCode'],
		':SageCarriageNominalCode' => $args['SageCarriageNominalCode'],
		':Status' => $args['Status'],
		':NetworkID' => $args['NetworkID'],
		":ServiceManagerForename" => $args["ServiceManagerForename"],
		":ServiceManagerSurname" => $args["ServiceManagerSurname"],
		":AdminSupervisorForename" => $args["AdminSupervisorForename"],
		":AdminSupervisorSurname" => $args["AdminSupervisorSurname"],
		":ServiceManagerEmail" => $args["ServiceManagerEmail"],
		":AdminSupervisorEmail" => $args["AdminSupervisorEmail"],
		":NetworkManagerForename" => $args["NetworkManagerForename"],
		":NetworkManagerSurname" => $args["NetworkManagerSurname"],
		":NetworkManagerEmail" => $args["NetworkManagerEmail"],
		":SendASCReport" => $args["SendASCReport"],
		":SendNetworkReport" => isset($args["SendNetworkReport"]) ? $args["SendNetworkReport"] : null
	    ]);
        
	    return ['status' => 'OK',
		    'message' => $this->controller->page['Text']['data_updated_msg']
		   ];
	       
        } else {
	    
	    return ['status' => 'ERROR',
                    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)
		   ];
	     
        }
	
    }
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    
    
    /**
     * getNetworkId
     *  
     * Get the NetworkID from the name of a network
     * 
     * @param string $nName    The comany name of the network
     * 
     * @return integer  Network ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getNetworkId($nName) {
        $sql = "
                SELECT
			`NetworkID`
		FROM
			`network`
		WHERE
			`CompanyName` = '$nName'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['NetworkID']);                                     /* Network exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    //Returns network table array
    //2012-01-23 © Vic <v.rutkunas@pccsuk.com>
    
    public function getAllNetworks() {
	
	$q = "SELECT * FROM network";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }
    
    
    
}
?>