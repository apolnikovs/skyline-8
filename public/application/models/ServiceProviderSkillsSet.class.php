<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Service Provider Skills Set in Appointment Diary section under Site Map
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.03
 * @Date         18/09/2012
 * 
 * Changes
 * Date        Version Author                Reason
 * 18/09/2012  1.00    Nageswara Rao Kanteti Initial Version
 * 22/10/2012  1.01    Andrew J. Williams    Added getIdByServiceProviderSkillset
 * 26/11/2012  1.02    Andris Polnikovs      addet geotag module working version
 * 21/02/2012  1.03    Andrew J. Williams    Added getServiceProviderSkillsetFromIds
 ******************************************************************************/

class ServiceProviderSkillsSet extends CustomModel {
    
    private $conn;
    
    private $table     = "service_provider_skillset";
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    
    
    
        /**
        * Description
        * 
        * This method is for fetching data from database
        * 
        * @param array $args Its an associative array contains where clause, limit and order etc.
        * @global $this->conn
        
       
        * @return array 
        * 
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        */  
    
        public function fetch($args) {

            $ServiceProviderID = isset($args['firstArg'])?$args['firstArg']:false;
           
            if($ServiceProviderID)
            {
                $tables = "`skillset` AS T1
            LEFT JOIN appointment_type AS T2 ON T1.AppointmentTypeID = T2.AppointmentTypeID
            LEFT JOIN service_provider_skillset AS T3 ON T1.SkillsetID = T3.SkillsetID AND T3.Status='Active' 
            LEFT JOIN repair_skill AS T4 ON T1.RepairSkillID = T4.RepairSkillID";
                
                $args['where'] = "T3.ServiceProviderID='".$ServiceProviderID."' AND T3.SkillsetID IS NOT NULL AND T1.Status='Active' GROUP BY T1.RepairSkillID";
                $output = $this->ServeDataTables($this->conn, $tables, array("T1.RepairSkillID", "T4.RepairSkillName"), $args);
                
                return  $output;
               
            }

          
        }
    
    
    
    
   
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
        $this->log($args,"rrrr");
         if(isset($args['SkillsAreaMap']) && $args['SkillsAreaMap'])
         {
             return $this->createSkillsAreaMap($args);
         }   
         else  if(isset($args['GeneralDefaults']))
         {
             
               return $this->updateSPGeneralDefaults($args);
             
         } 
         else
         {
             $successStatus = true;
             
             
             $this->inactiveSkillsSet($args['ServiceProviderID'], $args['RepairSkillID']);
             
             if(isset($args['SkillsetID']) && is_array($args['SkillsetID']))
             {
                 for($i=0;$i<count($args['SkillsetID']);$i++)
                 {
                     
                     $ServiceProviderSkillsetID = $this->isValid($args['ServiceProviderID'], $args['SkillsetID'][$i], 0, true);
                     
                     if($ServiceProviderSkillsetID)
                     {
                         
                         $dataArray = array(

                            'SkillsetID' => $args['SkillsetID'][$i],
                            'ServiceProviderID' => $args['ServiceProviderID'], 
                            'ServiceDuration' => $args['SkillsetIDST_'.$args['SkillsetID'][$i]], 
                            'EngineersRequired' => $args['SkillsetIDER_'.$args['SkillsetID'][$i]], 
                            'Status' => 'Active',
                            'ServiceProviderSkillsetID' => $ServiceProviderSkillsetID
                         );
                        
                         $result = $this->update($dataArray);
                         
                       
                         
                     }   
                     else 
                     {
                         $dataArray = array(

                            'SkillsetID' => $args['SkillsetID'][$i],
                            'ServiceProviderID' => $args['ServiceProviderID'], 
                            'ServiceDuration' => $args['SkillsetIDST_'.$args['SkillsetID'][$i]], 
                            'EngineersRequired' => $args['SkillsetIDER_'.$args['SkillsetID'][$i]], 
                            'Status' => 'Active'
                         );
                        
                         $result = $this->create($dataArray);
                        
                       
                     }
                     
                    if($result['status']=='ERROR')
                    {
                        $successStatus = false;
                    }
                 }
             }
             
             
            if($successStatus)
            {
                    return array('status' => 'OK',
                            'message' => "Your data has been saved successfully.");
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => "Sorry, your data has not been processed properly since there is a problem.  Please check it.");
            }
              
             
            // $this->controller->log(var_export($args, true));
             
         }
     }
     
     
     
     
    
     
     
     
     
    
    
     
     
       /**
     * Description
     * 
     * This method is used for to check whether SkillsetID exists for service provider.
     *
     * @param interger $ServiceProviderID
     * @param interger $SkillsetID
     * @param string   $ServiceProviderSkillsetID
    
     * @global $this->table
     * 
     * @return boolean 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($ServiceProviderID, $SkillsetID, $ServiceProviderSkillsetID, $ReturnID=false) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceProviderSkillsetID FROM service_provider_skillset WHERE ServiceProviderID=:ServiceProviderID AND SkillsetID=:SkillsetID AND ServiceProviderSkillsetID!=:ServiceProviderSkillsetID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':SkillsetID' => $SkillsetID, ':ServiceProviderSkillsetID' => $ServiceProviderSkillsetID));
        $result = $fetchQuery->fetch();
                
        if(is_array($result) && $result['ServiceProviderSkillsetID'])
        {
                if($ReturnID)
                {
                    return $result['ServiceProviderSkillsetID'];
                }
                else
                {    
                    return false;
                }
        }
        
        if($ReturnID)
        {
            return false;
        }
        else
        {    
            return true;
        }
        
    }
    
    
    
    
    
    
     /**
     * Description
     * 
     * It checks for engineer is mapped for postcodes forgiven date.
     *
     * @param interger $ServiceProviderEngineerID
     * @param interger $AllocatedDate
     * @param string   $AppointmentAllocationSlotID
     * 
     * 
     * @return int DiaryAllocationID on success 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isSkillsetMapped($ServiceProviderEngineerID, $AllocatedDate, $AppointmentAllocationSlotID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT DiaryAllocationID FROM diary_allocation WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID AND AllocatedDate=:AllocatedDate AND AppointmentAllocationSlotID=:AppointmentAllocationSlotID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceProviderEngineerID' => $ServiceProviderEngineerID, ':AllocatedDate' => $AllocatedDate, ':AppointmentAllocationSlotID' => $AppointmentAllocationSlotID));
        $result = $fetchQuery->fetch();
        
     
        if(is_array($result) && $result['DiaryAllocationID'])
        {
                return $result['DiaryAllocationID'];
        }

        return false;
        
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch Diary Allocation for given date range, engineer and time slot.
     *
     * @param interger $ServiceProviderEngineerID
     * @param interger $AllocatedDate
     * @param interger $AllocatedToDate 
     * @param string   $AppointmentAllocationSlotID
     * 
     * 
     * @return int DiaryAllocationID on success 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getDiaryAllocation($ServiceProviderEngineerID, $AllocatedDate, $AllocatedToDate, $AppointmentAllocationSlotID) {
        
        $result = array();
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT DiaryAllocationID, NumberOfAllocations FROM diary_allocation WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID AND AllocatedDate=:AllocatedDate AND AppointmentAllocationSlotID=:AppointmentAllocationSlotID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        $sltdDate  = explode("/", $AllocatedDate);
        $exactDate = $sltdDate[2]."-".$sltdDate[1]."-".$sltdDate[0];

        $selectedAllocatedDate  = strtotime($exactDate);
        
        $sltdToDate = explode("/", $AllocatedToDate);
        $exactToDate  = $sltdToDate[2]."-".$sltdToDate[1]."-".$sltdToDate[0];

        $selectedAllocatedToDate  = strtotime($exactToDate);
        
        
        //$datediff = strtotime($exactToDate) - strtotime($exactDate);
        
        
       // $this->controller->log("datediff:".$datediff);
        
        //$noOfDays = floor((strtotime($exactToDate) - strtotime($exactDate))/(60*60*24)) + 1;
        
        
        $datetime1 = new DateTime($exactDate);
        $datetime2 = new DateTime($exactToDate);
        $interval  = $datetime1->diff($datetime2);
        $noOfDays  =  $interval->format('%R%a')+1;
        

//        
//        $this->controller->log("No Of days:".$noOfDays);
//        $this->controller->log("sd:".date("Y-m-d", $selectedAllocatedDate));
//        $this->controller->log("ed:".date("Y-m-d", $selectedAllocatedToDate));
//            
//        
        
        for($i=1;$i<=$noOfDays;$i++)
        {
            
            $AllocatedTime = strtotime ( ($i-1).' day' , $selectedAllocatedDate);
            
           // $this->controller->log(var_export("at:".$AllocatedTime, true));
            
            $fetchDate  = date("Y-m-d", $AllocatedTime);
           //$displayDay = date("j", $AllocatedTime)."<sup>".date("S", $AllocatedTime)."</sup>";
            $displayDay  = date("D d M", $AllocatedTime); 
            
          //  $this->controller->log(var_export("fd:".$fetchDate, true));

            $fetchQuery->execute(array(':ServiceProviderEngineerID' => $ServiceProviderEngineerID, ':AllocatedDate' => $fetchDate, ':AppointmentAllocationSlotID' => $AppointmentAllocationSlotID));


            $r_result = $fetchQuery->fetch();


            if(is_array($r_result) && $r_result['DiaryAllocationID'])
            {
                 //Fethcing postcodes for given DiaryAllocationID
                 $sql2        = 'SELECT PostCode FROM diary_postcode_allocation WHERE DiaryAllocationID=:DiaryAllocationID';
                 $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                
                 $fetchQuery2->execute(array(':DiaryAllocationID' => $r_result['DiaryAllocationID']));
                 $PostCodeList    = $fetchQuery2->fetchAll();
                 
                 $PostCodesString = '';
                 $sep = '';
                 foreach($PostCodeList as $pc)
                 {
                     $PostCodesString = $PostCodesString.$sep.$pc['PostCode'];
                     $sep = ",";
                 }
                 
                 $result[$i] =    array("Date"=>$fetchDate, "NumberOfAllocations"=>$r_result["NumberOfAllocations"], "PostCode"=>$PostCodesString, "DisplayDay"=>$displayDay);
            }
            else
            {
                $result[$i] = array("Date"=>$fetchDate, "NumberOfAllocations"=>0, "PostCode"=>'', "DisplayDay"=>$displayDay);
            }
            
            //Getting Engineers day status...
            
            $sql4        = 'SELECT Status FROM service_provider_engineer_details WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID AND WorkDate=:WorkDate';
            $fetchQuery4 = $this->conn->prepare($sql4, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

            $fetchQuery4->execute(array(':ServiceProviderEngineerID' => $ServiceProviderEngineerID, ':WorkDate' => $fetchDate));
            $day_status    = $fetchQuery4->fetch();
                
            if(isset($day_status['Status']) && $day_status['Status']=='Active')
            {
                $result[$i]['DayStatus'] =  'Active';
            }
            else
            {    
                $result[$i]['DayStatus'] =  'In-active';
            }
            if(is_array($r_result) && $r_result['DiaryAllocationID'])
            {
                //Getting Engineers day GridCellsStatus...
            
            $sql5        = 'SELECT * FROM sp_geo_cells  WHERE DiaryAllocationID=:DiaryAllocationID';
            $fetchQuery5 = $this->conn->prepare($sql5, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

            $fetchQuery5->execute(array(':DiaryAllocationID' => $r_result['DiaryAllocationID']));
            $day_status    = $fetchQuery5->fetch();
                $this->controller->log($r_result['DiaryAllocationID']);
            if($day_status)
            {
                $result[$i]['MapData'] =  'Yes';
            }
            else
            {    
                $result[$i]['MapData'] =  'No';
            }
            }else{
                $result[$i]['MapData'] =  'No';
            }
            
        }
        
        
        if(isset($result[1]))
        {
            $result[1]['StartWeekDay'] = date("l jS F Y", $selectedAllocatedDate); 
            $result[1]['EndWeekDay']   = date("l jS F Y", $selectedAllocatedToDate); 
            
            
            $EngineersModel    =   $this->controller->loadModel('Engineers');
            $EngineerDetails   =   $EngineersModel->fetchRow(array('ServiceProviderEngineerID'=>$ServiceProviderEngineerID));
            
            
            $result[1]['SetupType'] = (isset($EngineerDetails['SetupType']))?$EngineerDetails['SetupType']:'Unique';
            $result[1]['ReplicateType'] = (isset($EngineerDetails['ReplicateType']))?$EngineerDetails['ReplicateType']:'FourWeeksOnly';
            $result[1]['ReplicateStatusFlag'] = (isset($EngineerDetails['ReplicateStatusFlag']))?$EngineerDetails['ReplicateStatusFlag']:'No';
            $result[1]['ReplicatePostcodeAllocation'] = (isset($EngineerDetails['ReplicatePostcodeAllocation']))?$EngineerDetails['ReplicatePostcodeAllocation']:'Yes';
           
            
        }
        
       // $this->controller->log("DialyAloocationResult:");
      //  $this->controller->log(var_export($result, true));
        
        
        return $result;
    }
    
    
    
    
    
     
    
    /**
     * Description
     * 
     * This method is used for to map skills and areas.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function createSkillsAreaMap($args) {
        
      //  $this->controller->log(var_export($args, true));
            
            if(isset($args['def']) && $args['def']==0)
            {
                $AllocationOnlyUser = true;
                
                if($args['SetupType']=="Unique")
                {
                    $args['ReplicateType'] = 'FourWeeksOnly';
                }    

                if($args['ReplicateStatusFlag']!='Yes')
                {
                    $args['ReplicateStatusFlag'] = 'No';
                }

                if($args['ReplicatePostcodeAllocation']!='Yes')
                {
                    $args['ReplicatePostcodeAllocation'] = 'No';
                }
                
                $EngineersModel    =   $this->controller->loadModel('Engineers');
            }
            else
            {
                $AllocationOnlyUser = false;
            }
                     
        
            $saveFlag   = false;
            $ErrorSlots = array();


            
            $sql = 'INSERT INTO diary_allocation (ServiceProviderEngineerID, ServiceProviderID, AllocatedDate, AppointmentAllocationSlotID, NumberOfAllocations, SlotsLeft)
            VALUES(:ServiceProviderEngineerID, :ServiceProviderID, :AllocatedDate, :AppointmentAllocationSlotID, :NumberOfAllocations, :SlotsLeft)';

            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            
            
            
            $updateSql   = 'UPDATE diary_allocation SET ServiceProviderEngineerID=:ServiceProviderEngineerID, ServiceProviderID=:ServiceProviderID, AllocatedDate=:AllocatedDate, AppointmentAllocationSlotID=:AppointmentAllocationSlotID, NumberOfAllocations=:NumberOfAllocations, SlotsLeft=:SlotsLeft WHERE DiaryAllocationID=:DiaryAllocationID';

            $updateQuery = $this->conn->prepare($updateSql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            
            
            
            $sltdDate     = explode("/", $args['AllocatedDate']);
            $exactDate    = $sltdDate[2]."-".$sltdDate[1]."-".$sltdDate[0];

            $selectedAllocatedDate  = strtotime($exactDate);

            $sltdToDate   = explode("/", $args['AllocatedToDate']);
            $exactToDate  = $sltdToDate[2]."-".$sltdToDate[1]."-".$sltdToDate[0];

            $selectedAllocatedToDate  = strtotime($exactToDate);


//            $datediff = $selectedAllocatedToDate - $selectedAllocatedDate;
//            $noOfDays = floor($datediff/(60*60*24)) + 1;
            
            $datetime1 = new DateTime($exactDate);
            $datetime2 = new DateTime($exactToDate);
            $interval  = $datetime1->diff($datetime2);
            $noOfDays  =  $interval->format('%R%a')+1;
            
            
            
            //Replicating week data for all weeks..
            if(isset($args['ReplicateWeeks']) && $args['ReplicateWeeks']==1)
            {
                for($i=1;$i<=$noOfDays;$i++)
                {
                
                     $dayNo = $i%7;
                     if(!$dayNo)
                     {
                         $dayNo = 7;
                     }
                    
                     $args['hiddenAllocatedSlots_'.$i]   = $args['AllocatedSlots_'.$dayNo];
                     $args['hiddenPostcode_'.$i]         = $args['Postcode_'.$dayNo];
                     
                     
                }
            }
            
            if($AllocationOnlyUser)
            {
                if($args['ReplicateStatusFlag']=='Yes' && $args['SetupType']=="Replicate")
                {    
                    
                   // $this->log("----------0000-------------");
                  //  $this->log($noOfDays);
                    
                    for($i=1;$i<=$noOfDays;$i++)
                        {

                             $dayNo = $i%7;
                             if(!$dayNo)
                             {
                                 $dayNo = 7;
                             }

                             if(isset($args['hiddenDayStatus_'.$dayNo]) && $args['hiddenDayStatus_'.$dayNo]=='Active')
                             {
                                $args['hiddenDayStatus_'.$i] = 'Active'; 
                             }
                             else
                             {
                                 $args['hiddenDayStatus_'.$i] = 'In-active'; 
                             }

                       }
                }
            }
            
          //  $this->log("---------111--------------");
          //  $this->log($args);
            
            if(isset($args['ReplicateToOtherSlot']) && $args['ReplicateToOtherSlot']==1)
            {

                $ReplicateToOtherSlot = true;
            }
            else {
                
                $ReplicateToOtherSlot = false;
                
            }
            
            if($AllocationOnlyUser)
            {
                    $EngineersModel->updateEngineerReplicateWeekSettings(array('ServiceProviderEngineerID'=>$args['ServiceProviderEngineerID'], 'SetupType'=>$args['SetupType'], 'ReplicateType'=>$args['ReplicateType'], 'ReplicateStatusFlag'=>$args['ReplicateStatusFlag'], 'ReplicatePostcodeAllocation'=>$args['ReplicatePostcodeAllocation']));
                     
                    for($i=1;$i<=$noOfDays;$i++)
                    {
                         $AllocatedTime = strtotime ( ($i-1).' day' , $selectedAllocatedDate);
                         $AllocatedDate = date("Y-m-d", $AllocatedTime);
                    
               
                         $EngineersModel->updateEngineerDetailsStatus($args['hiddenDayStatus_'.$i], $AllocatedDate, $args['ServiceProviderEngineerID']);
                        
                         
//                         $this->log("------------");
//                         $this->log($args['hiddenDayStatus_'.$i]);
//                         $this->log($AllocatedDate);
//                         $this->log($args['ServiceProviderEngineerID']);
//                         
//                         
                    }
            }  
             
             

            for($i=1;$i<=$noOfDays;$i++)
            {
                $NumberOfAllocations = trim($args['hiddenAllocatedSlots_'.$i]);
                $Postcode            = trim($args['hiddenPostcode_'.$i]);
                
                //if($NumberOfAllocations!='' && $NumberOfAllocations && $Postcode!='' && $Postcode)
                //{    
                    
                    $AllocatedTime = strtotime ( ($i-1).' day' , $selectedAllocatedDate);
                    $AllocatedDate = date("Y-m-d", $AllocatedTime);
                    
                  
                    
                    
                    
                    $DiaryAllocationID = $this->isSkillsetMapped($args['ServiceProviderEngineerID'], $AllocatedDate, $args['AppointmentAllocationSlotID']);
                    
                     if($DiaryAllocationID)//Update details into database.
                     {    
                         $SlotsLeft  = 0;
                         
                         
                         //Getting allocation slots
                         $AllocatedSlots = $this->getAllocatedSlots($DiaryAllocationID);
                         
                         //Adjusting data prior to save if allocated slots are less than number of slots entered by user.
//                         if($NumberOfAllocations<$AllocatedSlots)
//                         {
//                             $NumberOfAllocations = $AllocatedSlots;
//                             $SlotsLeft =   0;
//                             $ErrorSlots[] = array($DiaryAllocationID, $AllocatedDate);
//                         }
//                         else
//                         {
//                             $SlotsLeft = $NumberOfAllocations-$AllocatedSlots;
//                         }
                         //commented by Andris for Samsung once toch thing 26/02/2013
                         
                         
                         
                            $result =  $updateQuery->execute(array(

                              ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'],
                              ':ServiceProviderID' => $args['ServiceProviderID'], 
                              ':AllocatedDate' => $AllocatedDate, 
                              ':AppointmentAllocationSlotID' => $args['AppointmentAllocationSlotID'], 
                              ':NumberOfAllocations' => $NumberOfAllocations,
                              ':DiaryAllocationID' => $DiaryAllocationID,
                              ':SlotsLeft' => $SlotsLeft  


                              ));
                         
                     }     
                     else //Insert details into database.
                     {
                            $result =  $insertQuery->execute(array(

                            ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'],
                            ':ServiceProviderID' => $args['ServiceProviderID'], 
                            ':AllocatedDate' => $AllocatedDate, 
                            ':AppointmentAllocationSlotID' => $args['AppointmentAllocationSlotID'], 
                            ':NumberOfAllocations' => $NumberOfAllocations,
                            ':SlotsLeft' => $NumberOfAllocations    
                                
                            ));
                            
                            $DiaryAllocationID = $this->conn->lastInsertId();
                     }
                     
                     
                    if($DiaryAllocationID)//Storing postcodes of engineer in separate table - starts here..
                    {
                                                    
                            //Deleting existing records of DiaryAllocationID.
                            $deleteSql   = 'DELETE FROM diary_postcode_allocation WHERE DiaryAllocationID=:DiaryAllocationID';
                            $deleteQuery = $this->conn->prepare($deleteSql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                            $deleteQuery->execute( array( ':DiaryAllocationID' => $DiaryAllocationID ) );
                            
                            
                            
                            $PostcodeArray  = explode(",", $Postcode);
                            
                            $PostcodeArrayUnique = array_unique($PostcodeArray);
                            
                            $sql2 = 'INSERT INTO diary_postcode_allocation (DiaryAllocationID, PostCode)
                            VALUES(:DiaryAllocationID, :PostCode)';

                            $insertQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                            
                            foreach($PostcodeArrayUnique as $pCode)
                            {
                                $pCode = strtoupper(trim($pCode));
                                
                                if($pCode!='' && $pCode)
                                {    
                                     $insertQuery2->execute(array(

                                        ':DiaryAllocationID' => $DiaryAllocationID,
                                        ':PostCode' => $pCode, 
                                        ));
                                }
                            }
                       
                     
                            
                        //Replicate data To Other Slot    --  starts here...
                        if($ReplicateToOtherSlot)
                        {    
                            
                                    $NumberOfAllocations         = trim($args['hiddenAllocatedSlots_'.$i]);
                            
                                    $AppointmentAllocationSlots  = $this->getAppointmentAllocationSlot($args['ServiceProviderID']);
                
                                    $OtherAppointmentAllocationSlotID = false;
                                    
                                    foreach($AppointmentAllocationSlots AS $asKey=>$asValue)
                                    {
                                        if($asValue['AppointmentAllocationSlotID']!=$args['AppointmentAllocationSlotID'] && $asValue['Type']!="ANY")
                                        {
                                           
                                             $OtherAppointmentAllocationSlotID   = $asValue['AppointmentAllocationSlotID'];
                                             break;
                                        }
                                    } 
                                    
                                    if($OtherAppointmentAllocationSlotID)
                                    {
                                        $OtherDiaryAllocationID = $this->isSkillsetMapped($args['ServiceProviderEngineerID'], $AllocatedDate, $OtherAppointmentAllocationSlotID);

                                        if($OtherDiaryAllocationID)//Update details into database.
                                        {    
                                            $SlotsLeft  = 0;


                                            //Getting allocation slots
                                            $AllocatedSlots = $this->getAllocatedSlots($OtherDiaryAllocationID);

                                            //Adjusting data prior to save if allocated slots are less than number of slots entered by user.
//                                            
                                           // andris 26/02/0213



                                               $result =  $updateQuery->execute(array(

                                                 ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'],
                                                 ':ServiceProviderID' => $args['ServiceProviderID'], 
                                                 ':AllocatedDate' => $AllocatedDate, 
                                                 ':AppointmentAllocationSlotID' => $OtherAppointmentAllocationSlotID, 
                                                 ':NumberOfAllocations' => $NumberOfAllocations,
                                                 ':DiaryAllocationID' => $OtherDiaryAllocationID,
                                                 ':SlotsLeft' => $SlotsLeft  


                                                 ));

                                        }     
                                        else //Insert details into database.
                                        {
                                               $result =  $insertQuery->execute(array(

                                               ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'],
                                               ':ServiceProviderID' => $args['ServiceProviderID'], 
                                               ':AllocatedDate' => $AllocatedDate, 
                                               ':AppointmentAllocationSlotID' => $OtherAppointmentAllocationSlotID, 
                                               ':NumberOfAllocations' => $NumberOfAllocations,
                                               ':SlotsLeft' => $NumberOfAllocations    

                                               ));

                                               $OtherDiaryAllocationID = $this->conn->lastInsertId();
                                        }


                                        if($OtherDiaryAllocationID)
                                        {
                                            //Deleting existing records of DiaryAllocationID.
                                            $deleteSql   = 'DELETE FROM diary_postcode_allocation WHERE DiaryAllocationID=:DiaryAllocationID';
                                            $deleteQuery = $this->conn->prepare($deleteSql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                                            $deleteQuery->execute( array( ':DiaryAllocationID' => $OtherDiaryAllocationID ) );


                                            $sql2 = 'INSERT INTO diary_postcode_allocation (DiaryAllocationID, PostCode)
                                            VALUES(:DiaryAllocationID, :PostCode)';

                                            $insertQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                                            foreach($PostcodeArrayUnique as $pCode)
                                            {
                                                $pCode = strtoupper(trim($pCode));

                                                if($pCode!='' && $pCode)
                                                {    
                                                     $insertQuery2->execute(array(

                                                        ':DiaryAllocationID' => $OtherDiaryAllocationID,
                                                        ':PostCode' => $pCode, 
                                                        ));
                                                }
                                            }
                                            
                                        }     //End if($OtherDiaryAllocationID) 

                                  }//End if($OtherAppointmentAllocationSlotID)
                            
                            
                        }    
                       //Replicate data To Other Slot     --  ends here...     
                            
                            
                            
                        if(count($ErrorSlots)==0)
                        {    
                            $saveFlag = true;
                        }
                        
                              
                            //Checking for ANY type timeslot needed, if yes creating it - starts here..

                        $this->processAnyTimeSlotDetails($DiaryAllocationID);

                            //Checking for ANY type timeslot needed, if yes creating it - ends here..
                         
                        
                    } //Storing postcodes of engineer in separate table - ends here..
                                
                //}     
            }
            
            
            if($AllocationOnlyUser)
            {
                //Replicating postcode allocation - starts here..
                if($args['ReplicatePostcodeAllocation']=='Yes' && $args['SetupType']=="Replicate" && ($args['ReplicateType']=="FourWeeksOnly" || $args['ReplicateType']=="WeeklyCycle"))
                {

                    $EngineersModel->replicatePostcodeAllocation($args['ServiceProviderEngineerID'], $args['ServiceProviderID']);

                }
                //Replicating postcode allocation - ends here..
            
            }
            
            
            
            if($saveFlag)
            {
                if(isset($args['SentToViamente']) && $args['SentToViamente']=="Yes")
                {
                    $api_viamente=$this->controller->loadModel('APIViamente');
                    $api_viamente->ProcessEngineer($args['ServiceProviderEngineerID'], 'update');
                    //$this->log ("VIA - UPDATE CALLING");
                }
                return array('status' => 'OK',
                             'message' => "Your data has been saved successfully.");
            }
            else if(count($ErrorSlots)>0)
            {
                 return array('status' => 'SLOT_ERROR',
                            'message' => "",
                            'slots'=>$ErrorSlots);
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => "Sorry, your data has not been processed since there is a problem.  Please check it."
                            );
            }
        
    }
    
   
    /**
     * Description
     * 
     * This method is used for, to check any time slot needed for given allocation, if yes creates any time slot if it is not exists or amends if it is already exists.
     * 
     * @param integer $DiaryAllocationID
    
     * 
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function processAnyTimeSlotDetails($DiaryAllocationID) {
    
        if($DiaryAllocationID)
        {    
            /* Getting diary allocation details */
            $DiaryAllocationResult =  $this->fetchDiaryAllocationRow(array("DiaryAllocationID"=>$DiaryAllocationID));
            
            
            //Getting Service Provider Time slots
            if(is_array($DiaryAllocationResult) && $DiaryAllocationResult['DiaryAllocationID'])
            {    
                $AppointmentAllocationSlots  = $this->getAppointmentAllocationSlot($DiaryAllocationResult['ServiceProviderID']);
                
                $OtherSlotTypeID = false;
                $AnySlotTypeID   = false;
                
                foreach($AppointmentAllocationSlots AS $asKey=>$asValue)
                {
                    if($asValue['AppointmentAllocationSlotID']!=$DiaryAllocationResult['AppointmentAllocationSlotID'])
                    {
                        if($asValue['Type']=="ANY")
                        {
                            $AnySlotTypeID   = $asValue['AppointmentAllocationSlotID'];
                        }
                        else 
                        {
                            $OtherSlotTypeID = $asValue['AppointmentAllocationSlotID'];
                        }
                    }
                }   
                
                
                if($OtherSlotTypeID)
                {
                     $OtherArgs = array("ServiceProviderEngineerID"=>$DiaryAllocationResult['ServiceProviderEngineerID'], "AllocatedDate"=>$DiaryAllocationResult['AllocatedDate'], "AppointmentAllocationSlotID"=>$OtherSlotTypeID);
                    
                     $OtherDiaryAllocationResult =  $this->fetchDiaryAllocationRow($OtherArgs);
                     
                     
                     
                     if(is_array($OtherDiaryAllocationResult) )
                     {    
                            $CommonPostCodes = array(); 

                            if(is_array($OtherDiaryAllocationResult['Postcode']) && is_array($DiaryAllocationResult['Postcode']))
                            {

                                    $CommonPostCodes  =  array_intersect ($OtherDiaryAllocationResult['Postcode'], $DiaryAllocationResult['Postcode']);
                            }
                            
                            
                            if($AnySlotTypeID)
                            {    
                                $AnyArgs = array("ServiceProviderEngineerID"=>$DiaryAllocationResult['ServiceProviderEngineerID'], "AllocatedDate"=>$DiaryAllocationResult['AllocatedDate'], "AppointmentAllocationSlotID"=>$AnySlotTypeID);

                                $AnyDiaryAllocationResult =  $this->fetchDiaryAllocationRow($AnyArgs);
                            
                                
                                $AnyDiaryAllocationID = false;
                            
                                if(is_array($AnyDiaryAllocationResult) && $AnyDiaryAllocationResult['DiaryAllocationID'])
                                {
                                    $AnyDiaryAllocationID = $AnyDiaryAllocationResult['DiaryAllocationID'];
                                }
                                else if (count($CommonPostCodes)>0)
                                {
                                    //Inserting ANY type time slot into diary_allocation database.
                                    
                                    
                                     $sql = 'INSERT INTO diary_allocation (ServiceProviderEngineerID, ServiceProviderID, AllocatedDate, AppointmentAllocationSlotID, NumberOfAllocations, SlotsLeft)
                                     VALUES(:ServiceProviderEngineerID, :ServiceProviderID, :AllocatedDate, :AppointmentAllocationSlotID, :NumberOfAllocations, :SlotsLeft)';

                                    $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                                     $result =  $insertQuery->execute(array(

                                                    ':ServiceProviderEngineerID' => $DiaryAllocationResult['ServiceProviderEngineerID'],
                                                    ':ServiceProviderID' => $DiaryAllocationResult['ServiceProviderID'], 
                                                    ':AllocatedDate' => $DiaryAllocationResult['AllocatedDate'], 
                                                    ':AppointmentAllocationSlotID' => $AnySlotTypeID, 
                                                    ':NumberOfAllocations' => 0,
                                                    ':SlotsLeft' => 0    

                                                    ));

                                     $AnyDiaryAllocationID = $this->conn->lastInsertId();
                                }
                                
                                
                                
                                  //Amending postcode relations for Any Type.. - starts here..
                                  if($AnyDiaryAllocationID)
                                  {
                                    
                                        //Deleting existing records of DiaryAllocationID.
                                        $deleteSql   = 'DELETE FROM diary_postcode_allocation WHERE DiaryAllocationID=:DiaryAllocationID';
                                        $deleteQuery = $this->conn->prepare($deleteSql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                                        $deleteQuery->execute( array( ':DiaryAllocationID' => $AnyDiaryAllocationID ) );



                                        $sql5 = 'INSERT INTO diary_postcode_allocation (DiaryAllocationID, PostCode)
                                        VALUES(:DiaryAllocationID, :PostCode)';

                                        $insertQuery5 = $this->conn->prepare($sql5, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                                        
                                       // $this->controller->log(var_export($CommonPostCodes, true));
                                        
                                        
                                        $CommonPostCodesUnique = array_unique($CommonPostCodes);
                                        
                                        
                                        foreach($CommonPostCodesUnique as $pCode)
                                        {
                                            $pCode = strtoupper(trim($pCode));
                                            
                                            if($pCode!='' && $pCode)
                                            {    
                                                 $insertQuery5->execute(array(

                                                    ':DiaryAllocationID' => $AnyDiaryAllocationID,
                                                    ':PostCode' => $pCode, 
                                                    ));
                                            }
                                        }

                                  }    
                                  //Amending postcode relations for Any Type.. - ends here..
                                
                            }//End  if($AnySlotTypeID)
                            
                     }//End if(is_array($OtherDiaryAllocationResult) )
                     
                }//End if($OtherSlotTypeID)
                
                
            }
            
        }    //End if if($DiaryAllocationID) 
       
     
     }//End function processAnyTimeSlotDetails
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch diary_allocation row from database.
     *
     * @param  array $args
      
    
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchDiaryAllocationRow($args) {
        
        
        if(isset($args['DiaryAllocationID']))
        {    
            $sql = 'SELECT DiaryAllocationID, ServiceProviderID, AllocatedDate, AppointmentAllocationSlotID, ServiceProviderEngineerID FROM diary_allocation WHERE DiaryAllocationID=:DiaryAllocationID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':DiaryAllocationID' => $args['DiaryAllocationID']));
        }
        else
        {
            $sql = 'SELECT DiaryAllocationID, ServiceProviderID, AllocatedDate, AppointmentAllocationSlotID, ServiceProviderEngineerID FROM diary_allocation WHERE AllocatedDate=:AllocatedDate AND AppointmentAllocationSlotID=:AppointmentAllocationSlotID AND ServiceProviderEngineerID=:ServiceProviderEngineerID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':AllocatedDate' => $args['AllocatedDate'], ':AppointmentAllocationSlotID' => $args['AppointmentAllocationSlotID'], ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID']));
        }
        
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['DiaryAllocationID'])
        {
            
            $sql2 = 'SELECT Postcode FROM diary_postcode_allocation WHERE DiaryAllocationID=:DiaryAllocationID ORDER BY Postcode';
            $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery2->execute(array(':DiaryAllocationID' => $result['DiaryAllocationID']));
            $result2 = $fetchQuery2->fetchAll();
            
            $result['Postcode'] = array();
            
            if(is_array($result2))
            {
                foreach($result2 AS $key=>$value)
                {
                    $result['Postcode'][] = $value[0];
                }    
            }    
            
            
        }
        
        return $result;
            
     }
     
     
     
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for, to get the list of skill set for given service provider.
     * 
     * @param integer $ServiceProviderID
    
     * 
     * @return array $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getServiceProviderSkillset($ServiceProviderID) {
        
        $result = array();
          
             
        $sql = "SELECT T1.ServiceProviderSkillsetID, T2.SkillSetName FROM service_provider_skillset AS T1 LEFT JOIN skillset AS T2 ON T1.SkillsetID=T2.SkillsetID WHERE T1.ServiceProviderID=:ServiceProviderID AND T1.Status='Active' AND T2.Status='Active' ORDER BY T2.SkillSetName";

        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID));

        while($row = $fetchQuery->fetch())
        {
            $result[] = array("ServiceProviderSkillsetID"=>$row['ServiceProviderSkillsetID'], "SkillSetName"=>$row['SkillSetName']);
        }
       
         return $result;  
         
    }
    
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to get skill type name.
     * 
     * @param integer $AppointmentID
    
     * 
     * @return string 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getSkillSetName($AppointmentID) {
        
        
        $sql = "SELECT rs.RepairSkillName 
            FROM 
            appointment ap 
            LEFT JOIN  service_provider_skillset sps ON ap.ServiceProviderSkillsetID=sps.ServiceProviderSkillsetID
            LEFT JOIN skillset sk ON sps.SkillsetID=sk.SkillsetID
            LEFT JOIN repair_skill rs ON rs.RepairSkillID=sk.RepairSkillID
            
            WHERE ap.AppointmentID=:AppointmentID";

         $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
         $fetchQuery->execute(array(':AppointmentID' => $AppointmentID));

         $result = $fetchQuery->fetch();
       
         if(isset($result['RepairSkillName']))
         {
             return $result['RepairSkillName'];
         }
         else
         {
             return '';  
         }
         
    }
    
    
    
    
    
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for, to get the list of skill set (from main) which are not added for given service provider.
     * 
     * @param integer $ServiceProviderID
     * @param integer $SkillsetID default null.  This is for to include this skill in result 
    
     * 
     * @return array $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getNotAssignedSkillSet($ServiceProviderID, $SkillsetID=null) {
        
       // $result = array();
          
        if($ServiceProviderID)
        {    
            if($SkillsetID)
            {    
                $sql = "SELECT T1.SkillsetID, T1.SkillSetName, T1.ServiceDuration, T1.EngineersRequired FROM skillset AS T1 LEFT JOIN service_provider_skillset  AS T2 ON T1.SkillsetID=T2.SkillsetID AND T2.ServiceProviderID=:ServiceProviderID WHERE (T1.Status='Active' AND T2.SkillsetID IS NULL) OR T1.SkillsetID=:SkillsetID ORDER BY T1.SkillSetName";
            
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':SkillsetID' => $SkillsetID));
                
            }
            else
            {
                $sql = "SELECT T1.SkillsetID, T1.SkillSetName, T1.ServiceDuration, T1.EngineersRequired FROM skillset AS T1 LEFT JOIN service_provider_skillset  AS T2 ON T1.SkillsetID=T2.SkillsetID AND T2.ServiceProviderID=:ServiceProviderID WHERE T1.Status='Active' AND T2.SkillsetID IS NULL ORDER BY T1.SkillSetName";
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID));
            }
            
        }
        else 
        {
            $sql = "SELECT SkillsetID, SkillSetName, ServiceDuration, EngineersRequired FROM skillset WHERE Status='Active' ORDER BY SkillSetName";

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute();
            
        }
        
        //$this->controller->log(var_export($sql, true));
        
        $result = $fetchQuery->fetchAll();
        return $result;  
         
    }
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to get the list of skill set repair skills.
     * 
     * @return array $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getSkillSetRepairSkills() {
               
        
        $sql = "SELECT T2.RepairSkillID, T2.RepairSkillName FROM skillset AS T1 LEFT JOIN repair_skill AS T2 ON T1.RepairSkillID=T2.RepairSkillID WHERE T1.RepairSkillID IS NOT NULL GROUP BY T1.RepairSkillID ORDER BY T2.RepairSkillName";

        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute();
            
        $result = $fetchQuery->fetchAll();
        return $result;  
         
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to get the list of skill set for given repair skill and service provider.
     *
     * @param int $RepairSkillID 
     * @param int $ServiceProviderID 
     * 
     * @return array $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getRepairSkillSkillsSet($RepairSkillID, $ServiceProviderID) {
               
        
        $sql = "SELECT T1.`SkillsetID`, T1.`AppointmentTypeID`, T2.`Type`, T1.`ServiceDuration`, T1.`EngineersRequired`, T3.`SkillsetID` AS SPSkillsetID, T3.`ServiceDuration` AS SPServiceDuration,
            T3.`EngineersRequired` AS SPEngineersRequired
            FROM `skillset` AS T1
            LEFT JOIN appointment_type AS T2 ON T1.AppointmentTypeID = T2.AppointmentTypeID
            LEFT JOIN service_provider_skillset AS T3 ON T1.SkillsetID = T3.SkillsetID AND T3.Status='Active'
            AND T3.ServiceProviderID=:ServiceProviderID
            WHERE T1.`RepairSkillID`=:RepairSkillID AND T1.Status='Active'
            ";

        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':RepairSkillID' => $RepairSkillID));
            
        $result = $fetchQuery->fetchAll();
        return $result;  
         
    }
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for, to get the list of all appointment allocation slots.
     *
     * @param int $ServiceProviderID 
     * 
     * @return array $result 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     */
    
     public function getAppointmentAllocationSlot($ServiceProviderID, $notAnyType=false) {
        
             
        if($notAnyType)
        {
            $sql = "SELECT AppointmentAllocationSlotID, Description, Type, TimeFrom, TimeTo, Colour FROM appointment_allocation_slot WHERE ServiceProviderID=:ServiceProviderID AND Type!='ANY' ORDER BY Type, AppointmentAllocationSlotID";
    
        }
        else
        {
            $sql = "SELECT AppointmentAllocationSlotID, Description, Type, TimeFrom, TimeTo, Colour FROM appointment_allocation_slot WHERE ServiceProviderID=:ServiceProviderID ORDER BY Type, AppointmentAllocationSlotID";
        }
            
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(":ServiceProviderID"=>$ServiceProviderID));
        
        $result  = $fetchQuery->fetchAll();
        
        return $result;  
         
    }
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        //$this->controller->log(var_export($args, true));
        
         if($this->isValid($args['ServiceProviderID'], $args['SkillsetID'], 0))
         {
                //$result = false;
                
                
                /* Execute a prepared statement by passing an array of values */
                $sql = 'INSERT INTO service_provider_skillset (SkillsetID, ServiceProviderID, ServiceDuration, EngineersRequired, Status)
                VALUES(:SkillsetID, :ServiceProviderID, :ServiceDuration, :EngineersRequired, :Status)';

                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                $result =  $insertQuery->execute(array(

                    ':SkillsetID' => $args['SkillsetID'],
                    ':ServiceProviderID' => $args['ServiceProviderID'], 
                    ':ServiceDuration' => $args['ServiceDuration'], 
                    ':EngineersRequired' => $args['EngineersRequired'], 
                    ':Status' => $args['Status']

                    ));
                

                if($result)
                {
                     return array('status' => 'OK',
                                'message' => "Your data has been inserted successfully.");
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => "Sorry, your data has not been processed since there is a problem.  Please check it.");
                }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
         }
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * @global $this->table  
    
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceProviderSkillsetID, SkillsetID, ServiceProviderID, ServiceDuration, EngineersRequired, Status FROM service_provider_skillset WHERE ServiceProviderSkillsetID=:ServiceProviderSkillsetID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ServiceProviderSkillsetID' => $args['ServiceProviderSkillsetID']));
        $result = $fetchQuery->fetch();
        
        return $result;
     }
     
     
     
     
     /**
     * Description
     * 
     * This method is to get the allocated slots for given DiaryAllocationID.
     *
     * @param  int $DiaryAllocationID
     * 
     * @global $this->table  
    
     * 
     * @return int It contains number of allocated slots.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getAllocatedSlots($DiaryAllocationID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT DiaryAllocationID, NumberOfAllocations, SlotsLeft FROM diary_allocation WHERE DiaryAllocationID=:DiaryAllocationID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':DiaryAllocationID' => $DiaryAllocationID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result))
        {
            return ($result['NumberOfAllocations']-$result['SlotsLeft']);
        }
        else
        {
            return 0;
        }
        
     }
     
     
     
    
     
    
     /**
     * Description
     * 
     * This method is used for to update a row into database.
     *
     * @param array $args
        
     * @global $this->table 
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
         if($this->isValid($args['ServiceProviderID'], $args['SkillsetID'], $args['ServiceProviderSkillsetID']))
         {         
           
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET SkillsetID=:SkillsetID, ServiceProviderID=:ServiceProviderID, ServiceDuration=:ServiceDuration, EngineersRequired=:EngineersRequired, Status=:Status WHERE ServiceProviderSkillsetID=:ServiceProviderSkillsetID';


            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $updateQuery->execute(

                    array(

                            ':SkillsetID' => $args['SkillsetID'],
                            ':ServiceProviderID' => $args['ServiceProviderID'], 
                            ':ServiceDuration' => $args['ServiceDuration'], 
                            ':EngineersRequired' => $args['EngineersRequired'], 
                            ':Status' => $args['Status'],
                            ':ServiceProviderSkillsetID' => $args['ServiceProviderSkillsetID']
                        )

                    );


            if($result)
            {
                    return array('status' => 'OK',
                            'message' => "Your data has been updated successfully.");
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => "Sorry, your data has not been processed since there is a problem.  Please check it.");
            }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
         }   
       
    }
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to update general defaults of given service provider.
     *
     * @param array $args
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updateSPGeneralDefaults($args) {
        
         if($args['ServiceProviderID'])
         {      
             
             
           if($args['GeneralDefaults']=="GeneralDefaults2")
           {    
               
               
               
               if(isset($args['RunViamenteToday']))
               {
                   $args['RunViamenteToday'] = "Yes";
               }    
               else
               {
                    $args['RunViamenteToday'] = "No";
               }
               if(isset($args['LockAppWindow']))
               {
                   $args['LockAppWindow'] = "Yes";
               }    
               else
               {
                    $args['LockAppWindow'] = "No";
               }
              
               
               
           
                /* Execute a prepared statement by passing an array of values */
                $sql = 'UPDATE service_provider SET DefaultTravelTime=:DefaultTravelTime,LockAppWindow=:LockAppWindow,LockAppWindowTime=:LockAppWindowTime, DefaultTravelSpeed=:DefaultTravelSpeed,BookingCapacityLimit=:BookingCapacityLimit,KeepEngInPCArea=:KeepEngInPCArea, RunViamenteToday=:RunViamenteToday, SendRouteMapsEmail=:SendRouteMapsEmail, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate  WHERE ServiceProviderID=:ServiceProviderID';


                $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $updateQuery->execute(

                        array(

                                ':DefaultTravelTime' => $args['DefaultTravelTime'],
                                ':LockAppWindow' => $args['LockAppWindow'],
                                ':LockAppWindowTime' => $args['LockAppWindowTime'],
                                ':DefaultTravelSpeed' => $args['DefaultTravelSpeed'], 
                                ':BookingCapacityLimit' => $args['BookingCapacityLimit'], 
                                ':KeepEngInPCArea' =>  isset($args['KeepEngInPCArea'])?'Yes':'No',
                                ':ServiceProviderID' => $args['ServiceProviderID'],
                                ':RunViamenteToday' => $args['RunViamenteToday'],
                                ':SendRouteMapsEmail' => ($args['SendRouteMapsEmail']!='')?$args['SendRouteMapsEmail']:NULL,                            
                                ':ModifiedUserID' => $this->controller->user->UserID,
                                ':ModifiedDate' => date("Y-m-d H:i:s")
                            
                            
                            )

                        );
                
                if($result && $args['ViamenteConsolidation'])
                {
                    
                    
                    $dateArray =  explode("/", $args['ViamenteConsolidation']);
                    $RouteDate =  $dateArray[2]."-".$dateArray[1]."-".$dateArray[0];
                    
                    if(isset($args['IndividualEngineers']) && count($args['IndividualEngineers']))
                    {
                        $this->viamenteUpdate($args['ServiceProviderID'], $RouteDate, $args['IndividualEngineers'], 2);
                    }
                    else 
                    {
                        $this->viamenteUpdate($args['ServiceProviderID'], $RouteDate, array(), 1);
                        
                        $this->removeDateViamenteEndDay($args);
                    }
                    
                    
                }
           }
           
           
           
           
           if($args['GeneralDefaults']=="GeneralDefaults4")
           {    
               
               if(isset($args['PasswordProtectNextDayBookings']))
               {
                   $args['PasswordProtectNextDayBookings'] = "Yes";
               }    
               else
               {
                    $args['PasswordProtectNextDayBookings'] = "No";
               }
               
                /* Execute a prepared statement by passing an array of values */
                $sql = 'UPDATE service_provider SET DiaryAdministratorEmail=:DiaryAdministratorEmail, UnlockingPassword=:UnlockingPassword, PasswordProtectNextDayBookings=:PasswordProtectNextDayBookings, PasswordProtectNextDayBookingsTime=:PasswordProtectNextDayBookingsTime, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate  WHERE ServiceProviderID=:ServiceProviderID';


                $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $updateQuery->execute(

                        array(

                                ':DiaryAdministratorEmail' => $args['DiaryAdministratorEmail'],
                                ':UnlockingPassword' => $args['UnlockingPassword'],
                                ':PasswordProtectNextDayBookings' => $args['PasswordProtectNextDayBookings'], 
                                ':PasswordProtectNextDayBookingsTime' => $args['PasswordProtectNextDayBookingsTime'], 
                                ':ServiceProviderID' => $args['ServiceProviderID'],
                                ':ModifiedUserID' => $this->controller->user->UserID,
                                ':ModifiedDate' => date("Y-m-d H:i:s")
                            
                            
                            )

                        );
                
                
           }
           
           
           
           if($args['GeneralDefaults']=="GeneralDefaults5")
           {    
               
              
               
                /* Execute a prepared statement by passing an array of values */
                $sql = 'UPDATE service_provider SET IPAddress=:IPAddress, Port=:Port, ViamenteKey=:ViamenteKey, NumberOfVehicles=:NumberOfVehicles, NumberOfWaypoints=:NumberOfWaypoints, OnlineDiary=:OnlineDiary,  ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate  WHERE ServiceProviderID=:ServiceProviderID';


                $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $updateQuery->execute(

                        array(

                                ':IPAddress' => trim($args['IPAddress']),
                                ':Port' => $args['Port'],
                                ':ViamenteKey' => $args['ViamenteKey'], 
                                ':NumberOfVehicles' => $args['NumberOfVehicles'], 
                                ':NumberOfWaypoints' => $args['NumberOfWaypoints'],
                                ':OnlineDiary' => $args['OnlineDiary'], 
                                ':ServiceProviderID' => $args['ServiceProviderID'],
                                ':ModifiedUserID' => $this->controller->user->UserID,
                                ':ModifiedDate' => date("Y-m-d H:i:s")
                            
                            
                            )

                        );
                
                
           }
           
           
           
           
           if($args['GeneralDefaults']=="GeneralDefaults6")
           {    
               
                if(!isset($args['AutoSelectDay']))
                {
                    $args['AutoSelectDay'] = '';
                }
                if(!isset($args['AutoDisplayTable']))
                {
                    $args['AutoDisplayTable'] = '';
                } 
                
                if(isset($args['AutoSpecifyEngineerByPostcode']))
                {
                    $args['AutoSpecifyEngineerByPostcode'] = 'Yes';
                }
                else
                {
                    $args['AutoSpecifyEngineerByPostcode'] = 'No';
                }
              
                
                if(isset($args['AutoChangeTimeSlotLabel']))
                {
                    $args['AutoChangeTimeSlotLabel'] = "Yes";
                }    
                else
                {
                     $args['AutoChangeTimeSlotLabel'] = "No";
                }
                 if(isset($args['DiaryShowSlotNumbers']))
                {
                    $args['DiaryShowSlotNumbers'] = "Yes";
                }    
                else
                {
                     $args['DiaryShowSlotNumbers'] = "No";
                }
                 if(isset($args['EmailAppBooking']))
                {
                    $args['EmailAppBooking'] = "Yes";
                }    
                else
                {
                     $args['EmailAppBooking'] = "No";
                }
                
               
                /* Execute a prepared statement by passing an array of values */
                $sql = 'UPDATE service_provider SET  AutoSelectDay=:AutoSelectDay,DiaryShowSlotNumbers=:DiaryShowSlotNumbers,EmailAppBooking=:EmailAppBooking, AutoDisplayTable=:AutoDisplayTable,ViamenteRunType=:ViamenteRunType, AutoSpecifyEngineerByPostcode=:AutoSpecifyEngineerByPostcode, Multimaps=:Multimaps, AutoChangeTimeSlotLabel=:AutoChangeTimeSlotLabel, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate  WHERE ServiceProviderID=:ServiceProviderID';


                $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $updateQuery->execute(

                        array(

                                ':AutoSelectDay' => $args['AutoSelectDay'],
                                ':AutoDisplayTable' => $args['AutoDisplayTable'],
                                ':AutoSpecifyEngineerByPostcode' => $args['AutoSpecifyEngineerByPostcode'],
                                ':ViamenteRunType' => $args['ViamenteRunType'],
                                ':Multimaps' => $args['Multimaps'], 
                                ':AutoChangeTimeSlotLabel' => $args['AutoChangeTimeSlotLabel'],
                                ':DiaryShowSlotNumbers' => $args['DiaryShowSlotNumbers'],
                                ':EmailAppBooking' => $args['EmailAppBooking'],
                                ':ServiceProviderID' => $args['ServiceProviderID'],
                                ':ModifiedUserID' => $this->controller->user->UserID,
                                ':ModifiedDate' => date("Y-m-d H:i:s")
                            
                            
                            )

                        );
                
                
           }
           
           
           
           
           
            if($args['GeneralDefaults']=="GeneralDefaults1")
            {
                    //Processing Appointment Time - starts here
                    $AppointmentAllocationSlots  = $this->getAppointmentAllocationSlot($args['ServiceProviderID']);
                   
                    $AM_TimeSlot_ID  = 0;
                    $PM_TimeSlot_ID  = 0;
                    $ANY_TimeSlot_ID = 0;
                    
                    foreach($AppointmentAllocationSlots AS $apsl)
                    {
                        if($apsl['Type']=="AM")
                        {
                            $AM_TimeSlot_ID = $apsl['AppointmentAllocationSlotID'];
                        }
                        else if($apsl['Type']=="PM")
                        {
                            $PM_TimeSlot_ID = $apsl['AppointmentAllocationSlotID'];
                        }
                        else if($apsl['Type']=="ANY")
                        {
                            $ANY_TimeSlot_ID = $apsl['AppointmentAllocationSlotID'];
                        }
                    }    
                    
                    
                    $args['ANYTimeFrom'] = ($args['AMTimeFrom'])?$args['AMTimeFrom']:$args['PMTimeFrom'];
                    $args['ANYTimeTo']   = ($args['PMTimeTo'])?$args['PMTimeTo']:$args['AMTimeTo'];
                    
                    $dataArray = array(
                        
                        0=>array( "AppointmentAllocationSlotID"=>$AM_TimeSlot_ID, "Description"=>"AM", "TimeFrom"=>$args['AMTimeFrom'], "TimeTo"=>$args['AMTimeTo'], "Type"=>"AM", "Colour"=>$args['AMColour']  ),
                        1=>array( "AppointmentAllocationSlotID"=>$PM_TimeSlot_ID, "Description"=>"PM", "TimeFrom"=>$args['PMTimeFrom'], "TimeTo"=>$args['PMTimeTo'], "Type"=>"PM", "Colour"=>$args['PMColour']  ),
                        2=>array( "AppointmentAllocationSlotID"=>$ANY_TimeSlot_ID, "Description"=>"ANY", "TimeFrom"=>$args['ANYTimeFrom'], "TimeTo"=>$args['ANYTimeTo'], "Type"=>"ANY", "Colour"=>'' )
                        
                    );
                    
                    
                    foreach($dataArray AS $dt)
                    {
                        
                        if($dt['AppointmentAllocationSlotID'])//Updating time slot data...
                        {
                            /* Execute a prepared statement by passing an array of values */
                            $sql = 'UPDATE appointment_allocation_slot SET Description=:Description, TimeFrom=:TimeFrom, TimeTo=:TimeTo, Type=:Type, Colour=:Colour WHERE AppointmentAllocationSlotID=:AppointmentAllocationSlotID';


                            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                            $result = $updateQuery->execute(

                                    array(

                                            ':Description' => $dt['Description'],
                                            ':TimeFrom' => $dt['TimeFrom'], 
                                            ':TimeTo' => $dt['TimeTo'], 
                                            ':Type' => $dt['Type'], 
                                            ':Colour' => $dt['Colour'], 
                                            ':AppointmentAllocationSlotID' => $dt['AppointmentAllocationSlotID']
                                        )

                                    );
                        }    
                        else if($dt['TimeFrom'] || $dt['TimeTo'])//Inserting time slot data...
                        {
                            /* Execute a prepared statement by passing an array of values */
                            $sql = 'INSERT INTO appointment_allocation_slot (Description, TimeFrom, TimeTo, Type, Colour, ServiceProviderID) VALUES (:Description, :TimeFrom, :TimeTo, :Type, :Colour, :ServiceProviderID)';


                            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                            $result = $insertQuery->execute(

                                    array(

                                            ':Description' => $dt['Description'],
                                            ':TimeFrom' => $dt['TimeFrom'], 
                                            ':TimeTo' => $dt['TimeTo'], 
                                            ':Type' => $dt['Type'], 
                                            ':Colour' => $dt['Colour'],
                                            ':ServiceProviderID' => $args['ServiceProviderID']
                                        )

                                    );
                        }
                        //default eng times 
                        $sql = 'UPDATE service_provider SET EngineerDefaultStartTime=:EngineerDefaultStartTime, EngineerDefaultEndTime=:EngineerDefaultEndTime WHERE ServiceProviderID=:ServiceProviderID';


                            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                            $result = $updateQuery->execute(

                                    array(

                                            ':EngineerDefaultStartTime' => $args['engDefStart'].":00:00",
                                            ':EngineerDefaultEndTime' => $args['engDefEnd'].":00:00", 
                                            ':ServiceProviderID' => $args['ServiceProviderID']
                                            
                                        )

                                    );
                        
                    }
                    
                
                
                    
                    
               if(isset($args['SetupUniqueTimeSlotPostcodes']))
               {
                   $args['SetupUniqueTimeSlotPostcodes'] = "Yes";
               }    
               else
               {
                    $args['SetupUniqueTimeSlotPostcodes'] = "No";
               }
               
               
           
                /* Execute a prepared statement by passing an array of values */
                $sql = 'UPDATE service_provider SET SetupUniqueTimeSlotPostcodes=:SetupUniqueTimeSlotPostcodes, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate  WHERE ServiceProviderID=:ServiceProviderID';


                $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $updateQuery->execute(

                        array(

                                ':SetupUniqueTimeSlotPostcodes' => $args['SetupUniqueTimeSlotPostcodes'],
                                ':ServiceProviderID' => $args['ServiceProviderID'],
                                ':ModifiedUserID' => $this->controller->user->UserID,
                                ':ModifiedDate' => date("Y-m-d H:i:s")
                            
                            
                            )

                        );     
                    
                    
                
                   
            }
            
             return array('status' => 'OK',
                                'message' => "Your data has been updated successfully.");
            
         }
        else
        {
            return array('status' => 'ERROR',
                        'message' => "Sorry, your data has not been processed since service provider id is missing.  Please check it.");
        } 
       
    }
    
    
    
    
    /**
     * Description
     * 
     * This method ll be called when Reset Viamente Consolidation Date is selected on diary defalt page.
     * And this code given by Andris and added by Nag.
     *
     * @param int     $ServiceProviderID
     * @param string  $Date  (YYYY-MM-DD)
     * @param array   $Engineers
     * @param int     $Type
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    
    public function viamenteUpdate($ServiceProviderID, $Date, $Engineers=array(),  $Type=1)
    {
         $this->controller->log('begin','new_log_');
        $this->controller->log($Engineers,'new_log_');
        $api_viamente  =   $this->controller->loadModel('APIViamente');
        $diary         =   $this->controller->loadModel('Diary');

        if($Type!=2)
        {
            $Engineers     =   $diary->getAssignedEngineers($Date, null,$ServiceProviderID);
        }

       //if $Type==2 means optimize 2 else optimize 1
        if($Type==2)
        {
             $this->controller->log('opt2','new_log_');
                 $this->controller->log($Engineers,'new_log_');
                //send to viamente
                $ret = $api_viamente->OptimiseRouteIndividualEngineers($ServiceProviderID, $Date, $Engineers);
               // $diary->ViamenteFinalize($Date, $ServiceProviderID);
                
        }
        else
        {
            $this->controller->log('opt1','new_log_');
                 $this->controller->log($Engineers,'new_log_');
                //send to viamente
                $ret = $api_viamente->OptimiseRoute($ServiceProviderID, $Date, $Engineers);
                 
        } 
        
        if(isset($ret->reqID))
        {
            //save map
            $diary->SaveMapUUID($ret->reqID, $ServiceProviderID, $Date);
        }
        
        if(isset($ret->routes))
        {

            $diary->updateEngineerWorkload($ret->routes, $Date, $ServiceProviderID,$Type);

        }

        if(isset($ret->unreachedWaypointNames))
        {

            $diary->setUnreached($ret->unreachedWaypointNames);
        }
        
           
        
        $apiA  = $this->controller->loadModel('APIAppointments');
        $apiA->RoutedAppointments($Date, $ServiceProviderID);
        
        
    }

   
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to remove given date in Viamente End Day table for specified Service Provider.
     *
     * @param array $args
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function removeDateViamenteEndDay($args) {
        
         if($args['ServiceProviderID'] && $args['ViamenteConsolidation'])
         {         
            $dateArray =  explode("/", $args['ViamenteConsolidation']);
            $RouteDate =  $dateArray[2]."-".$dateArray[1]."-".$dateArray[0];
            $selectSql = 'SELECT * FROM viamente_end_day WHERE ServiceProviderID=:ServiceProviderID AND RouteDate=:RouteDate';
            $fetchQuery = $this->conn->prepare($selectSql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':RouteDate' => $RouteDate,':ServiceProviderID' => $args['ServiceProviderID']));
            $results = $fetchQuery->fetchAll();
            foreach($results as $result)
            {
                $historySql = "insert into viamente_end_day_history (ServiceProviderID,CreatedUserID,RouteDate,EndDayType,CreatedDate,ActionType) values (:ServiceProviderID,:CreatedUserID,:RouteDate,:EndDayType,now(),'Reset Viamente')";
                $historyParams = array('ServiceProviderID' => $result['ServiceProviderID'], 'CreatedUserID' => $this->controller->user->UserID, 'RouteDate' => $result['RouteDate'], 'EndDayType' => $result['EndDayType'] );
                $this->execute($this->conn, $historySql, $historyParams);
            }
            /* Execute a prepared statement by passing an array of values */
            $sql = 'DELETE FROM viamente_end_day WHERE ServiceProviderID=:ServiceProviderID AND RouteDate=:RouteDate';


            $deleteQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result      = $deleteQuery->execute(

                    array(

                            ':RouteDate' => $RouteDate,
                            ':ServiceProviderID' => $args['ServiceProviderID']
                        )

                    );


            if($result)
            {
                    return array('status' => 'OK',
                            'message' => "Your request has been processed successfully.");
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => "Sorry, your data has not been processed since there is a problem.  Please check it.");
            }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
         }   
       
    }
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to check Viamente End Day Date exists or not for given $ServiceProviderID and Date
     *
     * @param array $args
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function isViamenteEndDayDateExists($ServiceProviderID, $Date) {
        
         if($ServiceProviderID && $Date)
         {         
            $dateArray =  explode("/", $Date);
            $RouteDate =  $dateArray[2]."-".$dateArray[1]."-".$dateArray[0];
           
            /* Execute a prepared statement by passing an array of values */
            $sql = 'SELECT ViamenteEndDayID FROM viamente_end_day WHERE ServiceProviderID=:ServiceProviderID AND RouteDate=:RouteDate';


            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(

                    array(

                            ':RouteDate' => $RouteDate,
                            ':ServiceProviderID' => $ServiceProviderID
                        )

                    );
            $result = $fetchQuery->fetch();
            

           

            if(isset($result['ViamenteEndDayID']) && $result['ViamenteEndDayID'])
            {
                    return array('status' => 'OK',
                            'message' => "");
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => "");
            }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => '');
         }   
       
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to inactive all skillset rows for given service provider.
     *
     * @param int $ServiceProviderID
     * @param int $RepairSkillID
        
     * @global $this->table 
     
     *    
     * @return void.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function inactiveSkillsSet($ServiceProviderID, $RepairSkillID) {
        
         if($ServiceProviderID && $RepairSkillID)
         {         
           
            //First getting SkillsetIDs for given $RepairSkillID from table skillset.
             /* Execute a prepared statement by passing an array of values */
            $sql = 'SELECT SkillsetID FROM skillset WHERE RepairSkillID=:RepairSkillID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            $fetchQuery->execute(array(':RepairSkillID' => $RepairSkillID));
            $SkillsetIDArray = $fetchQuery->fetchAll(); 
             
             
            /* Execute a prepared statement by passing an array of values */
            $update_sql = 'UPDATE '.$this->table.' SET Status=:Status WHERE ServiceProviderID=:ServiceProviderID AND SkillsetID=:SkillsetID';
            $updateQuery = $this->conn->prepare($update_sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            
            
            foreach($SkillsetIDArray AS $SSID=>$SSData)
            {
                $updateQuery->execute(

                        array(


                                ':ServiceProviderID' => $ServiceProviderID, 
                                ':SkillsetID' => $SSData['SkillsetID'], 
                                ':Status' => 'In-active'                            
                            )

                        );
            }    

            
         }
         
    }
    
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
   
    
    /**
     * getIdByServiceProviderSkillset
     *  
     * Return ID of a service provider skillset 
     * 
     * @param integer $ssID     Skillset ID
     *                $spId     Service Provider ID
     *        
     * 
     * @return      Array containing recrd selected 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getIdByServiceProviderSkillset($ssID,$spId) {
        $sql = "
                SELECT 
                        `ServiceProviderSkillsetID`
                FROM 
                        `service_provider_skillset`
                WHERE 
                        `SkillsetID` = $ssID
                        AND `ServiceProviderID` = $spId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['ServiceProviderSkillsetID']);                    /* Entry found so return record*/
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * getByServiceProviderSkillsetFromIds
     *  
     * Return ID of a service provider skillset 
     * 
     * @param integer $ssId     Skillset ID
     *                $spId     Service Provider ID
     * 
     * @return      Array containing recrd selected 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getServiceProviderSkillsetFromIds($ssId,$spId) {
        $sql = "
                SELECT 
                        *
                FROM 
                        `service_provider_skillset`
                WHERE 
                        `SkillsetID` = $ssId
                        AND `ServiceProviderID` = $spId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]);                    /* Entry found so return record*/
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
}
?>
