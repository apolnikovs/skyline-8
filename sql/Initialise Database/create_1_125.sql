# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.3.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Database creation script                        #
# Created on:            2012-11-21 10:15                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Tables                                                                 #
# ---------------------------------------------------------------------- #

# ---------------------------------------------------------------------- #
# Add table "vat_rate"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `vat_rate` (
    `VatRateID` INTEGER NOT NULL AUTO_INCREMENT,
    `Code` VARCHAR(255) NOT NULL,
    `VatCode` INTEGER NOT NULL,
    `VatRate` DOUBLE(6,2) NOT NULL,
    `Status` ENUM('Active','In-active') NOT NULL,
    PRIMARY KEY (`VatRateID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains Vat Rate details.';

# ---------------------------------------------------------------------- #
# Add table "version"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `version` (
    `VersionNo` VARCHAR(10) NOT NULL,
    `Updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `versionID` PRIMARY KEY (`VersionNo`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "postcode"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `postcode` (
    `Postcode` VARCHAR(4) NOT NULL,
    `CountyCode` INTEGER,
    CONSTRAINT `postcodeID` PRIMARY KEY (`Postcode`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "doc_api"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_api` (
    `DocApiID` INTEGER NOT NULL AUTO_INCREMENT,
    `Name` VARCHAR(50) NOT NULL,
    `Version` VARCHAR(10) NOT NULL,
    `Author` VARCHAR(50) NOT NULL,
    `Description` TEXT NOT NULL,
    `Baseuri` VARCHAR(50) NOT NULL,
    `TimeStamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted` TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`DocApiID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "doc_categories"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_categories` (
    `DocCategoriesID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocApiID` INTEGER NOT NULL,
    `Name` VARCHAR(50) NOT NULL,
    `Description` TEXT NOT NULL,
    `Path` VARCHAR(50) NOT NULL,
    `TimeStamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted` TINYINT(4) NOT NULL DEFAULT 0,
    PRIMARY KEY (`DocCategoriesID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_categories_DocApiID_FK` ON `doc_categories` (`DocApiID`);

# ---------------------------------------------------------------------- #
# Add table "doc_procedures"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_procedures` (
    `DocProceduresID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocCategoriesID` INTEGER NOT NULL,
    `Name` VARCHAR(50) NOT NULL,
    `Description` TEXT NOT NULL,
    `Path` VARCHAR(50),
    `methods` VARCHAR(100) NOT NULL,
    `formats` VARCHAR(100) NOT NULL,
    `auth_types` VARCHAR(100),
    `DocApiID` INTEGER,
    PRIMARY KEY (`DocProceduresID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_procedures_DocCategoriesID_FK` ON `doc_procedures` (`DocCategoriesID`);

# ---------------------------------------------------------------------- #
# Add table "doc_procedure_parameters"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_procedure_parameters` (
    `DocProcedureParametersID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocProceduresID` INTEGER NOT NULL,
    `CodeLanguage` VARCHAR(50) NOT NULL,
    `CDATA` TEXT NOT NULL,
    `Default` VARCHAR(50) NOT NULL,
    `Option` VARCHAR(50) NOT NULL,
    `Type` VARCHAR(50),
    `Size` INTEGER,
    `Example` TEXT NOT NULL,
    `response_ex` TEXT NOT NULL,
    `Required` TINYINT(1) NOT NULL,
    PRIMARY KEY (`DocProcedureParametersID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_procedure_parameters_DocProceduresID_FK` ON `doc_procedure_parameters` (`DocProceduresID`);

# ---------------------------------------------------------------------- #
# Add table "contact_us_subject"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `contact_us_subject` (
    `ContactUsSubjectID` INTEGER,
    `Subject` VARCHAR(200),
    `PriorityOrder` INTEGER,
    `CreatedDate` DATE,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` DATE
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "contact_us_messages"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `contact_us_messages` (
    `ContactUsMessageID` INTEGER,
    `CustomerID` INTEGER,
    `ContactUsSubjectID` INTEGER,
    `Message` TEXT,
    `CreatedDate` DATE,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active') DEFAULT 'Active'
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "welcome_message_defaults"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `welcome_message_defaults` (
    `WelcomeMessageDefaultsID` INTEGER NOT NULL,
    `Title` TEXT,
    `HyperlinkText` VARCHAR(100),
    `Message` TEXT,
    `Type` ENUM('Message','Alert'),
    `CreatedDateTime` DATETIME,
    `ModifiedUserID` INTEGER,
    `ModifiedDate` DATE,
    PRIMARY KEY (`WelcomeMessageDefaultsID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "welcome_messages"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `welcome_messages` (
    `WelcomeMessageID` INTEGER NOT NULL,
    `CustomerID` INTEGER,
    `WelcomeMessageDefaultsID` INTEGER,
    `JobID` INTEGER,
    `CreatedDateTime` DATETIME,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    PRIMARY KEY (`WelcomeMessageID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "diary_allocation"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_allocation` (
    `DiaryAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `AllocatedDate` DATE,
    `AppointmentAllocationSlotID` INTEGER,
    `TimeFrom` TIME,
    `NumberOfAllocations` INTEGER,
    `ServiceProviderEngineerID` INTEGER,
    `SlotsLeft` TINYINT(3),
    CONSTRAINT `diary_allocationID` PRIMARY KEY (`DiaryAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_diary_allocation_1` ON `diary_allocation` (`ServiceProviderID`);

CREATE INDEX `IDX_diary_allocation_2` ON `diary_allocation` (`AllocatedDate`);

CREATE INDEX `IDX_diary_allocation_3` ON `diary_allocation` (`AppointmentAllocationSlotID`);

# ---------------------------------------------------------------------- #
# Add table "diary_town_allocation"                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_town_allocation` (
    `DiaryTownAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `DiaryAllocationID` INTEGER,
    `Town` VARCHAR(40),
    CONSTRAINT `diary_town_allocationID` PRIMARY KEY (`DiaryTownAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_diary_town_allocation_1` ON `diary_town_allocation` (`DiaryAllocationID`);

# ---------------------------------------------------------------------- #
# Add table "diary_postcode_allocation"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_postcode_allocation` (
    `DiaryPostcodeAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `DiaryAllocationID` INTEGER,
    `Postcode` VARCHAR(8),
    CONSTRAINT `diary_postcode_allocationID` PRIMARY KEY (`DiaryPostcodeAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_diary_postcode_allocation_1` ON `diary_postcode_allocation` (`DiaryAllocationID`);

# ---------------------------------------------------------------------- #
# Add table "appointment_source"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment_source` (
    `AppointmentSourceID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `appointment_sourceID` PRIMARY KEY (`AppointmentSourceID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "appointment_status"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment_status` (
    `AppointmentStatusID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `appointment_statusID` PRIMARY KEY (`AppointmentStatusID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_engineer` (
    `ServiceProviderEngineerID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `EngineerFirstName` VARCHAR(40),
    `EngineerLastName` VARCHAR(40),
    `ServiceBaseUserCode` VARCHAR(4),
    `EmailAddress` VARCHAR(255),
    `RouteColour` VARCHAR(20),
    `StartShift` TIME,
    `EndShift` TIME,
    `LunchBreakDuration` TINYINT(3),
    `LunchPeriodFrom` TIME,
    `LunchPeriodTo` TIME,
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    `StartPostcode` VARCHAR(8),
    `EndPostcode` VARCHAR(8),
    `StartLocation` ENUM('Home','Office','Other'),
    `EndLocation` ENUM('Home','Office','Other'),
    `StartHomePostcode` VARCHAR(10),
    `StartOfficePostcode` VARCHAR(10),
    `StartOtherPostcode` VARCHAR(10),
    `EndHomePostcode` VARCHAR(10),
    `EndOfficePostcode` VARCHAR(10),
    `EndOtherPostcode` VARCHAR(10),
    `MondayActive` ENUM('yes','no') DEFAULT 'no',
    `MondayStartShift` TIME,
    `MondayEndShift` TIME,
    `MondayStartPostcode` VARCHAR(8),
    `MondayEndPostcode` VARCHAR(8),
    `TuesdayActive` ENUM('yes','no') DEFAULT 'no',
    `TuesdayStartShift` TIME,
    `TuesdayEndShift` TIME,
    `TuesdayStartPostcode` VARCHAR(8),
    `TuesdayEndPostcode` VARCHAR(8),
    `WednesdayActive` ENUM('yes','no') DEFAULT 'no',
    `WednesdayStartShift` TIME,
    `WednesdayEndShift` TIME,
    `WednesdayStartPostcode` VARCHAR(8),
    `WednesdayEndPostcode` VARCHAR(8),
    `ThursdayActive` ENUM('yes','no') DEFAULT 'no',
    `ThursdayStartShift` TIME,
    `ThursdayEndShift` TIME,
    `ThursdayStartPostcode` VARCHAR(8),
    `ThursdayEndPostcode` VARCHAR(8),
    `FridayActive` ENUM('yes','no') DEFAULT 'no',
    `FridayStartShift` TIME,
    `FridayEndShift` TIME,
    `FridayStartPostcode` VARCHAR(8),
    `FridayEndPostcode` VARCHAR(8),
    `SaturdayActive` ENUM('yes','no') DEFAULT 'no',
    `SaturdayStartShift` TIME,
    `SaturdayEndShift` TIME,
    `SaturdayStartPostcode` VARCHAR(8),
    `SaturdayEndPostcode` VARCHAR(8),
    `SundayActive` ENUM('yes','no') DEFAULT 'no',
    `SundayStartShift` TIME,
    `SundayEndShift` TIME,
    `SundayStartPostcode` VARCHAR(8),
    `SundayEndPostcode` VARCHAR(8),
    CONSTRAINT `service_provider_engineerID` PRIMARY KEY (`ServiceProviderEngineerID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_service_provider_engineer_1` ON `service_provider_engineer` (`ServiceProviderID`);

# ---------------------------------------------------------------------- #
# Add table "service_provider_skillset"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_skillset` (
    `ServiceProviderSkillsetID` INTEGER NOT NULL AUTO_INCREMENT,
    `SkillsetID` INTEGER,
    `ServiceDuration` SMALLINT(4),
    `ServiceProviderID` INTEGER,
    `EngineersRequired` TINYINT(2) DEFAULT 1,
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    CONSTRAINT `service_provider_skillsetID` PRIMARY KEY (`ServiceProviderSkillsetID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_service_provider_skillset_1` ON `service_provider_skillset` (`ServiceProviderID`);

CREATE INDEX `IDX_service_provider_skillset_2` ON `service_provider_skillset` (`SkillsetID`);

# ---------------------------------------------------------------------- #
# Add table "skillset"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `skillset` (
    `SkillsetID` INTEGER NOT NULL AUTO_INCREMENT,
    `SkillsetName` VARCHAR(40),
    `ServiceDuration` SMALLINT(4),
    `EngineersRequired` TINYINT(2) DEFAULT 1,
    `RepairSkillID` INTEGER,
    `ServiceTypeID` INTEGER,
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    `AppointmentTypeID` INTEGER,
    CONSTRAINT `skillsetID` PRIMARY KEY (`SkillsetID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_skillset_1` ON `skillset` (`AppointmentTypeID`);

# ---------------------------------------------------------------------- #
# Add table "appointment_allocation_slot"                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment_allocation_slot` (
    `AppointmentAllocationSlotID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    `TimeFrom` TIME,
    `TimeTo` TIME,
    `ServiceProviderID` INTEGER,
    `Type` ENUM('AM','PM','ANY'),
    CONSTRAINT `appointment_allocation_slotID` PRIMARY KEY (`AppointmentAllocationSlotID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "non_skyline_job"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `non_skyline_job` (
    `NonSkylineJobID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `AppointmentID` INTEGER,
    `ServiceProviderJobNo` INTEGER,
    `CustomerSurname` VARCHAR(40),
    `CustomerTitle` VARCHAR(10),
    `Postcode` VARCHAR(8),
    `JobType` VARCHAR(50),
    `JobSourceID` INTEGER,
    `RepairSkillID` INTEGER,
    `JobTypeID` INTEGER,
    `ServiceType` VARCHAR(50),
    `CustomerAddress1` VARCHAR(50),
    `CustomerAddress2` VARCHAR(50),
    `CustomerAddress3` VARCHAR(50),
    `CustomerAddress4` VARCHAR(50),
    `ProductType` VARCHAR(50),
    `Manufacturer` VARCHAR(50),
    `ModelNumber` VARCHAR(50),
    `Client` VARCHAR(50),
    `ReportedFault` VARCHAR(2000),
    CONSTRAINT `non_skyline_jobID` PRIMARY KEY (`NonSkylineJobID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_non_skyline_job_1` ON `non_skyline_job` (`ServiceProviderID`);

CREATE INDEX `IDX_non_skyline_job_2` ON `non_skyline_job` (`AppointmentID`);

CREATE INDEX `IDX_non_skyline_job_3` ON `non_skyline_job` (`ServiceProviderJobNo`);

# ---------------------------------------------------------------------- #
# Add table "job_source"                                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `job_source` (
    `JobSourceID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `job_sourceID` PRIMARY KEY (`JobSourceID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer_skillset"                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_engineer_skillset` (
    `ServiceProviderEngineerSkillsetID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderSkillsetID` INTEGER,
    `ServiceProviderEngineerID` INTEGER,
    CONSTRAINT `service_provider_engineer_skillsetID` PRIMARY KEY (`ServiceProviderEngineerSkillsetID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_service_provider_engineer_skillset_1` ON `service_provider_engineer_skillset` (`ServiceProviderSkillsetID`);

CREATE INDEX `IDX_service_provider_engineer_skillset_2` ON `service_provider_engineer_skillset` (`ServiceProviderEngineerID`);

# ---------------------------------------------------------------------- #
# Add table "testimonials"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `testimonials` (
    `CustomerID` INTEGER NOT NULL,
    `ServiceProviderID` INTEGER NOT NULL,
    `TestimonialText` TEXT NOT NULL,
    `TestimonialUpdated` TIMESTAMP NOT NULL
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "appointment_type"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment_type` (
    `AppointmentTypeID` INTEGER NOT NULL AUTO_INCREMENT,
    `Type` VARCHAR(60),
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    `CreatedDate` TIMESTAMP,
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP,
    CONSTRAINT `appointment_typeID` PRIMARY KEY (`AppointmentTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "postcode_lat_long"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `postcode_lat_long` (
    `PostCode` VARCHAR(10) NOT NULL,
    `Latitude` FLOAT,
    `Longitude` FLOAT,
    CONSTRAINT `postcode_lat_longID` PRIMARY KEY (`PostCode`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "diary_sp_map"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_sp_map` (
    `diarySPMapID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `MapDate` DATE,
    `MapUUID` VARCHAR(64),
    `dateCreated` TIMESTAMP,
    CONSTRAINT `diary_sp_mapID` PRIMARY KEY (`diarySPMapID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "primary_fields"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `primary_fields` (
    `primaryFieldID` INTEGER NOT NULL AUTO_INCREMENT,
    `primaryFieldName` VARCHAR(250) NOT NULL,
    CONSTRAINT `primary_fieldsID` PRIMARY KEY (`primaryFieldID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "alternative_fields"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `alternative_fields` (
    `alternativeFieldID` INTEGER NOT NULL AUTO_INCREMENT,
    `primaryFieldID` INTEGER NOT NULL,
    `alternativeFieldName` VARCHAR(250) NOT NULL,
    `status` ENUM('Active','In-active') NOT NULL DEFAULT 'Active',
    `brandID` INTEGER NOT NULL,
    CONSTRAINT `alternative_fieldsID` PRIMARY KEY (`alternativeFieldID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "job_import_batch_data"                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `job_import_batch_data` (
    `batchID` INTEGER NOT NULL AUTO_INCREMENT,
    `batchNumber` INTEGER NOT NULL,
    `clientID` INTEGER NOT NULL,
    `batchRowCount` INTEGER NOT NULL,
    `batchDescription` VARCHAR(250) NOT NULL,
    `batchState` ENUM('created','importing','finished') NOT NULL,
    `batchHash` VARCHAR(40) NOT NULL,
    `batchTime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `job_import_batch_dataID` PRIMARY KEY (`batchID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "viamente_end_day"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `viamente_end_day` (
    `ViamenteEndDayID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `RouteDate` DATE,
    `CreatedDate` TIMESTAMP,
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP,
    CONSTRAINT `viamente_end_dayID` PRIMARY KEY (`ViamenteEndDayID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer_workload"                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_engineer_workload` (
    `ServiceProviderEngineerWorkloadID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER NOT NULL,
    `ServiceProviderEngineerID` INTEGER NOT NULL,
    `WorkingDay` DATE NOT NULL,
    `TotalWorkTimeSec` INTEGER NOT NULL,
    `TotalIdleTimeSec` INTEGER NOT NULL,
    `TotalServiceTimeSec` INTEGER NOT NULL,
    `TotalDriveTimeSec` INTEGER NOT NULL,
    `TotalSteps` INTEGER NOT NULL,
    CONSTRAINT `service_provider_engineer_workloadID` PRIMARY KEY (`ServiceProviderEngineerWorkloadID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "service_provider_contacts"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_contacts` (
    `ServiceProviderID` INTEGER NOT NULL,
    `ClientID` INTEGER NOT NULL,
    `ContactPhone` VARCHAR(40),
    `ContactEmail` VARCHAR(40),
    CONSTRAINT `service_provider_contactsID` PRIMARY KEY (`ServiceProviderID`, `ClientID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "service_provider_trade_account"                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_trade_account` (
    `ServiceProviderTradeAccountID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `TradeAccount` VARCHAR(40),
    `Postcode` VARCHAR(10),
    `Monday` ENUM('Yes','No') DEFAULT 'No',
    `Tuesday` ENUM('Yes','No') DEFAULT 'No',
    `Wednesday` ENUM('Yes','No') DEFAULT 'No',
    `Thursday` ENUM('Yes','No') DEFAULT 'No',
    `Friday` ENUM('Yes','No') DEFAULT 'No',
    `Saturday` ENUM('Yes','No') DEFAULT 'No',
    `Sunday` ENUM('Yes','No') DEFAULT 'No',
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `service_provider_trade_accountID` PRIMARY KEY (`ServiceProviderTradeAccountID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "doc_method_response"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_method_response` (
    `DocMethodResponseID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocProceduresID` INTEGER NOT NULL,
    `AnswerCode` INTEGER NOT NULL,
    `Response` TEXT NOT NULL,
    `Description` TEXT,
    `CDATA` TEXT,
    PRIMARY KEY (`DocMethodResponseID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_method_response_DocProceduresID_FK` ON `doc_method_response` (`DocProceduresID`);

# ---------------------------------------------------------------------- #
# Add table "network"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `network` (
    `NetworkID` INTEGER NOT NULL AUTO_INCREMENT,
    `DefaultClientID` INTEGER,
    `CreditPrice` DECIMAL(10,2) DEFAULT 0.00,
    `CreditLevel` INTEGER DEFAULT 0,
    `ReorderLevel` INTEGER DEFAULT 0,
    `CreditLevelMailSentOn` DATE,
    `InvoiceCompanyName` VARCHAR(30),
    `VATRateID` INTEGER,
    `VATNumber` VARCHAR(15),
    `SageReferralNominalCode` INTEGER,
    `SageLabourNominalCode` INTEGER,
    `SagePartsNominalCode` INTEGER,
    `SageCarriageNominalCode` INTEGER,
    `CompanyName` VARCHAR(50),
    `PostalCode` VARCHAR(10),
    `BuildingNameNumber` VARCHAR(40),
    `Street` VARCHAR(40),
    `LocalArea` VARCHAR(40),
    `TownCity` VARCHAR(60),
    `ContactPhone` VARCHAR(40),
    `ContactPhoneExt` VARCHAR(10),
    `CountyID` INTEGER,
    `CountryID` INTEGER,
    `ContactEmail` VARCHAR(40),
    `Terms` TEXT,
    `CustomerAssurancePolicy` TEXT,
    `TermsConditions` TEXT,
    `Skin` VARCHAR(20),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`NetworkID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_network_1` ON `network` (`CompanyName`);

CREATE INDEX `IDX_network_CountryID_FK` ON `network` (`CountryID`);

CREATE INDEX `IDX_network_CountyID_FK` ON `network` (`CountyID`);

CREATE INDEX `IDX_network_ModifiedUserID_FK` ON `network` (`ModifiedUserID`);

CREATE INDEX `IDX_network_DefaultClientID_FK` ON `network` (`DefaultClientID`);

# ---------------------------------------------------------------------- #
# Add table "client"                                                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `client` (
    `ClientID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `DefaultBranchID` INTEGER,
    `ClientName` VARCHAR(40),
    `AccountNumber` TEXT,
    `GroupName` VARCHAR(40),
    `InvoiceNetwork` ENUM('Yes', 'No'),
    `VATRateID` INTEGER,
    `VATNumber` VARCHAR(40),
    `PaymentDiscount` DECIMAL(10,2) DEFAULT 0.00,
    `PaymentTerms` INTEGER,
    `VendorNumber` VARCHAR(40),
    `PurchaseOrderNumber` VARCHAR(40),
    `SageAccountNumber` VARCHAR(40),
    `SageNominalCode` INTEGER,
    `ForcePolicyNo` CHAR(1) DEFAULT 'Y',
    `PromptETD` CHAR(1) DEFAULT 'Y',
    `TradeSpecificUnitTypes` CHAR(1) DEFAULT 'Y',
    `TradeSpecificManufacturers` CHAR(1) DEFAULT 'Y',
    `TradeSpecificCallTypes` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `TradeSpecificJobTypes` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `EnableCompletionStatus` CHAR(1) DEFAULT 'Y',
    `EnableProductDatabase` ENUM('Yes','No') NOT NULL DEFAULT 'No',
    `EnableCatalogueSearch` ENUM('Yes','No') NOT NULL DEFAULT 'Yes',
    `ClientShortName` VARCHAR(9),
    `BuildingNameNumber` VARCHAR(40),
    `PostalCode` VARCHAR(10),
    `Street` VARCHAR(40),
    `LocalArea` VARCHAR(40),
    `TownCity` VARCHAR(60),
    `CountyID` INTEGER,
    `CountryID` INTEGER,
    `ContactPhone` VARCHAR(40),
    `ContactPhoneExt` VARCHAR(10),
    `ContactEmail` VARCHAR(40),
    `InvoicedCompanyName` VARCHAR(40),
    `InvoicedBuilding` VARCHAR(40),
    `InvoicedStreet` VARCHAR(40),
    `InvoicedArea` VARCHAR(40),
    `InvoicedTownCity` VARCHAR(40),
    `InvoicedCountyID` INTEGER,
    `InvoicedCountryID` INTEGER,
    `InvoicedPostalCode` VARCHAR(40),
    `Skin` VARCHAR(20),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ClientID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_client_ModifiedUserID_FK` ON `client` (`ModifiedUserID`);

CREATE INDEX `IDX_client_ServiceProviderID_FK` ON `client` (`ServiceProviderID`);

CREATE INDEX `IDX_client_DefaultBranchID_FK` ON `client` (`DefaultBranchID`);

# ---------------------------------------------------------------------- #
# Add table "branch"                                                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `branch` (
    `BranchID` INTEGER NOT NULL AUTO_INCREMENT,
    `BranchName` VARCHAR(40),
    `BranchNumber` VARCHAR(40),
    `BranchType` ENUM('Store', 'Extra', 'Call Centre', 'Website'),
    `BuildingNameNumber` VARCHAR(40),
    `Street` VARCHAR(40),
    `LocalArea` VARCHAR(40),
    `TownCity` VARCHAR(60),
    `CountyID` INTEGER,
    `CountryID` INTEGER,
    `PostalCode` VARCHAR(10),
    `ContactPhone` VARCHAR(40),
    `ContactPhoneExt` VARCHAR(10),
    `ContactEmail` VARCHAR(40),
    `UseAddressProgram` ENUM('Yes', 'No'),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `ContactFax` VARCHAR(40),
    `AccountNo` VARCHAR(40),
    PRIMARY KEY (`BranchID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_branch_CountyID_FK` ON `branch` (`CountyID`);

CREATE INDEX `IDX_branch_CountryID_FK` ON `branch` (`CountryID`);

CREATE INDEX `IDX_branch_ModifiedUserID_FK` ON `branch` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "service_provider"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider` (
    `ServiceProviderID` INTEGER NOT NULL AUTO_INCREMENT,
    `CompanyName` VARCHAR(50),
    `BuildingNameNumber` VARCHAR(40),
    `PostalCode` VARCHAR(10),
    `Street` VARCHAR(40),
    `LocalArea` VARCHAR(40),
    `TownCity` VARCHAR(60),
    `CountyID` INTEGER,
    `CountryID` INTEGER,
    `WeekdaysOpenTime` TIME,
    `WeekdaysCloseTime` TIME,
    `SaturdayOpenTime` TIME,
    `SaturdayCloseTime` TIME,
    `ServiceProvided` TEXT,
    `SynopsisOfBusiness` TEXT,
    `ContactPhoneExt` VARCHAR(10),
    `ContactPhone` VARCHAR(40),
    `ContactEmail` VARCHAR(40),
    `AdminSupervisor` VARCHAR(40),
    `ServiceManager` VARCHAR(40),
    `Accounts` VARCHAR(40),
    `IPAddress` VARCHAR(40),
    `Port` INTEGER,
    `InvoiceToCompanyName` VARCHAR(40),
    `UseAddressProgram` ENUM('Yes', 'No'),
    `ServiceBaseVersion` VARCHAR(255),
    `ServiceBaseLicence` VARCHAR(255),
    `VATRateID` VARCHAR(40),
    `VATNumber` VARCHAR(40),
    `Platform` ENUM( 'ServiceBase', 'API', 'Skyline','RMA Tracker' ),
    `Skin` VARCHAR(20),
    `ReplyEmail` VARCHAR(64),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `ViamenteKey` VARCHAR(50),
    `DiaryStatus` ENUM('Active', 'In-Active') NOT NULL DEFAULT 'In-Active',
    `OnlineDiary` ENUM('Active','In-Active') DEFAULT 'In-Active',
    `DefaultTravelTime` INTEGER NOT NULL DEFAULT 120,
    `DefaultTravelSpeed` INTEGER NOT NULL DEFAULT 120,
    `AutoChangeTimeSlotLabel` ENUM('Yes', 'No') NOT NULL DEFAULT 'No',
    PRIMARY KEY (`ServiceProviderID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_service_provider_1` ON `service_provider` (`CompanyName`);

CREATE INDEX `IDX_service_provider_2` ON `service_provider` (`DiaryStatus`);

CREATE INDEX `IDX_service_provider_CountyID_FK` ON `service_provider` (`CountyID`);

CREATE INDEX `IDX_service_provider_CountryID_FK` ON `service_provider` (`CountryID`);

CREATE INDEX `IDX_service_provider_ModifiedUserID_FK` ON `service_provider` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "manufacturer"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `manufacturer` (
    `ManufacturerID` INTEGER NOT NULL AUTO_INCREMENT,
    `ManufacturerName` VARCHAR(30),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ManufacturerID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE UNIQUE INDEX `IDX_manufacturer_1` ON `manufacturer` (`ManufacturerName`);

CREATE INDEX `IDX_manufacturer_ModifiedUserID_FK` ON `manufacturer` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "service_type"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_type` (
    `ServiceTypeID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER,
    `JobTypeID` INTEGER,
    `ServiceTypeName` VARCHAR(40),
    `ServiceTypeCode` INTEGER,
    `Code` VARCHAR(64),
    `Priority` INTEGER,
    `Chargeable` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `Warranty` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `DateOfPurchaseForced` ENUM('N','B','C') NOT NULL DEFAULT 'N',
    `OverrideTradeSettingsPolicy` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `ForcePolicyNoAtBooking` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `OverrideTradeSettingsModel` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `ForceModelNumber` ENUM('Y','N') NOT NULL DEFAULT 'N',
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ServiceTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_service_type_1` ON `service_type` (`ServiceTypeName`);

CREATE INDEX `IDX_service_type_BrandID_FK` ON `service_type` (`BrandID`);

CREATE INDEX `IDX_service_type_JobTypeID_FK` ON `service_type` (`JobTypeID`);

CREATE INDEX `IDX_service_type_ModifiedUserID_FK` ON `service_type` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "service_type_alias"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_type_alias` (
    `ServiceTypeAliasID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER,
    `ClientID` INTEGER NOT NULL,
    `ServiceTypeID` INTEGER NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ServiceTypeAliasID`),
    CONSTRAINT `TUC_service_type_alias_1` UNIQUE (`ClientID`, `NetworkID`, `ServiceTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_service_type_alias_ClientID_FK` ON `service_type_alias` (`ClientID`);

CREATE INDEX `IDX_service_type_alias_ServiceTypeID_FK` ON `service_type_alias` (`ServiceTypeID`);

CREATE INDEX `IDX_service_type_alias_NetworkID_FK` ON `service_type_alias` (`NetworkID`);

CREATE INDEX `IDX_service_type_alias_ModifiedUserID_FK` ON `service_type_alias` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "network_client"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `network_client` (
    `NetworkClientID` INTEGER NOT NULL AUTO_INCREMENT,
    `ClientID` INTEGER NOT NULL,
    `NetworkID` INTEGER NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`NetworkClientID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_network_client_NetworkID_FK` ON `network_client` (`NetworkID`);

CREATE INDEX `IDX_network_client_ClientID_FK` ON `network_client` (`ClientID`);

CREATE INDEX `IDX_network_client_ModifiedUserID_FK` ON `network_client` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "network_manufacturer"                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `network_manufacturer` (
    `NetworkManufacturerID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER NOT NULL,
    `ManufacturerID` INTEGER NOT NULL,
    `CostCodeAccountNumber` VARCHAR(40),
    `AccountNumber` VARCHAR(40),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`NetworkManufacturerID`),
    CONSTRAINT `TUC_network_manufacturer_1` UNIQUE (`NetworkID`, `ManufacturerID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_network_manufacturer_ManufacturerID_FK` ON `network_manufacturer` (`ManufacturerID`);

CREATE INDEX `IDX_network_manufacturer_NetworkID_FK` ON `network_manufacturer` (`NetworkID`);

CREATE INDEX `IDX_network_manufacturer_ModifiedUserID_FK` ON `network_manufacturer` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "client_branch"                                              #
# ---------------------------------------------------------------------- #

CREATE TABLE `client_branch` (
    `ClientBranchID` INTEGER NOT NULL AUTO_INCREMENT,
    `ClientID` INTEGER NOT NULL,
    `BranchID` INTEGER NOT NULL,
    `NetworkID` INTEGER NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ClientBranchID`),
    CONSTRAINT `TUC_client_branch_1` UNIQUE (`NetworkID`, `BranchID`, `ClientID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_client_branch_ClientID_FK` ON `client_branch` (`ClientID`);

CREATE INDEX `IDX_client_branch_BranchID_FK` ON `client_branch` (`BranchID`);

CREATE INDEX `IDX_client_branch_NetworkID_FK` ON `client_branch` (`NetworkID`);

CREATE INDEX `IDX_client_branch_ModifiedUserID_FK` ON `client_branch` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "network_service_provider"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `network_service_provider` (
    `NetworkServiceProviderID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER NOT NULL,
    `ServiceProviderID` INTEGER NOT NULL,
    `AccountNo` VARCHAR(20),
    `SageSalesAccountNo` VARCHAR(40),
    `SagePurchaseAccountNo` VARCHAR(40),
    `TrackingUsername` VARCHAR(40),
    `TrackingPassword` VARCHAR(40),
    `WarrantyUsername` VARCHAR(40),
    `WarrantyPassword` VARCHAR(40),
    `WarrantyAccountNumber` VARCHAR(40),
    `Area` VARCHAR(40),
    `WalkinJobsUpload` BOOL,
    `CompanyCode` VARCHAR(255),
    `SamsungDownloadEnabled` BOOL NOT NULL DEFAULT false,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`NetworkServiceProviderID`),
    CONSTRAINT `TUC_network_service_provider_1` UNIQUE (`NetworkID`, `ServiceProviderID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_network_service_provider_NetworkID_FK` ON `network_service_provider` (`NetworkID`);

CREATE INDEX `IDX_network_service_provider_ServiceProviderID_FK` ON `network_service_provider` (`ServiceProviderID`);

CREATE INDEX `IDX_network_service_provider_ModifiedUserID_FK` ON `network_service_provider` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "model"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `model` (
    `ModelID` INTEGER NOT NULL AUTO_INCREMENT,
    `ManufacturerID` INTEGER,
    `UnitTypeID` INTEGER,
    `ModelNumber` VARCHAR(30),
    `ModelName` VARCHAR(24),
    `ModelDescription` TEXT,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ModelID`),
    CONSTRAINT `TUC_model_1` UNIQUE (`ModelNumber`, `ManufacturerID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_model_ManufacturerID_FK` ON `model` (`ManufacturerID`);

CREATE INDEX `IDX_model_UnitTypeID_FK` ON `model` (`UnitTypeID`);

CREATE INDEX `IDX_model_ModifiedUserID_FK` ON `model` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "unit_type"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `unit_type` (
    `UnitTypeID` INTEGER NOT NULL AUTO_INCREMENT,
    `RepairSkillID` INTEGER,
    `UnitTypeName` VARCHAR(30),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`UnitTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE UNIQUE INDEX `IDX_unit_type_1` ON `unit_type` (`RepairSkillID`,`UnitTypeName`);

CREATE INDEX `IDX_unit_type_RepairSkillID_FK` ON `unit_type` (`RepairSkillID`);

CREATE INDEX `IDX_unit_type_ModifiedUserID_FK` ON `unit_type` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "unit_client_type"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `unit_client_type` (
    `UnitClientTypeID` INTEGER NOT NULL AUTO_INCREMENT,
    `UnitTypeID` INTEGER NOT NULL,
    `ClientID` INTEGER NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`UnitClientTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_unit_client_type_ClientID_FK` ON `unit_client_type` (`ClientID`);

CREATE INDEX `IDX_unit_client_type_UnitTypeID_FK` ON `unit_client_type` (`UnitTypeID`);

CREATE INDEX `IDX_unit_client_type_ModifiedUserID_FK` ON `unit_client_type` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "user"                                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `user` (
    `UserID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER,
    `ServiceProviderID` INTEGER,
    `ClientID` INTEGER,
    `BranchID` INTEGER,
    `ManufacturerID` INTEGER,
    `ExtendedWarrantorID` INTEGER,
    `SecurityQuestionID` INTEGER NOT NULL,
    `DefaultBrandID` INTEGER,
    `Username` VARCHAR(20),
    `Password` VARCHAR(32),
    `Answer` VARCHAR(40),
    `Position` VARCHAR(40),
    `SuperAdmin` BOOL NOT NULL DEFAULT false,
    `LastLoggedIn` TIMESTAMP DEFAULT '0000-00-00 00:00:00',
    `CustomerTitleID` INTEGER,
    `ContactFirstName` VARCHAR(40),
    `ContactLastName` VARCHAR(40),
    `ContactHomePhone` VARCHAR(40),
    `ContactWorkPhoneExt` VARCHAR(10),
    `ContactWorkPhone` VARCHAR(40),
    `ContactFax` VARCHAR(40),
    `ContactMobile` VARCHAR(40),
    `ContactEmail` VARCHAR(40),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `BranchJobSearchScope` ENUM('Branch Only', 'Brand', 'Client') DEFAULT 'Client',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`UserID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE UNIQUE INDEX `IDX_user_Username` ON `user` (`Username`);

CREATE INDEX `IDX_user_NetworkID_FK` ON `user` (`NetworkID`);

CREATE INDEX `IDX_user_ServiceProviderID_FK` ON `user` (`ServiceProviderID`);

CREATE INDEX `IDX_user_ClientID_FK` ON `user` (`ClientID`);

CREATE INDEX `IDX_user_BranchID_FK` ON `user` (`BranchID`);

CREATE INDEX `IDX_user_DefaultBrandID_FK` ON `user` (`DefaultBrandID`);

CREATE INDEX `IDX_user_SecurityQuestionID_FK` ON `user` (`SecurityQuestionID`);

CREATE INDEX `IDX_user_ModifiedUserID_FK` ON `user` (`ModifiedUserID`);

CREATE INDEX `IDX_user_CustomerTitleID_FK` ON `user` (`CustomerTitleID`);

# ---------------------------------------------------------------------- #
# Add table "permission"                                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `permission` (
    `PermissionID` INTEGER NOT NULL,
    `Name` VARCHAR(40),
    `Description` TEXT,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `URLSegment` VARCHAR(50),
    PRIMARY KEY (`PermissionID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_permission_ModifiedUserID_FK` ON `permission` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "assigned_permission"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `assigned_permission` (
    `PermissionID` INTEGER NOT NULL,
    `RoleID` INTEGER NOT NULL,
    `AccessLevel` CHAR(6),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`PermissionID`, `RoleID`),
    CONSTRAINT `TUC_assigned_permission_1` UNIQUE (`RoleID`, `PermissionID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_assigned_permission_PermissionID_FK` ON `assigned_permission` (`PermissionID`);

CREATE INDEX `IDX_assigned_permission_RoleID_FK` ON `assigned_permission` (`RoleID`);

CREATE INDEX `IDX_assigned_permission_ModifiedUserID_FK` ON `assigned_permission` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "role"                                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `role` (
    `RoleID` INTEGER NOT NULL AUTO_INCREMENT,
    `Parent` INTEGER DEFAULT 0,
    `Name` VARCHAR(40) NOT NULL,
    `UserType` ENUM('Service Network','Client','Branch','Service Provider','Manufacturer', 'Extended Warrantor') NOT NULL,
    `Comment` TEXT,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`RoleID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_role_ModifiedUserID_FK` ON `role` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "user_role"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `user_role` (
    `UserID` INTEGER NOT NULL,
    `RoleID` INTEGER NOT NULL,
    PRIMARY KEY (`UserID`, `RoleID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_user_role_UserID_FK` ON `user_role` (`UserID`);

CREATE INDEX `IDX_user_role_RoleID_FK` ON `user_role` (`RoleID`);

# ---------------------------------------------------------------------- #
# Add table "job"                                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `job` (
    `JobID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER,
    `ClientID` INTEGER,
    `BranchID` INTEGER,
    `ServiceProviderID` INTEGER,
    `CustomerID` INTEGER,
    `ProductID` INTEGER,
    `ManufacturerID` INTEGER,
    `ServiceTypeID` INTEGER,
    `JobTypeID` INTEGER NOT NULL,
    `StatusID` INTEGER,
    `OpenJobStatus` ENUM('in_store','with_supplier','awaiting_collection','customer_notified'),
    `NetworkRefNo` VARCHAR(20),
    `AgentRefNo` VARCHAR(20),
    `ServiceCentreJobNo` INTEGER,
    `JobSite` ENUM('field call', 'workshop'),
    `ServiceBaseManufacturer` VARCHAR(30),
    `ServiceBaseModel` VARCHAR(24),
    `ServiceBaseUnitType` VARCHAR(30),
    `SerialNo` VARCHAR(24),
    `VestelModelCode` VARCHAR(30),
    `VestelManufactureDate` CHAR(4),
    `DateOfPurchase` DATE,
    `GuaranteeCode` CHAR(4),
    `CustomerType` CHAR(1),
    `ConditionCode` CHAR(4),
    `SymptomCode` CHAR(4),
    `DefectType` CHAR(4),
    `ReportedFault` VARCHAR(2000),
    `RepairType` ENUM('customer', 'stock'),
    `Notes` TEXT,
    `FaultOccurredDate` DATE,
    `FaultOccurredTime` TIME,
    `RepairCompleteDate` DATE,
    `DownloadedToSC` BOOL,
    `ItemLocation` VARCHAR(40),
    `RepairDescription` TEXT,
    `CompletionStatus` VARCHAR(40),
    `OriginalRetailer` VARCHAR(75),
    `RetailerLocation` VARCHAR(100),
    `DateOfManufacture` DATE,
    `PolicyNo` VARCHAR(20),
    `ETDDate` DATE,
    `ETDTime` TIME,
    `Accessories` VARCHAR(200),
    `UnitCondition` VARCHAR(50),
    `Insurer` VARCHAR(30),
    `AuthorisationNo` VARCHAR(20),
    `DateBooked` DATE,
    `TimeBooked` TIME,
    `DateUnitReceived` DATE,
    `ClosedDate` DATE,
    `Status` VARCHAR(40),
    `EngineerCode` VARCHAR(20),
    `EngineerName` VARCHAR(50),
    `ClaimNumber` VARCHAR(20),
    `ClaimTransmitStatus` INTEGER,
    `eInvoiceStatus` INTEGER,
    `ChargeableLabourCost` DECIMAL(10,2) DEFAULT 0.00,
    `ChargeableLabourVATCost` DECIMAL(10,2) DEFAULT 0.00,
    `ChargeablePartsCost` DECIMAL(10,2) DEFAULT 0.00,
    `ChargeableDeliveryCost` DECIMAL(10,2) DEFAULT 0.00,
    `ChargeableDeliveryVATCost` DECIMAL(10,2) DEFAULT 0.00,
    `ChargeableVATCost` DECIMAL(10,2) DEFAULT 0.00,
    `ChargeableSubTotal` DECIMAL(10,2) DEFAULT 0.00,
    `ChargeableTotalCost` DECIMAL(10,2) DEFAULT 0.00,
    `AgentChargeableInvoiceNo` INTEGER DEFAULT 0.00,
    `AgentChargeableInvoiceDate` DATE,
    `ProductLocation` ENUM('Customer','Branch','Service Provider','Supplier','other'),
    `ModelID` INTEGER,
    `BookedBy` INTEGER NOT NULL,
    `ModifiedUserID` INTEGER NOT NULL,
    `ModifiedDate` DATETIME NOT NULL,
    `ContractTracking` VARCHAR(4),
    `SamsungWarrantyParts` VARCHAR(1),
    `SamsungWarrantyLabour` VARCHAR(1),
    `RMANumber` INTEGER,
    `AgentStatus` VARCHAR(30),
    `StockCode` VARCHAR(64),
    `EngineerAssignedDate` DATE,
    `EngineerAssignedTime` DATE,
    `WarrantyNotes` VARCHAR(50),
    `NewFirmwareVersion` VARCHAR(20),
    `ClaimBillNumber` VARCHAR(15),
    `EstimateStatus` ENUM('Awaiting Confiirmation','Accepted','Rejected'),
    `ReceiptNo` VARCHAR(40),
    `ColAddCompanyName` VARCHAR(100),
    `ColAddBuildingNameNumber` VARCHAR(40),
    `ColAddStreet` VARCHAR(40),
    `ColAddLocalArea` VARCHAR(40),
    `ColAddTownCity` VARCHAR(60),
    `ColAddCountyID` INTEGER,
    `ColAddCountryID` INTEGER,
    `ColAddPostcode` VARCHAR(10),
    `ExtendedWarrantyNo` VARCHAR(40),
    `JobSourceID` INTEGER,
    `JobRating` INTEGER(2),
    `batchID` INTEGER,
    `RAType` ENUM('repair','return','estimate','invoice','on hold'),
    `RAStatusID` INTEGER,
    PRIMARY KEY (`JobID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_job_1` ON `job` (`GuaranteeCode`);

CREATE INDEX `IDX_job_2` ON `job` (`DateBooked`);

CREATE INDEX `IDX_job_3` ON `job` (`ClosedDate`);

CREATE INDEX `IDX_job_4` ON `job` (`JobSourceID`);

CREATE INDEX `IDX_job_5` ON `job` (`NetworkRefNo`);

CREATE INDEX `IDX_job_6` ON `job` (`ServiceCentreJobNo`);

CREATE INDEX `IDX_job_NetworkID_FK` ON `job` (`NetworkID`);

CREATE INDEX `IDX_job_BranchID_FK` ON `job` (`BranchID`);

CREATE INDEX `IDX_job_ClientID_FK` ON `job` (`ClientID`);

CREATE INDEX `IDX_job_ServiceProviderID_FK` ON `job` (`ServiceProviderID`);

CREATE INDEX `IDX_job_CustomerID_FK` ON `job` (`CustomerID`);

CREATE INDEX `IDX_job_ProductID_FK` ON `job` (`ProductID`);

CREATE INDEX `IDX_job_ServiceTypeID_FK` ON `job` (`ServiceTypeID`);

CREATE INDEX `IDX_job_ManufacturerID_FK` ON `job` (`ManufacturerID`);

CREATE INDEX `IDX_job_JobTypeID_FK` ON `job` (`JobTypeID`);

CREATE INDEX `IDX_job_StatusID_FK` ON `job` (`StatusID`);

CREATE INDEX `IDX_job_ModelID_FK` ON `job` (`ModelID`);

CREATE INDEX `IDX_job_BookedBy_FK` ON `job` (`BookedBy`);

CREATE INDEX `IDX_job_ModifiedUserID_FK` ON `job` (`ModifiedUserID`);

CREATE INDEX `IDX_job_ColAddCountyID_FK` ON `job` (`ColAddCountyID`);

CREATE INDEX `IDX_job_ColAddCountryID_FK` ON `job` (`ColAddCountryID`);

CREATE INDEX `IDX_job_RAStatusID_FK` ON `job` (`RAStatusID`);

# ---------------------------------------------------------------------- #
# Add table "audit"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `audit` (
    `AuditID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER,
    `AuditTrailActionID` INTEGER NOT NULL,
    `Description` VARCHAR(40),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`AuditID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_audit_JobID_FK` ON `audit` (`JobID`);

CREATE INDEX `IDX_audit_AuditTrailActionID_FK` ON `audit` (`AuditTrailActionID`);

CREATE INDEX `IDX_audit_ModifiedUserID_FK` ON `audit` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "customer"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `customer` (
    `CustomerID` INTEGER NOT NULL AUTO_INCREMENT,
    `BuildingNameNumber` VARCHAR(40),
    `PostalCode` VARCHAR(10),
    `Street` VARCHAR(40),
    `LocalArea` VARCHAR(40),
    `TownCity` VARCHAR(60),
    `CountyID` INTEGER,
    `CountryID` INTEGER,
    `CustomerTitleID` INTEGER,
    `ContactFirstName` VARCHAR(40),
    `ContactLastName` VARCHAR(40),
    `ContactHomePhone` VARCHAR(40),
    `ContactWorkPhoneExt` VARCHAR(10),
    `ContactWorkPhone` VARCHAR(40),
    `ContactFax` VARCHAR(40),
    `ContactMobile` VARCHAR(40),
    `ContactEmail` VARCHAR(40),
    `DataProtection` ENUM('Yes','No'),
    `DataProtectionEmail` ENUM('Marketing','SVCCampaign','Both'),
    `DataProtectionLetter` ENUM('Marketing','SVCCampaign','Both'),
    `DataProtectionSMS` ENUM('Marketing','SVCCampaign','Both'),
    `DataProtectionMobile` ENUM('Marketing','SVCCampaign','Both'),
    `DataProtectionHome` ENUM('Marketing','SVCCampaign','Both'),
    `DataProtectionOffice` ENUM('Marketing','SVCCampaign','Both'),
    `SamsungCustomerRef` VARCHAR(10),
    `CustomerType` VARCHAR(10),
    `CompanyName` VARCHAR(100),
    `Password` VARCHAR(256),
    `Username` VARCHAR(100),
    `LastLoggedIn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `SecurityQuestionID` INTEGER,
    `SecurityQuestionAnswer` VARCHAR(256),
    `EmailConfirmed` TINYINT(1) NOT NULL DEFAULT 0,
    `EmailAuthKey` VARCHAR(256),
    PRIMARY KEY (`CustomerID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_customer_1` ON `customer` (`PostalCode`);

CREATE INDEX `IDX_customer_2` ON `customer` (`ContactLastName`);

CREATE INDEX `IDX_customer_3` ON `customer` (`ContactFirstName`);

CREATE INDEX `IDX_customer_CountyID_FK` ON `customer` (`CountyID`);

CREATE INDEX `IDX_customer_CountryID_FK` ON `customer` (`CountryID`);

CREATE INDEX `IDX_customer_CustomerTitleID_FK` ON `customer` (`CustomerTitleID`);

# ---------------------------------------------------------------------- #
# Add table "status"                                                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `status` (
    `StatusID` INTEGER NOT NULL AUTO_INCREMENT,
    `StatusName` VARCHAR(40),
    `Colour` VARCHAR(10),
    `Category` ENUM('Cancelled', 'Client', 'Complete', 'Field Call', 'In Progress', 'Invoiced', 'Parts', 'System', 'Waiting For Info'),
    `Branch` BOOL,
    `ServiceProvider` BOOL,
    `FieldCall` BOOL,
    `Support` BOOL,
    `PartsOrders` BOOL,
    `RelatedTable` VARCHAR(40),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`StatusID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_status_1` ON `status` (`StatusName`);

CREATE INDEX `IDX_status_ModifiedUserID_FK` ON `status` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "product"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `product` (
    `ProductID` INTEGER NOT NULL AUTO_INCREMENT,
    `ModelID` INTEGER,
    `ClientID` INTEGER,
    `UnitTypeID` INTEGER,
    `NetworkID` INTEGER,
    `ProductNo` VARCHAR(10),
    `ActualSellingPrice` DECIMAL(10,2) DEFAULT 0.00,
    `AuthorityLimit` DECIMAL(10,2) DEFAULT 0.00,
    `PaymentRoute` ENUM('Claim Back', 'Invoice'),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ProductID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_product_ModelID_FK` ON `product` (`ModelID`);

CREATE INDEX `IDX_product_ClientID_FK` ON `product` (`ClientID`);

CREATE INDEX `IDX_product_UnitTypeID_FK` ON `product` (`UnitTypeID`);

CREATE INDEX `IDX_product_NetworkID_FK` ON `product` (`NetworkID`);

CREATE INDEX `IDX_product_ModifiedUserID_FK` ON `product` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "part"                                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `part` (
    `PartID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER,
    `PartNo` VARCHAR(25),
    `PartDescription` VARCHAR(30),
    `Quantity` INTEGER,
    `SectionCode` CHAR(4),
    `DefectCode` CHAR(4),
    `RepairCode` CHAR(4),
    `IsMajorFault` BOOL,
    `CircuitReference` VARCHAR(10),
    `PartSerialNo` VARCHAR(50),
    `OrderNo` INTEGER,
    `OrderStatus` VARCHAR(30),
    `OrderDate` DATE,
    `ReceivedDate` DATE,
    `InvoiceNo` VARCHAR(30),
    `UnitCost` DECIMAL(10,2) DEFAULT 0.00,
    `SaleCost` DECIMAL(10,2) DEFAULT 0.00,
    `VATRate` DECIMAL(4,2),
    `SBPartID` INTEGER,
    `PartStatus` VARCHAR(30),
    `OldPartSerialNo` VARCHAR(20),
    `IsOtherCost` CHAR(1),
    `IsAdjustment` CHAR(1),
    `DueDate` DATE,
    `SupplierOrderNo` VARCHAR(10),
    PRIMARY KEY (`PartID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_part_JobID_FK` ON `part` (`JobID`);

# ---------------------------------------------------------------------- #
# Add table "appointment"                                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment` (
    `AppointmentID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER,
    `UserID` INTEGER,
    `AppointmentDate` DATE,
    `AppointmentTime` VARCHAR(8) COMMENT 'May be AM, PM, ANY or hh:mm:ss',
    `AppointmentType` VARCHAR(30),
    `EngineerCode` CHAR(3),
    `OutCardLeft` BOOL NOT NULL DEFAULT 0,
    `SBAppointID` INTEGER,
    `ServiceProviderID` INTEGER,
    `NonSkylineJobID` INTEGER,
    `DiaryAllocationID` INTEGER,
    `AppointmentStartTime` TIME,
    `AppointmentEndTime` TIME,
    `ServiceProviderEngineerID` INTEGER,
    `CreatedUserID` INTEGER,
    `AppointmentOrphaned` ENUM('Yes','No'),
    `AppointmentStatusID` INTEGER,
    `Notes` VARCHAR(1000),
    `AppointmentChangeReason` VARCHAR(1000),
    `WallMount` TINYINT(1),
    `MenRequired` TINYINT(2),
    `BookedBy` VARCHAR(60),
    `ServiceProviderSkillsetID` INTEGER,
    `Duration` INTEGER,
    `ScreenSize` TINYINT(3),
    `orphan` ENUM('Yes','No') DEFAULT 'No',
    `importance` TINYINT(2) DEFAULT 0,
    `ViamenteStartTime` DATETIME,
    `CreatedTimeStamp` TIMESTAMP,
    `ViamenteTravelTime` INTEGER,
    `SBusercode` VARCHAR(6),
    `ViamenteUnreached` TINYINT DEFAULT 0,
    `OnlineDiarySkillSet` INTEGER,
    `ViamenteDepartTime` DATETIME,
    `ViamenteServiceTime` INTEGER,
    `ViamenteReturnTime` INTEGER,
    `ForceEngineerToViamente` TINYINT(1) DEFAULT 0,
    PRIMARY KEY (`AppointmentID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_appointment_1` ON `appointment` (`AppointmentDate`);

CREATE INDEX `IDX_appointment_2` ON `appointment` (`ServiceProviderID`);

CREATE INDEX `IDX_appointment_3` ON `appointment` (`NonSkylineJobID`);

CREATE INDEX `IDX_appointment_4` ON `appointment` (`DiaryAllocationID`);

CREATE INDEX `IDX_appointment_5` ON `appointment` (`ServiceProviderEngineerID`);

CREATE INDEX `IDX_appointment_6` ON `appointment` (`ServiceProviderID`);

CREATE INDEX `IDX_appointment_7` ON `appointment` (`NonSkylineJobID`);

CREATE INDEX `IDX_appointment_8` ON `appointment` (`DiaryAllocationID`);

CREATE INDEX `IDX_appointment_9` ON `appointment` (`AppointmentDate`);

CREATE INDEX `IDX_appointment_10` ON `appointment` (`ServiceProviderEngineerID`);

CREATE INDEX `IDX_appointment_JobID_FK` ON `appointment` (`JobID`);

CREATE INDEX `IDX_appointment_UserID_FK` ON `appointment` (`UserID`);

# ---------------------------------------------------------------------- #
# Add table "contact_history"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `contact_history` (
    `ContactHistoryID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER,
    `ContactHistoryActionID` INTEGER NOT NULL,
    `BrandID` INTEGER DEFAULT 1000,
    `ContactDate` DATE,
    `ContactTime` TIME,
    `UserCode` VARCHAR(15),
    `Subject` VARCHAR(50),
    `Note` VARCHAR(254),
    `IsDownloadedToSC` BOOL NOT NULL DEFAULT 0,
    `SystemAllocated` ENUM('yes', 'no'),
    PRIMARY KEY (`ContactHistoryID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_contact_history_JobID_FK` ON `contact_history` (`JobID`);

CREATE INDEX `IDX_contact_history_ContactHistoryActionID_FK` ON `contact_history` (`ContactHistoryActionID`);

# ---------------------------------------------------------------------- #
# Add table "brand"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `brand` (
    `BrandID` INTEGER NOT NULL,
    `NetworkID` INTEGER,
    `ClientID` INTEGER,
    `BrandName` VARCHAR(40),
    `BrandLogo` VARCHAR(255),
    `Skin` VARCHAR(20),
    `AutoSendEmails` ENUM('1','0') DEFAULT '1',
    `EmailType` ENUM('Generic','CRM') NOT NULL DEFAULT 'Generic',
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `brandID` PRIMARY KEY (`BrandID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_brand_ClientID_FK` ON `brand` (`ClientID`);

CREATE INDEX `IDX_brand_NetworkID_FK` ON `brand` (`NetworkID`);

CREATE INDEX `IDX_brand_ModifiedUserID_FK` ON `brand` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "brand_branch"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `brand_branch` (
    `BrandBranchID` INTEGER NOT NULL AUTO_INCREMENT,
    `BranchID` INTEGER NOT NULL,
    `BrandID` INTEGER NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`BrandBranchID`),
    CONSTRAINT `TUC_brand_branch_1` UNIQUE (`BrandID`, `BranchID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_brand_branch_BrandID_FK` ON `brand_branch` (`BrandID`);

CREATE INDEX `IDX_brand_branch_BranchID_FK` ON `brand_branch` (`BranchID`);

CREATE INDEX `IDX_brand_branch_ModifiedUserID_FK` ON `brand_branch` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "audit_trail_action"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `audit_trail_action` (
    `AuditTrailActionID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `Action` VARCHAR(255) NOT NULL,
    `ActionCode` INTEGER(11) NOT NULL,
    `Type` ENUM('Process','Action','Edit') NOT NULL,
    `Status` ENUM('Active','In-active') NOT NULL DEFAULT 'Active',
    CONSTRAINT `audit_trail_actionID` PRIMARY KEY (`AuditTrailActionID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_audit_trail_action_BrandID_FK` ON `audit_trail_action` (`BrandID`);

# ---------------------------------------------------------------------- #
# Add table "contact_history_action"                                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `contact_history_action` (
    `ContactHistoryActionID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `Action` VARCHAR(255) NOT NULL,
    `ActionCode` INTEGER(11) NOT NULL,
    `Type` ENUM('User Defined','System Defined') NOT NULL,
    `Status` ENUM('Active','In-active') NOT NULL,
    PRIMARY KEY (`ContactHistoryActionID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains Contact History Actions details.';

CREATE INDEX `IDX_contact_history_action_BrandID_FK` ON `contact_history_action` (`BrandID`);

# ---------------------------------------------------------------------- #
# Add table "status_history"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `status_history` (
    `StatusHistoryID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER,
    `StatusID` INTEGER NOT NULL,
    `UserID` INTEGER,
    `Date` DATETIME,
    `AdditionalInformation` VARCHAR(40),
    `UserCode` VARCHAR(3),
    CONSTRAINT `status_historyID` PRIMARY KEY (`StatusHistoryID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_status_history_StatusID_FK` ON `status_history` (`StatusID`);

CREATE INDEX `IDX_status_history_JobID_FK` ON `status_history` (`JobID`);

CREATE INDEX `IDX_status_history_UserID_FK` ON `status_history` (`UserID`);

# ---------------------------------------------------------------------- #
# Add table "customer_title"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `customer_title` (
    `CustomerTitleID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `Title` VARCHAR(255) NOT NULL,
    `TitleCode` INTEGER,
    `Type` ENUM('Male', 'Female', 'Unknown'),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`CustomerTitleID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains Customer Titles details.';

CREATE INDEX `IDX_customer_title_BrandID_FK` ON `customer_title` (`BrandID`);

CREATE INDEX `IDX_customer_title_ModifiedUserID_FK` ON `customer_title` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "job_type"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `job_type` (
    `JobTypeID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `Type` VARCHAR(30) NOT NULL,
    `TypeCode` INTEGER NOT NULL,
    `Priority` INTEGER,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`JobTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains Job Type details.';

CREATE INDEX `IDX_job_type_1` ON `job_type` (`Type`);

CREATE INDEX `IDX_job_type_BrandID_FK` ON `job_type` (`BrandID`);

CREATE INDEX `IDX_job_type_ModifiedUserID_FK` ON `job_type` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "payment_type"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `payment_type` (
    `PaymentTypeID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `Type` VARCHAR(255) NOT NULL,
    `TypeCode` INTEGER,
    `SystemCode` VARCHAR(255),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`PaymentTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains Payment Type details.';

CREATE INDEX `IDX_payment_type_BrandID_FK` ON `payment_type` (`BrandID`);

CREATE INDEX `IDX_payment_type_ModifiedUserID_FK` ON `payment_type` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "security_question"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `security_question` (
    `SecurityQuestionID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `Question` VARCHAR(255) NOT NULL,
    `QuestionCode` INTEGER(11) NOT NULL,
    `SystemCode` VARCHAR(255) NOT NULL,
    `Status` ENUM('Active','In-active') NOT NULL,
    PRIMARY KEY (`SecurityQuestionID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains Security Question details.';

CREATE INDEX `IDX_security_question_BrandID_FK` ON `security_question` (`BrandID`);

# ---------------------------------------------------------------------- #
# Add table "country"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `country` (
    `CountryID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `Name` VARCHAR(40) NOT NULL,
    `CountryCode` INTEGER,
    `InternationalCode` VARCHAR(3) NOT NULL,
    `Currency` VARCHAR(20),
    `CurrencySymbol` VARCHAR(1),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`CountryID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains country details.';

CREATE INDEX `IDX_country_BrandID_FK` ON `country` (`BrandID`);

CREATE INDEX `IDX_country_ModifiedUserID_FK` ON `country` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "county"                                                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `county` (
    `CountyID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `CountryID` INTEGER NOT NULL,
    `Name` VARCHAR(40) NOT NULL,
    `CountyCode` INTEGER NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`CountyID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci COMMENT = 'This table contains county details of each country.';

CREATE INDEX `IDX_county_BrandID_FK` ON `county` (`BrandID`);

CREATE INDEX `IDX_county_CountryID_FK` ON `county` (`CountryID`);

CREATE INDEX `IDX_county_ModifiedUserID_FK` ON `county` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "general_default"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `general_default` (
    `GeneralDefaultID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER NOT NULL,
    `IDNo` INTEGER NOT NULL,
    `Category` VARCHAR(20) NOT NULL,
    `DefaultName` VARCHAR(40) NOT NULL,
    `Default` VARCHAR(20) NOT NULL,
    `Description` TEXT NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`GeneralDefaultID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_general_default_BrandID_FK` ON `general_default` (`BrandID`);

CREATE INDEX `IDX_general_default_ModifiedUserID_FK` ON `general_default` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "client_purchase_order"                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `client_purchase_order` (
    `ClientPurchaseOrder` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER,
    `PurchaseOrderNumber` VARCHAR(40),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `client_purchase_orderID` PRIMARY KEY (`ClientPurchaseOrder`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_client_purchase_order_BrandID_FK` ON `client_purchase_order` (`BrandID`);

CREATE INDEX `IDX_client_purchase_order_ModifiedUserID_FK` ON `client_purchase_order` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "unit_pricing_structure"                                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `unit_pricing_structure` (
    `UnitPricingStructureID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER,
    `ClientID` INTEGER,
    `UnitTypeID` INTEGER,
    `ManufacturerID` INTEGER,
    `CompletionStatusID` INTEGER,
    `JobSite` ENUM('field call', 'workshop') NOT NULL,
    `ServiceRate` DECIMAL(10,2) DEFAULT 0.00,
    `ReferralFee` DECIMAL(10,2) DEFAULT 0.00,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `unit_pricing_structureID` PRIMARY KEY (`UnitPricingStructureID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_unit_pricing_structure_NetworkID_FK` ON `unit_pricing_structure` (`NetworkID`);

CREATE INDEX `IDX_unit_pricing_structure_ClientID_FK` ON `unit_pricing_structure` (`ClientID`);

CREATE INDEX `IDX_unit_pricing_structure_UnitTypeID_FK` ON `unit_pricing_structure` (`UnitTypeID`);

CREATE INDEX `IDX_unit_pricing_structure_ManufacturerID_FK` ON `unit_pricing_structure` (`ManufacturerID`);

CREATE INDEX `IDX_unit_pricing_structure_CompletionStatusID_FK` ON `unit_pricing_structure` (`CompletionStatusID`);

CREATE INDEX `IDX_unit_pricing_structure_ModifiedUserID_FK` ON `unit_pricing_structure` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "completion_status"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `completion_status` (
    `CompletionStatusID` INTEGER NOT NULL AUTO_INCREMENT,
    `CompletionStatus` VARCHAR(20),
    `Description` VARCHAR(50),
    `Sort` INTEGER,
    `BrandID` INTEGER,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `completion_statusID` PRIMARY KEY (`CompletionStatusID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_completion_status_ModifiedUserID_FK` ON `completion_status` (`ModifiedUserID`);

CREATE INDEX `IDX_completion_status_BrandID_FK` ON `completion_status` (`BrandID`);

# ---------------------------------------------------------------------- #
# Add table "repair_skill"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `repair_skill` (
    `RepairSkillID` INTEGER NOT NULL AUTO_INCREMENT,
    `RepairSkillName` VARCHAR(40),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `repair_skillID` PRIMARY KEY (`RepairSkillID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_repair_skill_1` ON `repair_skill` (`RepairSkillName`);

CREATE INDEX `IDX_repair_skill_ModifiedUserID_FK` ON `repair_skill` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "country_vat_rate"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `country_vat_rate` (
    `CountryID` INTEGER NOT NULL,
    `VatRateID` INTEGER NOT NULL,
    PRIMARY KEY (`CountryID`, `VatRateID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_country_vat_rate_VatRateID_FK` ON `country_vat_rate` (`VatRateID`);

CREATE INDEX `IDX_country_vat_rate_CountryID_FK` ON `country_vat_rate` (`CountryID`);

# ---------------------------------------------------------------------- #
# Add table "central_service_allocation"                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `central_service_allocation` (
    `CentralServiceAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER NOT NULL,
    `ManufacturerID` INTEGER,
    `RepairSkillID` INTEGER NOT NULL,
    `ServiceProviderID` INTEGER NOT NULL,
    `JobTypeID` INTEGER,
    `ServiceTypeID` INTEGER,
    `UnitTypeID` INTEGER,
    `ClientID` INTEGER,
    `CountyID` INTEGER,
    `CountryID` INTEGER NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `central_service_allocationID` PRIMARY KEY (`CentralServiceAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_central_service_allocation_ModifiedUserID_FK` ON `central_service_allocation` (`ModifiedUserID`);

CREATE INDEX `IDX_central_service_allocation_NetworkID_FK` ON `central_service_allocation` (`NetworkID`);

CREATE INDEX `IDX_central_service_allocation_ManufacturerID_FK` ON `central_service_allocation` (`ManufacturerID`);

CREATE INDEX `IDX_central_service_allocation_RepairSkillID_FK` ON `central_service_allocation` (`RepairSkillID`);

CREATE INDEX `IDX_central_service_allocation_ServiceProviderID_FK` ON `central_service_allocation` (`ServiceProviderID`);

CREATE INDEX `IDX_central_service_allocation_JobTypeID_FK` ON `central_service_allocation` (`JobTypeID`);

CREATE INDEX `IDX_central_service_allocation_ServiceTypeID_FK` ON `central_service_allocation` (`ServiceTypeID`);

CREATE INDEX `IDX_central_service_allocation_UnitTypeID_FK` ON `central_service_allocation` (`UnitTypeID`);

CREATE INDEX `IDX_central_service_allocation_ClientID_FK` ON `central_service_allocation` (`ClientID`);

CREATE INDEX `IDX_central_service_allocation_CountyID_FK` ON `central_service_allocation` (`CountyID`);

CREATE INDEX `IDX_central_service_allocation_CountryID_FK` ON `central_service_allocation` (`CountryID`);

# ---------------------------------------------------------------------- #
# Add table "postcode_allocation"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `postcode_allocation` (
    `PostcodeAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER NOT NULL,
    `ManufacturerID` INTEGER NOT NULL,
    `RepairSkillID` INTEGER,
    `JobTypeID` INTEGER,
    `ServiceTypeID` INTEGER,
    `ClientID` INTEGER,
    `CountryID` INTEGER,
    `ServiceProviderID` INTEGER NOT NULL,
    `OutOfArea` ENUM('No','Yes') NOT NULL DEFAULT 'No',
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `postcode_allocationID` PRIMARY KEY (`PostcodeAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_postcode_allocation_ModifiedUserID_FK` ON `postcode_allocation` (`ModifiedUserID`);

CREATE INDEX `IDX_postcode_allocation_NetworkID_FK` ON `postcode_allocation` (`NetworkID`);

CREATE INDEX `IDX_postcode_allocation_ManufacturerID_FK` ON `postcode_allocation` (`ManufacturerID`);

CREATE INDEX `IDX_postcode_allocation_RepairSkillID_FK` ON `postcode_allocation` (`RepairSkillID`);

CREATE INDEX `IDX_postcode_allocation_JobTypeID_FK` ON `postcode_allocation` (`JobTypeID`);

CREATE INDEX `IDX_postcode_allocation_ServiceTypeID_FK` ON `postcode_allocation` (`ServiceTypeID`);

CREATE INDEX `IDX_postcode_allocation_ClientID_FK` ON `postcode_allocation` (`ClientID`);

CREATE INDEX `IDX_postcode_allocation_CountryID_FK` ON `postcode_allocation` (`CountryID`);

CREATE INDEX `IDX_postcode_allocation_ServiceProviderID_FK` ON `postcode_allocation` (`ServiceProviderID`);

# ---------------------------------------------------------------------- #
# Add table "postcode_allocation_data"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `postcode_allocation_data` (
    `PostcodeAllocationID` INTEGER NOT NULL,
    `PostcodeArea` VARCHAR(15) NOT NULL
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_postcode_allocation_data_1` ON `postcode_allocation_data` (`PostcodeArea`);

CREATE INDEX `IDX_postcode_allocation_data_PostcodeAllocationID_FK` ON `postcode_allocation_data` (`PostcodeAllocationID`);

# ---------------------------------------------------------------------- #
# Add table "town_allocation"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `town_allocation` (
    `TownAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER NOT NULL,
    `ManufacturerID` INTEGER,
    `RepairSkillID` INTEGER,
    `ServiceProviderID` INTEGER,
    `ClientID` INTEGER,
    `JobTypeID` INTEGER,
    `ServiceTypeID` INTEGER,
    `UnitTypeID` INTEGER,
    `CustomerID` INTEGER,
    `CountryID` INTEGER,
    `CountyID` INTEGER NOT NULL,
    `Town` VARCHAR(40),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `town_allocationID` PRIMARY KEY (`TownAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_town_allocation_ModifiedUserID_FK` ON `town_allocation` (`ModifiedUserID`);

CREATE INDEX `IDX_town_allocation_NetworkID_FK` ON `town_allocation` (`NetworkID`);

CREATE INDEX `IDX_town_allocation_ManufacturerID_FK` ON `town_allocation` (`ManufacturerID`);

CREATE INDEX `IDX_town_allocation_RepairSkillID_FK` ON `town_allocation` (`RepairSkillID`);

CREATE INDEX `IDX_town_allocation_ServiceProviderID_FK` ON `town_allocation` (`ServiceProviderID`);

CREATE INDEX `IDX_town_allocation_ClientID_FK` ON `town_allocation` (`ClientID`);

CREATE INDEX `IDX_town_allocation_JobTypeID_FK` ON `town_allocation` (`JobTypeID`);

CREATE INDEX `IDX_town_allocation_ServiceTypeID_FK` ON `town_allocation` (`ServiceTypeID`);

CREATE INDEX `IDX_town_allocation_UnitTypeID_FK` ON `town_allocation` (`UnitTypeID`);

CREATE INDEX `IDX_town_allocation_CountryID_FK` ON `town_allocation` (`CountryID`);

CREATE INDEX `IDX_town_allocation_CustomerID_FK` ON `town_allocation` (`CustomerID`);

CREATE INDEX `IDX_town_allocation_CountyID_FK` ON `town_allocation` (`CountyID`);

# ---------------------------------------------------------------------- #
# Add table "bought_out_guarantee"                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `bought_out_guarantee` (
    `BoughtOutGuaranteeID` INTEGER NOT NULL AUTO_INCREMENT,
    `NetworkID` INTEGER NOT NULL,
    `ClientID` INTEGER NOT NULL,
    `ManufacturerID` INTEGER NOT NULL,
    `RepairSkillID` INTEGER,
    `UnitTypeID` INTEGER,
    `ModelID` INTEGER,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `bought_out_guaranteeID` PRIMARY KEY (`BoughtOutGuaranteeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_bought_out_guarantee_NetworkID_FK` ON `bought_out_guarantee` (`NetworkID`);

CREATE INDEX `IDX_bought_out_guarantee_ClientID_FK` ON `bought_out_guarantee` (`ClientID`);

CREATE INDEX `IDX_bought_out_guarantee_ManufacturerID_FK` ON `bought_out_guarantee` (`ManufacturerID`);

CREATE INDEX `IDX_bought_out_guarantee_RepairSkillID_FK` ON `bought_out_guarantee` (`RepairSkillID`);

CREATE INDEX `IDX_bought_out_guarantee_UnitTypeID_FK` ON `bought_out_guarantee` (`UnitTypeID`);

CREATE INDEX `IDX_bought_out_guarantee_ModelID_FK` ON `bought_out_guarantee` (`ModelID`);

CREATE INDEX `IDX_bought_out_guarantee_ModifiedUserID_FK` ON `bought_out_guarantee` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "claim_response"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `claim_response` (
    `ClaimResponseID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER NOT NULL,
    `ErrorCode` VARCHAR(18),
    `ErrorDescription` VARCHAR(50),
    CONSTRAINT `claim_responseID` PRIMARY KEY (`ClaimResponseID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_claim_response_JobID_FK` ON `claim_response` (`JobID`);

# ---------------------------------------------------------------------- #
# Add table "email"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `email` (
    `EmailID` INTEGER NOT NULL AUTO_INCREMENT,
    `EmailCode` VARCHAR(64),
    `Title` VARCHAR(100),
    `Message` TEXT,
    `ModifiedDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `ModifiedUserID` INTEGER,
    CONSTRAINT `emailID` PRIMARY KEY (`EmailID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_email_ModifiedUserID_FK` ON `email` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "email_job"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `email_job` (
    `EmailJobID` INTEGER NOT NULL AUTO_INCREMENT,
    `MailBody` TEXT,
    `MailAccessCode` VARCHAR(32),
    `JobID` INTEGER,
    `EmailID` INTEGER,
    `ModifiedDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `ModifiedUserID` INTEGER,
    CONSTRAINT `email_jobID` PRIMARY KEY (`EmailJobID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_email_job_JobID_FK` ON `email_job` (`JobID`);

CREATE INDEX `IDX_email_job_EmailID_FK` ON `email_job` (`EmailID`);

CREATE INDEX `IDX_email_job_ModifiedUserID_FK` ON `email_job` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "ra_status"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `ra_status` (
    `RAStatusID` INTEGER NOT NULL AUTO_INCREMENT,
    `ListOrder` INTEGER NOT NULL,
    `BrandID` INTEGER NOT NULL,
    `Status` ENUM('Active','In-active'),
    `CreatedDateTime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `ModifiedUserID` INTEGER NOT NULL,
    `ModifiedDate` TIMESTAMP,
    `RAStatusName` VARCHAR(40),
    `RAStatusCode` INTEGER NOT NULL,
    `Colour` VARCHAR(10),
    CONSTRAINT `ra_statusID` PRIMARY KEY (`RAStatusID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_ra_status_BrandID_FK` ON `ra_status` (`BrandID`);

CREATE INDEX `IDX_ra_status_ModifiedUserID_FK` ON `ra_status` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "ra_history"                                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `ra_history` (
    `RAHistoryID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER NOT NULL,
    `RAStatusID` INTEGER NOT NULL,
    `Notes` VARCHAR(1000),
    `CreatedDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ModifiedUserID` INTEGER NOT NULL,
    `RAType` ENUM('repair','return','estimate','invoice','on hold'),
    PRIMARY KEY (`RAHistoryID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_ra_history_JobID_FK` ON `ra_history` (`JobID`);

CREATE INDEX `IDX_ra_history_RAStatusID_FK` ON `ra_history` (`RAStatusID`);

CREATE INDEX `IDX_ra_history_ModifiedUserID_FK` ON `ra_history` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "extended_warrantor"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `extended_warrantor` (
    `ExtendedWarrantorID` INTEGER NOT NULL AUTO_INCREMENT,
    `ExtendedWarrantorName` VARCHAR(64) NOT NULL,
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `extended_warrantorID` PRIMARY KEY (`ExtendedWarrantorID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_extended_warrantor_ModifiedUserID_FK` ON `extended_warrantor` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "status_preference"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `status_preference` (
    `UserID` INTEGER NOT NULL,
    `StatusID` INTEGER NOT NULL,
    `Page` VARCHAR(40) NOT NULL,
    `Type` VARCHAR(2) NOT NULL
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE UNIQUE INDEX `IDX_status_preference_1` ON `status_preference` (`Page`,`UserID`,`Type`,`StatusID`);

# ---------------------------------------------------------------------- #
# Foreign key constraints                                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` ADD CONSTRAINT `country_TO_network` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `network` ADD CONSTRAINT `county_TO_network` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `network` ADD CONSTRAINT `user_TO_network` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network` ADD CONSTRAINT `client_TO_network` 
    FOREIGN KEY (`DefaultClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `client` ADD CONSTRAINT `user_TO_client` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `client` ADD CONSTRAINT `branch_TO_client` 
    FOREIGN KEY (`DefaultBranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `branch` ADD CONSTRAINT `county_TO_branch` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `branch` ADD CONSTRAINT `country_TO_branch` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `branch` ADD CONSTRAINT `user_TO_branch` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `county_TO_service_provider` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `country_TO_service_provider` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `user_TO_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `manufacturer` ADD CONSTRAINT `user_TO_manufacturer` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `service_type` ADD CONSTRAINT `brand_TO_service_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `service_type` ADD CONSTRAINT `job_type_TO_service_type` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `service_type` ADD CONSTRAINT `user_TO_service_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `client_TO_service_type_alias` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `service_type_TO_service_type_alias` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `network_TO_service_type_alias` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `user_TO_service_type_alias` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_client` ADD CONSTRAINT `network_TO_network_client` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `network_client` ADD CONSTRAINT `client_TO_network_client` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `network_client` ADD CONSTRAINT `user_TO_network_client` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_manufacturer` ADD CONSTRAINT `manufacturer_TO_network_manufacturer` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `network_manufacturer` ADD CONSTRAINT `network_TO_network_manufacturer` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `network_manufacturer` ADD CONSTRAINT `user_TO_network_manufacturer` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `client_TO_client_branch` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `branch_TO_client_branch` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `network_TO_client_branch` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `user_TO_client_branch` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `network_TO_network_service_provider` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `service_provider_TO_network_service_provider` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `user_TO_network_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `model` ADD CONSTRAINT `manufacturer_TO_model` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `model` ADD CONSTRAINT `unit_type_TO_model` 
    FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`);

ALTER TABLE `model` ADD CONSTRAINT `user_TO_model` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `unit_type` ADD CONSTRAINT `repair_skill_TO_unit_type` 
    FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`);

ALTER TABLE `unit_type` ADD CONSTRAINT `user_TO_unit_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `unit_client_type` ADD CONSTRAINT `client_TO_unit_client_type` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `unit_client_type` ADD CONSTRAINT `unit_type_TO_unit_client_type` 
    FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`);

ALTER TABLE `unit_client_type` ADD CONSTRAINT `user_TO_unit_client_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `user` ADD CONSTRAINT `network_TO_user` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `user` ADD CONSTRAINT `service_provider_TO_user` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user` ADD CONSTRAINT `client_TO_user` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `user` ADD CONSTRAINT `branch_TO_user` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `user` ADD CONSTRAINT `brand_TO_user` 
    FOREIGN KEY (`DefaultBrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `user` ADD CONSTRAINT `security_question_TO_user` 
    FOREIGN KEY (`SecurityQuestionID`) REFERENCES `security_question` (`SecurityQuestionID`);

ALTER TABLE `user` ADD CONSTRAINT `user_TO_user` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `user` ADD CONSTRAINT `customer_title_TO_user` 
    FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`);

ALTER TABLE `permission` ADD CONSTRAINT `user_TO_permission` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `assigned_permission` ADD CONSTRAINT `permission_TO_assigned_permission` 
    FOREIGN KEY (`PermissionID`) REFERENCES `permission` (`PermissionID`);

ALTER TABLE `assigned_permission` ADD CONSTRAINT `role_TO_assigned_permission` 
    FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`);

ALTER TABLE `assigned_permission` ADD CONSTRAINT `user_TO_assigned_permission` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `role` ADD CONSTRAINT `user_TO_role` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `user_role` ADD CONSTRAINT `user_TO_user_role` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `user_role` ADD CONSTRAINT `role_TO_user_role` 
    FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `county_TO_job` 
    FOREIGN KEY (`ColAddCountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `job` ADD CONSTRAINT `country_TO_job` 
    FOREIGN KEY (`ColAddCountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `job` ADD CONSTRAINT `ra_status_TO_job` 
    FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `audit` ADD CONSTRAINT `audit_trail_action_TO_audit` 
    FOREIGN KEY (`AuditTrailActionID`) REFERENCES `audit_trail_action` (`AuditTrailActionID`);

ALTER TABLE `audit` ADD CONSTRAINT `user_TO_audit` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `customer` ADD CONSTRAINT `county_TO_customer` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `customer` ADD CONSTRAINT `country_TO_customer` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `customer` ADD CONSTRAINT `customer_title_TO_customer` 
    FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`);

ALTER TABLE `status` ADD CONSTRAINT `user_TO_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `product` ADD CONSTRAINT `model_TO_product` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `product` ADD CONSTRAINT `client_TO_product` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `product` ADD CONSTRAINT `unit_type_TO_product` 
    FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`);

ALTER TABLE `product` ADD CONSTRAINT `network_TO_product` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `product` ADD CONSTRAINT `user_TO_product` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `user_TO_appointment` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `contact_history_action_TO_contact_history` 
    FOREIGN KEY (`ContactHistoryActionID`) REFERENCES `contact_history_action` (`ContactHistoryActionID`);

ALTER TABLE `brand` ADD CONSTRAINT `client_TO_brand` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `brand` ADD CONSTRAINT `network_TO_brand` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `brand` ADD CONSTRAINT `user_TO_brand` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `brand_branch` ADD CONSTRAINT `brand_TO_brand_branch` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`) ON UPDATE CASCADE;

ALTER TABLE `brand_branch` ADD CONSTRAINT `branch_TO_brand_branch` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `brand_branch` ADD CONSTRAINT `user_TO_brand_branch` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `audit_trail_action` ADD CONSTRAINT `brand_TO_audit_trail_action` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `contact_history_action` ADD CONSTRAINT `brand_TO_contact_history_action` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `status_history` ADD CONSTRAINT `status_TO_status_history` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `status_history` ADD CONSTRAINT `user_TO_status_history` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `customer_title` ADD CONSTRAINT `brand_TO_customer_title` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `customer_title` ADD CONSTRAINT `user_TO_customer_title` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job_type` ADD CONSTRAINT `brand_TO_job_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `job_type` ADD CONSTRAINT `user_TO_job_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `payment_type` ADD CONSTRAINT `brand_TO_payment_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `payment_type` ADD CONSTRAINT `user_TO_payment_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `security_question` ADD CONSTRAINT `brand_TO_security_question` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `country` ADD CONSTRAINT `brand_TO_country` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `country` ADD CONSTRAINT `user_TO_country` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `county` ADD CONSTRAINT `brand_TO_county` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `county` ADD CONSTRAINT `country_TO_county` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `county` ADD CONSTRAINT `user_TO_county` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `general_default` ADD CONSTRAINT `brand_TO_general_default` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `general_default` ADD CONSTRAINT `user_TO_general_default` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client_purchase_order` ADD CONSTRAINT `brand_TO_client_purchase_order` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `client_purchase_order` ADD CONSTRAINT `user_TO_client_purchase_order` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `network_TO_unit_pricing_structure` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `client_TO_unit_pricing_structure` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `unit_type_TO_unit_pricing_structure` 
    FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `manufacturer_TO_unit_pricing_structure` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `completion_status_TO_unit_pricing_structure` 
    FOREIGN KEY (`CompletionStatusID`) REFERENCES `completion_status` (`CompletionStatusID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `user_TO_unit_pricing_structure` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `completion_status` ADD CONSTRAINT `user_TO_completion_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `completion_status` ADD CONSTRAINT `brand_TO_completion_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `repair_skill` ADD CONSTRAINT `user_TO_repair_skill` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `country_vat_rate` ADD CONSTRAINT `vat_rate_TO_country_vat_rate` 
    FOREIGN KEY (`VatRateID`) REFERENCES `vat_rate` (`VatRateID`);

ALTER TABLE `country_vat_rate` ADD CONSTRAINT `country_TO_country_vat_rate` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `user_TO_central_service_allocation` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `network_TO_central_service_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `manufacturer_TO_central_service_allocation` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `repair_skill_TO_central_service_allocation` 
    FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_provider_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `job_type_TO_central_service_allocation` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_type_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `unit_type_TO_central_service_allocation` 
    FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `client_TO_central_service_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `county_TO_central_service_allocation` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `country_TO_central_service_allocation` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `user_TO_postcode_allocation` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `network_TO_postcode_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `manufacturer_TO_postcode_allocation` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `repair_skill_TO_postcode_allocation` 
    FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `job_type_TO_postcode_allocation` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_type_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `client_TO_postcode_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `country_TO_postcode_allocation` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_provider_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `postcode_allocation_data` ADD CONSTRAINT `postcode_allocation_TO_postcode_allocation_data` 
    FOREIGN KEY (`PostcodeAllocationID`) REFERENCES `postcode_allocation` (`PostcodeAllocationID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `user_TO_town_allocation` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `network_TO_town_allocation` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `manufacturer_TO_town_allocation` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `repair_skill_TO_town_allocation` 
    FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_provider_TO_town_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `client_TO_town_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `job_type_TO_town_allocation` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_type_TO_town_allocation` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `unit_type_TO_town_allocation` 
    FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `country_TO_town_allocation` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `customer_TO_town_allocation` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `county_TO_town_allocation` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `network_TO_bought_out_guarantee` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `client_TO_bought_out_guarantee` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `manufacturer_TO_bought_out_guarantee` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `repair_skill_TO_bought_out_guarantee` 
    FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `unit_type_TO_bought_out_guarantee` 
    FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `model_TO_bought_out_guarantee` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `user_TO_bought_out_guarantee` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `doc_categories` ADD CONSTRAINT `doc_api_TO_doc_categories` 
    FOREIGN KEY (`DocApiID`) REFERENCES `doc_api` (`DocApiID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `doc_method_response` ADD CONSTRAINT `doc_procedures_TO_doc_method_response` 
    FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `doc_procedures` ADD CONSTRAINT `doc_categories_TO_doc_procedures` 
    FOREIGN KEY (`DocCategoriesID`) REFERENCES `doc_categories` (`DocCategoriesID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `doc_procedure_parameters` ADD CONSTRAINT `doc_procedures_TO_doc_procedure_parameters` 
    FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `claim_response` ADD CONSTRAINT `job_TO_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `email` ADD CONSTRAINT `user_TO_email` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `email_job` ADD CONSTRAINT `job_TO_email_job` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `email_job` ADD CONSTRAINT `email_TO_email_job` 
    FOREIGN KEY (`EmailID`) REFERENCES `email` (`EmailID`);

ALTER TABLE `email_job` ADD CONSTRAINT `user_TO_email_job` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `brand_TO_ra_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `user_TO_ra_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `job_TO_ra_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `ra_status_TO_ra_history_2` 
    FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `user_TO_ra_history` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `extended_warrantor` ADD CONSTRAINT `user_TO_extended_warrantor` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `status_preference` ADD CONSTRAINT `user_TO_status_preference` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

# ---------------------------------------------------------------------- #
# Procedures                                                             #
# ---------------------------------------------------------------------- #

DROP PROCEDURE IF EXISTS `UpgradeSchemaVersion`;
 DELIMITER //
CREATE PROCEDURE `UpgradeSchemaVersion`  
	(p_version varchar(20))
BEGIN
    DECLARE bad_version CONDITION FOR SQLSTATE '99001';
    DECLARE current_version varchar(20);
    SELECT `VersionNo` from `version` order by `Updated` desc limit 0,1 into current_version;
    IF p_version <> current_version THEN
         SIGNAL bad_version
            SET MESSAGE_TEXT='Cannot Upgrade Schema.';
    END IF;
END;//
DELIMITER ;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

insert into version (VersionNo) values ('1.125');
