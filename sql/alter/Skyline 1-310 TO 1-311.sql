# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.310');

# ---------------------------------------------------------------------- #
# Praveen Kumar Nakka Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE service_provider_part_category ADD ApproveStatus ENUM( 'Approved', 'Pending' ) NOT NULL DEFAULT 'Pending' AFTER Status ;
UPDATE group_headings SET GroupHeading = 'Job Fault Code Lookups' WHERE group_headings.GroupID =36;

INSERT INTO permission (PermissionID, Name, Description) VALUES (12008, 'Report Generator', 'Report Generator');


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.311');