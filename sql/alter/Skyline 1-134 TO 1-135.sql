# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.134');


# ---------------------------------------------------------------------- #
# Modify table "service_provider_geotags"                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_geotags`
	CHANGE COLUMN `ServiceProviderGeoTagsID` `ServiceProviderGeoTagsID` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	ADD INDEX `Postcode` (`Postcode`);


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.135');