# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.306');

# ---------------------------------------------------------------------- #
# Vykintas Rutkunas Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE branch ADD COLUMN OpenJobsManagement ENUM('Yes','No') NULL DEFAULT NULL AFTER SMSID;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.307');



