# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.265');

# ---------------------------------------------------------------------- #
# Modify Table preferential_clients_manufacturers                        #
# ---------------------------------------------------------------------- # 
ALTER TABLE preferential_clients_manufacturers ADD GroupEnable TINYINT( 1 ) NULL DEFAULT '1';
ALTER TABLE preferential_clients_manufacturers CHANGE PreferentialType PreferentialType ENUM('Manufacturer', 'Brand', 'ServiceProvider', 'Network', 'Branch', 'Client', 'UnitType', 'SkillSet') NOT NULL ;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.266');
