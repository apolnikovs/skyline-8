# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.278');

# ---------------------------------------------------------------------- #
# Andris Stock Changes                                                   #
# ---------------------------------------------------------------------- # 

CREATE TABLE service_provider_currency ( ServiceProviderCurrencyID INT(10) NOT NULL AUTO_INCREMENT, CurrencyID INT(10) NULL DEFAULT NULL, ServiceProviderID INT(10) NULL DEFAULT NULL, CurrencyName VARCHAR(50) NULL DEFAULT NULL, CurrencyCode VARCHAR(5) NULL DEFAULT NULL, CurrencySymbol VARCHAR(5) NULL DEFAULT NULL, AutomaticExchangeRateCalculation ENUM('Yes','No') NOT NULL DEFAULT 'Yes', ConversionCalculation ENUM('Multiply','Divide') NOT NULL DEFAULT 'Multiply', ExchangeRate DECIMAL(10,4) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, ExchangeRateLastModified DATETIME NOT NULL, ModifiedUserID INT(11) NOT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', PRIMARY KEY (ServiceProviderCurrencyID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE part ADD COLUMN CreatedUserID INT(11) NULL DEFAULT NULL AFTER SpPartStockTemplateID, ADD COLUMN CreatedDate TIMESTAMP NULL DEFAULT NULL AFTER CreatedUserID;

CREATE TABLE sp_part_requisition ( SPPartRequisitionID INT(11) NOT NULL AUTO_INCREMENT, ServiceProviderSupplierID INT(11) NULL DEFAULT NULL, ModifiedDate DATETIME NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, CreatedDate TIMESTAMP NULL DEFAULT NULL, CreatedUserID INT(11) NULL DEFAULT NULL, ServiceProviderID INT(10) NULL DEFAULT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', PRIMARY KEY (SPPartRequisitionID)) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE sp_part_stock_item ADD COLUMN SPPartRequisitionID INT(11) NOT NULL AFTER PartID;

ALTER TABLE part ADD COLUMN SPPartRequisitionID INT(11) NULL DEFAULT NULL AFTER CreatedDate;


# ---------------------------------------------------------------------- #
# Modify non_skyline_job table                                           #
# ---------------------------------------------------------------------- # 
ALTER TABLE `non_skyline_job`
	ADD COLUMN `CustomerEmail` VARCHAR(255) NULL DEFAULT NULL AFTER `DateCreated`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.279');
