# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.275');

# ---------------------------------------------------------------------- #
# Andris Stock Changes                                                   #
# ---------------------------------------------------------------------- # 

ALTER TABLE service_provider_job_fault_code ADD COLUMN JobFaultCodeID INT(11) NOT NULL DEFAULT '0' AFTER ServiceProviderJobFaultCodeID;

ALTER TABLE service_provider_job_fault_code_lookup ADD COLUMN JobFaultCodeLookupID INT(10) NOT NULL DEFAULT '0' AFTER ServiceProviderJobFaultCodeLookupID;

ALTER TABLE service_provider_part_fault_code ADD COLUMN PartFaultCodeID INT(10) NOT NULL DEFAULT '0' AFTER ServiceProviderPartFaultCodeID;

ALTER TABLE service_provider_part_fault_code_lookup ADD COLUMN PartFaultCodeLookupID INT(10) NOT NULL DEFAULT '0' AFTER ServiceProviderPartFaultCodeLookupID;

CREATE TABLE service_provider_part_category ( ServiceProviderPartCategoryID INT(10) NOT NULL AUTO_INCREMENT, CategoryName VARCHAR(50) NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, CreatedDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', CreatedUserID INT(11) NULL DEFAULT NULL, PRIMARY KEY (ServiceProviderPartCategoryID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE service_provider_part_sub_category ( ServiceProviderPartSubCategoryID INT(10) NOT NULL AUTO_INCREMENT, SubCategoryName VARCHAR(50) NULL DEFAULT NULL, ServiceProviderCategoryID INT(11) NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, CreatedDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', CreatedUserID INT(11) NULL DEFAULT NULL, PRIMARY KEY (ServiceProviderPartSubCategoryID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.276');
