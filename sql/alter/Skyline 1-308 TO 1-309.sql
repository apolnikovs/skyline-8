# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.308');

# ---------------------------------------------------------------------- #
# Krishnam Raju Nalla Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE `remote_engineer_history`
	CHANGE COLUMN `AppointmentID` `AppointmentID` INT(10) NULL DEFAULT NULL AFTER `RemoteEngineerHistoryID`,
	CHANGE COLUMN `SBJobNo` `SBJobNo` INT(10) NULL DEFAULT NULL AFTER `AppointmentID`,
	CHANGE COLUMN `UserCode` `UserCode` VARCHAR(30) NULL DEFAULT NULL AFTER `Time`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.309');



#test