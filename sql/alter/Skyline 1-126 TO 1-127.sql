# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.3.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-26 10:33                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.126');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` DROP FOREIGN KEY `county_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `country_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `user_TO_service_provider`;

ALTER TABLE `role` DROP FOREIGN KEY `user_TO_role`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `job` DROP FOREIGN KEY `county_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `country_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `ra_status_TO_job`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `service_provider_TO_network_service_provider`;

ALTER TABLE `user` DROP FOREIGN KEY `service_provider_TO_user`;

ALTER TABLE `assigned_permission` DROP FOREIGN KEY `role_TO_assigned_permission`;

ALTER TABLE `user_role` DROP FOREIGN KEY `role_TO_user_role`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_provider_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_provider_TO_town_allocation`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_provider_TO_postcode_allocation`;

ALTER TABLE `claim_response` DROP FOREIGN KEY `job_TO_claim_response`;

ALTER TABLE `email_job` DROP FOREIGN KEY `job_TO_email_job`;

ALTER TABLE `ra_history` DROP FOREIGN KEY `job_TO_ra_history`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD COLUMN `CourierStatus` ENUM('Active', 'In-active') NOT NULL DEFAULT 'In-active';

# ---------------------------------------------------------------------- #
# Modify table "role"                                                    #
# ---------------------------------------------------------------------- #

ALTER TABLE `role` ADD COLUMN `InactivityTimeout` INTEGER NOT NULL DEFAULT 24 COMMENT 'Inactivity Timeout for logged in users in minutes';

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `CollectionDate` DATE;

ALTER TABLE `job` ADD COLUMN `CourierID` INTEGER;

# ---------------------------------------------------------------------- #
# Add table "courier"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `courier` (
    `CourierID` INTEGER NOT NULL AUTO_INCREMENT,
    `BrandID` INTEGER,
    `ManufacturerID` INTEGER,
    `ServiceProviderID` INTEGER,
    `Online` ENUM('Yes','No') NOT NULL DEFAULT 'Yes',
    `AccountNo` VARCHAR(40) NOT NULL,
    `IPAddress` VARCHAR(32),
    `UserName` VARCHAR(40),
    `Password` VARCHAR(40),
    `ReportFailureDays` TINYINT(2),
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `courierID` PRIMARY KEY (`CourierID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "ra_status_subset"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `ra_status_subset` (
    `ParentRAStatusID` INTEGER NOT NULL,
    `ChildRAStatusID` INTEGER NOT NULL
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE UNIQUE INDEX `IDX_ra_status_subset_1` ON `ra_status_subset` (`ParentRAStatusID`,`ChildRAStatusID`);

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer_skillset_day"                     #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_engineer_skillset_day` (
    `ServiceProviderEngineerSkillsetDayID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderEngineerID` INTEGER NOT NULL,
    `Day` ENUM('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
    `ServiceProviderSkillsetID` INTEGER NOT NULL,
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP,
    `CreatedDate` TIMESTAMP,
    CONSTRAINT `service_provider_engineer_skillset_dayID` PRIMARY KEY (`ServiceProviderEngineerSkillsetDayID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE UNIQUE INDEX `IDX_service_provider_engineer_skillset_day_1` ON `service_provider_engineer_skillset_day` (`ServiceProviderEngineerID`,`Day`,`ServiceProviderSkillsetID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD CONSTRAINT `county_TO_service_provider` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `country_TO_service_provider` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `user_TO_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `role` ADD CONSTRAINT `user_TO_role` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `county_TO_job` 
    FOREIGN KEY (`ColAddCountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `job` ADD CONSTRAINT `country_TO_job` 
    FOREIGN KEY (`ColAddCountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `job` ADD CONSTRAINT `ra_status_TO_job` 
    FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `ra_status_subset` ADD CONSTRAINT `ra_status_TO_ra_status_subset` 
    FOREIGN KEY (`ParentRAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `service_provider_TO_network_service_provider` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user` ADD CONSTRAINT `service_provider_TO_user` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `assigned_permission` ADD CONSTRAINT `role_TO_assigned_permission` 
    FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`);

ALTER TABLE `user_role` ADD CONSTRAINT `role_TO_user_role` 
    FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_provider_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_provider_TO_town_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_provider_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `claim_response` ADD CONSTRAINT `job_TO_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `email_job` ADD CONSTRAINT `job_TO_email_job` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `job_TO_ra_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.127');
