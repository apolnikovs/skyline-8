# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.287');

# ---------------------------------------------------------------------- #
# Modify sp_part_stock_item table       								 #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_part_stock_item ADD COLUMN SpStockReceivingHistoryID INT(11) NOT NULL AFTER SPPartRequisitionID;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.288');
