DROP FUNCTION IF EXISTS turnaround;

DELIMITER $$

CREATE FUNCTION turnaround(jobID INT)

RETURNS INT

DETERMINISTIC

BEGIN

        DECLARE turnaround INT;

        SET turnaround = (

            CASE

                        WHEN 	(@unit :=	(
                                                                        SELECT	    uct.UnitTurnaroundTime
                                                                        FROM	    job AS j
                                                                        LEFT JOIN   product AS p ON j.ProductID = p.ProductID
                                                                        LEFT JOIN   model AS m ON j.ModelID = m.ModelID
                                                                        LEFT JOIN   unit_type AS ut ON p.UnitTypeID = ut.UnitTypeID OR 
                                                                                        m.UnitTypeID = ut.UnitTypeID
                                                                        LEFT JOIN   unit_client_type AS uct 
                                                                                        ON ut.UnitTypeID = uct.UnitTypeID AND 
                                                                                        uct.ClientID = j.ClientID
                                                                        WHERE	    j.JobID = jobID
                                                                        LIMIT	    1
                                                        )
                                        ) IS NOT NULL

                        THEN    @unit

                        ELSE    CASE
                                        WHEN	(@clnt :=	(   
                                                                                            SELECT  DefaultTurnaroundTime AS dtt
                                                                                            FROM    client
                                                                                            WHERE   client.ClientID = (SELECT ClientID FROM job AS j WHERE j.JobID = jobID)
                                                                                        )
                                                        ) IS NOT NULL

                                        THEN    @clnt

                                        ELSE    (
                                                                        SELECT  gen.Default
                                                                        FROM    general_default AS gen
                                                                        WHERE   gen.GeneralDefaultID = 1
                                                            )
                                        END

        END

        );

        RETURN turnaround;

END$$
