DROP FUNCTION IF EXISTS statustat;

DELIMITER $$

CREATE FUNCTION statustat(jobID INT)

RETURNS INT

DETERMINISTIC

BEGIN

	DECLARE result INT;
	DECLARE serviceProviderID INT;
	DECLARE statusDate DATE;
	DECLARE statusTime TIME;
	DECLARE diff TIME;
	
	SET serviceProviderID = (SELECT job.ServiceProviderID FROM job WHERE job.JobID = jobID);
	SET statusDate = 	(
							SELECT 		CAST(DATE_FORMAT(status_history.Date, "%Y-%m-%d") AS DATE)
							FROM		job 
							LEFT JOIN	status_history ON job.JobID = status_history.JobID
							WHERE 		job.JobID = jobID AND status_history.StatusID = job.StatusID
							LIMIT		1
						);
	SET statusTime =	(
							SELECT 		CAST(DATE_FORMAT(status_history.Date, "%H:%i:%s") AS TIME)
							FROM		job 
							LEFT JOIN	status_history ON job.JobID = status_history.JobID
							WHERE 		job.JobID = jobID AND status_history.StatusID = job.StatusID
							LIMIT		1
						);

	SET @normalOpen := CAST((SELECT sp.WeekdaysOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	SET @normalClose := CAST((SELECT sp.WeekdaysCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);

	#normal working day in seconds
	SET @normalDay := TIME_TO_SEC(TIMEDIFF(@normalClose, @normalOpen));

	#SET @satOpen := CAST((SELECT sp.SaturdayOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	#SET @satClose := CAST((SELECT sp.SaturdayCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);

	#SET @sunOpen := CAST((SELECT sp.SundayOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	#SET @sunClose := CAST((SELECT sp.SundayCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	
	
	SET @tatType :=	(
						SELECT	status.StatusTATType 
						FROM 	status 
						WHERE 	status.StatusID = (SELECT job.StatusID FROM job WHERE job.JobID = jobID)
						LIMIT	1
					);
	
	SET @tatTemp :=	(
						SELECT	status.StatusTAT 
						FROM 	status 
						WHERE 	status.StatusID = (SELECT job.StatusID FROM job WHERE job.JobID = jobID)
						LIMIT	1
					);
	
	IF (@tatType = "Minutes") THEN SET @tatTime := @tatTemp * 60;
	ELSEIF (@tatType = "Hours") THEN SET @tatTime := @tatTemp * 3600;
	ELSEIF (@tatType = "Days") THEN SET @tatTime := @tatTemp * @normalDay;
	END IF;
	
	
	IF DATEDIFF(CURDATE(), statusDate) = 0
	
	THEN 
	
		SET result := 0;
		
	ELSE
	
		BEGIN

			IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = statusDate) > 0)
			THEN 
				#bank holiday
				SET @firstDay := 0;
			/*	
			ELSEIF (DATE_FORMAT(statusDate, "%w") = 0)
			THEN
				BEGIN
					#sunday
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@sunClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF (TIME_TO_SEC(TIMEDIFF(@sunOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@sunOpen, statusTime))); END IF;
					IF (TIME_TO_SEC(TIMEDIFF(statusTime, @sunClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @sunClose))); END IF;
				END;
			ELSEIF (DATE_FORMAT(statusDate, "%w") = 6)
			THEN
				BEGIN
					#saturday 
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@satClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@satOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@satOpen, statusTime))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(statusTime, @satClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @satClose))); END IF;
				END;
			*/
			ELSE 
				BEGIN
					#working day
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@normalClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@normalOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@normalOpen, statusTime))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(statusTime, @normalClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @normalClose))); END IF;
				END;
			END IF;			
			
			IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = CURDATE()) > 0)
			THEN 
				#bank holiday
				SET @lastDay := 0;
			/*	
			ELSEIF (DATE_FORMAT(CURDATE(), "%w") = 0)
			THEN
				BEGIN
					#sunday
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@sunOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@sunOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunClose))); END IF;
				END;
			ELSEIF (DATE_FORMAT(CURDATE(), "%w") = 6)
			THEN 
				BEGIN
					#saturday
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @satOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@satOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@satOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @satClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @satClose))); END IF;
				END;
			*/
			ELSE 
				BEGIN
					#working day
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@normalOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@normalOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalClose))); END IF;
				END;
			END IF;			
						
			
			SET @diffSeconds := @firstDay + @lastDay;

						
			SET @daysDiff := DATEDIFF(CURDATE(), statusDate);
			
			IF @daysDiff > 1
			THEN
				BEGIN
					
					SET @curDay := 1;
					
					SET @seconds := 0;
					
					WHILE @curDay < @daysDiff DO
						
						SET @curDayDate := INTERVAL @curDay DAY + statusDate;
						
						IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = @curDayDate) > 0)
						THEN 
							#bank holiday
							SET @diffSeconds := @diffSeconds;
						/*	
						ELSEIF (DATE_FORMAT(@curDayDate, "%w") = 0)
						THEN
							#sunday
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@sunClose, @sunOpen));
						ELSEIF (DATE_FORMAT(@curDayDate, "%w") = 6)
						THEN 
							#saturday
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@satClose, @satOpen));
						*/
						ELSE 
							#working day
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@normalClose, @normalOpen));
						END IF;			
						
						SET @curDay := @curDay + 1;
						
					END WHILE;
					
				END;
			END IF;
			
			
			IF (@diffSeconds > @tatTime) 
			THEN SET result = @daysDiff;
			ELSE SET result = -1;
			END IF;
			
		END;
		
	END IF;

	
	RETURN result;

END$$
