DROP TRIGGER IF EXISTS part_delete;

DELIMITER ;;

CREATE TRIGGER part_delete BEFORE DELETE ON part FOR EACH ROW 
BEGIN

	/*Parameter definitions*/
	/*CALL trigger_delete([table name], [primary ID], [user ID], [table row (JSON)]);*/

	DECLARE json TEXT;

	IF @userID IS NULL
	THEN SET @userID = 0;
	END IF;
	
	/*
	CASE
		WHEN (SELECT userID IS NULL) 
		THEN SET userID = 1
	END;
	*/

	/*This is where table row is concatenated into JSON format*/
	/*IMPORTANT: keep existing single-double quote format, this is compatible with official JSON guidlines*/
	SELECT CONCAT_WS('', '{', 
						'"PartID":"', OLD.PartID, '"', 
						', "JobID":"', OLD.JobID, '"', 
						', "PartNo":"', OLD.PartNo, '"', 
						', "PartDescription":"', OLD.PartDescription, '"', 
						', "Quantity":"', OLD.Quantity, '"', 
						', "Supplier":"', OLD.Supplier, '"', 
						', "SectionCode":"', OLD.SectionCode, '"', 
						', "DefectCode":"', OLD.DefectCode, '"', 
						', "RepairCode":"', OLD.RepairCode, '"', 
						', "IsMajorFault":"', OLD.IsMajorFault, '"', 
						', "CircuitReference":"', OLD.CircuitReference, '"', 
						', "PartSerialNo":"', OLD.PartSerialNo, '"', 
						', "OrderNo":"', OLD.OrderNo, '"', 
						', "OrderStatus":"', OLD.OrderStatus, '"', 
						', "OrderDate":"', OLD.OrderDate, '"', 
						', "ReceivedDate":"', OLD.ReceivedDate, '"', 
						', "InvoiceNo":"', OLD.InvoiceNo, '"', 
						', "UnitCost":"', OLD.UnitCost, '"', 
						', "SaleCost":"', OLD.SaleCost, '"', 
						', "VATRate":"', OLD.VATRate, '"', 
						', "SBPartID":"', OLD.SBPartID, '"', 
						', "PartStatus":"', OLD.PartStatus, '"', 
						', "OldPartSerialNo":"', OLD.OldPartSerialNo, '"', 
						', "IsOtherCost":"', OLD.IsOtherCost, '"', 
						', "IsAdjustment":"', OLD.IsAdjustment, '"', 
						', "DueDate":"', OLD.DueDate, '"', 
						', "SupplierOrderNo":"', OLD.SupplierOrderNo, '"', 
				  '}') INTO json;

	CALL trigger_delete('part', OLD.PartID, @userID, json);
	
END
;;

DELIMITER ;